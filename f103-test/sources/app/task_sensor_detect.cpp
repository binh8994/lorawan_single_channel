#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_dbg.h"
#include "../sys/sys_io.h"
#include "../common/utils.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_list_if.h"

#include "task_sensor_detect.h"
#include "task_if.h"
#include "task_life.h"
static warning_sensor_packet_t wr_sensor_packet;
static data_queue_t buffer_sensor;
static data_queue_t data_water_sensor;

void task_sensor_detect(ak_msg_t* msg) {
    switch (msg->sig) {
    case WR_SENSOR_DETECTOR_STATUS_REQ: {
        uint32_t data = 0;
        for(uint8_t i=0; i<DATA_LENGHT_MAX; i++) {
            data = data + data_water_sensor.data[i];
        }
        data = data / data_water_sensor.counter;
        APP_DBG("data_total= %d\n",data);
        if(data < MILESTONE_WARNING_WATER) {
            wr_sensor_packet.water_sensor = SENSOR_WARNING;
        }
        else {
            wr_sensor_packet.water_sensor = SENSOR_OK;
        }
        wr_sensor_packet.smoke = smoke_sensor_read();
        wr_sensor_packet.door  = door_sensor_read();
        wr_sensor_packet.fire  = fire_sensor_read();
        wr_sensor_packet.button_fire_alarm = button_fire_alarm_read();

        ak_msg_t* s_msg = get_common_msg();
        set_msg_sig(s_msg,WR_SENSOR_DETECTOR_STATUS_RES);
        set_data_common_msg(s_msg, (uint8_t*)&wr_sensor_packet, sizeof(warning_sensor_packet_t));
        task_post(WR_TASK_SHELL_ID, s_msg);

        //        ak_msg_t* s_msg = get_common_msg();
        //        set_msg_sig(s_msg, WR_IF_COMMON_MSG_OUT);

        //        set_if_type(s_msg, IF_TYPE_RF24);
        //        set_msg_sig(s_msg,AC_SENSOR_DETECTOR_STATUS_RES);
        //        set_data_common_msg(s_msg, (uint8_t*)&wr_sensor_packet, sizeof(warning_sensor_packet_t));
        //        task_post(WR_TASK_IF_ID, s_msg);

        /*write data to flash*/
        //        flash_erase_sector(APP_FLASH_IR_SECTOR_1);
        //        flash_write(APP_FLASH_IR_SECTOR_1,(uint8_t*)&wr_sensor_packet.door,sizeof(uint8_t));


    }
        break;
    case WR_SENSOR_READ_DATA_REQ: {

        data_queue_put(&data_water_sensor,&buffer_sensor,adc_water_read(0,100));

    }
        break;
        //    case WR_SENSOR_GET_DATA_REQ: {
        //        uint16_t data;
        //        for(uint8_t i=0; i<DATA_LENGHT_MAX; i++) {
        //            data += buffer_sensor->data[i];
        //        }
        //        data /= DATA_LENGHT_MAX;
        //        APP_DBG("data_total= %d\n",data);

        //        ak_msg_t* s_msg = get_common_msg();
        //        set_msg_sig(s_msg,WR_SENSOR_GET_DATA_RES);
        //        set_data_common_msg(s_msg, (uint8_t*)&data_water_sensor, sizeof(data_queue_t));
        //        task_post(WR_TASK_SHELL_ID, s_msg);
        //    }
        //        break;
    default:
        break;
    }
}
uint16_t data_queue_full(data_queue_t* q) {
    if((q->counter==DATA_LENGHT_MAX)){

        return DATA_QUEUE_RET_NG;
    }
    return DATA_QUEUE_RET_OK;
}

uint16_t data_queue_put(data_queue_t* data,data_queue_t* q, uint16_t d) {
    if(q->tail_index==DATA_LENGHT_MAX){
        q->tail_index=0;
    }
    q->data[q->tail_index]=d;

    //    APP_DBG("tail = %d\n",q->tail_index);
    //    APP_DBG("put = %d\n",q->data[q->tail_index]);

    q->tail_index++;
    if(q->counter<DATA_LENGHT_MAX) {
        q->counter++;
    }
    mem_cpy(data,q,sizeof(data_queue_t));
    return DATA_QUEUE_RET_OK;
}

uint16_t data_queue_get(data_queue_t* q) {
    if(!q->counter) {

        return DATA_QUEUE_RET_NG;
    }

    //    APP_DBG("head = %d\n",q->head_index);
    //    APP_DBG("get = %d\n",q->data[q->head_index]);

    if(q->head_index==DATA_LENGHT_MAX-1){
        q->counter--;
        q->head_index=0;
        return q->data[DATA_LENGHT_MAX-1];
    }
    else {
        q->head_index++;
    }

    q->counter--;

    return q->data[q->head_index-1];
}
