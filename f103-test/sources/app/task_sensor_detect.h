#ifndef TASK_SENSOR_DETECT_H
#define TASK_SENSOR_DETECT_H

#include <stdint.h>

#define DATA_LENGHT_MAX                 (3)
#define DATA_QUEUE_RET_OK               (0x00)
#define DATA_QUEUE_RET_NG               (0x01)
#define SENSOR_WARNING                  (0X00)
#define SENSOR_OK                       (0X01)
#define MILESTONE_WARNING_WATER         (3713)
typedef struct {
    uint8_t head_index;
    uint8_t tail_index;
    uint8_t counter;
    uint16_t data[DATA_LENGHT_MAX];
} data_queue_t;

extern uint16_t data_queue_full(data_queue_t* q);
extern uint16_t data_queue_put(data_queue_t* data,data_queue_t* q, uint16_t d);
extern uint16_t data_queue_get(data_queue_t* q);

#endif // TASK_SENSOR_DETECT_H
