/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef APP_H
#define APP_H

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************/
/*  life task define
 */
/*****************************************************************************/
/* define timer */
#define AK_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
#define AK_LIFE_SYSTEM_CHECK						(0)

/*****************************************************************************/
/*  shell task define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
#define AK_SHELL_LOGIN_CMD							(0)
#define AK_SHELL_REMOTE_CMD							(1)

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK                                      (0x00)
#define APP_NG                                      (0x01)

#define APP_FLAG_OFF                                (0x00)
#define APP_FLAG_ON                                 (0x01)

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
#define AC_NUMBER_SAMPLE_CT_SENSOR				3000

extern int  main_app();

#ifdef __cplusplus
}
#endif

#endif //APP_H
