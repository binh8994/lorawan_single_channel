#ifndef __DEVICE_H__
#define __DEVICE_H__

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "../mac/LoRaMac.h"

class device_mng
{
public:
	device_mng();
	void initializer(const char*);
	int set_file_path(const char*);
	void get_file_path(char*);
	int remove_device_by(uint32_t address);
	int remove_device_by(const uint8_t* device_Eui);
	int add_new_device(const lorawan_device_mng_t* device);
	int device_exist(uint32_t address);
	int update_device(const lorawan_device_mng_t* device);
	lorawan_device_mng_t* get_device_by(uint32_t address);
	lorawan_device_mng_t* get_device_by(const uint8_t* device_Eui);
	void update_file();

private:
	char m_file_path[256];
	vector<lorawan_device_mng_t> m_device_vector;
	void parser_file_to_vector();
	void write_vector_to_file();
};

#endif // __DEVICE_H__
