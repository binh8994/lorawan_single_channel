#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_lorawan.h"
#include "app_lorawan.h"

#include "../loramac/radio/sx1276/sx1276_cfg.h"
#include "../loramac/mac/LoRaMac.h"

#define LORAWAN_NETWORK_ID					(uint32_t)0
#define LORAWAN_APPKEY						{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, \
												0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }

static uint32_t NwkID		= LORAWAN_NETWORK_ID;
static uint8_t AppKey[]		= LORAWAN_APPKEY;
static bool enable_join;

q_msg_t mt_task_lorawan_mailbox;
void* mt_task_lorawan_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_lorawan_entry\n");

	SX1276IoInit();

	loramac_initial(NwkID, AppKey);

	enable_join = false;

	while (1) {
		while (msg_available(MT_TASK_LORAWAN_ID)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(MT_TASK_LORAWAN_ID);

			switch (msg->header->sig) {
			case LORAWAN_DEVICE_SEND: {
				APP_DBG("LORAWAN_DEVICE_SEND\n");
				lorawan_zcl_msg_t* zcl_msg = (lorawan_zcl_msg_t*)msg->header->payload;

				/*msg to send radio*/
				lorawan_msg_t lorawan_msg;
				lorawan_msg.clusterId = zcl_msg->clusterId;
				lorawan_msg.dataType = zcl_msg->dataType;
				lorawan_msg.dataLen = zcl_msg->dataLen;
				memcpy(&lorawan_msg.data[0], &zcl_msg->data[0], zcl_msg->dataLen);

				lorawan_data_t lorawan_data;
				lorawan_data.type_msg = UNCONFIRMED;
				lorawan_data.unconfirmed.port = APP_PORT;
				lorawan_data.unconfirmed.device_addr = zcl_msg->deviceAddr;
				lorawan_data.unconfirmed.data_len = sizeof(lorawan_msg);
				lorawan_data.unconfirmed.data = (uint8_t*)&lorawan_msg;
				send_lorawan_msg(&lorawan_data);

			}
				break;

			case LORAWAN_DEVICE_SEND_PROPRIETARY: {
				APP_DBG("LORAWAN_DEVICE_SEND_PROPRIETARY\n");
				lorawan_zcl_msg_t* zcl_msg = (lorawan_zcl_msg_t*)msg->header->payload;

				lorawan_data_t send_data;
				send_data.type_msg = PROPRIETARY;
				send_data.proprietary.data_len = sizeof(lorawan_msg_t);
				send_data.proprietary.data = (uint8_t*)zcl_msg;
				send_lorawan_msg(&send_data);

			}
				break;

			case LORAWAN_DEVICE_RECV: {
				APP_DBG("LORAWAN_DEVICE_RECV\n");
				lorawan_data_t* data_recv = (lorawan_data_t*)msg->header->payload;

				switch (data_recv->type_msg) {
				case JOIN: {

					if(data_recv->join.is_rejoin) {
						APP_DBG("Rejoin request\n");
						/*auto acept join*/
						lorawan_data_t* data_send = data_recv;
						send_lorawan_msg(data_send);
					}
					else {
						if( enable_join == true ) {
							/*acept join*/
							lorawan_data_t* data_send = data_recv;
							send_lorawan_msg(data_send);
						}
					}
				}
					break;

				case UNCONFIRMED: {
					lorawan_msg_t* lorawan_msg = (lorawan_msg_t*)data_recv->unconfirmed.data;

					/*convert to zcl format*/
					lorawan_zcl_msg_t zcl_msg;
					zcl_msg.deviceAddr = data_recv->unconfirmed.device_addr;
					zcl_msg.clusterId = lorawan_msg->clusterId;
					zcl_msg.sequence = 0;
					zcl_msg.dataType = lorawan_msg->dataType;
					zcl_msg.dataLen = lorawan_msg->dataLen;
					memcpy(&zcl_msg.data[0], &lorawan_msg->data[0], lorawan_msg->dataLen);

					APP_DBG("zcl_msg.clusterId:x%04X\n" , zcl_msg.clusterId);
					APP_DBG("zcl_msg.deviceAddr:x%08X\n", zcl_msg.deviceAddr);
					APP_DBG("zcl_msg.sequence:x%04X\n" , zcl_msg.sequence);
					APP_DBG("zcl_msg.dataType:x%02X\n" , zcl_msg.dataType);
					APP_DBG("zcl_msg.dataLen:x%02X\n" , zcl_msg.dataLen);
					APP_DBG("zcl_msg.data:x%02X\n" , zcl_msg.data[0]);

					free(data_recv->unconfirmed.data);
				}
					break;

				case CONFIRMED: {
					lorawan_msg_t* lorawan_msg = (lorawan_msg_t*)data_recv->confirmed.data;

					/*convert to zcl format*/
					lorawan_zcl_msg_t zcl_msg;
					zcl_msg.deviceAddr = data_recv->confirmed.device_addr;
					zcl_msg.clusterId = lorawan_msg->clusterId;
					zcl_msg.sequence = 0;
					zcl_msg.dataType = lorawan_msg->dataType;
					zcl_msg.dataLen = lorawan_msg->dataLen;
					memcpy(&zcl_msg.data[0], &lorawan_msg->data[0], lorawan_msg->dataLen);

					APP_DBG("zcl_msg.clusterId:x%04X\n" , zcl_msg.clusterId);
					APP_DBG("zcl_msg.deviceAddr:x%08X\n", zcl_msg.deviceAddr);
					APP_DBG("zcl_msg.sequence:x%04X\n" , zcl_msg.sequence);
					APP_DBG("zcl_msg.dataType:x%02X\n" , zcl_msg.dataType);
					APP_DBG("zcl_msg.dataLen:x%02X\n" , zcl_msg.dataLen);
					APP_DBG("zcl_msg.data:x%02X\n" , zcl_msg.data[0]);

					free(data_recv->confirmed.data);
				}
					break;

				case PROPRIETARY: {
					lorawan_zcl_msg_t* zcl_msg = (lorawan_zcl_msg_t*)data_recv->proprietary.data;

					APP_DBG("zcl_msg->clusterId:x%04X\n" , zcl_msg->clusterId);
					APP_DBG("zcl_msg->deviceAddr:x%08X\n", zcl_msg->deviceAddr);
					APP_DBG("zcl_msg->sequence:x%04X\n" , zcl_msg->sequence);
					APP_DBG("zcl_msg->dataType:x%02X\n" , zcl_msg->dataType);
					APP_DBG("zcl_msg->dataLen:x%02X\n" , zcl_msg->dataLen);
					APP_DBG("zcl_msg->data[0]:x%02X\n" , zcl_msg->data[0]);

					free(data_recv->proprietary.data);
				}
					break;

				default:
					break;
				}
			}
				break;

			case LORAWAN_DEVICE_JOIN_ENABLE : {
				APP_DBG("LORAWAN_DEVICE_JOIN_ENABLE\n");
				uint8_t timeout = *((uint8_t*)msg->header->payload);//get 1 byte

				enable_join = true;
				timer_set(MT_TASK_LORAWAN_ID, LORAWAN_DEVICE_JOIN_TIMER, timeout*1000, TIMER_ONE_SHOT );

			}
				break;

			case LORAWAN_DEVICE_JOIN_TIMER : {
				APP_DBG("LORAWAN_DEVICE_JOIN_TIMER\n");
				if(enable_join == true ) {
					enable_join = false;
				}
			}
				break;

			case LORAWAN_DEVICE_REMOVE : {
				APP_DBG("LORAWAN_DEVICE_REMOVE\n");
				uint8_t mac[8];
				memcpy(mac, msg->header->payload, 8);
				remove_device(mac);

			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(100);
	}

	return (void*)0;
}
