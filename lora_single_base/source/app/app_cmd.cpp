#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/stat.h>

#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "../common/cmd_line.h"

#include "app.h"
#include "app_data.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_config.h"
#include "app_cmd.h"

#include "task_list.h"
#include "task_list_if.h"

#include "app_lorawan.h"

static int32_t i_shell_ver(uint8_t* argv);
static int32_t i_shell_help(uint8_t* argv);
static int32_t i_shell_cfg(uint8_t* argv);
static int32_t i_shell_dbg(uint8_t* argv);

cmd_line_t lgn_cmd_table[] = {
	{(const int8_t*)"ver",		i_shell_ver,			(const int8_t*)"get kernel version"},
	{(const int8_t*)"help",		i_shell_help,			(const int8_t*)"help command info"},
	{(const int8_t*)"cfg",		i_shell_cfg,			(const int8_t*)"config"},
	{(const int8_t*)"dbg",		i_shell_dbg,			(const int8_t*)"debug"},

	/* End Of Table */
	{(const int8_t*)0,(pf_cmd_func)0,(const int8_t*)0}
};

int32_t i_shell_ver(uint8_t* argv) {
	(void)argv;
	APP_PRINT("version: %s\n", AK_VERSION);
	return 0;
}

int32_t i_shell_help(uint8_t* argv) {
	uint32_t idx = 0;
	switch (*(argv + 4)) {
	default:
		APP_PRINT("\nCOMMANDS INFORMATION:\n\n");
		while(lgn_cmd_table[idx].cmd != (const int8_t*)0) {
			APP_PRINT("%s\n\t%s\n\n", lgn_cmd_table[idx].cmd, lgn_cmd_table[idx].info);
			idx++;
		}
		break;
	}
	return 0;
}

int32_t i_shell_cfg(uint8_t* argv) {
	switch (*(argv + 4)) {
	case '0': {
		app_config_parameter_t config;

		/* lora gateway */
		strcpy(config.lora_gateway.lora_host,			"1.1.1.1");
		strcpy(config.lora_gateway.mqtt_host,			"118.69.135.199");
		config.lora_gateway.mqtt_port =					1883;
		strcpy(config.lora_gateway.mqtt_user_name,		"y55fYL");
		strcpy(config.lora_gateway.mqtt_psk,			"eJwKMNV2BQwC69PC");

		/* mqtt server */
		strcpy(config.pop_gateway.gateway_id_prefix,	"iot-");
		strcpy(config.pop_gateway.gateway_id,			"pop-dev");
		strcpy(config.pop_gateway.host,					"118.69.135.199");
		config.pop_gateway.port =						1883;
		strcpy(config.pop_gateway.user_name_view,		"fiot");
		strcpy(config.pop_gateway.user_psk_view	,		"ZmlvdEA5MTFmaW90");
		strcpy(config.pop_gateway.user_name_control,	"fciot");
		strcpy(config.pop_gateway.user_psk_control,		"ZmNpb3RAOTExOTExZmNpb3Q=");

		gateway_configure.write_config_data(&config);
		gateway_configure.parser_config_file(&config);

		APP_DBG("lora_gateway.lora_host:%s\n"			, config.lora_gateway.lora_host);
		APP_DBG("lora_gateway.mqtt_host:%s\n"			, config.lora_gateway.mqtt_host);
		APP_DBG("lora_gateway.mqtt_port:%d\n"			, config.lora_gateway.mqtt_port);
		APP_DBG("lora_gateway.mqtt_user_name:%s\n"		, config.lora_gateway.mqtt_user_name);
		APP_DBG("lora_gateway.mqtt_psk:%s\n"			, config.lora_gateway.mqtt_psk);

		APP_DBG("mqtt_server.gateway_id_prefix:%s\n"	, config.pop_gateway.gateway_id_prefix);
		APP_DBG("mqtt_server.gateway_id:%s\n"			, config.pop_gateway.gateway_id);
		APP_DBG("mqtt_server.host:%s\n"					, config.pop_gateway.host);
		APP_DBG("mqtt_server.port:%d\n"					, config.pop_gateway.port);
		APP_DBG("mqtt_server.user_name_view:%s\n"		, config.pop_gateway.user_name_view);
		APP_DBG("mqtt_server.user_psk_view:%s\n"		, config.pop_gateway.user_psk_view);
		APP_DBG("mqtt_server.user_name_control:%s\n"	, config.pop_gateway.user_name_control);
		APP_DBG("mqtt_server.user_psk_control:%s\n"		, config.pop_gateway.user_psk_control);
	}
		break;

	default:
		break;
	}

	return 0;
}

int32_t i_shell_dbg(uint8_t* argv) {
	switch (*(argv + 4)) {

	case 'o': {
		lorawan_zcl_msg_t zcl_msg;
		zcl_msg.deviceAddr = STM32F_ADDR;
		zcl_msg.clusterId = ZCL_CLUSTER_ID_GEN_ON_OFF;
		zcl_msg.sequence = 0;//not use
		zcl_msg.dataType = ZCL_DATATYPE_UINT8;
		zcl_msg.dataLen = 1;
		zcl_msg.data[0] = 1;

		ak_msg_t* smsg = get_dymanic_msg();
		set_data_dynamic_msg(smsg, (uint8_t*)&zcl_msg, sizeof(zcl_msg));
		set_msg_sig(smsg, LORAWAN_DEVICE_SEND);
		task_post(MT_TASK_LORAWAN_ID, smsg);

		APP_DBG("Send o\n");
	}
		break;

	case 'f': {

		lorawan_zcl_msg_t zcl_msg;
		zcl_msg.deviceAddr = STM32F_ADDR;
		zcl_msg.clusterId = ZCL_CLUSTER_ID_GEN_ON_OFF;
		zcl_msg.sequence = 0;//not use
		zcl_msg.dataType = ZCL_DATATYPE_UINT8;
		zcl_msg.dataLen = 1;
		zcl_msg.data[0] = 0;

		ak_msg_t* smsg = get_dymanic_msg();
		set_data_dynamic_msg(smsg, (uint8_t*)&zcl_msg, sizeof(zcl_msg));
		set_msg_sig(smsg, LORAWAN_DEVICE_SEND);
		task_post(MT_TASK_LORAWAN_ID, smsg);

		APP_DBG("Send 1\n");
	}
		break;

	case 'p': {

		lorawan_zcl_msg_t zcl_msg;
		zcl_msg.deviceAddr = ESP_DEVICE_ADDR;
		zcl_msg.clusterId = ZCL_CLUSTER_ID_GEN_ON_OFF;
		zcl_msg.sequence = 0;
		zcl_msg.dataType = ZCL_DATATYPE_UINT8;
		zcl_msg.dataLen = 1;
		zcl_msg.data[0] = 1;

		ak_msg_t* smsg = get_dymanic_msg();
		set_data_dynamic_msg(smsg, (uint8_t*)&zcl_msg, sizeof(zcl_msg));
		set_msg_sig(smsg, LORAWAN_DEVICE_SEND_PROPRIETARY);
		task_post(MT_TASK_LORAWAN_ID, smsg);

		APP_DBG("Send proprietary 1\n");
	}
		break;

	case 'e': {
		uint8_t time = 20;//30s
		ak_msg_t* smsg = get_dymanic_msg();
		set_data_dynamic_msg(smsg, (uint8_t*)&time, 1);
		set_msg_sig(smsg, LORAWAN_DEVICE_JOIN_ENABLE);
		task_post(MT_TASK_LORAWAN_ID, smsg);

		APP_DBG("Enable join\n");
	}
		break;

	case 'r': {
		uint8_t mac[8] = {0x05, 0x04, 0x03, 0x02, 0x01, 0xFF, 0xFF, 0xFF};
		ak_msg_t* smsg = get_dymanic_msg();
		set_data_dynamic_msg(smsg, (uint8_t*)mac, 8);
		set_msg_sig(smsg, LORAWAN_DEVICE_REMOVE);
		task_post(MT_TASK_LORAWAN_ID, smsg);

		APP_DBG("Remove device by device Eui\n");
	}
		break;

	default:
		break;
	}
	return 0;
}
