#ifndef __APP_LORAWAN_H__
#define __APP_LORAWAN_H__

#define ESP_DEVICE_ADDR					0x01020304
#define NOTIFY_BUTTON_ADDR				0x00001122
#define STM32F_ADDR						0x00005555

#define APP_PORT						80
#define ZCL_CLUSTER_ID_GEN_ON_OFF		0x0006
#define ZCL_DATATYPE_UINT8				0x20

/* message communicate endevice and gw */
typedef struct {
	uint16_t clusterId;
	uint8_t dataType;
	uint8_t dataLen;
	uint8_t data[1];
} __attribute__((__packed__)) lorawan_msg_t;

typedef struct {
	uint16_t clusterId;
	uint32_t deviceAddr;
	uint16_t sequence;
	uint8_t dataType;
	uint8_t dataLen;
	uint8_t data[1];
} __attribute__((__packed__)) lorawan_zcl_msg_t;

#endif // __APP_LORAWAN_H__
