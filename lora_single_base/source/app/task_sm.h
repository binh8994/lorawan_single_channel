#ifndef __TASK_SM_H__
#define __TASK_SM_H__

#include "../ak/ak.h"
#include "../ak/message.h"

extern q_msg_t mt_task_sm_mailbox;
extern void* mt_task_sm_entry(void*);

#endif //__TASK_SM_H__
