#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../common/utils.h"

#include "../app/app.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../sys/sys_ctrl.h"
#include "../platform/stm32f10x/io_cfg.h"

#include "../driver/ds1302/DS1302.h"

#include "../platform/stm32f10x/sx1276_cfg.h"
#include "../lora/radio.h"
#include "../lora/task_sx1276.h"

#include "task_switch.h"
#include "xprintf.h"

#define MY_RX_TIMEOUT_VALUE                            3000

/*!
 * Radio events function pointer
 */

static void on_tx_done( void );
static void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_tx_timeout( void );
static void on_rx_timeout( void );
static void on_rx_error( void );

static RadioEvents_t radioevents;

void task_switch(ak_msg_t* msg){

	switch (msg->sig) {

	case LORA_SWITCH_INIT:{
		APP_PRINT("LORA_SWITCH_INIT\n\n");

		radioevents.TxDone = on_tx_done;
		radioevents.RxDone = on_rx_done;
		radioevents.TxTimeout = on_tx_timeout;
		radioevents.RxTimeout = on_rx_timeout;
		radioevents.RxError = on_rx_error;

		Radio.Init( &radioevents );

		Radio.SetChannel( RF_FREQUENCY );

		//Radio.SetPublicNetwork(true);

		Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
						   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
						   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
						   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

		Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
						   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
						   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
						   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

		Radio.Rx( MY_RX_TIMEOUT_VALUE );


		/* gateway sync timer */
		//timer_set(LORA_TASK_SWITCH_ID, LORA_SWITCH_SYNC_TIME_REQ, 1000, TIMER_ONE_SHOT);

	}
		break;

	default:
		break;
	}

}


void on_tx_done( void )
{
	Radio.Sleep( );
	APP_DBG("on_tx_done\n\n");
	Radio.Rx( MY_RX_TIMEOUT_VALUE );
}

void on_tx_timeout( void )
{
	Radio.Sleep( );
	APP_DBG("on_tx_timeout\n\n");
	Radio.Rx( MY_RX_TIMEOUT_VALUE );
}

void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	Radio.Sleep( );

	APP_DBG("on_rx_done\n\n");
	APP_DBG("size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);

	for(int i = 0; i< size; i++)
		xprintf("x%02X ", *(payload+i));
	xprintf("\n");

	Radio.Rx( MY_RX_TIMEOUT_VALUE );


}

void on_rx_timeout( void )
{
	APP_DBG("on_rx_timeout\n\n");
	Radio.Rx( MY_RX_TIMEOUT_VALUE );
}

void on_rx_error( void )
{
	Radio.Sleep( );
	APP_DBG("on_rx_error\n\n");
	Radio.Rx( MY_RX_TIMEOUT_VALUE );
}
