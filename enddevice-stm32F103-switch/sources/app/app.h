/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef APP_H
#define APP_H

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************/
/*  life task define
 */
/*****************************************************************************/
/* define timer */
#define AK_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
#define AK_LIFE_SYSTEM_CHECK						(1)

/*****************************************************************************/
/*  shell task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define AK_SHELL_LOGIN_CMD							(1)
#define AK_SHELL_REMOTE_CMD							(2)

/*****************************************************************************/
/*  loramac task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */

#define LORAMAC_RADIO_TXTIMEOUT						(1)
#define LORAMAC_RADIO_RXTIMEOUT						(2)
#define LORAMAC_RADIO_RXTIMEOUT_SYNCWORD			(3)
#define LORAMAC_MAC_STATE_CHECK_TIMER				(4)
#define LORAMAC_MAC_TXDELAYED_TIMER					(5)
#define LORAMAC_MAC_RXWINDOW_1_TIMER				(6)
#define LORAMAC_MAC_RXWINDOW_2_TIMER				(7)
#define LORAMAC_MAC_ACK_TIMEOUT						(8)

#define LORAMAC_DEVICE_INIT							(9)
#define LORAMAC_DEVICE_JOIN							(10)
#define LORAMAC_DEVICE_SEND							(11)
#define LORAMAC_DEVICE_SLEEP						(12)
#define PINGPONG_DEVICE_INIT						(13)


/* sx1276 task define*/
/*signal*/
#define LORA_SX1276_TXTIMEROUT						(0)
#define LORA_SX1276_RXTIMEROUT						(1)
#define LORA_SX1276_RXTIMEROUT_SYNCWORD				(2)

/* task sensor define*/
/*timer*/
#define LORA_SWITCH_SYNC_TIME_REQ_INTERVAL			(30*60*1000)
#define LORA_SWITCH_CHECK_SETTING_TIME_INTERVAL		(10000)
/*signal*/
#define LORA_SWITCH_INIT							(0)
#define LORA_SWITCH_SYNC_TIME_REQ					(1)
#define LORA_SWITCH_TXDONE							(2)
#define LORA_SWITCH_TXTIMEOUT						(3)
#define LORA_SWITCH_RXDONE							(4)
#define LORA_SWITCH_RXTIMEOUT						(5)
#define LORA_SWITCH_RXERR							(6)
#define LORA_SWITCH_FHSS_CHANGE_CHAN				(7)
#define LORA_SWITCH_CADDONE							(8)
#define LORA_SWITCH_CONTROL							(9)
#define LORA_SWITCH_STATUS							(10)
#define LORA_SWITCH_SET_TIME						(11)
#define LORA_SWITCH_CHECK_SETTING_TIMEOUT			(12)

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK                                      (0x00)
#define APP_NG                                      (0x01)

#define APP_FLAG_OFF                                (0x00)
#define APP_FLAG_ON                                 (0x01)

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
#define AC_NUMBER_SAMPLE_CT_SENSOR				3000

extern int  main_app();

#ifdef __cplusplus
}
#endif

#endif //APP_H
