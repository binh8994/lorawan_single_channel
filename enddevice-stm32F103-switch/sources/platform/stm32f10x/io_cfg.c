/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#include <stdint.h>
#include <stdbool.h>

#include "io_cfg.h"
#include "stm32.h"
#include "arduino/Arduino.h"

#include "../sys/sys_dbg.h"

#include "../common/utils.h"
#include "../app/app_dbg.h"

/******************************************************************************
* led status function
*******************************************************************************/
void led_life_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_LIFE_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_LIFE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LED_LIFE_IO_PORT, &GPIO_InitStructure);
}

void led_life_on() {
	GPIO_SetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}

void led_life_off() {
	GPIO_ResetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}


/******************************************************************************
* SPI1 cfg function
*******************************************************************************/
void spi1_cfg() {
	GPIO_InitTypeDef  GPIO_InitStructure;
	SPI_InitTypeDef   SPI_InitStructure;

	/*!< SPI GPIO Periph clock enable */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);

	/*!< SPI Periph clock enable */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

	/*!< Configure SPI pins: SCK */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*!< Configure SPI pins: MISO */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*!< Configure SPI pins: MOSI */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*!< SPI Config */
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;

	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPI1, &SPI_InitStructure);

	SPI_Cmd(SPI1, ENABLE); /*!< SPI enable */

}

/******************************************************************************
* Lora sx1276 IO function
*******************************************************************************/
void sx1276_io_ctrl_init(void) {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(SX1276_CS_IO_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(SX1276_RST_IO_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(SX1276_DIO0_IO_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(SX1276_DIO1_IO_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(SX1276_DIO2_IO_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(SX1276_DIO3_IO_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(SX1276_DIO4_IO_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(SX1276_DIO5_IO_CLOCK, ENABLE);

	/*CS -> PB0*/
	GPIO_InitStructure.GPIO_Pin = SX1276_CS_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SX1276_CS_IO_PORT, &GPIO_InitStructure);
	GPIO_SetBits(SX1276_CS_IO_PORT, SX1276_CS_IO_PIN);
	/*RST -> PB3*/
	GPIO_InitStructure.GPIO_Pin = SX1276_RST_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(SX1276_RST_IO_PORT, &GPIO_InitStructure);
	GPIO_SetBits(SX1276_RST_IO_PORT, SX1276_RST_IO_PIN);
	/*DIO0 -> PB4*/
	GPIO_InitStructure.GPIO_Pin = SX1276_DIO0_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(SX1276_DIO0_IO_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(SX1276_DIO0_IO_PORT, SX1276_DIO0_IO_PIN);
	/*DIO1 -> PB5*/
	GPIO_InitStructure.GPIO_Pin = SX1276_DIO1_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(SX1276_DIO1_IO_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(SX1276_DIO1_IO_PORT, SX1276_DIO1_IO_PIN);
	/*DIO2 -> PB6*/
	GPIO_InitStructure.GPIO_Pin = SX1276_DIO2_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(SX1276_DIO2_IO_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(SX1276_DIO2_IO_PORT, SX1276_DIO2_IO_PIN);
	/*DIO3 -> PB7*/
	GPIO_InitStructure.GPIO_Pin = SX1276_DIO3_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(SX1276_DIO3_IO_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(SX1276_DIO3_IO_PORT, SX1276_DIO3_IO_PIN);
	/*DIO4 -> PB8*/
	GPIO_InitStructure.GPIO_Pin = SX1276_DIO4_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(SX1276_DIO4_IO_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(SX1276_DIO4_IO_PORT, SX1276_DIO4_IO_PIN);
	/*DIO5 -> PB9*/
	GPIO_InitStructure.GPIO_Pin = SX1276_DIO5_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(SX1276_DIO5_IO_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(SX1276_DIO5_IO_PORT, SX1276_DIO5_IO_PIN);

}

void sx1276_io_irq_ctrl_init(void){
	EXTI_InitTypeDef        EXTI_InitStruct;
	NVIC_InitTypeDef        NVIC_InitStruct;

	/* Enable AFIO clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	/* DIO0 - B4*/
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource4);

	EXTI_InitStruct.EXTI_Line = EXTI_Line4;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI4_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	/* DIO1 - B5*/
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource5);

	EXTI_InitStruct.EXTI_Line = EXTI_Line5;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	/* DIO2 - B6*/
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource6);

	EXTI_InitStruct.EXTI_Line = EXTI_Line6;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	//	/* DIO3 - B7*/
	//	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource7);

	//	EXTI_InitStruct.EXTI_Line = EXTI_Line7;
	//	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	//	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	//	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	//	EXTI_Init(&EXTI_InitStruct);

	//	NVIC_InitStruct.NVIC_IRQChannel = EXTI9_5_IRQn;
	//	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	//	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	//	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	//	NVIC_Init(&NVIC_InitStruct);

	//	/* DIO4 - B8*/
	//	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource8);

	//	EXTI_InitStruct.EXTI_Line = EXTI_Line8;
	//	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	//	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	//	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	//	EXTI_Init(&EXTI_InitStruct);

	//	NVIC_InitStruct.NVIC_IRQChannel = EXTI9_5_IRQn;
	//	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	//	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	//	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	//	NVIC_Init(&NVIC_InitStruct);

	/* DIO5 - A9*/
	//	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource9);

	//	EXTI_InitStruct.EXTI_Line = EXTI_Line9;
	//	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	//	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	//	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	//	EXTI_Init(&EXTI_InitStruct);

	//	NVIC_InitStruct.NVIC_IRQChannel = EXTI9_5_IRQn;
	//	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	//	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	//	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	//	NVIC_Init(&NVIC_InitStruct);

}

void sx1276_cs_low(void) {
	GPIO_ResetBits(SX1276_CS_IO_PORT, SX1276_CS_IO_PIN);
}

void sx1276_cs_high(void) {
	GPIO_SetBits(SX1276_CS_IO_PORT, SX1276_CS_IO_PIN);
}

void sx1276_rst_low(void) {
	GPIO_ResetBits(SX1276_RST_IO_PORT, SX1276_RST_IO_PIN);
}

void sx1276_rst_high(void) {
	GPIO_SetBits(SX1276_RST_IO_PORT, SX1276_RST_IO_PIN);
}

void sx1276_rst_to_input(void) {
	GPIO_InitTypeDef        GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = SX1276_RST_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SX1276_RST_IO_PORT, &GPIO_InitStructure);
}

uint8_t sx1276_transfer(const uint8_t data) {
	uint32_t rxtxData = data;

	/* waiting send idle then send data */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(SPI1, (uint8_t)rxtxData);

	/* waiting conplete rev data */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
	rxtxData = (uint8_t)SPI_I2S_ReceiveData(SPI1);

	return (uint8_t)rxtxData;
}
