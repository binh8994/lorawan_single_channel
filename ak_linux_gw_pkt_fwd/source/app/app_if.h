/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   13/08/2016
 ******************************************************************************
**/

#ifndef __APP_IF_H__
#define __APP_IF_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************/
/* task SL_LIFE define
 */
/*****************************************************************************/
/* define timer */
#define SL_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
#define SL_LIFE_SYSTEM_CHECK						(0)

/*****************************************************************************/
/* task SL_SHELL define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
#define SL_SHELL_LOGIN_CMD							(0)

/*****************************************************************************/
/* task SL_CPU_SERIAL define
 */
/*****************************************************************************/
/* timer signal */
/* define signal */
#define SL_CPU_SERIAL_IF_PURE_MSG_OUT				(1)
#define SL_CPU_SERIAL_IF_COMMON_MSG_OUT				(2)

/*****************************************************************************/
/* task SL_IF define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define SL_IF_PURE_MSG_IN							(1)
#define SL_IF_PURE_MSG_OUT							(2)
#define SL_IF_COMMON_MSG_IN							(3)
#define SL_IF_COMMON_MSG_OUT						(4)

/*****************************************************************************/
/* task SL_SM define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define SL_SM_MT_SYNC_REQ							(1)

/*****************************************************************************/
/* task SL_SENSOR define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define SL_SENSOR_REPORT_REQ						(1)

/*****************************************************************************/
/* task SL_POP_CTRL define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define SL_POP_CTRL_MODE_SWITCH_REQ					(1)
#define SL_POP_CTRL_CONTROL_REQ						(2)
#define SL_POP_CTRL_CONTROL_REQ						(2)

/*****************************************************************************/
/* task SL_IO_CTRL define
 */
/*****************************************************************************/
/* define timer */
#define SL_POP_CTRL_AUTO_CONTROL_REQ_INTERVAL		(10000)

/* define signal */
#define SL_POP_CTRL_MODE_SWITCH_REQ					(1)
#define SL_POP_CTRL_CONTROL_REQ						(2)
#define SL_POP_CTRL_CONTROL_INFO_REQ				(3)
#define SL_POP_CTRL_AUTO_CONTROL_REQ				(4)

/*****************************************************************************/
/* task SL_POP_CTRL define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define SL_IO_CTRL_CONTROL_REQ						(1)
#define SL_IO_CTRL_CONTROL_INFO_REQ					(2)

/*****************************************************************************/
/* task SL_DEV_CTRL define
 */
/*****************************************************************************/
/* define timer */
#define SL_DEV_CTRL_FAN_AUTO_CONTROL_REQ_INTERVAL	(3000)

/* define signal */
#define SL_DEV_CTRL_FAN_SPEED_CONTROL_REQ			(1)
#define SL_DEV_CTRL_FAN_AUTO_CONTROL_REQ			(2)

#ifdef __cplusplus
}
#endif

#endif //__APP_IF_H__
