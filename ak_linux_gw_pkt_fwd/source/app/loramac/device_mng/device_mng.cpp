#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "../../../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"
#include "app_config.h"
#include "device_mng.h"

#include "../mac/LoRaMac.h"

//#define DEVICE_MNG_DBG	APP_DBG
#define DEVICE_MNG_DBG(fmt, ...)

device_mng::device_mng() {

}

void device_mng::initializer(const char* file_name) {
	struct stat st_folder = {0};
	struct stat st_file = {0};
	string folder_path = \
			static_cast<string>(APP_ROOT_PATH_DISK) + \
			static_cast<string>("/lorawan_device");

	/* create app root path */
	if (stat(folder_path.data(), &st_folder) == -1) {
		mkdir(folder_path.data(), ALLPERMS);
		DEVICE_MNG_DBG("device_mng create folder %s\n", folder_path.data());
	}

	/*file path*/
	string file_path = folder_path + static_cast<string>((const char*)file_name);
	memset(m_file_path, 0, sizeof(m_file_path));
	strcpy(m_file_path, file_path.data());
	DEVICE_MNG_DBG("device_mng file_path %s\n", m_file_path);

	/*check file exist*/
	if (stat(m_file_path, &st_file) == -1) {
		m_device_vector.clear();
	}
	else {
		/*parser file to vector*/
		parser_file_to_vector();
	}
}

int device_mng::set_file_path(const char* path) {
	if(path != NULL) {
		memset(m_file_path, 0, sizeof(m_file_path));
		strcpy(m_file_path, path);
		DEVICE_MNG_DBG("device_mng set_file_path %s\n", m_file_path);
		return 0;
	}
	return -1;
}

void device_mng::get_file_path(char* path) {
	strcpy(path, m_file_path);
}


int device_mng::add_new_device(const lorawan_device_mng_t * device) {
	lorawan_device_mng_t* dv;
	/*check exist*/
	for (vector<lorawan_device_mng_t>::iterator it = m_device_vector.begin(); it != m_device_vector.end(); it++) {
		dv = &(*it);
		if(dv->device_addr == device->device_addr ) {
			DEVICE_MNG_DBG("device_mng device_addr exist\n");
			return -1;
		}
	}

	/*add device*/
	m_device_vector.push_back(*device);
	write_vector_to_file();
	return 0;
}

int device_mng::remove_device_by(uint32_t address) {
	lorawan_device_mng_t* dv;
	/*check exist*/
	for (vector<lorawan_device_mng_t>::iterator it = m_device_vector.begin(); it != m_device_vector.end(); it++) {
		dv = &(*it);
		if(dv->device_addr == address ) {
			m_device_vector.erase(it);
			write_vector_to_file();
			DEVICE_MNG_DBG("device_mng device_addr exist\n");
			return 0;
		}
	}
	/*device not found*/
	return -1;
}

int device_mng::remove_device_by(const uint8_t* device_Eui) {
	lorawan_device_mng_t* dv;
	/*check exist*/
	for (vector<lorawan_device_mng_t>::iterator it = m_device_vector.begin(); it != m_device_vector.end(); it++) {
		dv = &(*it);
		if( memcmp(dv->device_Eui, device_Eui, 8) == 0 ) {
			m_device_vector.erase(it);
			write_vector_to_file();
			DEVICE_MNG_DBG("device_mng device_Eui exist\n");
			return 0;
		}
	}
	/*device not found*/
	return -1;
}

int device_mng::device_exist(uint32_t address) {
	lorawan_device_mng_t* dv;
	for (vector<lorawan_device_mng_t>::iterator it = m_device_vector.begin(); it != m_device_vector.end(); it++) {
		dv = &(*it);
		if(dv->device_addr == address ) {
			DEVICE_MNG_DBG("device_mng device_addr exist\n");
			return 0;
		}
	}
	return -1;
}

int device_mng::update_device(const lorawan_device_mng_t* device) {
	lorawan_device_mng_t* dv;
	for (vector<lorawan_device_mng_t>::iterator it = m_device_vector.begin(); it != m_device_vector.end(); it++) {
		dv = &(*it);
		if(dv->device_addr == device->device_addr ) {
			m_device_vector.erase(it);
			m_device_vector.push_back(*device);
			DEVICE_MNG_DBG("device_mng update device\n");
			write_vector_to_file();
			return 0;
		}
	}
	return -1;
}

lorawan_device_mng_t* device_mng::get_device_by(uint32_t address) {
	lorawan_device_mng_t* dv;
	for (vector<lorawan_device_mng_t>::iterator it = m_device_vector.begin(); it != m_device_vector.end(); it++) {
		dv = &(*it);
		if(dv->device_addr == address ) {
			DEVICE_MNG_DBG("device_mng device_addr exist\n");
			return dv;
		}
	}
	return NULL;
}

lorawan_device_mng_t* device_mng::get_device_by(const uint8_t* device_Eui) {
	if(device_Eui == NULL)
		return NULL;

	lorawan_device_mng_t* dv;
	for (vector<lorawan_device_mng_t>::iterator it = m_device_vector.begin(); it != m_device_vector.end(); it++) {
		dv = &(*it);
		if( memcmp(dv->device_Eui, device_Eui, 8) == 0 ) {
			DEVICE_MNG_DBG("device_mng device_Eui exist\n");
			return dv;
		}
	}
	return NULL;
}

void device_mng::update_file() {
	write_vector_to_file();
}

void device_mng::parser_file_to_vector() {
	ifstream i_file(m_file_path, ios::in | ifstream::binary);
	int size_of_vector = m_device_vector.size();
	i_file.read((char*)&size_of_vector, 4);
	m_device_vector.resize(size_of_vector);
	i_file.read((char*)&m_device_vector[0], size_of_vector * sizeof(lorawan_device_mng_t));
	i_file.close();
}

void device_mng::write_vector_to_file() {
	ofstream o_file(m_file_path, ios::out | ofstream::binary);
	int size_of_arr = m_device_vector.size();
	o_file.write((const char*)&size_of_arr, 4);
	o_file.write((const char*)&m_device_vector[0], size_of_arr * sizeof(lorawan_device_mng_t));
	o_file.close();
}

