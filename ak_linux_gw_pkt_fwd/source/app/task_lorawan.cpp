#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/time.h>
#include <timer.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_lorawan.h"
#include "lorawan.h"

#include "../../common/mbase64.h"

#include "../loramac/radio/sx1276/sx1276_cfg.h"
#include "../loramac/radio/sx1276/sx1276.h"
#include "../loramac/mac/LoRaMac-definitions.h"
#include "../loramac/mac/LoRaMac.h"
#include "../loramac/mac/LoRaMacCrypto.h"

#define LORAWAN_NETWORK_ID					(uint32_t)0
#define LORAWAN_APPKEY						{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }

static uint32_t NwkID		= LORAWAN_NETWORK_ID;
static uint8_t AppKey[]		= LORAWAN_APPKEY;
static bool enable_join = false;


#define TX_BUFF_SIZE		2048
#define STATUS_SIZE			1024
#define PROTOCOL_VERSION	1
#define PKT_PUSH_DATA		0
#define SERVER1 "118.69.167.36"
#define PORT 1680
struct sockaddr_in si_other;
int s, slen=sizeof(si_other);
struct ifreq ifr;

// Set location
float lat=0.0;
float lon=0.0;
int   alt=0;
uint32_t cp_nb_rx_rcv=0;
uint32_t cp_nb_rx_ok=0;
uint32_t cp_nb_rx_bad=0;
uint32_t cp_nb_rx_nocrc=0;
uint32_t cp_up_pkt_fwd=0;

static char buff_up[TX_BUFF_SIZE]; /* buffer to compose the upstream packet */
static char status_report[STATUS_SIZE];

q_msg_t mt_task_lorawan_mailbox;
void* mt_task_lorawan_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_lorawan_entry\n");

	SX1276IoInit();

	loramac_initial(NwkID, AppKey);
	enable_join = false;

	//PKT_FWD
	if ( (s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	{
		APP_DBG("socket() error");
		while(1);
	}

	memset((char *) &si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(PORT);

	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);  // can we rely on eth0?
	ioctl(s, SIOCGIFHWADDR, &ifr);

	/* display result */
	APP_DBG("Gateway ID: %.2x:%.2x:%.2x:ff:ff:%.2x:%.2x:%.2x\n",
			(unsigned char)ifr.ifr_hwaddr.sa_data[0],
			(unsigned char)ifr.ifr_hwaddr.sa_data[1],
			(unsigned char)ifr.ifr_hwaddr.sa_data[2],
			(unsigned char)ifr.ifr_hwaddr.sa_data[3],
			(unsigned char)ifr.ifr_hwaddr.sa_data[4],
			(unsigned char)ifr.ifr_hwaddr.sa_data[5]);

	timer_set(MT_TASK_LORAWAN_ID, LORAWAN_PKT_FWD_STAT, 20000, TIMER_PERIODIC);

	while (1) {
		while (msg_available(MT_TASK_LORAWAN_ID)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(MT_TASK_LORAWAN_ID);

			switch (msg->header->sig) {
			case LORAWAN_DEVICE_SEND: {
				APP_DBG("LORAWAN_DEVICE_SEND\n");
				lorawan_msg_t* lora_msg = (lorawan_msg_t*)msg->header->payload;

				lorawan_data_t send_data;
				send_data.type_msg = UNCONFIRMED_TYPE;
				send_data.unconfirmed.port = APP_PORT;
				send_data.unconfirmed.device_addr = lora_msg->device_addr;
				send_data.unconfirmed.data_len = sizeof(lorawan_msg_t);
				send_data.unconfirmed.data = (uint8_t*)lora_msg;
				loramac_send_msg(&send_data);

			}
				break;

			case LORAWAN_DEVICE_SEND_PROPRIETARY: {
				APP_DBG("LORAWAN_DEVICE_SEND_PROPRIETARY\n");
				lorawan_msg_t* lora_msg = (lorawan_msg_t*)msg->header->payload;

				lorawan_data_t send_data;
				send_data.type_msg = PROPRIETARY_TYPE;
				send_data.proprietary.data_len = sizeof(lorawan_msg_t);
				send_data.proprietary.data = (uint8_t*)lora_msg;
				loramac_send_msg(&send_data);

			}
				break;

			case LORAWAN_DEVICE_RECV: {
				APP_DBG("LORAWAN_DEVICE_RECV\n");
				lorawan_data_t* data_recv = (lorawan_data_t*)msg->header->payload;

				switch (data_recv->type_msg) {

				case JOIN_TYPE: {
					if(data_recv->join.is_rejoin) {
						APP_DBG("Rejoin request\n");
						/*auto acept*/
						lorawan_data_t* data_send = data_recv;
						loramac_send_msg(data_send);
					}
					else {
						if( enable_join == true ) {
							timer_remove_attr(MT_TASK_LORAWAN_ID, LORAWAN_DEVICE_JOIN_TIMER);
							/*enable acept join*/
							lorawan_data_t* data_send = data_recv;
							loramac_send_msg(data_send);
						}
					}
				}
					break;

				case UNCONFIRMED_TYPE: {
					if(data_recv->unconfirmed.data_len) {

						lorawan_msg_t* notify_msg = (lorawan_msg_t*)data_recv->unconfirmed.data;

						notify_msg->device_addr = data_recv->unconfirmed.device_addr;

						APP_DBG("\nnotify_msg->clusterId:x%04X\n" , notify_msg->cluster_id);
						APP_DBG("notify_msg->deviceAddr:x%08X\n", notify_msg->device_addr);
						APP_DBG("notify_msg->sequence:x%04X\n" , notify_msg->sequence);
						APP_DBG("notify_msg->cmd_id:x%02X\n" , notify_msg->cmd_id);
						APP_DBG("notify_msg->dataType:x%02X\n" , notify_msg->data_type);
						APP_DBG("notify_msg->dataLen:x%02X\n" , notify_msg->data_len);
						APP_DBG("notify_msg->data:x%02X\n\n" , notify_msg->data[0]);

						//send ack
						lorawan_msg_t* ack_msg = notify_msg;
						ack_msg->cmd_id =  ZCL_CMD_ID_DEFAULT_RESPONSE;
						ack_msg->data_len = 1;
						ack_msg->data[0] = 1;
						ak_msg_t* smsg = get_dymanic_msg();
						set_data_dynamic_msg(smsg, (uint8_t*)ack_msg, sizeof(lorawan_msg_t));
						set_msg_sig(smsg, LORAWAN_DEVICE_SEND);
						task_post(MT_TASK_LORAWAN_ID, smsg);

						free(data_recv->unconfirmed.data);

					}
				}
					break;

				case CONFIRMED_TYPE: {
					if(data_recv->confirmed.data_len) {

						lorawan_msg_t* notify_msg = (lorawan_msg_t*)data_recv->confirmed.data;

						notify_msg->device_addr = data_recv->confirmed.device_addr;

						free(data_recv->confirmed.data);
					}
				}
					break;

				case PROPRIETARY_TYPE: {
					if(data_recv->proprietary.data_len ) {


						free(data_recv->proprietary.data);
					}
				}
					break;

				default:
					break;
				}
			}
				break;

			case LORAWAN_DEVICE_JOIN_ENABLE : {
				APP_DBG("LORAWAN_DEVICE_JOIN_ENABLE\n");
				uint8_t timeout = *((uint8_t*)msg->header->payload);

				enable_join = true;
				timer_set(MT_TASK_LORAWAN_ID, LORAWAN_DEVICE_JOIN_TIMER, timeout*1000, TIMER_ONE_SHOT );

			}
				break;

			case LORAWAN_DEVICE_JOIN_TIMER : {
				APP_DBG("LORAWAN_DEVICE_JOIN_TIMER\n");
				if(enable_join == true ) {
					enable_join = false;
				}
			}
				break;

			case LORAWAN_DEVICE_REMOVE : {
				APP_DBG("LORAWAN_DEVICE_REMOVE\n");
				uint8_t mac[8];
				memcpy(mac, msg->header->payload, 8);
				remove_device(mac);

			}
				break;

			case LORAWAN_PKT_FWD : {
				APP_DBG("LORAWAN_PKT_FWD\n");

				rpkt_fwd_t* rpkt = (rpkt_fwd_t*)msg->header->payload;

				cp_nb_rx_rcv ++;
				cp_nb_rx_ok++;

				int buff_index=0, j=0;

				char fetch_timestamp[28];
				struct timespec fetch_time;
				struct tm * x;

				/* pre-fill the data buffer with fixed fields */
				memset(buff_up, 0, TX_BUFF_SIZE);

				buff_up[0] = PROTOCOL_VERSION;
				buff_up[3] = PKT_PUSH_DATA;
				buff_up[4] = (unsigned char)ifr.ifr_hwaddr.sa_data[0];
				buff_up[5] = (unsigned char)ifr.ifr_hwaddr.sa_data[1];
				buff_up[6] = (unsigned char)ifr.ifr_hwaddr.sa_data[2];
				buff_up[7] = 0xFF;
				buff_up[8] = 0xFF;
				buff_up[9] = (unsigned char)ifr.ifr_hwaddr.sa_data[3];
				buff_up[10] = (unsigned char)ifr.ifr_hwaddr.sa_data[4];
				buff_up[11] = (unsigned char)ifr.ifr_hwaddr.sa_data[5];

				/* start composing datagram with the header */
				uint8_t token_h = (uint8_t)rand(); /* random token */
				uint8_t token_l = (uint8_t)rand(); /* random token */
				buff_up[1] = token_h;
				buff_up[2] = token_l;
				buff_index = 12; /* 12-byte header */

				// TODO: tmst can jump is time is (re)set, not good.
				struct timeval now;
				gettimeofday(&now, NULL);
				uint32_t tmst = (uint32_t)(now.tv_sec*1000000 + now.tv_usec);

				/* start of JSON structure */
				memcpy((void *)(buff_up + buff_index), (void *)"{\"rxpk\":[", 9);
				buff_index += 9;
				buff_up[buff_index] = '{';
				++buff_index;

				clock_gettime(CLOCK_REALTIME, &fetch_time);
				x = gmtime(&(fetch_time.tv_sec)); /* split the UNIX timestamp to its calendar components */
				snprintf(fetch_timestamp, sizeof fetch_timestamp, "%04i-%02i-%02iT%02i:%02i:%02i.%06liZ", \
						 (x->tm_year)+1900, (x->tm_mon)+1, x->tm_mday, x->tm_hour, x->tm_min, x->tm_sec, (fetch_time.tv_nsec)/1000); /* ISO 8601 format */
				j = snprintf((char *)(buff_up + buff_index), TX_BUFF_SIZE-buff_index, "\"time\":\"%s\"", fetch_timestamp);

				buff_index += j;
				j = snprintf((char *)(buff_up + buff_index), TX_BUFF_SIZE-buff_index, ",\"tmst\":%u", tmst);
				buff_index += j;
				j = snprintf((char *)(buff_up + buff_index), TX_BUFF_SIZE-buff_index, ",\"chan\":%1u,\"rfch\":%1u,\"freq\":%.6lf", 0, 0, (double)SINGLE_CHANNEL_GW_FREQ/1000000);
				buff_index += j;
				memcpy((void *)(buff_up + buff_index), (void *)",\"stat\":1", 9);
				buff_index += 9;
				memcpy((void *)(buff_up + buff_index), (void *)",\"modu\":\"LORA\"", 14);
				buff_index += 14;
				memcpy((void *)(buff_up + buff_index), (void *)",\"datr\":\"SF12", 13);
				buff_index += 13;
				memcpy((void *)(buff_up + buff_index), (void *)"BW125\"", 6);
				buff_index += 6;
				memcpy((void *)(buff_up + buff_index), (void *)",\"codr\":\"4/5\"", 13);
				buff_index += 13;
				j = snprintf((char *)(buff_up + buff_index), TX_BUFF_SIZE-buff_index, ",\"lsnr\":%d", rpkt->snr);
				buff_index += j;
				j = snprintf((char *)(buff_up + buff_index), TX_BUFF_SIZE-buff_index, ",\"rssi\":%d,\"size\":%u", rpkt->rssi, rpkt->size);
				buff_index += j;
				memcpy((void *)(buff_up + buff_index), (void *)",\"data\":\"", 9);
				buff_index += 9;

				//Base64encode((char *)buff_up + buff_index, (char *)rpkt->rpkt, rpkt->size);
				//j = Base64encode_len(rpkt->size);
				j = bin_to_b64((uint8_t *)rpkt->rpkt, rpkt->size, (char *)(buff_up + buff_index), 341);

				buff_index += j;
				buff_up[buff_index] = '"';
				++buff_index;

				/* End of packet serialization */
				buff_up[buff_index] = '}';
				++buff_index;
				buff_up[buff_index] = ']';
				++buff_index;
				/* end of JSON datagram payload */
				buff_up[buff_index] = '}';
				++buff_index;
				buff_up[buff_index] = 0; /* add string terminator, for safety */

				APP_DBG("rxpk update: %s\n", (char *)(buff_up + 12)); /* DEBUG: display JSON payload */

				//send the messages
				inet_aton(SERVER1 , &si_other.sin_addr);
				if (sendto(s, (char *)buff_up, buff_index, 0 , (struct sockaddr *) &si_other, slen)==-1)
				{
					APP_DBG("sendto() error");
					break;
				}
				cp_up_pkt_fwd++;

			}
				break;

			case LORAWAN_PKT_FWD_STAT : {
				APP_DBG("LORAWAN_PKT_FWD_STAT\n");

				int stat_index=0;

				char stat_timestamp[28];
				struct timespec stat_time;
				struct tm * x;

				/* pre-fill the data buffer with fixed fields */
				status_report[0] = PROTOCOL_VERSION;
				status_report[3] = PKT_PUSH_DATA;

				status_report[4] = (unsigned char)ifr.ifr_hwaddr.sa_data[0];
				status_report[5] = (unsigned char)ifr.ifr_hwaddr.sa_data[1];
				status_report[6] = (unsigned char)ifr.ifr_hwaddr.sa_data[2];
				status_report[7] = 0xFF;
				status_report[8] = 0xFF;
				status_report[9] = (unsigned char)ifr.ifr_hwaddr.sa_data[3];
				status_report[10] = (unsigned char)ifr.ifr_hwaddr.sa_data[4];
				status_report[11] = (unsigned char)ifr.ifr_hwaddr.sa_data[5];

				/* start composing datagram with the header */
				uint8_t token_h = (uint8_t)rand(); /* random token */
				uint8_t token_l = (uint8_t)rand(); /* random token */
				status_report[1] = token_h;
				status_report[2] = token_l;
				stat_index = 12; /* 12-byte header */

				clock_gettime(CLOCK_REALTIME, &stat_time);
				x = gmtime(&(stat_time.tv_sec)); /* split the UNIX timestamp to its calendar components */
				snprintf(stat_timestamp, sizeof stat_timestamp, "%04i-%02i-%02iT%02i:%02i:%02i.%06liZ", \
						 (x->tm_year)+1900, (x->tm_mon)+1, x->tm_mday, x->tm_hour, x->tm_min, x->tm_sec, (stat_time.tv_nsec)/1000); /* ISO 8601 format */

				int j = snprintf((char *)(status_report + stat_index), STATUS_SIZE-stat_index, \
								 "{\"stat\":{\"time\":\"%s\",\"lati\":%.5f,\"long\":%.5f,\"alti\":%i,\"rxnb\":%u,\"rxok\":%u,\"rxfw\":%u,\"ackr\":%.1f,\"dwnb\":%u,\"txnb\":%u}}", \
								 stat_timestamp, lat, lon, (int)alt, cp_nb_rx_rcv, cp_nb_rx_ok, cp_up_pkt_fwd, (float)0, 0, 0);
				stat_index += j;
				status_report[stat_index] = 0; /* add string terminator, for safety */

				APP_DBG("stat update: %s\n", (char *)(status_report+12)); /* DEBUG: display JSON stat */

				//send the messages
				inet_aton(SERVER1 , &si_other.sin_addr);
				if (sendto(s, (char *)status_report, stat_index, 0 , (struct sockaddr *) &si_other, slen)==-1)
				{
					APP_DBG("sendto() error");
					break;
				}

			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(100);
	}

	return (void*)0;
}
