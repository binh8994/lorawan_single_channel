/*
 * global_parameters.h
 *
 *  Created on: Jun 14, 2017
 *      Author: TuDT13
 */

#ifndef SOURCE_GLOBAL_PARAMETERS_H_
#define SOURCE_GLOBAL_PARAMETERS_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#define AES_ENCRYPT_FLAG
using namespace std;
#define TRUE										1
#define FALSE										0

#define DIFF_TIME  	180 /*(seconds)*/

#define MAX_MESSAGE_LEN		2000

const uint8_t SEPARATE_CHAR = ':';
const int8_t ONLINE = 0x01;
const int8_t OFFLINE = 0x00;

extern uint8_t g_u8ServerStatus;
extern uint8_t g_strTokenFactory[255];
//extern string g_strTokenCommunicate;
extern const char g_libAES_version[];

extern const char mqtt_host_domain_str[];

#define LEN_COMMUNICATE_STRING  	20
typedef struct {
	int8_t totalString;
	int32_t position[LEN_COMMUNICATE_STRING];
	int32_t len[LEN_COMMUNICATE_STRING];
} parse_communicate_string_t;

extern string g_strCommand[];
enum {
	adddevice,
	removedevice,
	sensordata,
	controldevice,
	canceladddevice,
    updatescene,
	updatesendsms,
	sendsms,
	responsedeviceleavenetwork, //8: response device leave network
	lorasensordata,
	loracontroldevice,
	resetgateway,
	commandlength
};
enum {
	error_adddevice,
	errorcommandlength
};

enum SENSOR_TYPE {
	UNKNOWN,
	ON_OFF,
	TEMPERATURE_MEASUREMENT,
	RELATIVE_HUMIDITY,
	OCCUPANCY_SENSING,
	INFO,
    SENSOR_NAME,
    BATTERY_VOLTAGE,
    PRE_STATUS,
    CURRENT_STATUS,
    LOAD_POWER,
    LOAD_VOLTAGE,
    VALUE_1,
    VALUE_2,
	SMOKING_ALARM,
	LAST_SENSOR_TYPE,
};

//extern uint8_t lastSensordataMessage[255];
extern string g_strSensorType[];
extern string g_strCommandError[];

// mac address
extern char s_ieee_address[17];


extern void bytetoHexChar(uint8_t ubyte, uint8_t *uHexChar);
extern void bytestoHexChars(uint8_t *ubyte, int32_t len, uint8_t *uHexChar);

extern void hexChartoByte(uint8_t *uHexChar, uint8_t *ubyte);
extern void hexCharsToBytes(uint8_t *uHexChar, int32_t len, uint8_t *ubyte);

extern parse_communicate_string_t parseClientString(uint8_t *client_message, int32_t len);
extern void string_copy(uint8_t *dest, uint8_t *src, uint8_t len);
#ifdef AES_ENCRYPT_FLAG
extern int remove_all_chars(char* str, char c, int len);
#endif
#endif /* SOURCE_GLOBAL_PARAMETERS_H_ */
extern size_t writeCallback(void *contents, size_t size, size_t nmemb, void *userp);
extern size_t downloadCallback(void *ptr, size_t size, size_t nmemb, FILE *stream);
extern bool curlGetConfigurationMessage(string &result);
extern bool curlPostgetFile(const char* url, char* filepath);
