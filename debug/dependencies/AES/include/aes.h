#ifndef _AES_H_
#define _AES_H_

#ifdef __cplusplus
extern "C" {
#endif

extern void handleErrors(void);
extern int aes_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv, unsigned char *ciphertext);
extern int aes_decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key, unsigned char *iv, unsigned char *plaintext);
extern int iot_final_encrypt(unsigned char* client_message, int client_message_length, unsigned char *encrypted_key);
extern int iot_final_decrypt(unsigned char* decryptedtext, int decryptedtext_len, unsigned char *encrypted_key);
#ifdef __cplusplus
}
#endif

#endif //_AES_H_
