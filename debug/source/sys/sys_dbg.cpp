#include <stdlib.h>
#include "sys_dbg.h"
#include "../ak/ak.h"
void sys_dbg_fatal(const char* s, uint8_t c) {
	printf("FATAL: %s \t %02X \t task id = %d\n", s, c, get_task_id());
	exit(EXIT_FAILURE);
}
