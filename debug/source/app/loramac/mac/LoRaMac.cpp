/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
	(C)2013 Semtech
 ___ _____ _   ___ _  _____ ___  ___  ___ ___
/ __|_   _/_\ / __| |/ / __/ _ \| _ \/ __| __|
\__ \ | |/ _ \ (__| ' <| _| (_) |   / (__| _|
|___/ |_/_/ \_\___|_|\_\_| \___/|_|_\\___|___|
embedded.connectivity.solutions===============

Description: LoRa MAC layer implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis ( Semtech ), Gregory Cristian ( Semtech ) and Daniel Jäckle ( STACKFORCE )
*/

#include <stdint.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

#include "../ak/ak.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../app/app.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../sys/sys_dbg.h"

#include "../radio/radio.h"
#include "../radio/sx1276/sx1276.h"
#include "../radio/sx1276/sx1276_cfg.h"
#include "LoRaMacCrypto.h"
#include "LoRaMac.h"
#include "LoRaMacTest.h"
#include "LoRaMac-definitions.h"


#define USE_BAND_433

/*!
 * Maximum PHY layer payload size
 */
#define LORAMAC_PHY_MAXPAYLOAD                      255

/*!
 * Maximum MAC commands buffer size
 */
#define LORA_MAC_COMMAND_MAX_LENGTH                 15

/*!
 * FRMPayload overhead to be used when setting the Radio.SetMaxPayloadLength
 * in RxWindowSetup function.
 * Maximum PHYPayload = MaxPayloadOfDatarate/MaxPayloadOfDatarateRepeater + LORA_MAC_FRMPAYLOAD_OVERHEAD
 */
#define LORA_MAC_FRMPAYLOAD_OVERHEAD                13 // MHDR(1) + FHDR(7) + Port(1) + MIC(4)

/*!
 * LoRaMac duty cycle for the back-off procedure during the first hour.
 */
#define BACKOFF_DC_1_HOUR                           100

/*!
 * LoRaMac duty cycle for the back-off procedure during the next 10 hours.
 */
#define BACKOFF_DC_10_HOURS                         1000

/*!
 * LoRaMac duty cycle for the back-off procedure during the next 24 hours.
 */
#define BACKOFF_DC_24_HOURS                         10000

typedef struct {
	uint32_t deviceAddr;
	uint8_t deviceEui[8];
	uint32_t downLinkCnt;
	uint32_t upLinkCnt;
	bool deviceAckReq;
	bool gwAckReq;
} device_mng_t;

static device_mng_t device_mng;

/*!
 * Device IEEE EUI
 */
static uint8_t *LoRaMacDevEui;

/*!
 * Application IEEE EUI
 */
static uint8_t *LoRaMacAppEui;

/*!
 * AES encryption/decryption cipher application key
 */
static uint8_t *LoRaMacAppKey;

/*!
 * AES encryption/decryption cipher network session key
 */
static uint8_t LoRaMacNwkSKey[] =
{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/*!
 * AES encryption/decryption cipher application session key
 */
static uint8_t LoRaMacAppSKey[] =
{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/*!
 * Device nonce is a random value extracted by issuing a sequence of RSSI
 * measurements
 */
static uint16_t LoRaMacDevNonce;

/*!
 * Network ID ( 3 bytes )
 */
static uint32_t LoRaMacNetID;

/*!
 * Mote Address
 */
static uint32_t LoRaMacDevAddr;

/*!
 * Multicast channels linked list
 */
static MulticastParams_t *MulticastChannels = NULL;

/*!
 * Actual device class
 */
static DeviceClass_t LoRaMacDeviceClass;

/*!
 * Indicates if the node is connected to a private or public network
 */
static bool PublicNetwork;

/*!
 * Indicates if the node supports repeaters
 */
static bool RepeaterSupport;

/*!
 * Buffer containing the data to be sent or received.
 */
static uint8_t LoRaMacBuffer[LORAMAC_PHY_MAXPAYLOAD];

/*!
 * Length of packet in LoRaMacBuffer
 */
static uint16_t LoRaMacBufferPktLen = 0;

/*!
 * Length of the payload in LoRaMacBuffer
 */
static uint8_t LoRaMacTxPayloadLen = 0;

/*!
 * Buffer containing the upper layer data.
 */
static uint8_t LoRaMacRxPayload[LORAMAC_PHY_MAXPAYLOAD];

/*!
 * LoRaMAC frame counter. Each time a packet is sent the counter is incremented.
 * Only the 16 LSB bits are sent
 */
static uint32_t UpLinkCounter = 0;

/*!
 * LoRaMAC frame counter. Each time a packet is received the counter is incremented.
 * Only the 16 LSB bits are received
 */
static uint32_t DownLinkCounter = 0;

/*!
 * IsPacketCounterFixed enables the MIC field tests by fixing the
 * UpLinkCounter value
 */
static bool IsUpLinkCounterFixed = false;

/*!
 * Used for test purposes. Disables the opening of the reception windows.
 */
static bool IsRxWindowsEnabled = true;

/*!
 * Indicates if the MAC layer has already joined a network.
 */
static bool IsLoRaMacNetworkJoined = false;

/*!
 * LoRaMac ADR control status
 */
static bool AdrCtrlOn = false;

/*!
 * Counts the number of missed ADR acknowledgements
 */
static uint32_t AdrAckCounter = 0;

/*!
 * If the node has sent a FRAME_TYPE_DATA_CONFIRMED_UP this variable indicates
 * if the nodes needs to manage the server acknowledgement.
 */
static bool NodeAckRequested = false;

/*!
 * If the server has sent a FRAME_TYPE_DATA_CONFIRMED_DOWN this variable indicates
 * if the ACK bit must be set for the next transmission
 */
static bool SrvAckRequested = false;

/*!
 * Indicates if the MAC layer wants to send MAC commands
 */
static bool MacCommandsInNextTx = false;

/*!
 * Contains the current MacCommandsBuffer index
 */
static uint8_t MacCommandsBufferIndex = 0;

/*!
 * Contains the current MacCommandsBuffer index for MAC commands to repeat
 */
static uint8_t MacCommandsBufferToRepeatIndex = 0;

/*!
 * Buffer containing the MAC layer commands
 */
static uint8_t MacCommandsBuffer[LORA_MAC_COMMAND_MAX_LENGTH];

/*!
 * Buffer containing the MAC layer commands which must be repeated
 */
static uint8_t MacCommandsBufferToRepeat[LORA_MAC_COMMAND_MAX_LENGTH];

#if defined( USE_BAND_433 )
/*!
 * Data rates table definition
 */
const uint8_t Datarates[]  = { 12, 11, 10,  9,  8,  7,  7, 50 };

/*!
 * Bandwidths table definition in Hz
 */
const uint32_t Bandwidths[] = { 125000, 125000, 125000, 125000, 125000, 125000, 250000, 0 };

/*!
 * Maximum payload with respect to the datarate index. Cannot operate with repeater.
 */
const uint8_t MaxPayloadOfDatarate[] = { 51, 51, 51, 115, 242, 242, 242, 242 };

/*!
 * Maximum payload with respect to the datarate index. Can operate with repeater.
 */
const uint8_t MaxPayloadOfDatarateRepeater[] = { 51, 51, 51, 115, 222, 222, 222, 222 };

/*!
 * Tx output powers table definition
 */
const int8_t TxPowers[]    = { 10, 7, 4, 1, -2, -5 };

/*!
 * LoRaMac bands
 */
static Band_t Bands[LORA_MAX_NB_BANDS] =
{
	BAND0,
};

/*!
 * LoRaMAC channels
 */
static ChannelParams_t Channels[LORA_MAX_NB_CHANNELS] =
{
	LC1,
	LC2,
	LC3,
};
#elif defined( USE_BAND_470 )

#endif

/*!
 * LoRaMac parameters
 */
LoRaMacParams_t LoRaMacParams;

/*!
 * LoRaMac default parameters
 */
LoRaMacParams_t LoRaMacParamsDefaults;

/*!
 * Uplink messages repetitions counter
 */
static uint8_t ChannelsNbRepCounter = 0;

/*!
 * Maximum duty cycle
 * \remark Possibility to shutdown the device.
 */
static uint8_t MaxDCycle = 0;

/*!
 * Aggregated duty cycle management
 */
static uint16_t AggregatedDCycle;
static uint32_t AggregatedLastTxDoneTime;
static uint32_t AggregatedTimeOff;

/*!
 * Enables/Disables duty cycle management (Test only)
 */
static bool DutyCycleOn;

/*!
 * Current channel index
 */
static uint8_t Channel;

/*!
 * Stores the time at LoRaMac initialization.
 *
 * \remark Used for the BACKOFF_DC computation.
 */
static uint32_t LoRaMacInitializationTime = 0;

/*!
 * LoRaMac internal states
 */
enum eLoRaMacState
{
	LORAMAC_IDLE          = 0x00000000,
	LORAMAC_TX_RUNNING    = 0x00000001,
	LORAMAC_RX            = 0x00000002,
	LORAMAC_ACK_REQ       = 0x00000004,
	LORAMAC_ACK_RETRY     = 0x00000008,
	LORAMAC_TX_DELAYED    = 0x00000010,
	LORAMAC_TX_CONFIG     = 0x00000020,
	LORAMAC_RX_ABORT      = 0x00000040,
};

/*!
 * LoRaMac internal state
 */
uint32_t LoRaMacState = LORAMAC_IDLE;

/*!
 * LoRaMac timer used to check the LoRaMacState (runs every second)
 */
//static uint32_t MacStateCheckTimer;

/*!
 * LoRaMac upper layer event functions
 */
static LoRaMacPrimitives_t *LoRaMacPrimitives;

/*!
 * LoRaMac upper layer callback functions
 */
static LoRaMacCallback_t *LoRaMacCallbacks;

/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

/*!
 * LoRaMac duty cycle delayed Tx timer
 */
//static uint32_t TxDelayedTimer;

/*!
 * LoRaMac reception windows timers
 */
//static uint32_t RxWindowTimer1;
//static uint32_t RxWindowTimer2;

/*!
 * LoRaMac reception windows delay
 * \remark normal frame: RxWindowXDelay = ReceiveDelayX - RADIO_WAKEUP_TIME
 *         join frame  : RxWindowXDelay = JoinAcceptDelayX - RADIO_WAKEUP_TIME
 */
static uint32_t RxWindow1Delay;
static uint32_t RxWindow2Delay;

/*!
 * Rx window parameters
 */
typedef struct
{
	int8_t Datarate;
	uint8_t Bandwidth;
	uint32_t RxWindowTimeout;
	int32_t RxOffset;
}RxConfigParams_t;

/*!
 * Rx windows params
 */
static RxConfigParams_t RxWindowsParams[2];

/*!
 * Acknowledge timeout timer. Used for packet retransmissions.
 */
//static uint32_t AckTimeoutTimer;

/*!
 * Number of trials to get a frame acknowledged
 */
static uint8_t AckTimeoutRetries = 1;

/*!
 * Number of trials to get a frame acknowledged
 */
static uint8_t AckTimeoutRetriesCounter = 1;

/*!
 * Indicates if the AckTimeout timer has expired or not
 */
static bool AckTimeoutRetry = false;

/*!
 * Last transmission time on air
 */
uint32_t TxTimeOnAir = 0;

/*!
 * Number of trials for the Join Request
 */
static uint8_t JoinRequestTrials;

/*!
 * Maximum number of trials for the Join Request
 */
static uint8_t MaxJoinRequestTrials;

/*!
 * Structure to hold an MCPS indication data.
 */
static McpsIndication_t McpsIndication;

/*!
 * Structure to hold MCPS confirm data.
 */
static McpsConfirm_t McpsConfirm;

/*!
 * Structure to hold MLME confirm data.
 */
static MlmeConfirm_t MlmeConfirm;

/*!
 * Holds the current rx window slot
 */
static uint8_t RxSlot = 0;

/*!
 * LoRaMac tx/rx operation state
 */
LoRaMacFlags_t LoRaMacFlags;

/*!
 * \brief Function to be executed on Radio Tx Done event
 */
static void OnRadioTxDone( void );

/*!
 * \brief This function prepares the MAC to abort the execution of function
 *        OnRadioRxDone in case of a reception error.
 */
static void PrepareRxDoneAbort( void );

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
static void OnRadioRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
static void OnRadioTxTimeout( void );

/*!
 * \brief Function executed on Radio Rx error event
 */
static void OnRadioRxError( void );

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
static void OnRadioRxTimeout( void );

///*!
// * \brief Function executed on Resend Frame timer event.
// */
//static void OnMacStateCheckTimerEvent( void );

///*!
// * \brief Function executed on duty cycle delayed Tx  timer event
// */
//static void OnTxDelayedTimerEvent( void );

///*!
// * \brief Function executed on first Rx window timer event
// */
//static void OnRxWindow1TimerEvent( void );

///*!
// * \brief Function executed on second Rx window timer event
// */
//static void OnRxWindow2TimerEvent( void );

///*!
// * \brief Function executed on AckTimeout timer event
// */
//static void OnAckTimeoutTimerEvent( void );

/*!
 * \brief Searches and set the next random available channel
 *
 * \param [OUT] Time to wait for the next transmission according to the duty
 *              cycle.
 *
 * \retval status  Function status [1: OK, 0: Unable to find a channel on the
 *                                  current datarate]
 */
static bool SetNextChannel( uint32_t* time );

/*!
 * \brief Initializes and opens the reception window
 *
 * \param [IN] freq window channel frequency
 * \param [IN] datarate window channel datarate
 * \param [IN] bandwidth window channel bandwidth
 * \param [IN] timeout window channel timeout
 *
 * \retval status Operation status [true: Success, false: Fail]
 */
static bool RxWindowSetup( uint32_t freq, int8_t datarate, uint32_t bandwidth, uint16_t timeout, bool rxContinuous );

/*!
 * \brief Verifies if the RX window 2 frequency is in range
 *
 * \param [IN] freq window channel frequency
 *
 * \retval status  Function status [1: OK, 0: Frequency not applicable]
 */
static bool Rx2FreqInRange( uint32_t freq );

/*!
 * \brief Adds a new MAC command to be sent.
 *
 * \Remark MAC layer internal function
 *
 * \param [in] cmd MAC command to be added
 *                 [MOTE_MAC_LINK_CHECK_REQ,
 *                  MOTE_MAC_LINK_ADR_ANS,
 *                  MOTE_MAC_DUTY_CYCLE_ANS,
 *                  MOTE_MAC_RX2_PARAM_SET_ANS,
 *                  MOTE_MAC_DEV_STATUS_ANS
 *                  MOTE_MAC_NEW_CHANNEL_ANS]
 * \param [in] p1  1st parameter ( optional depends on the command )
 * \param [in] p2  2nd parameter ( optional depends on the command )
 *
 * \retval status  Function status [0: OK, 1: Unknown command, 2: Buffer full]
 */
static LoRaMacStatus_t AddMacCommand( uint8_t cmd, uint8_t p1, uint8_t p2 );

/*!
 * \brief Parses the MAC commands which must be repeated.
 *
 * \Remark MAC layer internal function
 *
 * \param [IN] cmdBufIn  Buffer which stores the MAC commands to send
 * \param [IN] length  Length of the input buffer to parse
 * \param [OUT] cmdBufOut  Buffer which stores the MAC commands which must be
 *                         repeated.
 *
 * \retval Size of the MAC commands to repeat.
 */
static uint8_t ParseMacCommandsToRepeat( uint8_t* cmdBufIn, uint8_t length, uint8_t* cmdBufOut );

/*!
 * \brief Validates if the payload fits into the frame, taking the datarate
 *        into account.
 *
 * \details Refer to chapter 4.3.2 of the LoRaWAN specification, v1.0
 *
 * \param lenN Length of the application payload. The length depends on the
 *             datarate and is region specific
 *
 * \param datarate Current datarate
 *
 * \param fOptsLen Length of the fOpts field
 *
 * \retval [false: payload does not fit into the frame, true: payload fits into
 *          the frame]
 */
static bool ValidatePayloadLength( uint8_t lenN, int8_t datarate, uint8_t fOptsLen );

/*!
 * \brief Counts the number of bits in a mask.
 *
 * \param [IN] mask A mask from which the function counts the active bits.
 * \param [IN] nbBits The number of bits to check.
 *
 * \retval Number of enabled bits in the mask.
 */
static uint8_t CountBits( uint16_t mask, uint8_t nbBits );

/*!
 * \brief Validates the correctness of the datarate against the enable channels.
 *
 * \param [IN] datarate Datarate to be check
 * \param [IN] channelsMask Pointer to the first element of the channel mask
 *
 * \retval [true: datarate can be used, false: datarate can not be used]
 */
static bool ValidateDatarate( int8_t datarate, uint16_t* channelsMask );

/*!
 * \brief Limits the Tx power according to the number of enabled channels
 *
 * \param [IN] txPower txPower to limit
 * \param [IN] maxBandTxPower Maximum band allowed TxPower
 *
 * \retval Returns the maximum valid tx power
 */
static int8_t LimitTxPower( int8_t txPower, int8_t maxBandTxPower );

/*!
 * \brief Verifies, if a value is in a given range.
 *
 * \param value Value to verify, if it is in range
 *
 * \param min Minimum possible value
 *
 * \param max Maximum possible value
 *
 * \retval Returns the maximum valid tx power
 */
static bool ValueInRange( int8_t value, int8_t min, int8_t max );

/*!
 * \brief Calculates the next datarate to set, when ADR is on or off
 *
 * \param [IN] adrEnabled Specify whether ADR is on or off
 *
 * \param [IN] updateChannelMask Set to true, if the channel masks shall be updated
 *
 * \param [OUT] datarateOut Reports the datarate which will be used next
 *
 * \retval Returns the state of ADR ack request
 */
static bool AdrNextDr( bool adrEnabled, bool updateChannelMask, int8_t* datarateOut );

/*!
 * \brief Disables channel in a specified channel mask
 *
 * \param [IN] id - Id of the channel
 *
 * \param [IN] mask - Pointer to the channel mask to edit
 *
 * \retval [true, if disable was successful, false if not]
 */
static bool DisableChannelInMask( uint8_t id, uint16_t* mask );

/*!
 * \brief Decodes MAC commands in the fOpts field and in the payload
 */
static void ProcessMacCommands( uint8_t *payload, uint8_t macIndex, uint8_t commandsSize, uint8_t snr );

/*!
 * \brief LoRaMAC layer generic send frame
 *
 * \param [IN] macHdr      MAC header field
 * \param [IN] fPort       MAC payload port
 * \param [IN] fBuffer     MAC data buffer to be sent
 * \param [IN] fBufferSize MAC data buffer size
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t Send( LoRaMacHeader_t *macHdr, uint8_t fPort, void *fBuffer, uint16_t fBufferSize );

/*!
 * \brief LoRaMAC layer frame buffer initialization
 *
 * \param [IN] macHdr      MAC header field
 * \param [IN] fCtrl       MAC frame control field
 * \param [IN] fOpts       MAC commands buffer
 * \param [IN] fPort       MAC payload port
 * \param [IN] fBuffer     MAC data buffer to be sent
 * \param [IN] fBufferSize MAC data buffer size
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t PrepareFrame( LoRaMacHeader_t *macHdr, LoRaMacFrameCtrl_t *fCtrl, uint8_t fPort, void *fBuffer, uint16_t fBufferSize );

/*!
 * \brief Schedules the frame according to the duty cycle
 *
 * \retval Status of the operation
 */
static LoRaMacStatus_t ScheduleTx( void );

/*!
 * \brief Sets the duty cycle for the join procedure.
 *
 * \retval Duty cycle
 */
static uint16_t JoinDutyCycle( void );

/*!
 * \brief Calculates the back-off time for the band of a channel.
 *
 * \param [IN] channel     The last Tx channel index
 */
static void CalculateBackOff( uint8_t channel );

/*!
 * \brief Alternates the datarate of the channel for the join request.
 *
 * \param [IN] nbTrials    Number of performed join requests.
 * \retval Datarate to apply
 */
static int8_t AlternateDatarate( uint16_t nbTrials );

/*!
 * \brief LoRaMAC layer prepared frame buffer transmission with channel specification
 *
 * \remark PrepareFrame must be called at least once before calling this
 *         function.
 *
 * \param [IN] channel     Channel parameters
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t SendFrameOnChannel( ChannelParams_t channel );

/*!
 * \brief Sets the radio in continuous transmission mode
 *
 * \remark Uses the radio parameters set on the previous transmission.
 *
 * \param [IN] timeout     Time in seconds while the radio is kept in continuous wave mode
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t SetTxContinuousWave( uint16_t timeout );

/*!
 * \brief Sets the radio in continuous transmission mode
 *
 * \remark Uses the radio parameters set on the previous transmission.
 *
 * \param [IN] timeout     Time in seconds while the radio is kept in continuous wave mode
 * \param [IN] frequency   RF frequency to be set.
 * \param [IN] power       RF ouptput power to be set.
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t SetTxContinuousWave1( uint16_t timeout, uint32_t frequency, uint8_t power );

/*!
 * \brief Resets MAC specific parameters to default
 */
static void ResetMacParameters( void );

/*
 * Rx window precise timing
 *
 * For more details please consult the following document, chapter 3.1.2.
 * http://www.semtech.com/images/datasheet/SX1272_settings_for_LoRaWAN_v2.0.pdf
 * or
 * http://www.semtech.com/images/datasheet/SX1276_settings_for_LoRaWAN_v2.0.pdf
 *
 *                 Downlink start: T = Tx + 1s (+/- 20 us)
 *                            |
 *             TRxEarly       |        TRxLate
 *                |           |           |
 *                |           |           +---+---+---+---+---+---+---+---+
 *                |           |           |       Latest Rx window        |
 *                |           |           +---+---+---+---+---+---+---+---+
 *                |           |           |
 *                +---+---+---+---+---+---+---+---+
 *                |       Earliest Rx window      |
 *                +---+---+---+---+---+---+---+---+
 *                            |
 *                            +---+---+---+---+---+---+---+---+
 *Downlink preamble 8 symbols |   |   |   |   |   |   |   |   |
 *                            +---+---+---+---+---+---+---+---+
 *
 *                     Worst case Rx window timings
 *
 * TRxLate  = DEFAULT_MIN_RX_SYMBOLS * tSymbol - RADIO_WAKEUP_TIME
 * TRxEarly = 8 - DEFAULT_MIN_RX_SYMBOLS * tSymbol - RxWindowTimeout - RADIO_WAKEUP_TIME
 *
 * TRxLate - TRxEarly = 2 * DEFAULT_SYSTEM_MAX_RX_ERROR
 *
 * RxOffset = ( TRxLate + TRxEarly ) / 2
 *
 * RxWindowTimeout = ( 2 * DEFAULT_MIN_RX_SYMBOLS - 8 ) * tSymbol + 2 * DEFAULT_SYSTEM_MAX_RX_ERROR
 * RxOffset = 4 * tSymbol - RxWindowTimeout / 2 - RADIO_WAKE_UP_TIME
 *
 * Minimal value of RxWindowTimeout must be 5 symbols which implies that the system always tolerates at least an error of 1.5 * tSymbol
 */
/*!
 * Computes the Rx window parameters.
 *
 * \param [IN] datarate     Rx window datarate to be used
 * \param [IN] rxError      Maximum timing error of the receiver. in milliseconds
 *                          The receiver will turn on in a [-rxError : +rxError] ms
 *                          interval around RxOffset
 *
 * \retval rxConfigParams   Returns a RxConfigParams_t structure.
 */
static RxConfigParams_t ComputeRxWindowParameters( int8_t datarate, uint32_t rxError );

/*
 * binhnt61
 */
static uint32_t TimerGetElapsedTime( uint32_t eventInTime );
static uint32_t TimerGetCurrentTime( void );
static int32_t	randr( int32_t min, int32_t max );
static void		memcpyr( uint8_t *dst, const uint8_t *src, uint16_t size );
static void		initialiseEpoch (void);
static uint32_t millis (void);
static void change_to_rx(void);
static uint64_t epochMilli;

static void OnRadioTxDone( void )
{
	SYS_DBG("OnRadioTxDone\n");

	change_to_rx();
}

static void PrepareRxDoneAbort( void )
{

}

static void OnRadioRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	SYS_DBG("on_rx_done-size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);

	for(int i=0; i<size; i++) {
		SYS_DBG("x%02X ", *(payload+i));
	}
	SYS_DBG("\n");

	LoRaMacHeader_t macHdr;
	LoRaMacFrameCtrl_t fCtrl;

	uint8_t pktHeaderLen = 0;
	uint32_t address = 0;
	uint8_t appPayloadStartIndex = 0;
	uint8_t port = 0xFF;
	uint8_t frameLen = 0;
	uint32_t mic = 0;
	uint32_t micRx = 0;

	uint16_t sequenceCounter = 0;
	uint16_t sequenceCounterPrev = 0;
	uint16_t sequenceCounterDiff = 0;
	uint32_t downLinkCounter = 0;

	uint8_t *nwkSKey = LoRaMacNwkSKey;
	uint8_t *appSKey = LoRaMacAppSKey;

	bool isMicOk = false;

	Radio.Sleep( );

	macHdr.Value = payload[pktHeaderLen++];

	SYS_DBG("Parser start\n");

	switch( macHdr.Bits.MType )
	{
	case FRAME_TYPE_JOIN_REQ:
		SYS_DBG("FRAME_TYPE_JOIN_REQ\n");

		break;

	case FRAME_TYPE_DATA_CONFIRMED_UP:
		SYS_DBG("FRAME_TYPE_DATA_CONFIRMED_UP\n");
	case FRAME_TYPE_DATA_UNCONFIRMED_UP:
	{
		SYS_DBG("FRAME_TYPE_DATA_UNCONFIRMED_UP\n");

		address = payload[pktHeaderLen++];
		address |= ( (uint32_t)payload[pktHeaderLen++] << 8 );
		address |= ( (uint32_t)payload[pktHeaderLen++] << 16 );
		address |= ( (uint32_t)payload[pktHeaderLen++] << 24 );

		SYS_DBG("address:%08X\n", address);

		//downLinkCounter = DownLinkCounter;
		downLinkCounter = device_mng.downLinkCnt;

		fCtrl.Value = payload[pktHeaderLen++];

		SYS_DBG("FOptsLen:%08X\n", fCtrl.Bits.FOptsLen);

		sequenceCounter = ( uint16_t )payload[pktHeaderLen++];
		sequenceCounter |= ( uint16_t )payload[pktHeaderLen++] << 8;

		SYS_DBG("sequenceCounter:%d\n", sequenceCounter);

		appPayloadStartIndex = 8 + fCtrl.Bits.FOptsLen;

		micRx |= ( uint32_t )payload[size - LORAMAC_MFR_LEN];
		micRx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 1] << 8 );
		micRx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 2] << 16 );
		micRx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 3] << 24 );

		SYS_DBG("micRx:%08X\n", micRx);

		sequenceCounterPrev = ( uint16_t )downLinkCounter;
		sequenceCounterDiff = ( sequenceCounter - sequenceCounterPrev );

		SYS_DBG("sequenceCounterDiff:%d\n", sequenceCounterDiff);

		if( sequenceCounterDiff < ( 1 << 15 ) )
		{
			downLinkCounter += sequenceCounterDiff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, nwkSKey, address, UP_LINK, downLinkCounter, &mic );
			if( micRx == mic )
			{
				isMicOk = true;
			}
		}
		else
		{
			// check for sequence roll-over
			uint32_t  downLinkCounterTmp = downLinkCounter + 0x10000 + ( int16_t )sequenceCounterDiff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, appSKey, address, UP_LINK, downLinkCounterTmp, &mic );
			if( micRx == mic )
			{
				isMicOk = true;
				downLinkCounter = downLinkCounterTmp;
			}
		}

		// Check for a the maximum allowed counter difference
		if( sequenceCounterDiff >= MAX_FCNT_GAP )
		{
			SYS_DBG("sequenceCounterDiff >= MAX_FCNT_GAP\n");
			SYS_DBG("Too many mes lossed\n");
			return;
		}

		if( isMicOk == true )
		{
			if( ( DownLinkCounter == downLinkCounter ) &&
					( DownLinkCounter != 0 ) )
			{
				SYS_DBG("DownLinkCounter == downLinkCounter\n");
				SYS_DBG("This mes was repeated\n");
				return;
			}

			//DownLinkCounter = downLinkCounter;
			device_mng.downLinkCnt = downLinkCounter;

			MacCommandsBufferIndex = 0;

			// Process payload and MAC commands
			if( ( ( size - 4 ) - appPayloadStartIndex ) > 0 )
			{
				port = payload[appPayloadStartIndex++];
				frameLen = ( size - 4 ) - appPayloadStartIndex;

				SYS_DBG("port:%d\n", port);

				memset(LoRaMacRxPayload, 0, LORAMAC_PHY_MAXPAYLOAD);

				if( port == 0 )
				{
					// Only allow frames which do not have fOpts
					if( fCtrl.Bits.FOptsLen == 0 )
					{
						SYS_DBG("downLinkCounter:%d\n", downLinkCounter);

						LoRaMacPayloadDecrypt( payload + appPayloadStartIndex,
											   frameLen,
											   nwkSKey,
											   address,
											   UP_LINK,
											   downLinkCounter,
											   LoRaMacRxPayload );

						// Decode frame payload MAC commands
						ProcessMacCommands( LoRaMacRxPayload, 0, frameLen, snr );
					}
				}
				else
				{
					if( fCtrl.Bits.FOptsLen > 0 )
					{
						// Decode Options field MAC commands. Omit the fPort.
						ProcessMacCommands( payload, 8, appPayloadStartIndex - 1, snr );
					}

					SYS_DBG("downLinkCounter:%d\n", downLinkCounter);

					LoRaMacPayloadDecrypt( payload + appPayloadStartIndex,
										   frameLen,
										   appSKey,
										   address,
										   UP_LINK,
										   downLinkCounter,
										   LoRaMacRxPayload );

					SYS_DBG("Frame_len:%d\n",frameLen);
					for(int i = 0; i < frameLen; i++) {
						SYS_DBG("x%02X ", *(LoRaMacRxPayload+i));
					}
					SYS_DBG("\n");

					ak_msg_t* smsg = get_dymanic_msg();
					set_data_dynamic_msg(smsg, LoRaMacRxPayload, frameLen);
					set_msg_sig(smsg, LORAWAN_DEVICE_RECV_PROPRIETARY);
					task_post(ID_TASK_LORAWAN_ID, smsg);
				}
			}
			else
			{
				if( fCtrl.Bits.FOptsLen > 0 )
				{
					// Decode Options field MAC commands
					ProcessMacCommands( payload, 8, appPayloadStartIndex, snr );
				}
			}
		}
		else {
			SYS_DBG("MIC != RxMIC\n");
		}
	}
		break;

	case FRAME_TYPE_PROPRIETARY:
	{
		SYS_DBG("FRAME_TYPE_PROPRIETARY\n");

		frameLen = size - pktHeaderLen;

		memcpy( LoRaMacRxPayload, &payload[pktHeaderLen], size );

		SYS_DBG("Frame_len:%d\n",frameLen);
		for(int i = 0; i < frameLen; i++) {
			SYS_DBG("x%02X ", *(LoRaMacRxPayload+i));
		}
		SYS_DBG("\n");

		ak_msg_t* smsg = get_dymanic_msg();
		set_data_dynamic_msg(smsg, LoRaMacRxPayload, frameLen);
		set_msg_sig(smsg, LORAWAN_DEVICE_RECV_PROPRIETARY);
		task_post(ID_TASK_LORAWAN_ID, smsg);

		break;
	}

	default:

		break;
	}

	change_to_rx();

}

static void OnRadioTxTimeout( void )
{
	SYS_DBG("OnRadioTxTimeout\n");

	change_to_rx();
}

static void OnRadioRxError( void )
{
	SYS_DBG("OnRadioRxError\n");

	change_to_rx();
}

static void OnRadioRxTimeout( void )
{
	SYS_DBG("OnRadioRxTimeout\n");

	change_to_rx();
}


static void ProcessMacCommands( uint8_t *payload, uint8_t macIndex, uint8_t commandsSize, uint8_t snr )
{
#if 0
	while( macIndex < commandsSize )
	{
		// Decode Frame MAC commands
		switch( payload[macIndex++] )
		{
		case SRV_MAC_LINK_CHECK_ANS:
			MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_OK;
			MlmeConfirm.DemodMargin = payload[macIndex++];
			MlmeConfirm.NbGateways = payload[macIndex++];
			break;
		case SRV_MAC_LINK_ADR_REQ:
		{
			uint8_t i;
			uint8_t status = 0x07;
			uint16_t chMask;
			int8_t txPower = 0;
			int8_t datarate = 0;
			uint8_t nbRep = 0;
			uint8_t chMaskCntl = 0;
			uint16_t channelsMask[6] = { 0, 0, 0, 0, 0, 0 };

			// Initialize local copy of the channels mask array
			for( i = 0; i < 6; i++ )
			{
				channelsMask[i] = LoRaMacParams.ChannelsMask[i];
			}
			datarate = payload[macIndex++];
			txPower = datarate & 0x0F;
			datarate = ( datarate >> 4 ) & 0x0F;

			if( ( AdrCtrlOn == false ) &&
					( ( LoRaMacParams.ChannelsDatarate != datarate ) || ( LoRaMacParams.ChannelsTxPower != txPower ) ) )
			{ // ADR disabled don't handle ADR requests if server tries to change datarate or txpower
				// Answer the server with fail status
				// Power ACK     = 0
				// Data rate ACK = 0
				// Channel mask  = 0
				AddMacCommand( MOTE_MAC_LINK_ADR_ANS, 0, 0 );
				macIndex += 3;  // Skip over the remaining bytes of the request
				break;
			}
			chMask = ( uint16_t )payload[macIndex++];
			chMask |= ( uint16_t )payload[macIndex++] << 8;

			nbRep = payload[macIndex++];
			chMaskCntl = ( nbRep >> 4 ) & 0x07;
			nbRep &= 0x0F;
			if( nbRep == 0 )
			{
				nbRep = 1;
			}
#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
			if( ( chMaskCntl == 0 ) && ( chMask == 0 ) )
			{
				status &= 0xFE; // Channel mask KO
			}
			else if( ( ( chMaskCntl >= 1 ) && ( chMaskCntl <= 5 )) ||
					 ( chMaskCntl >= 7 ) )
			{
				// RFU
				status &= 0xFE; // Channel mask KO
			}
			else
			{
				for( i = 0; i < LORA_MAX_NB_CHANNELS; i++ )
				{
					if( chMaskCntl == 6 )
					{
						if( Channels[i].Frequency != 0 )
						{
							chMask |= 1 << i;
						}
					}
					else
					{
						if( ( ( chMask & ( 1 << i ) ) != 0 ) &&
								( Channels[i].Frequency == 0 ) )
						{// Trying to enable an undefined channel
							status &= 0xFE; // Channel mask KO
						}
					}
				}
				channelsMask[0] = chMask;
			}
#elif defined( USE_BAND_470 )

#endif
			if( ValidateDatarate( datarate, channelsMask ) == false )
			{
				status &= 0xFD; // Datarate KO
			}

			//
			// Remark MaxTxPower = 0 and MinTxPower = 5
			//
			if( ValueInRange( txPower, LORAMAC_MAX_TX_POWER, LORAMAC_MIN_TX_POWER ) == false )
			{
				status &= 0xFB; // TxPower KO
			}
			if( ( status & 0x07 ) == 0x07 )
			{
				LoRaMacParams.ChannelsDatarate = datarate;
				LoRaMacParams.ChannelsTxPower = txPower;

				mem_cpy( ( uint8_t* )LoRaMacParams.ChannelsMask, ( uint8_t* )channelsMask, sizeof( LoRaMacParams.ChannelsMask ) );

				LoRaMacParams.ChannelsNbRep = nbRep;
#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#endif
			}
			AddMacCommand( MOTE_MAC_LINK_ADR_ANS, status, 0 );
		}
			break;
		case SRV_MAC_DUTY_CYCLE_REQ:
			MaxDCycle = payload[macIndex++];
			AggregatedDCycle = 1 << MaxDCycle;
			AddMacCommand( MOTE_MAC_DUTY_CYCLE_ANS, 0, 0 );
			break;
		case SRV_MAC_RX_PARAM_SETUP_REQ:
		{
			uint8_t status = 0x07;
			int8_t datarate = 0;
			int8_t drOffset = 0;
			uint32_t freq = 0;

			drOffset = ( payload[macIndex] >> 4 ) & 0x07;
			datarate = payload[macIndex] & 0x0F;
			macIndex++;

			freq =  ( uint32_t )payload[macIndex++];
			freq |= ( uint32_t )payload[macIndex++] << 8;
			freq |= ( uint32_t )payload[macIndex++] << 16;
			freq *= 100;

			if( Rx2FreqInRange( freq ) == false )
			{
				status &= 0xFE; // Channel frequency KO
			}

			if( ValueInRange( datarate, LORAMAC_RX_MIN_DATARATE, LORAMAC_RX_MAX_DATARATE ) == false )
			{
				status &= 0xFD; // Datarate KO
			}
#if ( defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID ) )

#endif
			if( ValueInRange( drOffset, LORAMAC_MIN_RX1_DR_OFFSET, LORAMAC_MAX_RX1_DR_OFFSET ) == false )
			{
				status &= 0xFB; // Rx1DrOffset range KO
			}

			if( ( status & 0x07 ) == 0x07 )
			{
				LoRaMacParams.Rx2Channel.Datarate = datarate;
				LoRaMacParams.Rx2Channel.Frequency = freq;
				LoRaMacParams.Rx1DrOffset = drOffset;
			}
			AddMacCommand( MOTE_MAC_RX_PARAM_SETUP_ANS, status, 0 );
		}
			break;
		case SRV_MAC_DEV_STATUS_REQ:
		{
			uint8_t batteryLevel = BAT_LEVEL_NO_MEASURE;
			if( ( LoRaMacCallbacks != NULL ) && ( LoRaMacCallbacks->GetBatteryLevel != NULL ) )
			{
				batteryLevel = LoRaMacCallbacks->GetBatteryLevel( );
			}
			AddMacCommand( MOTE_MAC_DEV_STATUS_ANS, batteryLevel, snr );
			break;
		}
		case SRV_MAC_NEW_CHANNEL_REQ:
		{
			uint8_t status = 0x03;

#if defined( USE_BAND_470 ) || defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#else
			int8_t channelIndex = 0;
			ChannelParams_t chParam;

			channelIndex = payload[macIndex++];
			chParam.Frequency = ( uint32_t )payload[macIndex++];
			chParam.Frequency |= ( uint32_t )payload[macIndex++] << 8;
			chParam.Frequency |= ( uint32_t )payload[macIndex++] << 16;
			chParam.Frequency *= 100;
			chParam.DrRange.Value = payload[macIndex++];

			LoRaMacState |= LORAMAC_TX_CONFIG;
			if( chParam.Frequency == 0 )
			{
				if( channelIndex < 3 )
				{
					status &= 0xFC;
				}
				else
				{
					if( LoRaMacChannelRemove( channelIndex ) != LORAMAC_STATUS_OK )
					{
						status &= 0xFC;
					}
				}
			}
			else
			{
				switch( LoRaMacChannelAdd( channelIndex, chParam ) )
				{
				case LORAMAC_STATUS_OK:
				{
					break;
				}
				case LORAMAC_STATUS_FREQUENCY_INVALID:
				{
					status &= 0xFE;
					break;
				}
				case LORAMAC_STATUS_DATARATE_INVALID:
				{
					status &= 0xFD;
					break;
				}
				case LORAMAC_STATUS_FREQ_AND_DR_INVALID:
				{
					status &= 0xFC;
					break;
				}
				default:
				{
					status &= 0xFC;
					break;
				}
				}
			}
			LoRaMacState &= ~LORAMAC_TX_CONFIG;
#endif
			AddMacCommand( MOTE_MAC_NEW_CHANNEL_ANS, status, 0 );
		}
			break;
		case SRV_MAC_RX_TIMING_SETUP_REQ:
		{
			uint8_t delay = payload[macIndex++] & 0x0F;

			if( delay == 0 )
			{
				delay++;
			}
			LoRaMacParams.ReceiveDelay1 = delay * 1e3;
			LoRaMacParams.ReceiveDelay2 = LoRaMacParams.ReceiveDelay1 + 1e3;
			AddMacCommand( MOTE_MAC_RX_TIMING_SETUP_ANS, 0, 0 );
		}
			break;
		default:
			// Unknown command. ABORT MAC commands processing
			return;
		}
#endif
	}
#if 0
	LoRaMacStatus_t Send( LoRaMacHeader_t *macHdr, uint8_t fPort, void *fBuffer, uint16_t fBufferSize )
	{
		LoRaMacFrameCtrl_t fCtrl;
		LoRaMacStatus_t status = LORAMAC_STATUS_PARAMETER_INVALID;

		fCtrl.Value = 0;
		fCtrl.Bits.FOptsLen      = 0;
		fCtrl.Bits.FPending      = 0;
		fCtrl.Bits.Ack           = false;
		fCtrl.Bits.AdrAckReq     = false;
		fCtrl.Bits.Adr           = AdrCtrlOn;

		// Prepare the frame
		status = PrepareFrame( macHdr, &fCtrl, fPort, fBuffer, fBufferSize );

		// Validate status
		if( status != LORAMAC_STATUS_OK )
		{
			return status;
		}

		// Reset confirm parameters
		McpsConfirm.NbRetries = 0;
		McpsConfirm.AckReceived = false;
		McpsConfirm.UpLinkCounter = UpLinkCounter;

		status = ScheduleTx( );

		return status;
	}

	LoRaMacStatus_t PrepareFrame( LoRaMacHeader_t *macHdr, LoRaMacFrameCtrl_t *fCtrl, uint8_t fPort, void *fBuffer, uint16_t fBufferSize )
	{

		uint16_t i;
		uint8_t pktHeaderLen = 0;
		uint32_t mic = 0;
		const void* payload = fBuffer;
		uint8_t framePort = fPort;

		LoRaMacBufferPktLen = 0;

		NodeAckRequested = false;

		if( fBuffer == NULL )
		{
			fBufferSize = 0;
		}

		LoRaMacTxPayloadLen = fBufferSize;

		LoRaMacBuffer[pktHeaderLen++] = macHdr->Value;

		switch( macHdr->Bits.MType )
		{
		case FRAME_TYPE_JOIN_REQ:
			LoRaMacBufferPktLen = pktHeaderLen;

			memcpyr( LoRaMacBuffer + LoRaMacBufferPktLen, LoRaMacAppEui, 8 );
			LoRaMacBufferPktLen += 8;
			memcpyr( LoRaMacBuffer + LoRaMacBufferPktLen, LoRaMacDevEui, 8 );
			LoRaMacBufferPktLen += 8;

			LoRaMacDevNonce = Radio.Random( );

			LoRaMacBuffer[LoRaMacBufferPktLen++] = LoRaMacDevNonce & 0xFF;
			LoRaMacBuffer[LoRaMacBufferPktLen++] = ( LoRaMacDevNonce >> 8 ) & 0xFF;

			LoRaMacJoinComputeMic( LoRaMacBuffer, LoRaMacBufferPktLen & 0xFF, LoRaMacAppKey, &mic );

			LoRaMacBuffer[LoRaMacBufferPktLen++] = mic & 0xFF;
			LoRaMacBuffer[LoRaMacBufferPktLen++] = ( mic >> 8 ) & 0xFF;
			LoRaMacBuffer[LoRaMacBufferPktLen++] = ( mic >> 16 ) & 0xFF;
			LoRaMacBuffer[LoRaMacBufferPktLen++] = ( mic >> 24 ) & 0xFF;

			break;
		case FRAME_TYPE_DATA_CONFIRMED_UP:
			NodeAckRequested = true;
			//Intentional fallthrough
		case FRAME_TYPE_DATA_UNCONFIRMED_UP:
			if( IsLoRaMacNetworkJoined == false )
			{
				return LORAMAC_STATUS_NO_NETWORK_JOINED; // No network has been joined yet
			}

			fCtrl->Bits.AdrAckReq = AdrNextDr( fCtrl->Bits.Adr, true, &LoRaMacParams.ChannelsDatarate );

			if( SrvAckRequested == true )
			{
				SrvAckRequested = false;
				fCtrl->Bits.Ack = 1;
			}

			LoRaMacBuffer[pktHeaderLen++] = ( LoRaMacDevAddr ) & 0xFF;
			LoRaMacBuffer[pktHeaderLen++] = ( LoRaMacDevAddr >> 8 ) & 0xFF;
			LoRaMacBuffer[pktHeaderLen++] = ( LoRaMacDevAddr >> 16 ) & 0xFF;
			LoRaMacBuffer[pktHeaderLen++] = ( LoRaMacDevAddr >> 24 ) & 0xFF;

			LoRaMacBuffer[pktHeaderLen++] = fCtrl->Value;

			LoRaMacBuffer[pktHeaderLen++] = UpLinkCounter & 0xFF;
			LoRaMacBuffer[pktHeaderLen++] = ( UpLinkCounter >> 8 ) & 0xFF;

			// Copy the MAC commands which must be re-send into the MAC command buffer
			memcpy( &MacCommandsBuffer[MacCommandsBufferIndex], MacCommandsBufferToRepeat, MacCommandsBufferToRepeatIndex );
			MacCommandsBufferIndex += MacCommandsBufferToRepeatIndex;

			if( ( payload != NULL ) && ( LoRaMacTxPayloadLen > 0 ) )
			{
				if( ( MacCommandsBufferIndex <= LORA_MAC_COMMAND_MAX_LENGTH ) && ( MacCommandsInNextTx == true ) )
				{
					fCtrl->Bits.FOptsLen += MacCommandsBufferIndex;

					// Update FCtrl field with new value of OptionsLength
					LoRaMacBuffer[0x05] = fCtrl->Value;
					for( i = 0; i < MacCommandsBufferIndex; i++ )
					{
						LoRaMacBuffer[pktHeaderLen++] = MacCommandsBuffer[i];
					}
				}
			}
			else
			{
				if( ( MacCommandsBufferIndex > 0 ) && ( MacCommandsInNextTx ) )
				{
					LoRaMacTxPayloadLen = MacCommandsBufferIndex;
					payload = MacCommandsBuffer;
					framePort = 0;
				}
			}
			MacCommandsInNextTx = false;
			// Store MAC commands which must be re-send in case the device does not receive a downlink anymore
			MacCommandsBufferToRepeatIndex = ParseMacCommandsToRepeat( MacCommandsBuffer, MacCommandsBufferIndex, MacCommandsBufferToRepeat );
			if( MacCommandsBufferToRepeatIndex > 0 )
			{
				MacCommandsInNextTx = true;
			}

			if( ( payload != NULL ) && ( LoRaMacTxPayloadLen > 0 ) )
			{
				LoRaMacBuffer[pktHeaderLen++] = framePort;

				if( framePort == 0 )
				{
					LoRaMacPayloadEncrypt( (uint8_t* ) payload, LoRaMacTxPayloadLen, LoRaMacNwkSKey, LoRaMacDevAddr, UP_LINK, UpLinkCounter, &LoRaMacBuffer[pktHeaderLen] );
				}
				else
				{
					LoRaMacPayloadEncrypt( (uint8_t* ) payload, LoRaMacTxPayloadLen, LoRaMacAppSKey, LoRaMacDevAddr, UP_LINK, UpLinkCounter, &LoRaMacBuffer[pktHeaderLen] );
				}
			}
			LoRaMacBufferPktLen = pktHeaderLen + LoRaMacTxPayloadLen;

			LoRaMacComputeMic( LoRaMacBuffer, LoRaMacBufferPktLen, LoRaMacNwkSKey, LoRaMacDevAddr, UP_LINK, UpLinkCounter, &mic );

			LoRaMacBuffer[LoRaMacBufferPktLen + 0] = mic & 0xFF;
			LoRaMacBuffer[LoRaMacBufferPktLen + 1] = ( mic >> 8 ) & 0xFF;
			LoRaMacBuffer[LoRaMacBufferPktLen + 2] = ( mic >> 16 ) & 0xFF;
			LoRaMacBuffer[LoRaMacBufferPktLen + 3] = ( mic >> 24 ) & 0xFF;

			LoRaMacBufferPktLen += LORAMAC_MFR_LEN;

			break;
		case FRAME_TYPE_PROPRIETARY:
			if( ( fBuffer != NULL ) && ( LoRaMacTxPayloadLen > 0 ) )
			{
				memcpy( LoRaMacBuffer + pktHeaderLen, ( uint8_t* ) fBuffer, LoRaMacTxPayloadLen );
				LoRaMacBufferPktLen = pktHeaderLen + LoRaMacTxPayloadLen;
			}
			break;
		default:
			return LORAMAC_STATUS_SERVICE_UNKNOWN;
		}

		return LORAMAC_STATUS_OK;

	}

	static LoRaMacStatus_t ScheduleTx( void )
	{
		SYS_DBG("ScheduleTx\n");

		uint32_t dutyCycleTimeOff = 0;

		// Check if the device is off
		if( MaxDCycle == 255 )
		{
			return LORAMAC_STATUS_DEVICE_OFF;
		}
		if( MaxDCycle == 0 )
		{
			AggregatedTimeOff = 0;
		}

		// Select channel
		while( SetNextChannel( &dutyCycleTimeOff ) == false )
		{
			// Set the default datarate
			LoRaMacParams.ChannelsDatarate = LoRaMacParamsDefaults.ChannelsDatarate;

#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
			// Re-enable default channels LC1, LC2, LC3
			LoRaMacParams.ChannelsMask[0] = LoRaMacParams.ChannelsMask[0] | ( LC( 1 ) + LC( 2 ) + LC( 3 ) );
#endif
		}

		//SYS_DBG("ScheduleTx\n");

		// Compute Rx1 windows parameters
		RxWindowsParams[0] = ComputeRxWindowParameters( MAX( DR_0, LoRaMacParams.ChannelsDatarate - LoRaMacParams.Rx1DrOffset ), LoRaMacParams.SystemMaxRxError );

		// Compute Rx2 windows parameters
		RxWindowsParams[1] = ComputeRxWindowParameters( LoRaMacParams.Rx2Channel.Datarate, LoRaMacParams.SystemMaxRxError );

		if( IsLoRaMacNetworkJoined == false )
		{
			RxWindow1Delay = LoRaMacParams.JoinAcceptDelay1 + RxWindowsParams[0].RxOffset;
			RxWindow2Delay = LoRaMacParams.JoinAcceptDelay2 + RxWindowsParams[1].RxOffset;
		}
		else
		{
			if( ValidatePayloadLength( LoRaMacTxPayloadLen, LoRaMacParams.ChannelsDatarate, MacCommandsBufferIndex ) == false )
			{
				return LORAMAC_STATUS_LENGTH_ERROR;
			}
			RxWindow1Delay = LoRaMacParams.ReceiveDelay1 + RxWindowsParams[0].RxOffset;
			RxWindow2Delay = LoRaMacParams.ReceiveDelay2 + RxWindowsParams[1].RxOffset;
		}

		// Schedule transmission of frame
		if( dutyCycleTimeOff == 0 )
		{
			// Try to send now
			return SendFrameOnChannel( Channels[Channel] );
		}
		else
		{
			// Send later - prepare timer
			LoRaMacState |= LORAMAC_TX_DELAYED;

			timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_TXDELAYED_TIMER, dutyCycleTimeOff, TIMER_PERIODIC);

			return LORAMAC_STATUS_OK;
		}

	}

	LoRaMacStatus_t SendFrameOnChannel( ChannelParams_t channel )
	{
		int8_t datarate = Datarates[LoRaMacParams.ChannelsDatarate];
		int8_t txPowerIndex = 0;
		int8_t txPower = 0;

		//txPowerIndex = LimitTxPower( LoRaMacParams.ChannelsTxPower, Bands[channel.Band].TxMaxPower );
		txPower = TxPowers[txPowerIndex];

		MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;
		McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;
		McpsConfirm.Datarate = LoRaMacParams.ChannelsDatarate;
		McpsConfirm.TxPower = txPowerIndex;
		McpsConfirm.UpLinkFrequency = channel.Frequency;

		//SYS_DBG("SendFrameOnChannel-freq\n");
		Radio.SetChannel( channel.Frequency );

#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
		if( LoRaMacParams.ChannelsDatarate == DR_7 )
		{ // High Speed FSK channel
			Radio.SetMaxPayloadLength( MODEM_FSK, LoRaMacBufferPktLen );
			Radio.SetTxConfig( MODEM_FSK, txPower, 25e3, 0, datarate * 1e3, 0, 5, false, true, 0, 0, false, 3e3 );
			TxTimeOnAir = Radio.TimeOnAir( MODEM_FSK, LoRaMacBufferPktLen );

		}
		else if( LoRaMacParams.ChannelsDatarate == DR_6 )
		{ // High speed LoRa channel
			Radio.SetMaxPayloadLength( MODEM_LORA, LoRaMacBufferPktLen );
			Radio.SetTxConfig( MODEM_LORA, txPower, 0, 1, datarate, 1, 8, false, true, 0, 0, LORA_IQ_INVERSION_ON_TX, 3e3 );
			TxTimeOnAir = Radio.TimeOnAir( MODEM_LORA, LoRaMacBufferPktLen );
		}
		else
		{ // Normal LoRa channel
			Radio.SetMaxPayloadLength( MODEM_LORA, LoRaMacBufferPktLen );
			Radio.SetTxConfig( MODEM_LORA, txPower, 0, 0, datarate, 1, 8, false, true, 0, 0, LORA_IQ_INVERSION_ON_TX, 3e3 );
			TxTimeOnAir = Radio.TimeOnAir( MODEM_LORA, LoRaMacBufferPktLen );
		}
#elif defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#endif

		// Store the time on air
		McpsConfirm.TxTimeOnAir = TxTimeOnAir;
		MlmeConfirm.TxTimeOnAir = TxTimeOnAir;

		// Starts the MAC layer status check timer

		//timer_set(MT_TASK_LORAMAC_ID, LORAMAC_MAC_STATE_CHECK_TIMER, MAC_STATE_CHECK_TIMEOUT, TIMER_PERIODIC);

		if( IsLoRaMacNetworkJoined == false )
		{
			JoinRequestTrials++;
		}

		// Send now
		Radio.Send( LoRaMacBuffer, LoRaMacBufferPktLen );

		LoRaMacState |= LORAMAC_TX_RUNNING;

		return LORAMAC_STATUS_OK;
	}


	LoRaMacStatus_t LoRaMacInitialization( LoRaMacPrimitives_t *primitives, LoRaMacCallback_t *callbacks )
	{
		SYS_DBG("LoRaMacInitialization\n");

		if( primitives == NULL )
		{
			return LORAMAC_STATUS_PARAMETER_INVALID;
		}

		if( ( primitives->MacMcpsConfirm == NULL ) ||
				( primitives->MacMcpsIndication == NULL ) ||
				( primitives->MacMlmeConfirm == NULL ) )
		{
			return LORAMAC_STATUS_PARAMETER_INVALID;
		}

		LoRaMacPrimitives = primitives;
		LoRaMacCallbacks = callbacks;

		LoRaMacFlags.Value = 0;

		LoRaMacDeviceClass = CLASS_A;
		LoRaMacState = LORAMAC_IDLE;

		JoinRequestTrials = 0;
		MaxJoinRequestTrials = 1;
		RepeaterSupport = false;

		// Reset duty cycle times
		AggregatedLastTxDoneTime = 0;
		AggregatedTimeOff = 0;

		// Duty cycle
#if defined( USE_BAND_433 )
		DutyCycleOn = true;
#endif

		// Reset to defaults
		LoRaMacParamsDefaults.ChannelsTxPower = LORAMAC_DEFAULT_TX_POWER;
		LoRaMacParamsDefaults.ChannelsDatarate = LORAMAC_DEFAULT_DATARATE;

		LoRaMacParamsDefaults.SystemMaxRxError = 10;
		LoRaMacParamsDefaults.MinRxSymbols = 6;
		LoRaMacParamsDefaults.MaxRxWindow = MAX_RX_WINDOW;
		LoRaMacParamsDefaults.ReceiveDelay1 = RECEIVE_DELAY1;
		LoRaMacParamsDefaults.ReceiveDelay2 = RECEIVE_DELAY2;
		LoRaMacParamsDefaults.JoinAcceptDelay1 = JOIN_ACCEPT_DELAY1;
		LoRaMacParamsDefaults.JoinAcceptDelay2 = JOIN_ACCEPT_DELAY2;

		LoRaMacParamsDefaults.ChannelsNbRep = 1;
		LoRaMacParamsDefaults.Rx1DrOffset = 0;

		LoRaMacParamsDefaults.Rx2Channel = ( Rx2ChannelParams_t )RX_WND_2_CHANNEL;



#if defined( USE_BAND_433 )
		LoRaMacParamsDefaults.ChannelsMask[0] = LC( 1 ) + LC( 2 ) + LC( 3 );
#endif

		// Init parameters which are not set in function ResetMacParameters
		LoRaMacParams.SystemMaxRxError = LoRaMacParamsDefaults.SystemMaxRxError;
		LoRaMacParams.MinRxSymbols = LoRaMacParamsDefaults.MinRxSymbols;
		LoRaMacParams.MaxRxWindow = LoRaMacParamsDefaults.MaxRxWindow;
		LoRaMacParams.ReceiveDelay1 = LoRaMacParamsDefaults.ReceiveDelay1;
		LoRaMacParams.ReceiveDelay2 = LoRaMacParamsDefaults.ReceiveDelay2;
		LoRaMacParams.JoinAcceptDelay1 = LoRaMacParamsDefaults.JoinAcceptDelay1;
		LoRaMacParams.JoinAcceptDelay2 = LoRaMacParamsDefaults.JoinAcceptDelay2;
		LoRaMacParams.ChannelsNbRep = LoRaMacParamsDefaults.ChannelsNbRep;

		ResetMacParameters( );

		// Initialize timers
		//TimerInit( &MacStateCheckTimer, OnMacStateCheckTimerEvent );
		//TimerSetValue( &MacStateCheckTimer, MAC_STATE_CHECK_TIMEOUT );

		//TimerInit( &TxDelayedTimer, OnTxDelayedTimerEvent );
		//TimerInit( &RxWindowTimer1, OnRxWindow1TimerEvent );
		//TimerInit( &RxWindowTimer2, OnRxWindow2TimerEvent );
		//TimerInit( &AckTimeoutTimer, OnAckTimeoutTimerEvent );

		// Store the current initialization time
		LoRaMacInitializationTime = TimerGetCurrentTime( );
		// Initialize Radio driver
		RadioEvents.TxDone = OnRadioTxDone;
		RadioEvents.RxDone = OnRadioRxDone;
		RadioEvents.RxError = OnRadioRxError;
		RadioEvents.TxTimeout = OnRadioTxTimeout;
		RadioEvents.RxTimeout = OnRadioRxTimeout;
		Radio.Init( &RadioEvents );

		// Random seed initialization
		//srand1( Radio.Random( ) );

		PublicNetwork = true;
		Radio.SetPublicNetwork( PublicNetwork );
		Radio.Sleep( );

		return LORAMAC_STATUS_OK;
	}

	LoRaMacStatus_t LoRaMacMibGetRequestConfirm( MibRequestConfirm_t *mibGet )
	{
		LoRaMacStatus_t status = LORAMAC_STATUS_OK;

		if( mibGet == NULL )
		{
			return LORAMAC_STATUS_PARAMETER_INVALID;
		}

		switch( mibGet->Type )
		{
		case MIB_DEVICE_CLASS:
		{
			mibGet->Param.Class = LoRaMacDeviceClass;
			break;
		}
		case MIB_NETWORK_JOINED:
		{
			mibGet->Param.IsNetworkJoined = IsLoRaMacNetworkJoined;
			break;
		}
		case MIB_ADR:
		{
			mibGet->Param.AdrEnable = AdrCtrlOn;
			break;
		}
		case MIB_NET_ID:
		{
			mibGet->Param.NetID = LoRaMacNetID;
			break;
		}
		case MIB_DEV_ADDR:
		{
			mibGet->Param.DevAddr = LoRaMacDevAddr;
			break;
		}
		case MIB_NWK_SKEY:
		{
			mibGet->Param.NwkSKey = LoRaMacNwkSKey;
			break;
		}
		case MIB_APP_SKEY:
		{
			mibGet->Param.AppSKey = LoRaMacAppSKey;
			break;
		}
		case MIB_PUBLIC_NETWORK:
		{
			mibGet->Param.EnablePublicNetwork = PublicNetwork;
			break;
		}
		case MIB_REPEATER_SUPPORT:
		{
			mibGet->Param.EnableRepeaterSupport = RepeaterSupport;
			break;
		}
		case MIB_CHANNELS:
		{
			mibGet->Param.ChannelList = Channels;
			break;
		}
		case MIB_RX2_CHANNEL:
		{
			mibGet->Param.Rx2Channel = LoRaMacParams.Rx2Channel;
			break;
		}
		case MIB_RX2_DEFAULT_CHANNEL:
		{
			mibGet->Param.Rx2Channel = LoRaMacParamsDefaults.Rx2Channel;
			break;
		}
		case MIB_CHANNELS_DEFAULT_MASK:
		{
			mibGet->Param.ChannelsDefaultMask = LoRaMacParamsDefaults.ChannelsMask;
			break;
		}
		case MIB_CHANNELS_MASK:
		{
			mibGet->Param.ChannelsMask = LoRaMacParams.ChannelsMask;
			break;
		}
		case MIB_CHANNELS_NB_REP:
		{
			mibGet->Param.ChannelNbRep = LoRaMacParams.ChannelsNbRep;
			break;
		}
		case MIB_MAX_RX_WINDOW_DURATION:
		{
			mibGet->Param.MaxRxWindow = LoRaMacParams.MaxRxWindow;
			break;
		}
		case MIB_RECEIVE_DELAY_1:
		{
			mibGet->Param.ReceiveDelay1 = LoRaMacParams.ReceiveDelay1;
			break;
		}
		case MIB_RECEIVE_DELAY_2:
		{
			mibGet->Param.ReceiveDelay2 = LoRaMacParams.ReceiveDelay2;
			break;
		}
		case MIB_JOIN_ACCEPT_DELAY_1:
		{
			mibGet->Param.JoinAcceptDelay1 = LoRaMacParams.JoinAcceptDelay1;
			break;
		}
		case MIB_JOIN_ACCEPT_DELAY_2:
		{
			mibGet->Param.JoinAcceptDelay2 = LoRaMacParams.JoinAcceptDelay2;
			break;
		}
		case MIB_CHANNELS_DEFAULT_DATARATE:
		{
			mibGet->Param.ChannelsDefaultDatarate = LoRaMacParamsDefaults.ChannelsDatarate;
			break;
		}
		case MIB_CHANNELS_DATARATE:
		{
			mibGet->Param.ChannelsDatarate = LoRaMacParams.ChannelsDatarate;
			break;
		}
		case MIB_CHANNELS_DEFAULT_TX_POWER:
		{
			mibGet->Param.ChannelsDefaultTxPower = LoRaMacParamsDefaults.ChannelsTxPower;
			break;
		}
		case MIB_CHANNELS_TX_POWER:
		{
			mibGet->Param.ChannelsTxPower = LoRaMacParams.ChannelsTxPower;
			break;
		}
		case MIB_UPLINK_COUNTER:
		{
			mibGet->Param.UpLinkCounter = UpLinkCounter;
			break;
		}
		case MIB_DOWNLINK_COUNTER:
		{
			mibGet->Param.DownLinkCounter = DownLinkCounter;
			break;
		}
		case MIB_MULTICAST_CHANNEL:
		{
			mibGet->Param.MulticastList = MulticastChannels;
			break;
		}
		case MIB_SYSTEM_MAX_RX_ERROR:
		{
			mibGet->Param.SystemMaxRxError = LoRaMacParams.SystemMaxRxError;
			break;
		}
		case MIB_MIN_RX_SYMBOLS:
		{
			mibGet->Param.MinRxSymbols = LoRaMacParams.MinRxSymbols;
			break;
		}
		default:
			status = LORAMAC_STATUS_SERVICE_UNKNOWN;
			break;
		}

		return status;
	}

	LoRaMacStatus_t LoRaMacMibSetRequestConfirm( MibRequestConfirm_t *mibSet )
	{
		LoRaMacStatus_t status = LORAMAC_STATUS_OK;

		if( mibSet == NULL )
		{
			return LORAMAC_STATUS_PARAMETER_INVALID;
		}
		if( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING )
		{
			return LORAMAC_STATUS_BUSY;
		}

		switch( mibSet->Type )
		{
		case MIB_DEVICE_CLASS:
		{
			LoRaMacDeviceClass = mibSet->Param.Class;
			switch( LoRaMacDeviceClass )
			{
			case CLASS_A:
			{
				// Set the radio into sleep to setup a defined state
				Radio.Sleep( );
				break;
			}
			case CLASS_B:
			{
				break;
			}
			case CLASS_C:
			{
				// Set the NodeAckRequested indicator to default
				NodeAckRequested = false;
				//OnRxWindow2TimerEvent( );
				break;
			}
			}
			break;
		}
		case MIB_NETWORK_JOINED:
		{
			IsLoRaMacNetworkJoined = mibSet->Param.IsNetworkJoined;
			break;
		}
		case MIB_ADR:
		{
			AdrCtrlOn = mibSet->Param.AdrEnable;
			break;
		}
		case MIB_NET_ID:
		{
			LoRaMacNetID = mibSet->Param.NetID;
			break;
		}
		case MIB_DEV_ADDR:
		{
			LoRaMacDevAddr = mibSet->Param.DevAddr;
			break;
		}
		case MIB_NWK_SKEY:
		{
			if( mibSet->Param.NwkSKey != NULL )
			{
				memcpy( LoRaMacNwkSKey, mibSet->Param.NwkSKey,
						sizeof( LoRaMacNwkSKey ) );
			}
			else
			{
				status = LORAMAC_STATUS_PARAMETER_INVALID;
			}
			break;
		}
		case MIB_APP_SKEY:
		{
			if( mibSet->Param.AppSKey != NULL )
			{
				memcpy( LoRaMacAppSKey, mibSet->Param.AppSKey,
						sizeof( LoRaMacAppSKey ) );
			}
			else
			{
				status = LORAMAC_STATUS_PARAMETER_INVALID;
			}
			break;
		}
		case MIB_PUBLIC_NETWORK:
		{
			PublicNetwork = mibSet->Param.EnablePublicNetwork;
			Radio.SetPublicNetwork( PublicNetwork );
			break;
		}
		case MIB_REPEATER_SUPPORT:
		{
			RepeaterSupport = mibSet->Param.EnableRepeaterSupport;
			break;
		}
		case MIB_RX2_CHANNEL:
		{
			LoRaMacParams.Rx2Channel = mibSet->Param.Rx2Channel;
			break;
		}
		case MIB_RX2_DEFAULT_CHANNEL:
		{
			LoRaMacParamsDefaults.Rx2Channel = mibSet->Param.Rx2DefaultChannel;
			break;
		}
		case MIB_CHANNELS_DEFAULT_MASK:
		{
			if( mibSet->Param.ChannelsDefaultMask )
			{
#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#else
				memcpy( ( uint8_t* ) LoRaMacParamsDefaults.ChannelsMask,
						( uint8_t* ) mibSet->Param.ChannelsDefaultMask, 2 );
#endif
			}
			else
			{
				status = LORAMAC_STATUS_PARAMETER_INVALID;
			}
			break;
		}
		case MIB_CHANNELS_MASK:
		{
			if( mibSet->Param.ChannelsMask )
			{
#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )
#else
				memcpy( ( uint8_t* ) LoRaMacParams.ChannelsMask,
						( uint8_t* ) mibSet->Param.ChannelsMask, 2 );
#endif
			}
			else
			{
				status = LORAMAC_STATUS_PARAMETER_INVALID;
			}
			break;
		}
		case MIB_CHANNELS_NB_REP:
		{
			if( ( mibSet->Param.ChannelNbRep >= 1 ) &&
					( mibSet->Param.ChannelNbRep <= 15 ) )
			{
				LoRaMacParams.ChannelsNbRep = mibSet->Param.ChannelNbRep;
			}
			else
			{
				status = LORAMAC_STATUS_PARAMETER_INVALID;
			}
			break;
		}
		case MIB_MAX_RX_WINDOW_DURATION:
		{
			LoRaMacParams.MaxRxWindow = mibSet->Param.MaxRxWindow;
			break;
		}
		case MIB_RECEIVE_DELAY_1:
		{
			LoRaMacParams.ReceiveDelay1 = mibSet->Param.ReceiveDelay1;
			break;
		}
		case MIB_RECEIVE_DELAY_2:
		{
			LoRaMacParams.ReceiveDelay2 = mibSet->Param.ReceiveDelay2;
			break;
		}
		case MIB_JOIN_ACCEPT_DELAY_1:
		{
			LoRaMacParams.JoinAcceptDelay1 = mibSet->Param.JoinAcceptDelay1;
			break;
		}
		case MIB_JOIN_ACCEPT_DELAY_2:
		{
			LoRaMacParams.JoinAcceptDelay2 = mibSet->Param.JoinAcceptDelay2;
			break;
		}
		case MIB_CHANNELS_DEFAULT_DATARATE:
		{
			if( ValueInRange( mibSet->Param.ChannelsDefaultDatarate,
							  DR_0, DR_5 ) )
			{
				LoRaMacParamsDefaults.ChannelsDatarate = mibSet->Param.ChannelsDefaultDatarate;
			}
			{
				status = LORAMAC_STATUS_PARAMETER_INVALID;
			}
			break;
		}
		case MIB_CHANNELS_DATARATE:
		{
			if( ValueInRange( mibSet->Param.ChannelsDatarate,
							  LORAMAC_TX_MIN_DATARATE, LORAMAC_TX_MAX_DATARATE ) )
			{
				LoRaMacParams.ChannelsDatarate = mibSet->Param.ChannelsDatarate;
			}
			else
			{
				status = LORAMAC_STATUS_PARAMETER_INVALID;
			}
			break;
		}
		case MIB_CHANNELS_DEFAULT_TX_POWER:
		{
			if( ValueInRange( mibSet->Param.ChannelsDefaultTxPower,
							  LORAMAC_MAX_TX_POWER, LORAMAC_MIN_TX_POWER ) )
			{
				LoRaMacParamsDefaults.ChannelsTxPower = mibSet->Param.ChannelsDefaultTxPower;
			}
			else
			{
				status = LORAMAC_STATUS_PARAMETER_INVALID;
			}
			break;
		}
		case MIB_CHANNELS_TX_POWER:
		{
			if( ValueInRange( mibSet->Param.ChannelsTxPower,
							  LORAMAC_MAX_TX_POWER, LORAMAC_MIN_TX_POWER ) )
			{
				LoRaMacParams.ChannelsTxPower = mibSet->Param.ChannelsTxPower;
			}
			else
			{
				status = LORAMAC_STATUS_PARAMETER_INVALID;
			}
			break;
		}
		case MIB_UPLINK_COUNTER:
		{
			UpLinkCounter = mibSet->Param.UpLinkCounter;
			break;
		}
		case MIB_DOWNLINK_COUNTER:
		{
			DownLinkCounter = mibSet->Param.DownLinkCounter;
			break;
		}
		case MIB_SYSTEM_MAX_RX_ERROR:
		{
			LoRaMacParams.SystemMaxRxError = LoRaMacParamsDefaults.SystemMaxRxError = mibSet->Param.SystemMaxRxError;
			break;
		}
		case MIB_MIN_RX_SYMBOLS:
		{
			LoRaMacParams.MinRxSymbols = LoRaMacParamsDefaults.MinRxSymbols = mibSet->Param.MinRxSymbols;
			break;
		}
		default:
			status = LORAMAC_STATUS_SERVICE_UNKNOWN;
			break;
		}

		return status;
	}

	LoRaMacStatus_t LoRaMacMlmeRequest( MlmeReq_t *mlmeRequest )
	{
		LoRaMacStatus_t status = LORAMAC_STATUS_SERVICE_UNKNOWN;
		LoRaMacHeader_t macHdr;

		if( mlmeRequest == NULL )
		{
			return LORAMAC_STATUS_PARAMETER_INVALID;
		}
		if( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING )
		{
			return LORAMAC_STATUS_BUSY;
		}

		memset( ( uint8_t* ) &MlmeConfirm, 0, sizeof( MlmeReq_t ) );

		MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;

		switch( mlmeRequest->Type )
		{
		case MLME_JOIN:
		{
			if( ( LoRaMacState & LORAMAC_TX_DELAYED ) == LORAMAC_TX_DELAYED )
			{
				return LORAMAC_STATUS_BUSY;
			}

			if( ( mlmeRequest->Req.Join.DevEui == NULL ) ||
					( mlmeRequest->Req.Join.AppEui == NULL ) ||
					( mlmeRequest->Req.Join.AppKey == NULL ) ||
					( mlmeRequest->Req.Join.NbTrials == 0 ) )
			{
				return LORAMAC_STATUS_PARAMETER_INVALID;
			}

#if ( defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID ) )

#else
			// Enables at least the usage of all datarates.
			if( mlmeRequest->Req.Join.NbTrials < 48 )
			{
				mlmeRequest->Req.Join.NbTrials = 48;
			}
#endif

			LoRaMacFlags.Bits.MlmeReq = 1;
			MlmeConfirm.MlmeRequest = mlmeRequest->Type;

			LoRaMacDevEui = mlmeRequest->Req.Join.DevEui;
			LoRaMacAppEui = mlmeRequest->Req.Join.AppEui;
			LoRaMacAppKey = mlmeRequest->Req.Join.AppKey;
			MaxJoinRequestTrials = mlmeRequest->Req.Join.NbTrials;

			// Reset variable JoinRequestTrials
			JoinRequestTrials = 0;

			// Setup header information
			macHdr.Value = 0;
			macHdr.Bits.MType  = FRAME_TYPE_JOIN_REQ;

			ResetMacParameters( );

			// Add a +1, since we start to count from 0
			//SYS_DBG("LoRaMacMlmeRequest.ChannelsDatarate:%d\n",LoRaMacParams.ChannelsDatarate);
			LoRaMacParams.ChannelsDatarate = AlternateDatarate( JoinRequestTrials + 1 );

			//SYS_DBG("LoRaMacMlmeRequest.ChannelsDatarate:%d\n",LoRaMacParams.ChannelsDatarate);
			status = Send( &macHdr, 0, NULL, 0 );
			break;
		}
		case MLME_LINK_CHECK:
		{
			LoRaMacFlags.Bits.MlmeReq = 1;
			// LoRaMac will send this command piggy-pack
			MlmeConfirm.MlmeRequest = mlmeRequest->Type;

			status = AddMacCommand( MOTE_MAC_LINK_CHECK_REQ, 0, 0 );
			break;
		}
		case MLME_TXCW:
		{
			MlmeConfirm.MlmeRequest = mlmeRequest->Type;
			LoRaMacFlags.Bits.MlmeReq = 1;
			status = SetTxContinuousWave( mlmeRequest->Req.TxCw.Timeout );
			break;
		}
		case MLME_TXCW_1:
		{
			MlmeConfirm.MlmeRequest = mlmeRequest->Type;
			LoRaMacFlags.Bits.MlmeReq = 1;
			status = SetTxContinuousWave1( mlmeRequest->Req.TxCw.Timeout, mlmeRequest->Req.TxCw.Frequency, mlmeRequest->Req.TxCw.Power );
			break;
		}
		default:
			break;
		}

		if( status != LORAMAC_STATUS_OK )
		{
			NodeAckRequested = false;
			LoRaMacFlags.Bits.MlmeReq = 0;
		}

		return status;
	}

	LoRaMacStatus_t LoRaMacMcpsRequest( McpsReq_t *mcpsRequest )
	{
		LoRaMacStatus_t status = LORAMAC_STATUS_SERVICE_UNKNOWN;
		LoRaMacHeader_t macHdr;
		uint8_t fPort = 0;
		void *fBuffer;
		uint16_t fBufferSize;
		int8_t datarate;
		bool readyToSend = false;

		if( mcpsRequest == NULL )
		{
			return LORAMAC_STATUS_PARAMETER_INVALID;
		}
		if( ( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING ) ||
				( ( LoRaMacState & LORAMAC_TX_DELAYED ) == LORAMAC_TX_DELAYED ) )
		{
			return LORAMAC_STATUS_BUSY;
		}

		macHdr.Value = 0;
		memset ( ( uint8_t* ) &McpsConfirm, 0, sizeof( McpsConfirm ) );
		McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;

		switch( mcpsRequest->Type )
		{
		case MCPS_UNCONFIRMED:
		{

			//SYS_DBG("\nMCPS_UNCONFIRMED\n");

			readyToSend = true;
			AckTimeoutRetries = 1;

			macHdr.Bits.MType = FRAME_TYPE_DATA_UNCONFIRMED_UP;
			fPort = mcpsRequest->Req.Unconfirmed.fPort;
			fBuffer = mcpsRequest->Req.Unconfirmed.fBuffer;
			fBufferSize = mcpsRequest->Req.Unconfirmed.fBufferSize;
			datarate = mcpsRequest->Req.Unconfirmed.Datarate;
			break;
		}
		case MCPS_CONFIRMED:
		{
			readyToSend = true;
			AckTimeoutRetriesCounter = 1;
			AckTimeoutRetries = mcpsRequest->Req.Confirmed.NbTrials;

			macHdr.Bits.MType = FRAME_TYPE_DATA_CONFIRMED_UP;
			fPort = mcpsRequest->Req.Confirmed.fPort;
			fBuffer = mcpsRequest->Req.Confirmed.fBuffer;
			fBufferSize = mcpsRequest->Req.Confirmed.fBufferSize;
			datarate = mcpsRequest->Req.Confirmed.Datarate;
			break;
		}
		case MCPS_PROPRIETARY:
		{
			readyToSend = true;
			AckTimeoutRetries = 1;

			macHdr.Bits.MType = FRAME_TYPE_PROPRIETARY;
			fBuffer = mcpsRequest->Req.Proprietary.fBuffer;
			fBufferSize = mcpsRequest->Req.Proprietary.fBufferSize;
			datarate = mcpsRequest->Req.Proprietary.Datarate;
			break;
		}
		default:
			break;
		}

		if( readyToSend == true )
		{
			if( AdrCtrlOn == false )
			{
				if( ValueInRange( datarate, LORAMAC_TX_MIN_DATARATE, LORAMAC_TX_MAX_DATARATE ) == true )
				{
					LoRaMacParams.ChannelsDatarate = datarate;
				}
				else
				{
					return LORAMAC_STATUS_PARAMETER_INVALID;
				}
			}

			//SYS_DBG("LoRaMacMcpsRequest.ChannelsDatarate:%d\n",LoRaMacParams.ChannelsDatarate);
			status = Send( &macHdr, fPort, fBuffer, fBufferSize );
			if( status == LORAMAC_STATUS_OK )
			{
				McpsConfirm.McpsRequest = mcpsRequest->Type;
				LoRaMacFlags.Bits.McpsReq = 1;
			}
			else
			{
				NodeAckRequested = false;
			}
		}

		return status;
	}
#endif

	uint32_t TimerGetElapsedTime( uint32_t eventInTime )
	{
		uint32_t elapsedTime = 0;

		// Needed at boot, cannot compute with 0 or elapsed time will be equal to current time
		if( eventInTime == 0 )
		{
			return 0;
		}

		elapsedTime = millis();

		if( elapsedTime < eventInTime )
		{ // roll over of the counter
			return( elapsedTime + ( 0xFFFFFFFF - eventInTime ) );
		}
		else
		{
			return( elapsedTime - eventInTime );
		}
	}

	uint32_t TimerGetCurrentTime(void) {
		return millis();
	}


	int32_t randr( int32_t min, int32_t max )
	{
		return ( int32_t )(millis()) % ( max - min + 1 ) + min;
	}

	void memcpyr( uint8_t *dst, const uint8_t *src, uint16_t size )
	{
		dst = dst + ( size - 1 );
		while( size-- )
		{
			*dst-- = *src++;
		}
	}

	void initialiseEpoch (void)
	{
		struct timeval tv ;

		gettimeofday (&tv, NULL) ;
		epochMilli = (uint64_t)tv.tv_sec * (uint64_t)1000    + (uint64_t)(tv.tv_usec / 1000) ;
	}

	unsigned int millis (void)
	{
		struct timeval tv ;
		uint64_t now ;

		gettimeofday (&tv, NULL) ;
		now  = (uint64_t)tv.tv_sec * (uint64_t)1000 + (uint64_t)(tv.tv_usec / 1000) ;

		return (uint32_t)(now - epochMilli) ;
	}

	void loramac_initial( const uint32_t nwkid, const uint8_t* apps, const uint8_t* nwks) {
		APP_DBG("loramac_initial\n");

		/*
		 * init device mng
		 * parser from file device list
		 */
		//memset(device_mng.deviceEui, 0, 8);
		device_mng.deviceAddr = MY_DEVICE_ADDR;
		device_mng.downLinkCnt = 0;
		device_mng.upLinkCnt = 0;
		device_mng.deviceAckReq = false;
		device_mng.gwAckReq = false;

		initialiseEpoch();

		LoRaMacNetID = nwkid;
		memcpy(LoRaMacAppSKey, apps, 16);
		memcpy(LoRaMacNwkSKey, nwks, 16);

		// Initialize Radio driver
		RadioEvents.TxDone = OnRadioTxDone;
		RadioEvents.RxDone = OnRadioRxDone;
		RadioEvents.RxError = OnRadioRxError;
		RadioEvents.TxTimeout = OnRadioTxTimeout;
		RadioEvents.RxTimeout = OnRadioRxTimeout;
		Radio.Init( &RadioEvents );

		Radio.SetChannel( SINGLE_CHANNEL_GW_FREQ );

		Radio.SetPublicNetwork(true);

		Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, \
						   SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF, \
						   SINGLE_CHANNEL_GW_CD, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, \
						   SINGLE_CHANNEL_GW_FIX_LENGTH_PAY, true, 0, 0, \
						   SINGLE_CHANNEL_GW_IQ_TX, 3000 );

		Radio.SetRxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF,
						   SINGLE_CHANNEL_GW_CD, 0, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH,
						   SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
						   0, true, 0, 0, SINGLE_CHANNEL_GW_IQ_RX, true );

		Radio.Rx(SINGLE_CHANNEL_GW_RX_TIMEOUT);
	}

	void send_propriatary(void* buffer, uint8_t len) {

		APP_DBG("send_propriatary\n");

		LoRaMacHeader_t macHdr;
		uint16_t fBufferSize = len;
		uint8_t* fBuffer = (uint8_t*)buffer;

		macHdr.Value = 0;
		macHdr.Bits.MType = FRAME_TYPE_PROPRIETARY;

//		LoRaMacFrameCtrl_t fCtrl;

//		fCtrl.Value = 0;
//		fCtrl.Bits.FOptsLen      = 0;
//		fCtrl.Bits.FPending      = 1;
//		fCtrl.Bits.Ack           = false;
//		fCtrl.Bits.AdrAckReq     = false;
//		fCtrl.Bits.Adr           = 0;

		uint8_t pktHeaderLen = 0;

		LoRaMacBufferPktLen = 0;

		LoRaMacTxPayloadLen = fBufferSize;

		LoRaMacBuffer[pktHeaderLen++] = macHdr.Value;

		memcpy( LoRaMacBuffer + pktHeaderLen, ( uint8_t* ) fBuffer, LoRaMacTxPayloadLen );
		LoRaMacBufferPktLen = pktHeaderLen + LoRaMacTxPayloadLen;

		//Radio.SetMaxPayloadLength( MODEM_LORA, LoRaMacBufferPktLen );
		Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, \
						   SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF, \
						   SINGLE_CHANNEL_GW_CD, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, \
						   SINGLE_CHANNEL_GW_FIX_LENGTH_PAY, true, 0, 0, \
						   SINGLE_CHANNEL_GW_IQ_TX, 3000 );

		// Send now
		Radio.Send( LoRaMacBuffer, LoRaMacBufferPktLen );
	}

	void change_to_rx(void) {
		Radio.SetRxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF,
						   SINGLE_CHANNEL_GW_CD, 0, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH,
						   SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
						   0, true, 0, 0, SINGLE_CHANNEL_GW_IQ_RX, true );
		Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
	}
