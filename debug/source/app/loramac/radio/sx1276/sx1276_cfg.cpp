#include <pthread.h>
#include <stdio.h>
#include <poll.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "../radio.h"
#include "../linux_driver/gpio.h"
#include "../linux_driver/spi.h"

#include "../sys/sys_dbg.h"
#include "../app/app_dbg.h"

#include "sx1276_cfg.h"

char* spi_device;
SPIState *spi;
pthread_t int_thread;
static int isr_fd;

void *lora_isr_thread();
static void ctr_c_event(int);

/*!
 * Flag used to set the RF switch control pins in low power mode when the radio is not active.
 */
static bool RadioIsActive = false;

/*!
 * Radio driver structure initialization
 */
const struct Radio_s Radio =
{
	SX1276Init,
	SX1276GetStatus,
	SX1276SetModem,
	SX1276SetChannel,
	SX1276IsChannelFree,
	SX1276Random,
	SX1276SetRxConfig,
	SX1276SetTxConfig,
	SX1276CheckRfFrequency,
	SX1276GetTimeOnAir,
	SX1276Send,
	SX1276SetSleep,
	SX1276SetStby,
	SX1276SetRx,
	SX1276StartCad,
	SX1276SetTxContinuousWave,
	SX1276ReadRssi,
	SX1276Write,
	SX1276Read,
	SX1276WriteBuffer,
	SX1276ReadBuffer,
	SX1276SetMaxPayloadLength,
	SX1276SetPublicNetwork
};


/***********************/
/* Register functions  */
/***********************/
uint8_t read_register_bytes(uint8_t reg, uint8_t* buf, uint8_t len) {
	uint8_t status;
	spi_enable(spi);
	spi_transfer(spi, reg & 0x7F, &status);
	while (len--) spi_transfer(spi, 0, buf++);
	spi_disable(spi);
	return status;
}

uint8_t read_register(uint8_t reg) {
	uint8_t result;
	spi_enable(spi);
	spi_transfer(spi, reg & 0x7F, NULL);
	spi_transfer(spi, 0, &result);
	spi_disable(spi);
	return result;
}

uint8_t write_register_bytes(uint8_t reg, const uint8_t* buf, uint8_t len) {
	uint8_t status;
	spi_enable(spi);
	spi_transfer(spi, reg | 0x80, &status);
	while (len--) spi_transfer(spi, *buf++, NULL);
	spi_disable(spi);
	return status;
}

uint8_t write_register(uint8_t reg, uint8_t value) {
	uint8_t status;
	spi_enable(spi);
	spi_transfer(spi, reg | 0x80, &status);
	spi_transfer(spi, value, NULL);
	spi_disable(spi);
	return status;
}

int setup_isr_thread(int pin) {
	char gpio_file[GPIO_FILE_MAXLEN];
	gpio_open(pin, GPIO_IN);
	gpio_enable_edge(pin, GPIO_RISING_EDGE);
	memset(gpio_file, 0x00, GPIO_FILE_MAXLEN);
	snprintf(gpio_file, GPIO_FILE_MAXLEN - 1, "/sys/class/gpio/gpio%d/value", pin);
	return open(gpio_file, O_RDWR);
}

void ctr_c_event(int sig) {
	gpio_close(RST_PIN);
	gpio_close(DIO0_PIN);
	gpio_close(DIO1_PIN);
	gpio_close(DIO2_PIN);
	exit(0);
}

int waitForInterrupt (int mS)
{
	int x ;
	uint8_t c ;
	struct pollfd polls ;

	// Setup poll structure
	polls.fd     = isr_fd ;
	polls.events = POLLPRI ;	// Urgent data!

	// Wait for it ...
	x = poll (&polls, 1, mS) ;

	// Do a dummy read to clear the interrupt
	//	A one character read appars to be enough.
	(void)read (isr_fd, &c, 1) ;

	return x ;
}

void *lora_isr_thread(void*) {
	uint8_t c;

	isr_fd = setup_isr_thread(DIO2_PIN);
	if (isr_fd < 0) {
		printf("ISR:gpio_file");
		return (void *)-1;
	}

	signal(SIGINT, ctr_c_event);

	//clear it
	(void)read (isr_fd, &c, 1) ;

	printf("ISR:start\n");

	while(1) {
		if (waitForInterrupt (-1) > 0)
			SX1276OnDio0Irq();
	}

	printf("ISR:return");
	close(isr_fd);
	return (void *)0;
}

/*!
 * Antenna switch GPIO pins objects
 */

void SX1276IoInit( void )
{
	gpio_open(RST_PIN, GPIO_OUT);
	gpio_write(RST_PIN, GPIO_HIGH);

	//connect DIO2 to DIO0
	gpio_open(DIO0_PIN, GPIO_IN);

	spi_device = (char*)SPI_DEVICE;
	spi = spi_init(spi_device, SPI_MODE, SPI_BITS, SPI_SPEED, SPI_CS_PIN);
	if (spi == NULL) {
		FATAL("LORA", 0x50);
	}

}

void SX1276IoIrqInit( void )
{
	//wiringPiISR(DIO0_PIN, INT_EDGE_RISING, SX1276OnDio0Irq);
	pthread_create(&int_thread, NULL, lora_isr_thread, NULL);
}

void SX1276IoDeInit( void )
{
	gpio_open(RST_PIN, GPIO_OUT);
	gpio_write(RST_PIN, GPIO_HIGH);

	//connect DIO2 to DIO0
	gpio_open(DIO0_PIN, GPIO_IN);
}

void SX1276SetRfTxPower( int8_t power )
{
	uint8_t paConfig = 0;
	uint8_t paDac = 0;

	paConfig = SX1276Read( REG_PACONFIG );
	paDac = SX1276Read( REG_PADAC );

	paConfig = ( paConfig & RF_PACONFIG_PASELECT_MASK ) | SX1276GetPaSelect( SX1276.Settings.Channel );
	paConfig = ( paConfig & RF_PACONFIG_MAX_POWER_MASK ) | 0x70;

	if( ( paConfig & RF_PACONFIG_PASELECT_PABOOST ) == RF_PACONFIG_PASELECT_PABOOST )
	{
		if( power > 17 )
		{
			paDac = ( paDac & RF_PADAC_20DBM_MASK ) | RF_PADAC_20DBM_ON;
		}
		else
		{
			paDac = ( paDac & RF_PADAC_20DBM_MASK ) | RF_PADAC_20DBM_OFF;
		}
		if( ( paDac & RF_PADAC_20DBM_ON ) == RF_PADAC_20DBM_ON )
		{
			if( power < 5 )
			{
				power = 5;
			}
			if( power > 20 )
			{
				power = 20;
			}
			paConfig = ( paConfig & RF_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power - 5 ) & 0x0F );
		}
		else
		{
			if( power < 2 )
			{
				power = 2;
			}
			if( power > 17 )
			{
				power = 17;
			}
			paConfig = ( paConfig & RF_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power - 2 ) & 0x0F );
		}
	}
	else
	{
		if( power < -1 )
		{
			power = -1;
		}
		if( power > 14 )
		{
			power = 14;
		}
		paConfig = ( paConfig & RF_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power + 1 ) & 0x0F );
	}
	SX1276Write( REG_PACONFIG, paConfig );
	SX1276Write( REG_PADAC, paDac );
}

uint8_t SX1276GetPaSelect( uint32_t channel )
{
	if( channel < RF_MID_BAND_THRESH )
	{
		return RF_PACONFIG_PASELECT_PABOOST;
	}
	else
	{
		return RF_PACONFIG_PASELECT_RFO;
	}
}

void SX1276SetAntSwLowPower( bool status )
{
	if( RadioIsActive != status )
	{
		RadioIsActive = status;

		if( status == false )
		{
			SX1276AntSwInit( );
		}
		else
		{
			SX1276AntSwDeInit( );
		}
	}
}

void SX1276AntSwInit( void )
{
	//	GpioInit( &AntSwitchLf, RADIO_ANT_SWITCH_LF, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 1 );
	//	GpioInit( &AntSwitchHf, RADIO_ANT_SWITCH_HF, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 0 );
}

void SX1276AntSwDeInit( void )
{
	//	GpioInit( &AntSwitchLf, RADIO_ANT_SWITCH_LF, PIN_OUTPUT, PIN_OPEN_DRAIN, PIN_NO_PULL, 0 );
	//	GpioInit( &AntSwitchHf, RADIO_ANT_SWITCH_HF, PIN_OUTPUT, PIN_OPEN_DRAIN, PIN_NO_PULL, 0 );
}

void SX1276SetAntSw( uint8_t opMode )
{
	switch( opMode )
	{
	case RFLR_OPMODE_TRANSMITTER:
		//		GpioWrite( &AntSwitchLf, 0 );
		//		GpioWrite( &AntSwitchHf, 1 );
		break;
	case RFLR_OPMODE_RECEIVER:
	case RFLR_OPMODE_RECEIVER_SINGLE:
	case RFLR_OPMODE_CAD:
	default:
		//		GpioWrite( &AntSwitchLf, 1 );
		//		GpioWrite( &AntSwitchHf, 0 );
		break;
	}
}

bool SX1276CheckRfFrequency( uint32_t frequency )
{
	// Implement check. Currently all frequencies are supported
	return true;
}

