#include <string.h>

#include "../ak/ak.h"

#include "mqtt_communication.h"

#include "app.h"
#include "app_dbg.h"
#include "global_parameters.h"
#ifdef AES_ENCRYPT_FLAG
#include "aes.h"
#endif
#include "task_list.h"

uint8_t* lastSensorControlMessage;
uint32_t len_lastControl = 0;

mqtt_communication::mqtt_communication(const char *id, const char *host, int port, char* username, char* password) :
		mosquittopp(id, true) {
	/* init private data */
	m_connect_ok_flag = -1;
	m_mid = 1;

	/* init mqtt */
	mosqpp::lib_init();

	/* connect */
	username_pw_set(username, password);
	string comm_topic_status = string((char*) g_config_parameters.mqtt_topic) + string("/status/");
//	APP_PRINT(" comm_topic_status: %s \n", comm_topic_status.data());

	uint8_t *offline = (uint8_t *) malloc(MAX_MESSAGE_LEN * sizeof(uint8_t));
	int i = 0;
	offline[i] = SEPARATE_CHAR;
	i++;
	memcpy((uint8_t*) &offline[i], "offline", strlen("offline"));
	i += strlen("offline");
	offline[i] = SEPARATE_CHAR;
	i++;
	memcpy((uint8_t*) &offline[i], (uint8_t*) g_app_version, strlen(g_app_version));
	i += strlen(g_app_version);
	offline[i] = '\0';
	i++;
//	setKey((unsigned char*) g_config_parameters.security_key, 32);
	int encrypted_offline = iot_final_encrypt((uint8_t*) offline, i, g_config_parameters.security_key);
	will_set(comm_topic_status.data(), encrypted_offline, offline, 1, true);
	free(offline);

	connect_async(host, port, 60);
	loop_start();
}

mqtt_communication::~mqtt_communication() {
	loop_stop();
	mosqpp::lib_cleanup();
}

void mqtt_communication::set_topic_subcribe(const char* topic) {
//	m_topic_subscribe = static_cast<string>(topic);
	subscribe(NULL, topic, 1);
}

void mqtt_communication::set_topic_publish(const char* topic) {
	m_topic_publish = static_cast<string>(topic);
}
void mqtt_communication::on_connect(int rc) {
	if (rc == 0) {
		m_connect_ok_flag = 0;
		APP_DBG("[mqtt_communication] on_connect OK\n");
		// Send last status of gateway
		string comm_topic_status = string((char*) g_config_parameters.mqtt_topic) + string("/status/");
		APP_PRINT(" comm_topic_status: %s \n", comm_topic_status.data());
		// Send status when gateway is online
		set_topic_publish(comm_topic_status.data());
		uint8_t *online = (uint8_t *) malloc(MAX_MESSAGE_LEN * sizeof(uint8_t));
		int i = 0;
		online[i] = SEPARATE_CHAR;
		i++;
		memcpy((uint8_t*) &online[i], "online", strlen("online"));
		i += strlen("online");
		online[i] = SEPARATE_CHAR;
		i++;
		memcpy((uint8_t*) &online[i], (uint8_t*) g_app_version, strlen(g_app_version));
		i += strlen(g_app_version);
		online[i] = '\0';
		i++;
//		setKey((unsigned char*) g_config_parameters.security_key, 32);
		int encrypted_online = iot_final_encrypt((uint8_t*) online, i, g_config_parameters.security_key);
		communication_public((uint8_t*) online, encrypted_online, true);
		free(online);
//		subscribe(NULL, m_topic_subscribe.data());
	} else {
		APP_DBG("[mqtt_communication] on_connect ERROR\n");
	}
}

void mqtt_communication::communication_public(uint8_t* msg, uint32_t len, bool retain) {
	APP_DBG("[mqtt_communication][public] msg:%s len:%d\n", msg, len);
	if (publish(&m_mid, m_topic_publish.data(), len, msg, 1, true) != MOSQ_ERR_SUCCESS) {
		printf("public error"); //qos = 1
	}
}

void mqtt_communication::on_publish(int mid) {
	APP_DBG("[mqtt_communication][on_publish] mid: %d\n", mid);
}

void mqtt_communication::on_subscribe(int mid, int qos_count, const int *granted_qos) {
	(void) granted_qos;

	APP_DBG("[mqtt_communication][on_subscribe] mid:%d\tqos_count:%d\n", mid, qos_count);
}

void mqtt_communication::on_message(const struct mosquitto_message *message) {
//	if (!strcmp(message->topic, m_topic_subscribe.data())) {
	if (message->retain == false) {
		if (message->payloadlen > 0) {

			APP_DBG("[mqtt_communication][on _message] topic:%s\tpayloadlen:%d\n", message->topic, message->payloadlen);
			char *p = (char*) message->payload;
			for(int i=0;i<message->payloadlen;i++){
				APP_DBG("%c",p[i]);
			}
			APP_DBG("\n");
			if (lastSensorControlMessage == NULL) {
				lastSensorControlMessage = (uint8_t*) malloc(message->payloadlen);
				if (lastSensorControlMessage == NULL) {
					return;
				}
				memcpy(lastSensorControlMessage, (uint8_t*) message->payload, message->payloadlen);
				len_lastControl = message->payloadlen;
			} else {
				if (message->payloadlen == len_lastControl) {
					uint8_t result_ms = strncmp((const char*) message->payload, (const char*) lastSensorControlMessage, message->payloadlen);

					if (result_ms == 0) { //repeated
						return;
					} else {
						memcpy(lastSensorControlMessage, (uint8_t*) message->payload, message->payloadlen);
						len_lastControl = message->payloadlen;
					}
				} else {
					lastSensorControlMessage = (uint8_t*) realloc((uint8_t*) lastSensorControlMessage, message->payloadlen);
					if (lastSensorControlMessage == NULL) {
						return;
					}
					memcpy(lastSensorControlMessage, message->payload, message->payloadlen);
					len_lastControl = message->payloadlen;
				}
			}

#ifdef AES_ENCRYPT_FLAG
			int len = remove_all_chars((char*) message->payload, '\n', message->payloadlen);
#else
			int len = message->payloadlen;
#endif

			/* post message to mqtt task */
			ak_msg_t* s_msg = get_dymanic_msg();
			set_msg_sig(s_msg, MQTT_DATA_COMMUNICATION);
			set_data_dynamic_msg(s_msg, (uint8_t*) message->payload, len);

			set_msg_src_task_id(s_msg, ID_TASK_MQTT);
			task_post(ID_TASK_MQTT, s_msg);

		}
	}
}
