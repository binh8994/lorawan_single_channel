#include "task_send_sms.h"

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "../ak/ak.h"
#include "send_sms.h"

#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"
#include "global_parameters.h"

#include "task_list.h"

q_msg_t gw_task_send_sms_mailbox;

void* gw_task_send_sms_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("gw_task_send_sms_entry\n");
	send_sms_initializer("send_sms.txt");

	while (1) {
		while (msg_available(ID_TASK_SEND_SMS)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(ID_TASK_SEND_SMS);

			/* handler message */
			switch (msg->header->sig) {
			case DELETE_SMS_FLIE: {
				APP_PRINT("delete file send sms\n");
				delete_send_sms_file();
			}
				break;
			case GET_SEND_SMS: {
				parser_send_sms_file((uint8_t*) msg->header->payload, msg->header->len);
			}
				break;
			case UPDATE_FILE_SMS: {
				APP_PRINT("update send sms file\n");
//				add_new_send_sms_file((uint8_t*) msg->header->payload, msg->header->len);

				char * url = (char*) msg->header->payload;
				APP_PRINT("url=====================: %s\n", url);
				char filepath[256];
				get_send_sms_path_file(filepath);
				printf("File path: %s\n", filepath);

				bool result = curlPostgetFile(url, filepath);
				if (result == true) {
					APP_PRINT("sms file Updated!\n");
				} else {
					APP_PRINT("ERROR: updated Scene file!\n");
				}
			}
				break;
			default:
				break;
			}
			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*) 0;
}
