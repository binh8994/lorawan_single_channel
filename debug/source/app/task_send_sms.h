#ifndef TASK_SEND_SMS_H
#define TASK_SEND_SMS_H
#include "../ak/message.h"

extern q_msg_t gw_task_send_sms_mailbox;
extern void* gw_task_send_sms_entry(void*);

#endif // TASK_SEND_SMS_H
