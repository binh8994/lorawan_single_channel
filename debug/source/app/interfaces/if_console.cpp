#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../ak/ak.h"

#include "app.h"
#include "app_dbg.h"

#include "if_console.h"
#include "global_parameters.h"
#include "task_list.h"
#include "../znp/znp_serial.h"
#include "scene.h"

#define FACC_LOGIN_OPT					(0x00)
#define INTERNAL_LOGIN_OPT				(0x01)

q_msg_t gw_task_if_console_mailbox;

static unsigned char cmd_buf[CMD_BUFFER_SIZE];
static int i_get_command(unsigned char* cmd_buf);

const char* __internal = "__internal\n";
const char* __facc = "__facc\n";

void* gw_task_if_console_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("gw_task_if_console_entry\n");

	while (1) {
//#if USE_IF_CONSOLE
		if (i_get_command(cmd_buf) == 0) {
			APP_PRINT("if console = %s \n", cmd_buf);
			if (cmd_buf[0] == '1') {
				uint8_t str_ac_sensor = TIME_ENABLE_PERMIT_JOIN;
				ak_msg_t* s_msg = get_dymanic_msg();
				set_msg_sig(s_msg, ZB_PERMIT_JOINING_REQUEST);
				set_data_dynamic_msg(s_msg, (uint8_t*) &str_ac_sensor, 1);

				set_msg_src_task_id(s_msg, ID_TASK_IF_CONSOLE);
				task_post(ID_TASK_IF_ZNP, s_msg);

			} else if (cmd_buf[0] == '2') {
				uint8_t str_ac_sensor = 0x01;
				ak_msg_t* s_msg = get_dymanic_msg();
				set_msg_sig(s_msg, UTIL_GET_DEVICE_INFO);
				set_data_dynamic_msg(s_msg, (uint8_t*) &str_ac_sensor, 1);

				set_msg_src_task_id(s_msg, ID_TASK_IF_CONSOLE);
				task_post(ID_TASK_IF_ZNP, s_msg);
			} else if (cmd_buf[0] == '3') {
				cmd_buf[34] = '\0';
				APP_PRINT("%s", cmd_buf);

				zdo_mgmt_leave_req_t zdoMgmtLeaveReq;

				int8_t i;
				i = 1;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * sizeof(uint16_t), (uint8_t*) &zdoMgmtLeaveReq.short_address);
				i += 2 * sizeof(uint16_t);
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * 8, (uint8_t*) &zdoMgmtLeaveReq.device_address[0]);
				i += 2 * 8;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &zdoMgmtLeaveReq.flags);
				i += 2;
				//memcpy((uint8_t*) &zdoMgmtLeaveReq, (uint8_t*) &client_message[10], sizeof(zdo_mgmt_leave_req_t));

				ak_msg_t* s_msg = get_dymanic_msg();
				set_msg_sig(s_msg, ZDO_MGMT_LEAVE_REQ);
				set_data_dynamic_msg(s_msg, (uint8_t*) &zdoMgmtLeaveReq, sizeof(zdo_mgmt_leave_req_t));
				set_msg_src_task_id(s_msg, ID_TASK_IF_CONSOLE);
				task_post(ID_TASK_IF_ZNP, s_msg);
			} else if (cmd_buf[0] == '4') {
				cmd_buf[34] = '\0';
				APP_PRINT("%s", cmd_buf);

				af_data_request_t afDataRequest;

				int32_t i;
				i = 1;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * sizeof(uint16_t), (uint8_t*) &afDataRequest.dst_address);
				i += 2 * sizeof(uint16_t);
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.dst_endpoint);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.src_endpoint);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * sizeof(uint16_t), (uint8_t*) &afDataRequest.cluster_id);
				i += 2 * sizeof(uint16_t);
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.trans_id);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.options);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.radius);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.len);
				i += 2;

				uint8_t *data = (uint8_t*) malloc(sizeof(af_data_request_t) + afDataRequest.len);
				memcpy(data, (uint8_t*) &afDataRequest, sizeof(af_data_request_t));
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * afDataRequest.len, (uint8_t*) &data[sizeof(af_data_request_t)]);
				i += 2 * afDataRequest.len;

				ak_msg_t* s_msg = get_dymanic_msg();
				set_msg_sig(s_msg, AF_DATA_REQUEST);
				set_data_dynamic_msg(s_msg, data, (sizeof(af_data_request_t) + afDataRequest.len));
				set_msg_src_task_id(s_msg, ID_TASK_IF_CONSOLE);
				task_post(ID_TASK_IF_ZNP, s_msg);

				free(data);
			} else if (cmd_buf[0] == '5') {
				char* reset_t = "reset";
				if (strlen((const char*) cmd_buf) < strlen(reset_t) - 1) {
					continue;
				}
				if (strncmp((const char*)&cmd_buf[1], (const char*)reset_t, strlen(reset_t)) == 0) {
					g_config_parameters.start_up_success = 1;
					string aes_key = string((const char*) s_ieee_address) + string((const char*) s_ieee_address);
					gateway_configure.write_config_data(&g_config_parameters, (uint8_t*) aes_key.c_str());
					APP_PRINT("Reset gateway\n");
					exit(0);
				}
			} else if (cmd_buf[0] == '6') {
				parse_communicate_string_t parse_cmd = parseClientString(cmd_buf, strlen((const char*)cmd_buf));
				if (parse_cmd.totalString < 2) {
					APP_PRINT("not message\n");
				}else {
					char* data;
					uint8_t len = parse_cmd.len[0] + parse_cmd.len[1] + 2;
					data = (char*) malloc(len);
					if ((parse_cmd.len[0] <10) || (parse_cmd.len[0] > 13) ) {
						APP_PRINT("Error phone number\n");
						continue;
					}

					uint8_t k = 0;
					data[k] =  parse_cmd.len[1];
					k++;
					memcpy(&data[k], (uint8_t*) &cmd_buf[parse_cmd.position[1]], parse_cmd.len[1]);
					k += parse_cmd.len[1];

					data[k] =  parse_cmd.len[0];
					k++;
					memcpy(&data[k], (uint8_t*) &cmd_buf[parse_cmd.position[0]], parse_cmd.len[0]);
					k += parse_cmd.len[0];

					uint8_t index = 0;
					uint8_t len_index;
					APP_PRINT("send message :");
					while(index < len) {
						len_index = data[index];
						index++;
						if (index != 1) {
							APP_PRINT("phone number:");
						}
						APP_PRINT("len : %d : ",len_index);
						for (uint8_t i = 0; i < len_index; i++) {
							APP_PRINT("%c",data[index + i]);
						}
						index += len_index;
						APP_PRINT("\n");
					}
					ak_msg_t* s_msg = get_dymanic_msg();
					set_msg_sig(s_msg, GSM_SEND_SMS);
					set_data_dynamic_msg(s_msg, (uint8_t*) data, len);
					set_msg_src_task_id(s_msg, ID_TASK_SEND_SMS);
					task_post(ID_TASK_GSM, s_msg);

					free(data);
				}
			}else if (cmd_buf[0] == '7') {
				/* check money in sim: ATD*101# or AT+CUSD=1,"*101#" */
				if (cmd_buf[1] == ':') {
					uint8_t len = strlen((const char*) cmd_buf) -2; // -cmd_buf[0] - cmd_buf[1]
					uint8_t * data;
					data = (uint8_t*) malloc(len + 1);
					string_copy(data, &cmd_buf[2], len);
					APP_PRINT("send AT COMM : %s\n",data);

					ak_msg_t* s_msg = get_dymanic_msg();
					set_msg_sig(s_msg, GSM_SEND_AT_COMM);
					set_data_dynamic_msg(s_msg, (uint8_t*) data, len);
					set_msg_src_task_id(s_msg, ID_TASK_SEND_SMS);
					task_post(ID_TASK_GSM, s_msg);
				}
			}

			/* clean command buffer */
			memset(cmd_buf, 0, CMD_BUFFER_SIZE);
		}
		usleep(1000);
	}
	return (void*) 0;
}

int i_get_command(unsigned char* cmd_buf) {
	unsigned char c = 0;
	unsigned char index = 0;

	do {
		c = getchar();
		if (c >= 0x20 && c < 0x7F) {
			cmd_buf[index++] = c;
		}
		if (index > CMD_BUFFER_SIZE) {
			index = 0;
			APP_PRINT("ERROR: buffer overload !\n");
			return (-1);
		}

		/* sleep 100us */
		usleep(1000);

	} while (c != '\n');

	cmd_buf[index] = 0;
	return (0);
}
