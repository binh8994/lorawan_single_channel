#include<arpa/inet.h> //inet_addr
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <net/if.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/ether.h>

#include <time.h>

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <semaphore.h>

#include "../ak/ak.h"

#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "if_udp_broadcast.h"
#include "global_parameters.h"

#define BROADCAST_PORT						7000
#define SUBNET								"255.255.255.255"

q_msg_t gw_task_if_udp_mailbox;
/*Function will trigger 1 shot udp broadcast*/
void udp_broadcast(uint8_t* gw_ieee_address, uint32_t gw_ieee_address_len);
bool is_interface_allow_broadcast(std::string interface);
bool is_interface_up(std::string interface);

void* gw_task_if_udp_broadcast_entry(void*) {

	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_if_udp_broadcast_entry\n");
	timer_set(ID_TASK_UDP_BROADCAST, GW_UDP_BROADCAST_SHOT, GW_UDP_BROADCAST_IF_TIMER_PACKET_DELAY_INTERVAL, TIMER_PERIODIC);

	/*MAC address*/
	uint32_t gw_ieee_address_len = 18;/*lenght of an MAC Address*/
	uint8_t gw_ieee_address[gw_ieee_address_len];

	while (1) {
		while (msg_available(ID_TASK_UDP_BROADCAST)) {
			/*Get message*/
			ak_msg_t* msg = rev_msg(ID_TASK_UDP_BROADCAST);

			switch (msg->header->sig) {
			case GW_UDP_BROADCAST_SHOT: {
//					APP_DBG("GW_UDP_BROADCAST_SHOT\n");
				if (gw_ieee_address[0] != 0) {
					udp_broadcast(gw_ieee_address, gw_ieee_address_len);
				}
			}
				break;
			case GW_UDP_MAC_ADDRESS_RSP: {
				if (msg->header->len > 0) {
					memcpy(gw_ieee_address, (uint8_t*) msg->header->payload, msg->header->len);
				}
			}
				break;
			default:
				break;
			}
			free_msg(msg);
		}
		usleep(1000);
	}
	return (void*) 0;
}

void udp_broadcast(uint8_t *gw_ieee_address, uint32_t gw_ieee_address_len) {
	/*Checking condition*/
	/*sd for socket create condition
	 rc for sending data condition*/
	int sd, rc;

	/*UDP Broadcast Client
	 A sockaddr_in is a structure containing an internet
	 ddress. This structure is already defined in netinet/in.h, so
	 we don't need to declare it again.*/
	struct sockaddr_in cliAddr, remoteServAddr;
	struct hostent *h; //host entry

	/*Get current machine IP address variable*/
	struct ifaddrs * ifAddrStruct = NULL;
	struct ifaddrs * ifa = NULL;
	void * tmpAddrPtr = NULL;
	char addressBuffer[INET_ADDRSTRLEN];
	char finalmessage[1 + INET_ADDRSTRLEN + 1 + gw_ieee_address_len + 1];
	getifaddrs(&ifAddrStruct);

	for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
		//        APP_PRINT("%s IP Address\n", ifa->ifa_name);
		if (is_interface_allow_broadcast(ifa->ifa_name) && is_interface_up(ifa->ifa_name)) {
			//create particular binding interface
			struct ifreq interface;
			strcpy(interface.ifr_ifrn.ifrn_name, ifa->ifa_name);

			if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
				// is a valid IP4 Address
				tmpAddrPtr = &((struct sockaddr_in *) ifa->ifa_addr)->sin_addr;
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
//				APP_PRINT("%s IP Address: %s \n", ifa->ifa_name, addressBuffer);

				/* Get server IP SUBNET address (no check if input is IP address or DNS name */
				h = gethostbyname(SUBNET);
				if (h == NULL) {
					APP_PRINT("Unknown host '%s' \n", SUBNET);
					return;
				}

				remoteServAddr.sin_family = h->h_addrtype;
				memcpy((char *) &remoteServAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length);
				remoteServAddr.sin_port = htons(BROADCAST_PORT);

				/* socket creation */
				sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
				if (sd < 0) {
					APP_PRINT("Cannot open socket \n");
					close(sd);
					return;
				}

				if (setsockopt(sd, SOL_SOCKET, SO_BROADCAST, &interface, sizeof interface) == -1) {
					perror("setsockopt (SO_BROADCAST)");
					close(sd);
					return;
				}

				/* Bind any port */
				cliAddr.sin_family = AF_INET;
				cliAddr.sin_addr.s_addr = htonl(INADDR_ANY);
				cliAddr.sin_port = htons(0);

				rc = bind(sd, (struct sockaddr *) &cliAddr, sizeof(cliAddr));
				if (rc < 0) {
					APP_PRINT("Cannot bind port\n");
					close(sd);
					return;
				}

				/*The final message*/
				if (strlen(addressBuffer) > 0 && gw_ieee_address_len > 0) {
					int8_t i = 0;
					finalmessage[i] = SEPARATE_CHAR;
					i++;
					memcpy((uint8_t*) &finalmessage[i], addressBuffer, strlen(addressBuffer));
					i += strlen(addressBuffer);
					finalmessage[i] = SEPARATE_CHAR;
					i++;
					memcpy((uint8_t*) &finalmessage[i], gw_ieee_address, gw_ieee_address_len);
					i += gw_ieee_address_len;
					finalmessage[i] = 0;
					i++;
					/* send data */
					rc = sendto(sd, finalmessage, i, 0, (struct sockaddr *) &remoteServAddr, sizeof(remoteServAddr));
				}
//				APP_PRINT("Message: %s\n", finalmessage);
				if (rc < 0) {
					APP_PRINT("%s: Cannot send data\n", finalmessage);
					close(sd);
					return;
				}
				close(sd);
			}
//            else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
//                // is a valid IP6 Address
//                tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
//                char addressBuffer[INET6_ADDRSTRLEN];
//                inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
//                printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
//            }
		}
	}
	if (ifAddrStruct != NULL)
		freeifaddrs(ifAddrStruct);
}
bool is_interface_allow_broadcast(std::string interface) {
	struct ifreq ifr;
	int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
	memset(&ifr, 0, sizeof(ifr));
	strcpy(ifr.ifr_name, interface.c_str());
	if (ioctl(sock, SIOCGIFFLAGS, &ifr) < 0) {
		perror("SIOCGIFFLAGS");
	}
	close(sock);
	return !!(ifr.ifr_flags & IFF_BROADCAST);
}
bool is_interface_up(std::string interface) {
	struct ifreq ifr;
	int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
	memset(&ifr, 0, sizeof(ifr));
	strcpy(ifr.ifr_name, interface.c_str());
	if (ioctl(sock, SIOCGIFFLAGS, &ifr) < 0) {
		perror("SIOCGIFFLAGS");
	}
	close(sock);
	return !!(ifr.ifr_flags & IFF_UP);
}
