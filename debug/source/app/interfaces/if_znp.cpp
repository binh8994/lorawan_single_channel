/*
 * if_znp_rx.cpp
 *
 *  Created on: Apr 24, 2017
 *      Author: TuDT13
 */

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <semaphore.h>

#include "../ak/ak.h"

#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"

#include "if_znp.h"
#include "../znp/znp_serial.h"
#include "global_parameters.h"
#include "aes.h"
// Start of frame character value
#define SOF_CHAR			0xFE

const char DEVICE_PATH_0[] = "/dev/ttyS3";
const char DEVICE_PATH_1[] = "/dev/ttyS3";

#define RX_BUFFER_SIZE		256

q_msg_t gw_task_if_znp_mailbox;
void* gw_task_if_znp_entry(void*);

int8_t flagAddDevice = 0; /*bit 0: 0x45CA, bit 1: 0x45C1*/
uint16_t g_short_address_new_device = 0;
int if_znp_fd;
struct termios options;
static int if_znp_rx_opentty(const char* devpath);
static void if_znp_rx_closetty();
static uint8_t if_znp_rx_calcfcs(uint8_t len, uint8_t cmd0, uint8_t cmd1, uint8_t *data_ptr);
static uint8_t if_znp_tx_calcfcs(uint8_t len, uint8_t *data_ptr);
static pthread_t if_znp_rx_thread;
static void* if_znp_rx_thread_handler(void*);

void permitJoinAddDevice(if_parseinfo_frame_t parseinfo_data);
void leaveDeviceResponse(if_parseinfo_frame_t parseinfo_data);

#define SOP_STATE      0x00
#define LEN_STATE      0x01
#define CMD0_STATE     0x02
#define CMD1_STATE     0x03
#define DATA_STATE     0x04
#define FCS_STATE      0x05

//static uint8_t rx_frame_state = SOP_STATE;

#define RX_FRAME_PARSER_FAILED			(-1)
#define RX_FRAME_PARSER_SUCCESS			(0)
#define RX_FRAME_PARSER_rx_remain		(1)

static if_parseinfo_frame_t if_parseinfo_frame;
static void rx_frame_parser(uint8_t* data, uint8_t len);
int tx_frame_post(uint8_t* data, uint8_t len);

static uint8_t tx_buffer[1024];

uint8_t check_app_version() {
	char app_version[] = "02-00-02";
	if (strcmp(app_version, g_libAES_version) == 0) {
		return ZNP_SUCCESS;
	}
	printf("\n version libAES %s is not such compatible with app version %s\n", g_libAES_version, app_version);
	return ZNP_NOT_SUCCESS;
}
void* gw_task_if_znp_entry(void*) {

	int bSuccess;
	uint8_t rx_buffer[RX_BUFFER_SIZE];
	int32_t rx_read_len = 0;
	task_mask_started();
	wait_all_tasks_started();

	if (check_app_version() != ZNP_SUCCESS) {
		printf("\nexit program!\n");
		exit(0);
	}
	APP_DBG("[STARTED] gw_task_if_znp_entry\n");
	while (1) {
		if (if_znp_rx_opentty(DEVICE_PATH_0) < 0) {
			APP_DBG("Cannot open %s !\n", DEVICE_PATH_0);
		} else {
			APP_DBG("Opened %s success !\n", DEVICE_PATH_0);
			// initialize UART receive thread related variables
			uint8_t znpResult;
			znpResult = zb_get_device_info(if_znp_fd, 1, rx_buffer, &rx_read_len);
			if (znpResult != ZNP_SUCCESS) {
				continue;
			}
			bytestoHexChars(&rx_buffer[5], 8, (uint8_t*) s_ieee_address);
			APP_PRINT("mac address : %s", s_ieee_address);
			APP_PRINT("\n");
			string aes_key = string((const char*) s_ieee_address) + string((const char*) s_ieee_address);
			read_config_file((uint8_t*) aes_key.c_str());

			if (g_config_parameters.start_up_success == 0) {
				//check panid
				znpResult = zbReadConfiguration(if_znp_fd, ZCD_NV_PANID, rx_buffer, (int32_t*) &rx_read_len);
				if (znpResult != ZNP_SUCCESS) {
					continue;
				}
				APP_PRINT("zbReadConfiguration\n");

				if (rx_buffer[7] == 0xFF && rx_buffer[8] == 0xFF) {
					g_config_parameters.start_up_success = 1;
					gateway_configure.write_config_data(&g_config_parameters, (uint8_t*) aes_key.c_str());
				}
			}
			// TODO: Start coordinator here
			APP_PRINT("Start coordinator here !\n");
			bSuccess = startZigbeeCoordinator(if_znp_fd, g_config_parameters.start_up_success);
			if (bSuccess == 0) {
				APP_PRINT("Start coordinator successfully !\n");
				break;
			}
		}
		if_znp_rx_closetty();
		sleep(5);
	}
	// update start_up_success
	g_config_parameters.start_up_success = 0;
	string aes_key = string((const char*) s_ieee_address) + string((const char*) s_ieee_address);
	gateway_configure.write_config_data(&g_config_parameters, (uint8_t*) aes_key.c_str());

	APP_DBG("Opened %s success !\n", DEVICE_PATH_0);
	pthread_create(&if_znp_rx_thread, NULL, if_znp_rx_thread_handler, NULL);

	while (1) {
		while (msg_available(ID_TASK_IF_ZNP)) {
			/* get message */
			ak_msg_t* msg = rev_msg(ID_TASK_IF_ZNP);

			switch (msg->header->sig) {
			case ZB_PERMIT_JOINING_REQUEST: {

				APP_PRINT("if_znp: add new device!\n");
				flagAddDevice = 0x00;
				g_short_address_new_device = 0;
				if (setPermitJoiningReq(if_znp_fd, ALL_ROUTER_AND_COORDINATOR, *(uint8_t *) msg->header->payload, 1) != ZNP_SUCCESS) {
					APP_PRINT("ERROR: request add new device!\n");
					exit(0);
				}
			}
				break;
			case ZDO_MGMT_LEAVE_REQ: {
				zdo_mgmt_leave_req_t *zdoMgmtLeaveReq;
				uint8_t* data = (uint8_t *) msg->header->payload;
				zdoMgmtLeaveReq = (zdo_mgmt_leave_req_t *) data;

				if (zdo_mgmt_leave_req(if_znp_fd, zdoMgmtLeaveReq->short_address, zdoMgmtLeaveReq->device_address, zdoMgmtLeaveReq->flags) == ZNP_SUCCESS) {
					APP_PRINT("SUCCESS!\n");
				} else {
					APP_PRINT("\nERROR: zdo_mgmt_leave_req!\n");
					exit(0);
				}

			}
				break;
			case UTIL_GET_DEVICE_INFO: {
				if (util_get_device_info(if_znp_fd) != ZNP_SUCCESS) {  // get short addr list
					APP_PRINT("\nERROR: get device info\n");
					exit(0);
				}
			}
				break;
			case ZB_RECEIVE_DATA_INDICATION: {
				if_parseinfo_frame_t parseinfo_data;
				get_data_dynamic_msg(msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));
				APP_PRINT("ZB_RECEIVE_DATA_INDICATION 0x4687 \n\n");
				APP_PRINT("AF_INCOMING_MSG: ");
				for (int i = 0; i < parseinfo_data.tempDataLen; i++) {
					APP_PRINT(" %02X", parseinfo_data.data[i]);
				}
				APP_PRINT("\n");
				af_incoming_msg_t *pkt;
				pkt = (af_incoming_msg_t *) &parseinfo_data.data[0];

				if (zcl_ProcessMessageMSG(pkt) == ZCL_PROC_SUCCESS) {
					//TODO: parse data to json structure and send to MQTT
				} else {
					APP_PRINT("It is not ZCL data\n");
				}
			}
				break;
			case AF_INCOMING_MSG: {
				if_parseinfo_frame_t parseinfo_data;
				get_data_dynamic_msg(msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));
//				APP_PRINT("AF_INCOMING_MSG 0x4481 \n\n");
//				APP_PRINT("AF_INCOMING_MSG: ");
//				for (int i = 0; i < parseinfo_data.tempDataLen; i++) {
//					APP_PRINT(" %02X", parseinfo_data.data[i]);
//				}
//				APP_PRINT("\n");
				af_incoming_msg_t *pkt;
				pkt = (af_incoming_msg_t *) &parseinfo_data.data[0];

				if (zcl_ProcessMessageMSG(pkt) == ZCL_PROC_SUCCESS) {
					//TODO: parse data to json structure and send to MQTT
				} else {
					APP_PRINT("It is not ZCL data\n");
				}

			}
				break;

			case ZDO_MGMT_LEAVE_RSP: {
				APP_PRINT("ZDO_MGMT_LEAVE_RSP 0x45B4 \n\n");
				if_parseinfo_frame_t parseinfo_data;
				get_data_dynamic_msg(msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));
				APP_PRINT("device id:");
				for (int i = 0; i < parseinfo_data.tempDataLen; i++) {
					APP_PRINT("%02X ", parseinfo_data.data[i]);
				}
				APP_PRINT("\n");
				leaveDeviceResponse(parseinfo_data);
			}
				break;
			case AF_DATA_REQUEST: {
				APP_PRINT("\nAF_DATA_REQUEST\n\n");

				uint8_t *data = (uint8_t*) msg->header->payload;
				af_data_request_t afDataRequest = *(af_data_request_t*) data;
				data += sizeof(af_data_request_t);
				afDataRequest.data = data;

				for (int j = 0; j < afDataRequest.len; j++) {
					APP_PRINT(" %02X", data[j]);
				}
				APP_PRINT("\n");
				if (send_af_data_request(if_znp_fd, afDataRequest) != ZNP_SUCCESS) {
					APP_PRINT("\nERROR : send af data request\n");
					exit(0);
				}

			}
				break;
			case ZDO_END_DEVICE_ANNCE_IND: {
				APP_PRINT("ZDO_END_DEVICE_ANNCE_IND 0x45C1 \n\n");

				if_parseinfo_frame_t parseinfo_data;
				get_data_dynamic_msg(msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));
				for (int i = 0; i < parseinfo_data.tempDataLen; i++) {
					APP_PRINT(" %02X ", parseinfo_data.data[i]);
				}
				APP_PRINT("\n");
				flagAddDevice = (flagAddDevice | 0x02);

				if (flagAddDevice == 0x03) {
					for (int i = 0; i < 10; i++) {
						parseinfo_data.data[i] = parseinfo_data.data[i + 2];
					}
					parseinfo_data.tempDataLen = 10;
					permitJoinAddDevice(parseinfo_data);
				}
				g_short_address_new_device = *(uint16_t*) &parseinfo_data.data[0];
			}
				break;

			case ZDO_TC_DEV_IND: {
				APP_PRINT("ZDO_TC_DEV_IND 0x45CA \n\n");
				if_parseinfo_frame_t parseinfo_data;
				get_data_dynamic_msg(msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));
				for (int i = 0; i < parseinfo_data.tempDataLen; i++) {
					APP_PRINT(" %02X ", parseinfo_data.data[i]);
				}
				APP_PRINT("\n");
				/*when receiving this signal meaning the new advice join network, need send MAC address to mqtt.
				 * Save MAC address to binding table*/
				flagAddDevice = (flagAddDevice | 0x01);
				parseinfo_data.tempDataLen = 10;
				permitJoinAddDevice(parseinfo_data);
				g_short_address_new_device = *(uint16_t*) &parseinfo_data.data[0];
			}
				break;
			case UTIL_GET_DEVICE_INFO_RESPONSE: {
				APP_PRINT("UTIL_GET_DEVICE_INFO 0x6700 \n\n");
				/*when receiving this signal meaning the new advice join network, need send MAC address to mqtt.
				 * Save MAC address to binding table*/
				if_parseinfo_frame_t parseinfo_data;
				get_data_dynamic_msg(msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));
				ak_msg_t* s_msg = get_dymanic_msg();
				set_msg_sig(s_msg, MQTT_CTRL_GET_DEVICE_LIST_RESPONSE);
				set_data_dynamic_msg(s_msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));

				set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
				task_post(ID_TASK_MQTT, s_msg);

			}
				break;
			case ZDO_IEEE_ADDR_RSP: {
				APP_PRINT("ZDO_IEEE_ADDR_RSP  0x4581 \n\n");
				if_parseinfo_frame_t parseinfo_data;
				get_data_dynamic_msg(msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));
				if (parseinfo_data.data[0] == ZNP_SUCCESS) {
					bytestoHexChars((uint8_t*) &parseinfo_data.data[1], 8, g_strTokenFactory);
					g_strTokenFactory[16] = '\0';
					string temp_str = string("echo \"") + string("#") + string(g_app_version) + string("#") + string((char*) g_strTokenFactory) + string("\" > mac_address.txt");
					int n = system(temp_str.c_str());

					APP_PRINT("MAC address = %s, write = %d\n", g_strTokenFactory, n);
					{
						ak_msg_t* s_msg = get_dymanic_msg();
						set_msg_sig(s_msg, MQTT_START_TOKEN_FACTORY);
						set_data_dynamic_msg(s_msg, (uint8_t*) g_strTokenFactory, 17);

						set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
						task_post(ID_TASK_MQTT, s_msg);
					}

					{
						ak_msg_t* s_msg = get_dymanic_msg();
						set_msg_sig(s_msg, GW_UDP_MAC_ADDRESS_RSP);
						set_data_dynamic_msg(s_msg, (uint8_t*) g_strTokenFactory, 17);

						set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
						task_post(ID_TASK_UDP_BROADCAST, s_msg);
					}
				}
			}
				break;
			case ZB_WRITE_CONFIGURATION: {
				APP_PRINT("ZB_WRITE_CONFIGURATION 0x2608\n");
			}
				break;
			default: {
				if_parseinfo_frame_t parseinfo_data;
				get_data_dynamic_msg(msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));
				if (((msg->header->sig & 0xE000/*RPC_CMD_TYPE_MASK*/) == 0x6000 /*RPC_CMD_SRSP*/) || ((msg->header->sig & 0x1F00/*RPC_SUBSYSTEM_MASK*/) == 0x0D00 /*RPC_SYS_BOOT*/)) {

					if (parseinfo_data.data[0] == 0) {
						APP_DBG("SUCCESSFUL!\n");
					} else {
						APP_DBG("FAIL: %02X\n", parseinfo_data.data[0]);
					}
				} else {

					APP_PRINT("cmd: %04X -data:", msg->header->sig);
					for (int i = 0; i < parseinfo_data.tempDataLen; i++) {
						APP_PRINT(" %02X", parseinfo_data.data[i]);
					}
					APP_PRINT("\n");
				}
			}
				break;
			}
			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*) 0;
}

void* if_znp_rx_thread_handler(void*) {
	APP_DBG("if_znp_rx_thread_handler entry successfully!\n");
	uint8_t rx_buffer[RX_BUFFER_SIZE];
	int32_t rx_read_len;

	if_parseinfo_frame.state = SOP_STATE;
	while (1) {

		rx_read_len = read(if_znp_fd, rx_buffer, RX_BUFFER_SIZE);
		if (rx_read_len > 0) {
//			APP_PRINT("raw data:");
//			for (int i = 0; i < rx_read_len; i++) {
//				APP_PRINT(" %02X ", rx_buffer[i]);
//			}
//			APP_PRINT(" \n ");
			rx_frame_parser(rx_buffer, rx_read_len);
		} else {
			APP_PRINT("Error from read: %d: %s\n", rx_read_len, strerror(errno));
			if_znp_rx_closetty();
			exit(0);
		}
		usleep(1000);
	}

	return (void*) 0;
}

int set_interface_attribs(int fd, int speed, int parity) {

	fcntl(if_znp_fd, F_SETFL, 0);
	memset(&options, 0, sizeof options);
	if (tcgetattr(fd, &options) != 0) {
		SYS_DBG("error %d from tcgetattr", errno);
		return -1;
	}

	cfsetospeed(&options, speed);
	cfsetispeed(&options, speed);

	options.c_cflag = (options.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
// disable IGNBRK for mismatched speed tests; otherwise receive break
// as \000 chars
	options.c_iflag &= ~IGNBRK;         // disable break processing
	options.c_lflag = 0;                // no signaling chars, no echo,
// no canonical processing
	options.c_oflag = 0;                // no remapping, no delays
	options.c_cc[VMIN] = 1;            // read doesn't block
	options.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

	options.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

	options.c_cflag |= (CLOCAL | CREAD); // ignore modem controls,
// enable reading
	options.c_cflag &= ~(PARENB | PARODD);      // shut off parity
	options.c_cflag |= parity;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CRTSCTS;

	if (tcsetattr(fd, TCSANOW, &options) != 0) {
		SYS_DBG("error %d from tcsetattr", errno);
		return -1;
	}
	return 0;
}

int set_blocking(int fd, int should_block) {
	memset(&options, 0, sizeof(options));
	if (tcgetattr(fd, &options) != 0) {
		SYS_DBG("error %d from tggetattr", errno);
		return -1;
	}

	options.c_cc[VMIN] = 1; //should_block ? 1 : 0;
	options.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

	if (tcsetattr(fd, TCSANOW, &options) != 0) {
		SYS_DBG("error %d setting term attributes", errno);
		return -1;
	}
	return 0;
}

int if_znp_rx_opentty(const char* devpath) {
#ifdef test_serial
	APP_DBG("[IF znp_rx][if_znp_rx_opentty] devpath: %s\n", devpath);
	if_znp_fd = open(devpath, O_RDWR | O_NOCTTY | O_SYNC | O_NONBLOCK);
	if (if_znp_fd < 0) {
		SYS_DBG("error %d opening %s: %s", errno, devpath, strerror(errno));
		return if_znp_fd;
	}

	if (set_interface_attribs(if_znp_fd, B115200, 0) == -1) {  // set speed to 115,200 bps, 8n1 (no parity)
		return -1;
	}
	if (set_blocking(if_znp_fd, 1) == -1) {  // set no blocking
		return -1;
	}
#endif

	for (uint8_t i = 0; i < 2; i++) {
		if (i == 0) {
			APP_DBG("[IF znp_rx][if_znp_rx_opentty] devpath: %s\n", DEVICE_PATH_0);
			if_znp_fd = open(DEVICE_PATH_0, O_RDWR | O_NOCTTY | O_NDELAY);
			if (if_znp_fd < 0) {
				SYS_DBG("error %d opening %s: %s\n", errno, DEVICE_PATH_0, strerror(errno));
				continue;
			} else {
				break;
			}
		} else {
			APP_DBG("[IF znp_rx][if_znp_rx_opentty] devpath: %s\n", DEVICE_PATH_1);
			if_znp_fd = open(DEVICE_PATH_1, O_RDWR | O_NOCTTY | O_NDELAY);
			if (if_znp_fd < 0) {
				SYS_DBG("error %d opening %s: %s\n", errno, DEVICE_PATH_1, strerror(errno));
				return -1;
			}
		}

	}
//	if_znp_fd = open(devpath, O_RDWR | O_NOCTTY | O_NDELAY);
//	if (if_znp_fd < 0) {
//		return if_znp_fd;
//	}
	fcntl(if_znp_fd, F_SETFL, 0);

	/* get current status */
	tcgetattr(if_znp_fd, &options);

	cfsetispeed(&options, B115200);
	cfsetospeed(&options, B115200);

//		cfsetispeed(&options, B38400);
//		cfsetospeed(&options, B38400);

	/* No parity (8N1) */
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;

	options.c_cflag |= (CLOCAL | CREAD);
	options.c_cflag &= ~CRTSCTS;
	options.c_cc[VMIN] = 1;		// read doesn't block
	options.c_cc[VTIME] = 5;		// 0.5 seconds read timeout
	cfmakeraw(&options);

	tcflush(if_znp_fd, TCIFLUSH);
	if (tcsetattr(if_znp_fd, TCSANOW, &options) != 0) {
		SYS_DBG("error in tcsetattr()\n");
		return -1;
	}

	return 0;
}

/* Close open device */
static void if_znp_rx_closetty(void) {
	/* revert to old settings */
	tcsetattr(if_znp_fd, TCSANOW, &options);

	/* close the device */
	close(if_znp_fd);
}
/* Calculate if znp receiving frame FCS */
static uint8_t if_znp_rx_calcfcs(uint8_t len, uint8_t cmd0, uint8_t cmd1, uint8_t *data_ptr) {
	uint8_t x;
	uint8_t xorResult;

	xorResult = len ^ cmd0 ^ cmd1;

	for (x = 0; x < len; x++, data_ptr++)
		xorResult = xorResult ^ *data_ptr;

	return (xorResult);
}
void rx_frame_parser(uint8_t* data, uint8_t len) {
	uint8_t ch;
	int rx_remain;

//	APP_PRINT("Data rx: \n");
//		for(int i=0;i<len;i++){
//			APP_PRINT("%02X ",data[i] );
//		}
//		APP_PRINT("\n\n " );

	while (len) {

		ch = *data++;
		len--;

		switch (if_parseinfo_frame.state) {
		case SOP_STATE:
			if (SOF_CHAR == ch) {
				if_parseinfo_frame.state = LEN_STATE;
			}
			break;

		case LEN_STATE:
			if_parseinfo_frame.len = ch;
			if_parseinfo_frame.tempDataLen = 0;
			/* Fill up what we can */
			if_parseinfo_frame.state = CMD0_STATE;
			break;

		case CMD0_STATE:
			if_parseinfo_frame.cmd0 = ch;
			if_parseinfo_frame.state = CMD1_STATE;

			break;
		case CMD1_STATE:

			if_parseinfo_frame.cmd1 = ch;
			/* If there is no data, skip to FCS state */
			if (if_parseinfo_frame.len) {
				if_parseinfo_frame.state = DATA_STATE;
			} else {
				if_parseinfo_frame.state = FCS_STATE;
			}
			break;

		case DATA_STATE: {
			if_parseinfo_frame.data[if_parseinfo_frame.tempDataLen++] = ch;

			rx_remain = if_parseinfo_frame.len - if_parseinfo_frame.tempDataLen;

			if (len >= rx_remain) {
				memcpy((uint8_t*) (if_parseinfo_frame.data + if_parseinfo_frame.tempDataLen), data, rx_remain);
				if_parseinfo_frame.tempDataLen += rx_remain;
				len -= rx_remain;
				data += rx_remain;
			} else {
				memcpy((uint8_t*) (if_parseinfo_frame.data + if_parseinfo_frame.tempDataLen), data, len);
				if_parseinfo_frame.tempDataLen += len;
				len = 0;
			}

			if (if_parseinfo_frame.len == if_parseinfo_frame.tempDataLen) {
				if_parseinfo_frame.state = FCS_STATE;
			}
		}
			break;

		case FCS_STATE: {
			if_parseinfo_frame.state = SOP_STATE;

			if_parseinfo_frame.frame_fcs = ch;

			if (if_parseinfo_frame.frame_fcs == if_znp_rx_calcfcs(if_parseinfo_frame.len, if_parseinfo_frame.cmd0, if_parseinfo_frame.cmd1, if_parseinfo_frame.data)) {

				uint16_t tempSig = (uint16_t)((if_parseinfo_frame.cmd1) + 0x0100 * (if_parseinfo_frame.cmd0));
				ak_msg_t* s_msg = get_dymanic_msg();
				set_msg_sig(s_msg, tempSig);
				set_data_dynamic_msg(s_msg, (uint8_t*) &if_parseinfo_frame, sizeof(if_parseinfo_frame_t));

				set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
				task_post(ID_TASK_IF_ZNP, s_msg);

			} else {
				/* TODO: handle checksum incorrect */
				APP_DBG("ERROR: checksum incorrect!\n");
			}
		}
			break;

		default:
			break;
		}
	}
}

/* Calculate IF_ZNP TX frame FCS */
uint8_t if_znp_tx_calcfcs(uint8_t len, uint8_t *data_ptr) {
	uint8_t xor_result;
	xor_result = len;

	for (int i = 0; i < len; i++, data_ptr++) {
		xor_result = xor_result ^ *data_ptr;
	}

	return xor_result;
}

int tx_frame_post(uint8_t* data, uint8_t len) {
	tx_buffer[0] = SOF_CHAR;
	tx_buffer[1] = len;
	memcpy(&tx_buffer[2], data, len);
	tx_buffer[2 + len] = if_znp_tx_calcfcs(len, data);
	return write(if_znp_fd, tx_buffer, (len + 3));
}

void permitJoinAddDevice(if_parseinfo_frame_t parseinfo_data) {
	if (flagAddDevice == 0x03) {
		if (g_short_address_new_device != *((uint16_t*) &parseinfo_data.data[0])) {
			flagAddDevice = 0x00;
			return;
		}
		setPermitJoiningReq(if_znp_fd, SHORT_ADDRESS_COORDINATOR, DISABLE_PERMIT_JOIN, 1);

//		uint8_t client_message[MAX_MESSAGE_LEN];
		uint8_t* client_message = (uint8_t *) malloc(MAX_MESSAGE_LEN * sizeof(uint8_t));

		int32_t i = 0;
		//add token communicate
		client_message[i] = SEPARATE_CHAR;
		i++;
		if (g_config_parameters.token_communicate[0] != 0) {
			memcpy((uint8_t*) &client_message[i], g_config_parameters.token_communicate, strlen((const char*) g_config_parameters.token_communicate));
			i += strlen((const char*) g_config_parameters.token_communicate);
		} else {
			free(client_message);
			return; //not start token communication
		}
		//add time
		client_message[i] = SEPARATE_CHAR;
		i++;
		time_t rawtime = time(NULL);
		bytestoHexChars((uint8_t*) &rawtime, sizeof(time_t), (uint8_t*) &client_message[i]);
		i += sizeof(time_t) * 2;

		//add command adddevice
		client_message[i] = SEPARATE_CHAR;
		i++;
		memcpy((uint8_t*) &client_message[i], g_strCommand[adddevice].c_str(), g_strCommand[adddevice].size());
		i += g_strCommand[adddevice].size();
		//add data
		client_message[i] = SEPARATE_CHAR;
		i++;

		//device id 2 bytes
		bytestoHexChars((uint8_t*) parseinfo_data.data, 2, (uint8_t*) &client_message[i]);
		i += 2 * 2;
		client_message[i] = SEPARATE_CHAR;
		i++;
		//MAC address 8 bytes
		bytestoHexChars((uint8_t*) &parseinfo_data.data[2], 8, (uint8_t*) &client_message[i]);
		i += 2 * 8;
		client_message[i] = SEPARATE_CHAR;
		i++;
		client_message[i] = '\0';
		APP_PRINT("\n %s\n", client_message);
#ifdef AES_ENCRYPT_FLAG
//		setKey((unsigned char*) g_config_parameters.security_key, 32);
		int postmessagelen = iot_final_encrypt(client_message, i, g_config_parameters.security_key);
//		printf("ciphertext  if_znp: %s\n", client_message);
		if (postmessagelen <= 0) {
			flagAddDevice = 0x00;
			free(client_message);
			return;
		}
		i = postmessagelen;
#endif
		client_message[i] = '\0';
		i++;
#ifdef ADD_SENSOR_BY_SOCKET
		{
			uint8_t *data = (uint8_t*) client_message;
			ak_msg_t* s_msg = get_dymanic_msg();
			set_msg_sig(s_msg, IF_SOCKET_ADD_NEW_ZED_RESPONSE);
			set_data_dynamic_msg(s_msg, data, i);

			set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
			task_post(ID_TASK_IF_SOCKET, s_msg);
		}
#endif
		{
			uint8_t *data = (uint8_t*) client_message;
			ak_msg_t* s_msg = get_dymanic_msg();
			set_msg_sig(s_msg, LOG_SENSOR_DATA);
			set_data_dynamic_msg(s_msg, data, i);

			set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
			task_post(ID_TASK_LOG, s_msg);
		}

		{
			uint8_t topic_string[1];
			uint8_t topic_len = 0;
			topic_string[topic_len] = '\0';
			topic_len++;
			int32_t message_len = i;
			client_message[i] = topic_len;
			i++;
			memcpy((uint8_t*) &client_message[i], (uint8_t*) topic_string, topic_len);
			i += topic_len;
			memcpy((uint8_t*) &client_message[i], (uint8_t*) &message_len, sizeof(message_len));
			i += sizeof(message_len);
			uint8_t *data = (uint8_t*) client_message;
			ak_msg_t* s_msg = get_dymanic_msg();
			set_msg_sig(s_msg, MQTT_SENSOR_DATA);
			set_data_dynamic_msg(s_msg, data, i);

			set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
			task_post(ID_TASK_MQTT, s_msg);
		}

		free(client_message);
		flagAddDevice = 0x00;
	}
}

void leaveDeviceResponse(if_parseinfo_frame_t parseinfo_data) {

//	uint8_t client_message[MAX_MESSAGE_LEN];
	uint8_t* client_message = (uint8_t *) malloc(MAX_MESSAGE_LEN * sizeof(uint8_t));
	int32_t i = 0;
	//add token communicate
	client_message[i] = SEPARATE_CHAR;
	i++;

	if (g_config_parameters.token_communicate[0] != 0) {
		memcpy((uint8_t*) &client_message[i], g_config_parameters.token_communicate, strlen((const char*) g_config_parameters.token_communicate));
		i += strlen((const char*) g_config_parameters.token_communicate);
	} else {
		free(client_message);
		return; //not start token communication
	}

	//add time
	client_message[i] = SEPARATE_CHAR;
	i++;
	time_t rawtime = time(NULL);
	bytestoHexChars((uint8_t*) &rawtime, sizeof(time_t), (uint8_t*) &client_message[i]);
	i += sizeof(time_t) * 2;

	//add command remove device
	client_message[i] = SEPARATE_CHAR;
	i++;
	memcpy((uint8_t*) &client_message[i], g_strCommand[responsedeviceleavenetwork].c_str(), g_strCommand[responsedeviceleavenetwork].size());
	i += g_strCommand[responsedeviceleavenetwork].size();
	//add data (device id 2bytes)
	client_message[i] = SEPARATE_CHAR;
	i++;
	bytestoHexChars((uint8_t*) parseinfo_data.data, 2, (uint8_t*) &client_message[i]);
	i += 2 * 2;
	client_message[i] = SEPARATE_CHAR;
	i++;
	client_message[i] = '\0';
	APP_PRINT("\n %s\n", client_message);
#ifdef AES_ENCRYPT_FLAG
//	setKey((unsigned char*) g_config_parameters.security_key, 32);
	int postmessagelen = iot_final_encrypt(client_message, i, g_config_parameters.security_key);
//		printf("ciphertext  if_znp: %s\n", client_message);
	if (postmessagelen <= 0) {
		free(client_message);
		return;
	}
	i = postmessagelen;
#endif
	client_message[i] = '\0';
	i++;
#ifdef ADD_SENSOR_BY_SOCKET
	{
		uint8_t *data = (uint8_t*) client_message;
		ak_msg_t* s_msg = get_dymanic_msg();
		set_msg_sig(s_msg, IF_SOCKET_LEAVE_DEVICE_RESPONSE);
		set_data_dynamic_msg(s_msg, data, i);

		set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
		task_post(ID_TASK_IF_SOCKET, s_msg);
	}
#endif
	{
		uint8_t *data = (uint8_t*) client_message;
		ak_msg_t* s_msg = get_dymanic_msg();
		set_msg_sig(s_msg, LOG_SENSOR_DATA);
		set_data_dynamic_msg(s_msg, data, i);

		set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
		task_post(ID_TASK_LOG, s_msg);
	}

	{
		uint8_t topic_string[1];
		uint8_t topic_len = 0;
		topic_string[topic_len] = '\0';
		topic_len++;
		int32_t message_len = i;
		client_message[i] = topic_len;
		i++;
		memcpy((uint8_t*) &client_message[i], (uint8_t*) topic_string, topic_len);
		i += topic_len;
		memcpy((uint8_t*) &client_message[i], (uint8_t*) &message_len, sizeof(message_len));
		i += sizeof(message_len);
		uint8_t *data = (uint8_t*) client_message;
		ak_msg_t* s_msg = get_dymanic_msg();
		set_msg_sig(s_msg, MQTT_SENSOR_DATA);
		set_data_dynamic_msg(s_msg, data, i);

		set_msg_src_task_id(s_msg, ID_TASK_IF_ZNP);
		task_post(ID_TASK_MQTT, s_msg);
	}

	free(client_message);
}
