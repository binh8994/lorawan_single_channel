#ifndef __APP_H__
#define __APP_H__

#include <string>
#include "app_config.h"
#include "../app/znp/zcl.h"
using namespace std;

/*****************************************************************************/
/*  gateway task mqtt define
 */
/*****************************************************************************/

/* define signal of task handle message*/
enum{
	HANDLE_MESSAGE_DATA_INCOMING,
	HANDLE_MESSAGE_DATA_INCOMING_LORA,
	HANDLE_MESSAGE_DATA_GSM_MONEY,
};

/* define signal of task MQTT*/
enum{
	MQTT_CTRL_ADD_NEW_ZED_REQ,	/*add a new ZED (zigbee end device)*/
	MQTT_CTRL_GET_DEVICE_LIST_REQ,  /*get device list from  UTIL_GET_DEVICE_INFO (0x2700)*/
	MQTT_CTRL_GET_DEVICE_LIST_RESPONSE,
	MQTT_START_TOKEN_FACTORY,
	MQTT_STOP_TOKEN_FACTORY,
	MQTT_START_TOKEN_COMMUNICATION,
	MQTT_ADD_GATEWAY,
	MQTT_DATA_COMMUNICATION,
	MQTT_SENSOR_DATA,
};


/* define signal of task IF_SOCKET*/
enum{
	IF_SOCKET_ADD_NEW_ZED_RESPONSE,	/*add a new ZED (zigbee end device)*/
	IF_SOCKET_SENSOR_DATA, /*data from sensor devices send to socket*/
	IF_SOCKET_LEAVE_DEVICE_RESPONSE,
	IF_SOCKET_ALIVE,
	IF_SOCKET_GET_DEVICE_LIST,

};

/*define signal of task IF_ADD_GATEWAY*/
enum{
	IF_ADD_GATEWAY_USERNAME,
	IF_ADD_GATEWAY_PASSWORD,
	IF_ADD_GATEWAY_TOPIC,
	IF_ADD_GATEWAY_TOKEN_COMMUNICATE,
};

/* define signal of task LOG */
enum{
	LOG_SENSOR_DATA,
	LOG_SENSOR_DATA_ACTIVITY_DATA,
	LOG_GATEWAY_ACTIVITY,
	LOG_GATEWAY_RESET,
};

/* define signal of task socket online*/
enum{
	IF_SOCKET_ONLINE_SENSOR_DATA, /*data from sensor devices send online socket*/
	IF_SOCKET_ONLINE_ADD_NEW_ZED_RESPONSE,	/*add a new ZED (zigbee end device)*/
};

/* define signal of task scene*/
enum{
    UPDATE_SCENE,      		/*update new scene*/
    DELETE_SCENE,   		/*delete scene */
    GET_SCENE,      		/*get output scene*/
};

/*define signal of task send sms*/
enum{
	UPDATE_FILE_SMS,		/*update new file send sms*/
	DELETE_SMS_FLIE,		/*delete file send sms*/
	GET_SEND_SMS,			/*get output send sms*/
};

/*****************************************************************************/
/*  task MT_UDP_BROADCAST define.
 */
/*****************************************************************************/
/* define timer */
#define GW_UDP_BROADCAST_IF_TIMER_PACKET_DELAY_INTERVAL		(5000) /* milliseconds */

/* define signals task if udp broadcast*/
enum{
	GW_UDP_BROADCAST_SHOT, /*create to execute broadcast udp 1 shot*/
	GW_UDP_MAC_ADDRESS_RSP,/*MAC ADDRESS response from if_znp*/
};

/*define signals task gsm */
enum{
	GSM_SEND_SMS,
	GSM_SEND_AT_COMM,
	GSM_GET_MONEY
};

/*****************************************************************************/
/*  loramac task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */

#define LORAMAC_RADIO_TXTIMEOUT						(1)
#define LORAMAC_RADIO_RXTIMEOUT						(2)
#define LORAMAC_RADIO_RXTIMEOUT_SYNCWORD			(3)

/*****************************************************************************/
/*  lorawan task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define PINGPONG_SEND_TEST							(1)
#define LORAWAN_DEVICE_SEND_PROPRIETARY				(2)
#define LORAWAN_DEVICE_RECV_JOIN_REQ				(3)
#define LORAWAN_DEVICE_RECV_PROPRIETARY				(4)
#define LORAWAN_DEVICE_RECV_CONFIRM					(5)
#define LORAWAN_DEVICE_RECV_UNCONFIRM				(6)

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK									(0x00)
#define APP_NG									(0x01)

#define APP_FLAG_OFF							(0x00)
#define APP_FLAG_ON								(0x01)

#define APP_ROOT_PATH							"./Smart_home"
#define LOG_FILE_NAME							"log_data_"
#define SCENE_FILE_NAME							"scene_data"

#define MY_DEVICE_ADDR							0x00001122

extern app_config gateway_configure;
extern config_parameter_t g_config_parameters;
extern zclOutgoingMsg_t g_outgoingMsg_lastData;
extern const char g_app_version[];
extern int8_t processingDataCommunication(uint8_t *client_message, int32_t len, uint32_t src_task_id, int32_t *outCmd);
#endif // __APP_H__
