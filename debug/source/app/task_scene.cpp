#include "task_scene.h"

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "../ak/ak.h"

#include "../sys/sys_dbg.h"

#include "scene.h"

#include "app.h"
#include "app_dbg.h"
#include "global_parameters.h"

#include "task_list.h"

q_msg_t gw_task_scene_mailbox;

void* gw_task_scene_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("gw_task_scene_entry\n");
	scene_initializer("scene.txt");

	while (1) {
		while (msg_available(ID_TASK_SCENE)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(ID_TASK_SCENE);

			/* handler message */
			switch (msg->header->sig) {
			case DELETE_SCENE: {
				APP_PRINT("delete scene\n");
				delete_scene_file();
			}
				break;
			case GET_SCENE: {
				//zclOutgoingMsg_t* scene_senser_in = (zclOutgoingMsg_t*)msg->header->payload;
				parser_scene_file((uint8_t*) msg->header->payload, msg->header->len);
			}
				break;
			case UPDATE_SCENE: {
				APP_PRINT("GET_SCENE_FROM_SERVER\n");
				char * url = (char*) msg->header->payload;
				APP_PRINT("url=====================: %s\n", url);

				char filepath[256];
				get_scene_path_file(filepath);
				printf("File path: %s\n", filepath);

				bool result = curlPostgetFile(url, filepath);
				if (result == true) {
					APP_PRINT("Scene file Updated!\n");
				} else {
					APP_PRINT("ERROR: updated Scene file!\n");
				}
			}
				break;
			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*) 0;
}

