#include <sys/types.h>
#include <sys/stat.h>

#include <string.h>

#include "../ak/ak.h"

#include "app.h"
#include "app_dbg.h"
#include "../app/znp/zcl.h"
app_config gateway_configure;
config_parameter_t g_config_parameters;
zclOutgoingMsg_t g_outgoingMsg_lastData;
const char g_app_version[] = "02.0.5t";
//string ac_firmware_name = "/ac_application.bin";
//string wr_firmware_name = "/wr_application.bin";

