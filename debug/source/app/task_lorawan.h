#ifndef __TASK_LORAWAN_H__
#define __TASK_LORAWAN_H__

#include "../ak/message.h"

extern q_msg_t gw_task_lorawan_mailbox;
extern void* gw_task_lorawan_entry(void*);


#endif // __TASK_LORAWAN_H__
