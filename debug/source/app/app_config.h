#ifndef __APP_CONFIG_H__
#define __APP_CONFIG_H__

#include <stdint.h>
#include <string>

#include "app.h"

using namespace std;
#define CONFIGURE_PARAMETER_BUFFER_SIZE		256

typedef struct {
	/* parameter for gateway*/
//	uint8_t gateway_name[CONFIGURE_PARAMETER_BUFFER_SIZE];
	uint32_t broadcast_udp_port;
	uint8_t mqtt_host[CONFIGURE_PARAMETER_BUFFER_SIZE];
	uint32_t mqtt_port;

	uint8_t mqtt_username[CONFIGURE_PARAMETER_BUFFER_SIZE];
	uint8_t mqtt_password[CONFIGURE_PARAMETER_BUFFER_SIZE];
	uint8_t mqtt_topic[CONFIGURE_PARAMETER_BUFFER_SIZE];
	uint8_t token_communicate[CONFIGURE_PARAMETER_BUFFER_SIZE];
	uint8_t security_key[CONFIGURE_PARAMETER_BUFFER_SIZE];
    uint8_t start_up_success;

} config_parameter_t;

class app_config {
public:
	app_config();
	void initializer(char*);
	void set_config_path_file(char*);
	void get_config_path_file(char*);
	int parser_config_file(config_parameter_t*, uint8_t *aes_key);
	int write_config_data(config_parameter_t*, uint8_t *aes_key);

private:
	char m_config_path[256];
};

uint8_t read_config_file(uint8_t* aes_key);
#endif //__APP_CONFIG_H__
