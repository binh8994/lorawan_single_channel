#ifndef SCENE_H
#define SCENE_H
#include <stdint.h>
#include "../znp/zcl.h"

//--------------------------------
// conditon of scene
#define EQUAL_SCENE     0
#define MAX_SCENE       1
#define MIN_SCENE       2

//--------------------

typedef struct {
    char m_scene_path[256];
} m_scene_path_t;

extern m_scene_path_t scene_path;

void scene_initializer(char * file_name);
void set_scene_path_file(char*);
void get_scene_path_file(char*);
uint8_t parser_scene_file(uint8_t* data, uint32_t len);
uint8_t add_new_scene_file(uint8_t* message, uint32_t len);
uint8_t delete_scene_file();
uint8_t compera_string(uint8_t* data1, uint8_t* data2, uint32_t len);



//extern string scene_folder;
//extern string scene_file_path;


#endif // SCENE_H
