#include "send_sms.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hal_defs.h"

#include <iostream>
#include <fstream>

#include "../sys/sys_dbg.h"

#include "task_list.h"

#include "global_parameters.h"

string send_sms_folder;
string send_sms_file_path;
m_send_sms_path_t send_sms_path;

void send_sms_initializer(char * file_name) {
	struct stat st = { 0 };
	/* create scene path */
	string send_sms_folder = static_cast<string>(APP_ROOT_PATH) + static_cast<string>("/send_sms/");
	if (stat(send_sms_folder.data(), &st) == -1) {
		mkdir(send_sms_folder.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}

	string send_sms_file_path = send_sms_folder + static_cast<string>((const char*) file_name);

	APP_PRINT("config_file_path: %s\n", send_sms_file_path.c_str());
	strcpy(send_sms_path.m_send_sms_path, send_sms_file_path.c_str());
}

void set_send_sms_path_file(char* path) {
	strcpy(send_sms_path.m_send_sms_path, (const char*) path);
}

void get_send_sms_path_file(char* path) {
	strcpy(path, (const char*) send_sms_path.m_send_sms_path);
}

uint8_t parser_send_sms_file(uint8_t* message, uint32_t len_message) {

	fstream f;
	FILE* configure_file_obj = fopen(send_sms_path.m_send_sms_path, "r");
	if (configure_file_obj == NULL) {
		return -1;
	}
	fclose(configure_file_obj);

	f.open(send_sms_path.m_send_sms_path, ios::in);

	string line;
	uint8_t* pline ;
	uint8_t check_scene;
	uint32_t len_sensor_in;
	uint32_t len_scene_in;
	uint16_t data_sensor;

	char* uint_temp = "°C";
	char* uint_humi = " %";

	parse_communicate_string_t parse_sensor = parseClientString(message, len_message );
	if (parse_sensor.totalString < 6) {
		f.close();
		return FALSE;
	}
	data_sensor = 0;
	// coppy data sensor
	if (parse_sensor.len[5] > 4) { // over 2 Bytes
		f.close();
		return FALSE;
	}
	hexCharsToBytes((uint8_t*) &message[parse_sensor.position[5]], parse_sensor.len[5], (uint8_t*) &data_sensor);

	len_sensor_in = parse_sensor.position[5] - parse_sensor.position[3];

	uint8_t condition;
	uint16_t data_in_line;
	uint32_t timer_sms;
	uint8_t k;
	while (!f.eof()) {
		check_scene = 0;
		getline(f, line);
		pline = (uint8_t*)line.c_str();

		parse_communicate_string_t parse_line = parseClientString(pline, line.length());

		if (parse_line.totalString < 8) {
			continue;
		}
		int8_t i = 0;

		len_scene_in = parse_line.position[2] - parse_line.position[0]; // short address + type data;
		if (len_sensor_in == len_scene_in) {
			if (strncmp((const char*)&message[parse_sensor.position[3]] ,(const char*)&pline[parse_line.position[0]],len_scene_in) == 0) {
				i +=2;
			} else {
			   continue;
			}
		}else {
		   continue;
		}

		// read condition in file
		if (parse_line.len[i] > 2) { // over 1 Bytes
			continue;
		}
		hexCharsToBytes((uint8_t*) &pline[parse_line.position[i]], parse_line.len[i], (uint8_t*) &condition);
		i++;

		data_in_line = 0;
		if (parse_line.len[i] > 4) { // over 2 Bytes
			continue;
		}
		hexCharsToBytes((uint8_t*) &pline[parse_line.position[i]], parse_line.len[i], (uint8_t*) &data_in_line);
		i++;

		//timer
		timer_sms = 0;
		if (parse_line.len[i] <= 8) {
			hexCharsToBytes((uint8_t*) &pline[parse_line.position[i]], parse_line.len[i], (uint8_t*) &timer_sms);
		}
		i++;

		//APP_PRINT("device in found scene action\n");
		switch (condition) {
		case EQUAL_CONDITION: {
			if(data_sensor == data_in_line){
				check_scene = 1;
			}
		}
			break;
		case MAX_CONDITION: {
			if(data_sensor > data_in_line){
				check_scene = 1;
			}
		}
			break;
		case MIN_CONTIDION: {
			if(data_sensor < data_in_line){
				check_scene = 1;
			}
		}
			break;
		default: {
			continue;
		}
			break;
		}

		if(check_scene) {
			uint8_t len_sms,len_data;
			uint8_t* data;

			len_sms = parse_line.len[i];
			len_data = line.length() - parse_line.position[i] + strlen(uint_temp);

			data = (uint8_t*) malloc(len_data);

			if (data == NULL) {
				break;
			}

			k = 0;
			data[k] = parse_line.len[i];
			k++;
			memcpy(&data[k], (uint8_t*) &pline[parse_line.position[i]], parse_line.len[i]);
			k +=parse_line.len[i];
			i++;
			if (g_strSensorType[TEMPERATURE_MEASUREMENT].size() == parse_sensor.len[4]) {
				if (strncmp((const char*)&message[parse_sensor.position[4]] ,g_strSensorType[TEMPERATURE_MEASUREMENT].c_str(),g_strSensorType[TEMPERATURE_MEASUREMENT].size()) == 0) {

					uint8_t len_str;
					uint16_t temperature;
					hexCharsToBytes((uint8_t*) &message[parse_sensor.position[5]], parse_sensor.len[5], (uint8_t*) &temperature);
					temperature =temperature/100;

					data[k] = SEPARATE_CHAR;
					k++;
					bytetostring(temperature,&data[k],&len_str);
					k += len_str;
					memcpy(&data[k], uint_temp, strlen(uint_temp));
					k +=strlen(uint_temp);
					data[0] += len_str + 1 + strlen(uint_temp);
				}
			}

			if (g_strSensorType[RELATIVE_HUMIDITY].size() == parse_sensor.len[4]) {
				if (strncmp((const char*)&message[parse_sensor.position[4]] ,g_strSensorType[RELATIVE_HUMIDITY].c_str(),g_strSensorType[RELATIVE_HUMIDITY].size()) == 0) {

					uint8_t len_str;
					uint16_t humidity;
					hexCharsToBytes((uint8_t*) &message[parse_sensor.position[5]], parse_sensor.len[5], (uint8_t*) &humidity);
					humidity =humidity/100;
					data[k] = SEPARATE_CHAR;
					k++;
					bytetostring(humidity,&data[k],&len_str);
					k += len_str;
					memcpy(&data[k], uint_humi, strlen(uint_humi));
					k += strlen(uint_humi);
					data[0] += len_str + 1 + strlen(uint_humi);
				}
			}

			if (pline[parse_line.position[i]] == '{'){
				i++;
				uint8_t number_list = (parse_line.totalString - i);
				for (uint8_t l = 0; l <number_list; l++) {
					if (pline[parse_line.position[i]] == '}') {
						break;
					}
					if ((parse_line.len[i] >= 10) && (parse_line.len[i] <= 13)) {
						data[k] = parse_line.len[i];
						k++;
						memcpy(&data[k], (uint8_t*) &pline[parse_line.position[i]], parse_line.len[i]);
						k +=parse_line.len[i];
						i++;
					}else {
						i++;
						APP_DBG("error telephone\n");
					}
				}

			}

			ak_msg_t* s_msg = get_dymanic_msg();
			set_msg_sig(s_msg, GSM_SEND_SMS);
			set_data_dynamic_msg(s_msg, (uint8_t*) data, k);
			set_msg_src_task_id(s_msg, ID_TASK_SEND_SMS);
			task_post(ID_TASK_GSM, s_msg);

			free(data);
			f.close();
			return TRUE;
		}
	}
	f.close();
	return TRUE;
}

uint8_t add_new_send_sms_file(uint8_t* message, uint32_t len) {

	FILE* configure_file_obj = fopen(send_sms_path.m_send_sms_path, "w+b");
	if (configure_file_obj == NULL) {
		return -1;
	}
   fwrite(message, len, 1, configure_file_obj);
   fclose(configure_file_obj);
   printf("add new send sms file sucessull\n");

	return TRUE;
}
uint8_t delete_send_sms_file() {

	FILE* configure_file_obj = fopen(send_sms_path.m_send_sms_path, "w");
	if (configure_file_obj == NULL) {
		return -1;
	}
	uint8_t data_str[] = " ";
	fwrite(data_str, strlen((const char*) data_str), 1, configure_file_obj);
	fclose(configure_file_obj);
	APP_PRINT("delete send sms file\n");

	return TRUE;
}
void bytetostring(uint8_t data, uint8_t* str, uint8_t* len) {
	uint8_t i = 0;
	if (data > 100) {
		str[i] = data / 100 + 48;
		data = data %100;
		i++;
	}
	str[i] = data /10 +48;
	i++;
	str[i] = data %10 + 48;
	i++;
	*len = i;

}
