#ifndef TASK_GSM_H
#define TASK_GSM_H

#include "../ak/message.h"

extern q_msg_t gw_task_gsm_mailbox;
extern void* gw_task_gsm_entry(void*);


#endif // TASK_GSM_H
