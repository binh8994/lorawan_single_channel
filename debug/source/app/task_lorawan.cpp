#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "app.h"
//#include "app_if.h"
#include "app_dbg.h"
//#include "app_data.h"

#include "task_list.h"
#include "task_lorawan.h"

#include "loramac/radio/sx1276/sx1276_cfg.h"
#include "loramac/radio/sx1276/sx1276.h"
#include "loramac/mac/LoRaMac-definitions.h"
#include "loramac/mac/LoRaMac.h"
#include "loramac/mac/LoRaMacCrypto.h"

#define LORAWAN_NETWORK_ID					(uint32_t)0
#define LORAWAN_NWKSKEY						{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }
#define LORAWAN_APPSKEY						{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }

static uint32_t NwkID		= LORAWAN_NETWORK_ID;
static uint8_t	NwkSKey[]	= LORAWAN_NWKSKEY;
static uint8_t	AppSKey[]	= LORAWAN_APPSKEY;

//static uint8_t LoRaMacRxPayload[255];
//static uint32_t DownLinkCounter = 0;
//static uint32_t MacCommandsBufferIndex = 0;
#define NOTIFY_BUTTON_ADDR				MY_DEVICE_ADDR
#define DATA_F_SIZE						1
#define ZCL_CLUSTER_ID_GEN_ON_OFF		0x0006
#define ZCL_DATATYPE_UINT8				0x20

/* message communicate endevice and gw */
typedef struct {
	uint16_t clusterId;
	uint32_t deviceAddr;
	uint16_t sequence;
	uint8_t dataType;
	uint8_t dataLen;
	uint8_t data[DATA_F_SIZE];
} __attribute__((__packed__)) lorawan_msg_t;

/* ping pong mode */
RadioEvents_t pingpong_events;
static void pingpong_init(void);
static void on_pingpong_tx_done( void );
static void on_pingpong_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_pingpong_tx_timeout( void );
static void on_pingpong_rx_timeout( void );
static void on_pingpong_rx_error( void );

q_msg_t gw_task_lorawan_mailbox;
void* gw_task_lorawan_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_lorawan_entry\n");

	SX1276IoInit();

	loramac_initial(NwkID, AppSKey, NwkSKey);

	while (1) {
		while (msg_available(ID_TASK_LORAWAN_ID)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(ID_TASK_LORAWAN_ID);

			switch (msg->header->sig) {
			case PINGPONG_SEND_TEST: {
				APP_DBG("PINGPONG_SEND_TEST\n");

				if(Radio.GetStatus() != RF_TX_RUNNING) {
					uint8_t buf[10];
					Radio.Send(buf, 10);
				}

			}
				break;

			case LORAWAN_DEVICE_SEND_PROPRIETARY: {
				APP_DBG("LORAWAN_DEVICE_SEND_PROPRIETARY\n");

				lorawan_msg_t lora_msg;
				//get_data_dynamic_msg(&lora_msg, msg->header->len);

				lora_msg.deviceAddr = NOTIFY_BUTTON_ADDR;
				lora_msg.clusterId = ZCL_CLUSTER_ID_GEN_ON_OFF;
				lora_msg.sequence = 0;
				lora_msg.dataType = ZCL_DATATYPE_UINT8;
				lora_msg.dataLen = DATA_F_SIZE;
				lora_msg.data[0] = 1;

				send_propriatary(&lora_msg, sizeof(lorawan_msg_t));

			}
				break;

			case LORAWAN_DEVICE_RECV_JOIN_REQ: {
				APP_DBG("LORAWAN_DEVICE_RECV_JOIN_REQ\n");

			}
				break;

			case LORAWAN_DEVICE_RECV_PROPRIETARY: {
				APP_DBG("LORAWAN_DEVICE_RECV_PROPRIETARY\n");
				lorawan_msg_t notify_msg;
				get_data_dynamic_msg(msg, (uint8_t*)&notify_msg, msg->header->len);

				APP_DBG("notify_msg.deviceAddr:x%08X\n", notify_msg.deviceAddr	);
				APP_DBG("notify_msg.clusterId:x%04X\n" , notify_msg.clusterId	);
				APP_DBG("notify_msg.sequence:x%04X\n" , notify_msg.sequence	);
				APP_DBG("notify_msg.dataType:x%02X\n" , notify_msg.dataType	);
				APP_DBG("notify_msg.dataLen:x%02X\n" , notify_msg.dataLen	);
				APP_DBG("notify_msg.data:x%02X\n" , notify_msg.data[0]	);

			}
				break;

			case LORAWAN_DEVICE_RECV_CONFIRM: {
				APP_DBG("LORAWAN_DEVICE_RECV_CONFIRM\n");

			}
				break;

			case LORAWAN_DEVICE_RECV_UNCONFIRM: {
				APP_DBG("LORAWAN_DEVICE_RECV_UNCONFIRM\n");

			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(100);
	}

	return (void*)0;
}

static void pingpong_init(void) {

	APP_DBG("init_loramac\n");
	pingpong_events.TxDone =	on_pingpong_tx_done;
	pingpong_events.RxDone =	on_pingpong_rx_done;
	pingpong_events.TxTimeout = on_pingpong_tx_timeout;
	pingpong_events.RxTimeout = on_pingpong_rx_timeout;
	pingpong_events.RxError =	on_pingpong_rx_error;

	Radio.Init( &pingpong_events );

	Radio.SetChannel( SINGLE_CHANNEL_GW_FREQ );

	//Radio.SetPublicNetwork(true);

	Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, SINGLE_CHANNEL_GW_BANDWIDTH,
					   SINGLE_CHANNEL_GW_SF, SINGLE_CHANNEL_GW_CD,
					   SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
					   true, 0, 0, SINGLE_CHANNEL_GW_IQ_TX, 2000 );

	Radio.SetRxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF,
					   SINGLE_CHANNEL_GW_CD, 0, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH,
					   SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
					   0, true, 0, 0, SINGLE_CHANNEL_GW_IQ_RX, true );

	const char* buf = "senFromGW";
	Radio.Send((uint8_t*)buf, strlen(buf));

}

void on_pingpong_tx_done( void )
{
	APP_DBG("on_tx_done\n");
	//Radio.Sleep( );
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}

void on_pingpong_tx_timeout( void )
{
	APP_DBG("on_tx_timeout\n");
	//Radio.Sleep( );
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}

void on_pingpong_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	APP_DBG("on_rx_done-size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);
#if 0
	for(int i=0; i<size; i++) {
		printf("x%02X ", *(payload+i));
	}
	printf("\n");

	LoRaMacHeader_t macHdr;
	LoRaMacFrameCtrl_t fCtrl;

	uint8_t pktHeaderLen = 0;
	uint32_t address = 0;
	uint8_t appPayloadStartIndex = 0;
	uint8_t port = 0xFF;
	uint8_t frameLen = 0;
	uint32_t mic = 0;
	uint32_t micRx = 0;

	uint16_t sequenceCounter = 0;
	uint16_t sequenceCounterPrev = 0;
	uint16_t sequenceCounterDiff = 0;
	uint32_t downLinkCounter = 0;

	bool isMicOk = false;

	Radio.Sleep( );

	macHdr.Value = payload[pktHeaderLen++];

	printf("Parser start\n");

	switch( macHdr.Bits.MType )
	{
	case FRAME_TYPE_JOIN_REQ:
		printf("FRAME_TYPE_JOIN_REQ\n");

		break;

	case FRAME_TYPE_DATA_CONFIRMED_UP:
		printf("FRAME_TYPE_DATA_CONFIRMED_UP\n");
	case FRAME_TYPE_DATA_UNCONFIRMED_UP:
	{
		printf("FRAME_TYPE_DATA_UNCONFIRMED_UP\n");

		address = payload[pktHeaderLen++];
		address |= ( (uint32_t)payload[pktHeaderLen++] << 8 );
		address |= ( (uint32_t)payload[pktHeaderLen++] << 16 );
		address |= ( (uint32_t)payload[pktHeaderLen++] << 24 );

		printf("address:%08X\n", address);

		downLinkCounter = DownLinkCounter;

		fCtrl.Value = payload[pktHeaderLen++];

		printf("fCtrl.Bits.FOptsLen:%08X\n", fCtrl.Bits.FOptsLen);

		sequenceCounter = ( uint16_t )payload[pktHeaderLen++];
		sequenceCounter |= ( uint16_t )payload[pktHeaderLen++] << 8;

		printf("sequenceCounter:%d\n", sequenceCounter);

		appPayloadStartIndex = 8 + fCtrl.Bits.FOptsLen;

		micRx |= ( uint32_t )payload[size - LORAMAC_MFR_LEN];
		micRx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 1] << 8 );
		micRx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 2] << 16 );
		micRx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 3] << 24 );

		printf("micRx:%08X\n", micRx);

		sequenceCounterPrev = ( uint16_t )downLinkCounter;
		sequenceCounterDiff = ( sequenceCounter - sequenceCounterPrev );

		printf("sequenceCounterDiff:%d\n", sequenceCounterDiff);

		if( sequenceCounterDiff < ( 1 << 15 ) )
		{
			downLinkCounter += sequenceCounterDiff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, NwkSKey, address, UP_LINK, downLinkCounter, &mic );
			if( micRx == mic )
			{
				isMicOk = true;
			}
		}
		else
		{
			// check for sequence roll-over
			uint32_t  downLinkCounterTmp = downLinkCounter + 0x10000 + ( int16_t )sequenceCounterDiff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, NwkSKey, address, UP_LINK, downLinkCounterTmp, &mic );
			if( micRx == mic )
			{
				isMicOk = true;
				downLinkCounter = downLinkCounterTmp;
			}
		}

		// Check for a the maximum allowed counter difference
		if( sequenceCounterDiff >= MAX_FCNT_GAP )
		{
			printf("sequenceCounterDiff >= MAX_FCNT_GAP\n");
			printf("Lossed messages\n");
			return;
		}

		if( isMicOk == true )
		{
			if( ( DownLinkCounter == downLinkCounter ) &&
					( DownLinkCounter != 0 ) )
			{
				printf("DownLinkCounter == downLinkCounter\n");
				printf("Message repeated\n");
				return;
			}

			DownLinkCounter = downLinkCounter;

			MacCommandsBufferIndex = 0;

			// Process payload and MAC commands
			if( ( ( size - 4 ) - appPayloadStartIndex ) > 0 )
			{
				port = payload[appPayloadStartIndex++];
				frameLen = ( size - 4 ) - appPayloadStartIndex;

				printf("Port:%d\n", port);

				memset(LoRaMacRxPayload, 0, 255);

				if( port == 0 )
				{
					// Only allow frames which do not have fOpts
					if( fCtrl.Bits.FOptsLen == 0 )
					{
						LoRaMacPayloadDecrypt( payload + appPayloadStartIndex,
											   frameLen,
											   NwkSKey,
											   address,
											   DOWN_LINK,
											   downLinkCounter,
											   LoRaMacRxPayload );

						// Decode frame payload MAC commands
						ProcessMacCommands( LoRaMacRxPayload, 0, frameLen, snr );
					}
				}
				else
				{
					if( fCtrl.Bits.FOptsLen > 0 )
					{
						// Decode Options field MAC commands. Omit the fPort.
						ProcessMacCommands( payload, 8, appPayloadStartIndex - 1, snr );
					}

					LoRaMacPayloadDecrypt( payload + appPayloadStartIndex,
										   frameLen,
										   AppSKey,
										   address,
										   DOWN_LINK,
										   downLinkCounter,
										   LoRaMacRxPayload );

					for(int i = 0; i < frameLen; i++) {
						printf("x%08X ", *(LoRaMacRxPayload+i));
					}
					printf("\n");

					printf("Frame_len:%d\n",frameLen);
					printf("LoRaMacRxPayload:%s\n",LoRaMacRxPayload);

				}
			}
			else
			{
				if( fCtrl.Bits.FOptsLen > 0 )
				{
					// Decode Options field MAC commands
					ProcessMacCommands( payload, 8, appPayloadStartIndex, snr );
				}
			}
		}
		else {
			printf("MIC != RxMIC\n");
		}
	}
		break;

	case FRAME_TYPE_PROPRIETARY:
	{
		printf("FRAME_TYPE_PROPRIETARY\n");

		frameLen = size - pktHeaderLen;

		memcpy( LoRaMacRxPayload, &payload[pktHeaderLen], size );

		for(int i = 0; i < frameLen; i++) {
			printf("x%08X ", *(LoRaMacRxPayload+i));
		}
		printf("\n");

		printf("Frame_len:%d\n",frameLen);
		printf("LoRaMacRxPayload:%s\n",LoRaMacRxPayload);

		break;
	}

	default:

		break;
	}

#endif
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}

void on_pingpong_rx_timeout( void )
{
	APP_DBG("on_rx_timeout\n");
	//Radio.Sleep( );
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}

void on_pingpong_rx_error( void )
{
	APP_DBG("on_rx_error\n");
	//Radio.Sleep( );
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}
