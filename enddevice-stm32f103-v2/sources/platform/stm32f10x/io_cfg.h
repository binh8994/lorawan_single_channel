/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

/*
 * define pin for arduino pinMode/digitalWrite/digitalRead
 * NOTE: define value MUST be deferrent
 */

#define SHT1X_CLK_PIN					(0x01)
#define SHT1X_DATA_PIN					(0x02)

#define SSD1306_CLK_PIN					(0x03)
#define SSD1306_DATA_PIN				(0x04)

#define DS1302_CLK_PIN					(0x05)
#define DS1302_DATA_PIN					(0x06)
#define DS1302_CE_PIN					(0x07)

/*****************************************************************************
 *Pin map led life
******************************************************************************/
#define LED_LIFE_IO_PIN					(GPIO_Pin_14)
#define LED_LIFE_IO_PORT				(GPIOB)
#define LED_LIFE_IO_CLOCK				(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin map Lora SX1276
******************************************************************************/
#define SX1276_CS_IO_PIN				(GPIO_Pin_1)
#define SX1276_CS_IO_PORT				(GPIOB)
#define SX1276_CS_IO_CLOCK				(RCC_APB2Periph_GPIOB)

#define SX1276_RST_IO_PIN				(GPIO_Pin_3)
#define SX1276_RST_IO_PORT				(GPIOB)
#define SX1276_RST_IO_CLOCK				(RCC_APB2Periph_GPIOB)

#define SX1276_DIO0_IO_PIN				(GPIO_Pin_4)
#define SX1276_DIO0_IO_PORT				(GPIOB)
#define SX1276_DIO0_IO_CLOCK			(RCC_APB2Periph_GPIOB)

#define SX1276_DIO1_IO_PIN				(GPIO_Pin_5)
#define SX1276_DIO1_IO_PORT				(GPIOB)
#define SX1276_DIO1_IO_CLOCK			(RCC_APB2Periph_GPIOB)

#define SX1276_DIO2_IO_PIN				(GPIO_Pin_6)
#define SX1276_DIO2_IO_PORT				(GPIOB)
#define SX1276_DIO2_IO_CLOCK			(RCC_APB2Periph_GPIOB)

#define SX1276_DIO3_IO_PIN				(GPIO_Pin_7)
#define SX1276_DIO3_IO_PORT				(GPIOB)
#define SX1276_DIO3_IO_CLOCK			(RCC_APB2Periph_GPIOB)

#define SX1276_DIO4_IO_PIN				(GPIO_Pin_8)
#define SX1276_DIO4_IO_PORT				(GPIOB)
#define SX1276_DIO4_IO_CLOCK			(RCC_APB2Periph_GPIOB)

#define SX1276_DIO5_IO_PIN				(GPIO_Pin_9)
#define SX1276_DIO5_IO_PORT				(GPIOB)
#define SX1276_DIO5_IO_CLOCK			(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin button
******************************************************************************/
#define BUTTON_IO_PIN					(GPIO_Pin_13)
#define BUTTON_IO_PORT					(GPIOB)
#define BUTTON_IO_CLOCK					(RCC_APB2Periph_GPIOB)

/*****************************************************************************
 *Pin map led notify
******************************************************************************/
#define LED_RED_IO_PIN					(GPIO_Pin_13)
#define LED_RED_IO_PORT					(GPIOC)
#define LED_RED_IO_CLOCK				(RCC_APB2Periph_GPIOC)

#define LED_GREEN_IO_PIN				(GPIO_Pin_13)
#define LED_GREEN_IO_PORT				(GPIOC)
#define LED_GREEN_IO_CLOCK				(RCC_APB2Periph_GPIOC)

/*****************************************************************************
 *Pin map DS2411
******************************************************************************/
#define DS2411_IO_PIN					(GPIO_Pin_12)
#define DS2411_IO_PORT					(GPIOA)
#define DS2411_IO_CLOCK					(RCC_AHBPeriph_GPIOA)

/******************************************************************************
* led status function
*******************************************************************************/
extern void led_life_init();
extern void led_life_on();
extern void led_life_off();

/******************************************************************************
* button function
*******************************************************************************/
extern void io_button_init();
extern uint8_t io_button_read();
/******************************************************************************
* led notify function
*******************************************************************************/
extern void led_red_init();
extern void led_red_on();
extern void led_red_off();
extern void led_green_init();
extern void led_green_on();
extern void led_green_off();

/* spi1 config */
extern void spi1_cfg() ;

/******************************************************************************
* LORA SX1276 IO function
*******************************************************************************/
extern void sx1276_io_ctrl_init(void);
extern void sx1276_io_irq_ctrl_init(void);
extern void sx1276_cs_low(void);
extern void sx1276_cs_high(void);
extern void sx1276_rst_high(void);
extern void sx1276_rst_low(void);
extern void sx1276_rst_to_input(void);
extern uint8_t sx1276_transfer(const uint8_t data);

#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
