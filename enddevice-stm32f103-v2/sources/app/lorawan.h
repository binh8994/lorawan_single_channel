#ifndef __LORAWAN_H__
#define __LORAWAN_H__

extern void lorawan_init();
extern void lorawan_join();
extern void lorawan_send();

extern void lora_pingpong_init();
extern void lora_pingpong_send();

#endif // __LORAWAN_H__
