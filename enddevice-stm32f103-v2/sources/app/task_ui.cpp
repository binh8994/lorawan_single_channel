#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../driver/button/button.h"
#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_ui.h"
#include "lorawan.h"
#include "app_bsp.h"


void task_ui(ak_msg_t* msg) {
	switch (msg->sig) {
	case AK_BUTTON_ONE_CLICK:

		lorawan_send();
		button_disable(&button);

		break;

	case AK_BUTTON_LONG_PRESS:

		sys_ctrl_reset();

		break;

	default:
		break;
	}
}
