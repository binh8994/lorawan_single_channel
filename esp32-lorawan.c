#join request
		macHdr.Value = 0;
		macHdr.Bits.MType  = FRAME_TYPE_JOIN_REQ;

		LoRaMacFrameCtrl_t fCtrl;

		fCtrl.Value = 0;
		fCtrl.Bits.FOptsLen      = 0;
		fCtrl.Bits.FPending      = 0;
		fCtrl.Bits.Ack           = false;
		fCtrl.Bits.AdrAckReq     = false;
		fCtrl.Bits.Adr           = AdrCtrlOn;

		uint16_t i;
		uint8_t pktHeaderLen = 0;
		uint32_t mic = 0;
		const void* payload = fBuffer;
		uint8_t framePort = fPort;

		LoRaMacBufferPktLen = 0;

		if( fBuffer == NULL )
		{
			fBufferSize = 0;
		}

		LoRaMacTxPayloadLen = fBufferSize;

		LoRaMacBuffer[pktHeaderLen++] = macHdr->Value;

		LoRaMacBufferPktLen = pktHeaderLen;

		memcpyr( LoRaMacBuffer + LoRaMacBufferPktLen, LoRaMacAppEui, 8 );
		LoRaMacBufferPktLen += 8;
		memcpyr( LoRaMacBuffer + LoRaMacBufferPktLen, LoRaMacDevEui, 8 );
		LoRaMacBufferPktLen += 8;

		LoRaMacDevNonce = Radio.Random( );

		LoRaMacBuffer[LoRaMacBufferPktLen++] = LoRaMacDevNonce & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen++] = ( LoRaMacDevNonce >> 8 ) & 0xFF;

		LoRaMacJoinComputeMic( LoRaMacBuffer, LoRaMacBufferPktLen & 0xFF, LoRaMacAppKey, &mic );

		LoRaMacBuffer[LoRaMacBufferPktLen++] = mic & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen++] = ( mic >> 8 ) & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen++] = ( mic >> 16 ) & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen++] = ( mic >> 24 ) & 0xFF;

#join accept parser
		LoRaMacJoinDecrypt( payload + 1, size - 1, LoRaMacAppKey, LoRaMacRxPayload + 1 );
		LoRaMacRxPayload[0] = macHdr.Value;
		LoRaMacJoinComputeMic( LoRaMacRxPayload, size - LORAMAC_MFR_LEN, LoRaMacAppKey, &mic );
		micRx |= ( uint32_t )LoRaMacRxPayload[size - LORAMAC_MFR_LEN];
		micRx |= ( ( uint32_t )LoRaMacRxPayload[size - LORAMAC_MFR_LEN + 1] << 8 );
		micRx |= ( ( uint32_t )LoRaMacRxPayload[size - LORAMAC_MFR_LEN + 2] << 16 );
		micRx |= ( ( uint32_t )LoRaMacRxPayload[size - LORAMAC_MFR_LEN + 3] << 24 );

		if( micRx == mic )
		{
			LoRaMacJoinComputeSKeys( LoRaMacAppKey, LoRaMacRxPayload + 1, LoRaMacDevNonce, LoRaMacNwkSKey, LoRaMacAppSKey );

			SYS_DBG("LoRaMacNwkSKey\n");
			for (int i=0; i< 16; i++) SYS_DBG("x%02X ", LoRaMacNwkSKey[i]);
			SYS_DBG("\n");

			SYS_DBG("LoRaMacAppSKey\n");
			for (int i=0; i< 16; i++) SYS_DBG("x%02X ", LoRaMacAppSKey[i]);
			SYS_DBG("\n");

			LoRaMacNetID = ( uint32_t )LoRaMacRxPayload[4];
			LoRaMacNetID |= ( ( uint32_t )LoRaMacRxPayload[5] << 8 );
			LoRaMacNetID |= ( ( uint32_t )LoRaMacRxPayload[6] << 16 );

			SYS_DBG("LoRaMacNetID x%08X\n", LoRaMacNetID);

			LoRaMacDevAddr = ( uint32_t )LoRaMacRxPayload[7];
			LoRaMacDevAddr |= ( ( uint32_t )LoRaMacRxPayload[8] << 8 );
			LoRaMacDevAddr |= ( ( uint32_t )LoRaMacRxPayload[9] << 16 );
			LoRaMacDevAddr |= ( ( uint32_t )LoRaMacRxPayload[10] << 24 );

			SYS_DBG("LoRaMacDevAddr x%08X\n", LoRaMacDevAddr);

			// DLSettings
			LoRaMacParams.Rx1DrOffset = ( LoRaMacRxPayload[11] >> 4 ) & 0x07;
			LoRaMacParams.Rx2Channel.Datarate = LoRaMacRxPayload[11] & 0x0F;

			SYS_DBG("Rx1DrOffset x%02X\n", ( LoRaMacRxPayload[11] >> 4 ) & 0x07);
			SYS_DBG("Rx2Channel.Datarate x%02X\n", LoRaMacRxPayload[11] & 0x0F);

			// RxDelay
			LoRaMacParams.ReceiveDelay1 = ( LoRaMacRxPayload[12] & 0x0F );
			SYS_DBG("ReceiveDelay1 x%02X\n", LoRaMacRxPayload[12] & 0x0F);

			if( LoRaMacParams.ReceiveDelay1 == 0 )
			{
				LoRaMacParams.ReceiveDelay1 = 1;
			}
			LoRaMacParams.ReceiveDelay1 *= 1e3;
			LoRaMacParams.ReceiveDelay2 = LoRaMacParams.ReceiveDelay1 + 1e3;\


		}


#send unconfirm
		SYS_DBG("SEND_TYPE_UNCONFIRMED\n");

		LoRaMacHeader_t macHdr;
		LoRaMacFrameCtrl_t fCtrl;
		uint8_t* buffer;
		uint8_t lora_mac_tx_pay_len;
		uint32_t device_addr;
		uint32_t down_link_counter;
		uint8_t port;
		uint32_t mic = 0;
		uint8_t* loramac_buffer;
		uint8_t pktHeaderLen = 0;
		uint16_t loramac_buf_pkt_len = 0;
		bool device_ack_req;

		uint8_t* nwkskey;
		uint8_t* appskey;

		device_addr = data->unconfirmed.device_addr;
		SYS_DBG("device_address:x%08X\n", device_addr);

		lorawan_device_mng_t* current_device;
		if((current_device = device_list.get_device_by(device_addr)) == NULL ) {
			SYS_DBG("device not found\n");
			break;
		}

		nwkskey = current_device->nwkskey;
		appskey = current_device->appskey;
		down_link_counter = current_device->down_link_cnt;
		device_ack_req = current_device->device_ack_req;

		lora_mac_tx_pay_len = data->unconfirmed.data_len;
		buffer = (uint8_t*)data->unconfirmed.data;
		if(buffer == NULL) break;

		port = data->unconfirmed.port;

		loramac_buffer = (uint8_t*) malloc(LORAMAC_PHY_MAXPAYLOAD);
		if(loramac_buffer == NULL) break;

		macHdr.Value = 0;
		macHdr.Bits.MType = FRAME_TYPE_DATA_UNCONFIRMED_DOWN;
		fCtrl.Value = 0;
		fCtrl.Bits.FOptsLen      = 0;
		fCtrl.Bits.FPending      = 1;
		fCtrl.Bits.Ack           = device_ack_req;
		fCtrl.Bits.AdrAckReq     = 0;
		fCtrl.Bits.Adr           = 0;

		loramac_buffer[pktHeaderLen++] = macHdr.Value;

		loramac_buffer[pktHeaderLen++] = ( device_addr ) & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( device_addr >> 8 ) & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( device_addr >> 16 ) & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( device_addr >> 24 ) & 0xFF;

		loramac_buffer[pktHeaderLen++] = fCtrl.Value;

		loramac_buffer[pktHeaderLen++] = down_link_counter & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( down_link_counter >> 8 ) & 0xFF;
		//SYS_DBG("down_link_counter:x%08X\n", down_link_counter);

		if(lora_mac_tx_pay_len > 0 ) {
			loramac_buffer[pktHeaderLen++] = port;
			if( port == 0 ) {
				LoRaMacPayloadEncrypt( (uint8_t* ) buffer, lora_mac_tx_pay_len, \
									   nwkskey, device_addr, DOWN_LINK, \
									   down_link_counter, &loramac_buffer[pktHeaderLen] );
			}
			else {
				LoRaMacPayloadEncrypt( (uint8_t* ) buffer, lora_mac_tx_pay_len, \
									   appskey, device_addr, DOWN_LINK, \
									   down_link_counter, &loramac_buffer[pktHeaderLen] );
			}
		}

		loramac_buf_pkt_len = pktHeaderLen + lora_mac_tx_pay_len;

		LoRaMacComputeMic( loramac_buffer, loramac_buf_pkt_len, nwkskey, device_addr, \
						   DOWN_LINK, down_link_counter, &mic );
		//SYS_DBG("mic_tx:x%08X\n", mic);

		loramac_buffer[loramac_buf_pkt_len + 0] =   mic & 0xFF;
		loramac_buffer[loramac_buf_pkt_len + 1] = ( mic >> 8 ) & 0xFF;
		loramac_buffer[loramac_buf_pkt_len + 2] = ( mic >> 16 ) & 0xFF;
		loramac_buffer[loramac_buf_pkt_len + 3] = ( mic >> 24 ) & 0xFF;

		loramac_buf_pkt_len += LORAMAC_MFR_LEN;

		Radio.SetMaxPayloadLength( MODEM_LORA, loramac_buf_pkt_len );
		Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, \
						   SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF, \
						   SINGLE_CHANNEL_GW_CD, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, \
						   SINGLE_CHANNEL_GW_FIX_LENGTH_PAY, true, 0, 0, \
						   SINGLE_CHANNEL_GW_IQ_TX, 3000 );
		Radio.Send( loramac_buffer, loramac_buf_pkt_len );

		SYS_DBG("loramac_buffer:\n");
		for (int i=0; i< loramac_buf_pkt_len; i++) SYS_DBG("x%02X ", loramac_buffer[i]);
		SYS_DBG("\n");

		free(loramac_buffer);

		/*update this device into file*/
		device_ack_req = false;
		down_link_counter++;
		current_device->device_ack_req = device_ack_req;
		current_device->down_link_cnt = down_link_counter;
		device_list.update_file();


#recv unconfirm

		LoRaMacFrameCtrl_t fCtrl;
		uint32_t mic_rx = 0;
		uint32_t compute_mic = 0;
		uint8_t frame_len = 0;
		uint8_t *nwkSKey = NULL;
		uint8_t *appSKey = NULL;

		uint32_t address = 0;
		uint8_t* loramac_rx_payload = NULL;
		uint8_t app_payload_start_index = 0;
		uint8_t port = 0xFF;

		uint16_t sequence_counter = 0;
		uint16_t sequence_counter_prev = 0;
		uint16_t sequence_counter_diff = 0;
		uint32_t up_link_counter = 0;

		address = payload[pkt_header_len++];
		address |= ( (uint32_t)payload[pkt_header_len++] << 8 );
		address |= ( (uint32_t)payload[pkt_header_len++] << 16 );
		address |= ( (uint32_t)payload[pkt_header_len++] << 24 );
		SYS_DBG("device_address:x%08X\n", address);

		lorawan_device_mng_t* current_device;
		if(device_list.device_exist(address) != 0) {
			SYS_DBG("device not found\n");
			break;
		}
		else {
			current_device = device_list.get_device_by(address);
			//SYS_DBG("device found\n");
		}

		nwkSKey = (uint8_t*)current_device->nwkskey;
		appSKey = (uint8_t*)current_device->appskey;

		up_link_counter = current_device->up_link_cnt;

		fCtrl.Value = payload[pkt_header_len++];
		//SYS_DBG("FOpts_len:%d\n", fCtrl.Bits.FOptsLen);

		sequence_counter = ( uint16_t )payload[pkt_header_len++];
		sequence_counter |= ( uint16_t )payload[pkt_header_len++] << 8;
		SYS_DBG("sequence_counter:%d\n", sequence_counter);

		app_payload_start_index = 8 + fCtrl.Bits.FOptsLen;

		mic_rx |= (   uint32_t )payload[size - LORAMAC_MFR_LEN];
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 1] << 8 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 2] << 16 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 3] << 24 );
		//SYS_DBG("mic_rx:x%08X\n", mic_rx);

		sequence_counter_prev = ( uint16_t )up_link_counter;
		sequence_counter_diff = ( sequence_counter - sequence_counter_prev );
		//SYS_DBG("sequence_counter_diff:%d\n", sequence_counter_diff);

		if( sequence_counter_diff < ( 1 << 15 ) ) {
			up_link_counter += sequence_counter_diff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, nwkSKey, address, UP_LINK, up_link_counter, &compute_mic );
			//SYS_DBG("compute_mic:x%08X\n", compute_mic);
			if( mic_rx == compute_mic ) {
				isMicOk = true;
				SYS_DBG("mic_rx=compute_mic\n");
			}
		}
		else {
			// check for sequence roll-over
			uint32_t  down_link_counter_tmp = up_link_counter + 0x10000 + ( int16_t )sequence_counter_diff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, \
							   appSKey, address, UP_LINK, \
							   down_link_counter_tmp, &compute_mic );
			//SYS_DBG("roll-over, compute_mic:x%08X\n", compute_mic);
			if( mic_rx == compute_mic ) {
				isMicOk = true;
				//SYS_DBG("mic_rx=compute_mic\n");
				up_link_counter = down_link_counter_tmp;
			}
		}

		if( isMicOk == true ) {
			if( ( current_device->up_link_cnt == up_link_counter ) &&
					( current_device->up_link_cnt != 0 ) ) {
				SYS_DBG("message repeated\n");
				break;
			}

			current_device->up_link_cnt = up_link_counter;

			// Process payload and MAC commands
			if( ( ( size - 4 ) - app_payload_start_index ) > 0 ) {
				port = payload[app_payload_start_index++];
				frame_len = ( size - 4 ) - app_payload_start_index;

				SYS_DBG("port:%d\n", port);
				SYS_DBG("frame_len:%d\n", frame_len);

				loramac_rx_payload = (uint8_t*)malloc(frame_len);
				if(loramac_rx_payload == NULL) break;
				memset(loramac_rx_payload, 0, frame_len);

				if( port == 0 ) {
					// Only allow frames which do not have fOpts
					if( fCtrl.Bits.FOptsLen == 0 )
					{
						LoRaMacPayloadDecrypt( payload + app_payload_start_index,
											   frame_len,
											   nwkSKey,
											   address,
											   UP_LINK,
											   up_link_counter,
											   loramac_rx_payload );

						// Decode frame payload MAC commands
						//process_mac_cmd( loramac_rx_pay_len, 0, frameLen, snr );
					}
				}
				else
				{
					if( fCtrl.Bits.FOptsLen > 0 ) {
						// Decode Options field MAC commands. Omit the fPort.
						//process_mac_cmd( payload, 8, appPayloadStartIndex - 1, snr );
					}

					LoRaMacPayloadDecrypt( payload + app_payload_start_index,
										   frame_len,
										   appSKey,
										   address,
										   UP_LINK,
										   up_link_counter,
										   loramac_rx_payload );

					SYS_DBG("loramac_rx_payload:");
					for(int i = 0; i < frame_len; i++) SYS_DBG("x%02X ", loramac_rx_payload[i]);
					SYS_DBG("\n");

					lorawan_data_t lorawan_data;
					memset(&lorawan_data, 0, sizeof(lorawan_data_t));

					if( macHdr.Bits.MType == FRAME_TYPE_DATA_CONFIRMED_UP ) {
						current_device->device_ack_req = true;

						lorawan_data.type_msg = CONFIRMED;
						lorawan_data.confirmed.port = port;
						lorawan_data.confirmed.device_addr = address;
						lorawan_data.confirmed.data_len = frame_len;
						lorawan_data.confirmed.data = (uint8_t*)loramac_rx_payload;
						lorawan_data.confirmed.receive_ack = fCtrl.Bits.Ack;

					}
					else {
						current_device->device_ack_req = false;

						lorawan_data.type_msg = UNCONFIRMED;
						lorawan_data.unconfirmed.port = port;
						lorawan_data.unconfirmed.device_addr = address;
						lorawan_data.unconfirmed.data_len = frame_len;
						lorawan_data.unconfirmed.data = (uint8_t*)loramac_rx_payload;
						lorawan_data.unconfirmed.receive_ack = fCtrl.Bits.Ack;

					}

					device_list.update_file();

					ak_msg_t* smsg = get_dymanic_msg();
					set_data_dynamic_msg(smsg, (uint8_t*)&lorawan_data, sizeof(lorawan_data_t));
					set_msg_sig(smsg, LORAWAN_DEVICE_RECV);
					task_post(MT_TASK_LORAWAN_ID, smsg);


				}
			}
			else {
				if( fCtrl.Bits.FOptsLen > 0 ) {
					// Decode Options field MAC commands
					//process_mac_cmd( payload, 8, appPayloadStartIndex, snr );
				}
			}
		}
		else {
			SYS_DBG("mic_rx!=compute_mic\n");
		}


