#include "task_list.h"
#include "../ak/timer.h"

task_t app_task_table[] = {
	/*************************************************************************/
	/* SYSTEM TASK */
	/*************************************************************************/
	{TASK_TIMER_TICK_ID,	TASK_PRI_LEVEL_7,		task_timer_tick			},

	/*************************************************************************/
	/* APP TASK */
	/*************************************************************************/
	{AK_TASK_SHELL_ID		,	TASK_PRI_LEVEL_2	,	task_shell			},
	{AK_TASK_LIFE_ID		,	TASK_PRI_LEVEL_6	,	task_life			},
	{AK_TASK_LORAMAC_ID		,	TASK_PRI_LEVEL_5	,	task_loramac		},
	{AK_TASK_UI_ID			,	TASK_PRI_LEVEL_4	,	task_ui			},

	/*************************************************************************/
	/* END OF TABLE */
	/*************************************************************************/
	{AK_TASK_EOT_ID,			TASK_PRI_LEVEL_0,		(pf_task)0			}
};
