#include <string.h>

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../platform/stm32f10x/io_cfg.h"
#include "../common/xprintf.h"
#include "../driver/led/led.h"
#include "../driver/button/button.h"
#include "app_bsp.h"

#include "task_life.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"

#include "loramac/radio/sx1276/sx1276_cfg.h"
#include "loramac/radio/sx1276/sx1276.h"

#include "loramac/mac/LoRaMac-definitions.h"
#include "loramac/mac/LoRaMac.h"
#include "loramac/mac/LoRaMacCrypto.h"

#include "../crypto/aes.h"
#include "../crypto/cmac.h"

#include "lorawan.h"

#define OVER_THE_AIR_ACTIVATION

#define LORAWAN_DEVICE_ADDRESS				0x00001122//only for ABP
static const uint32_t	DevAddr		= LORAWAN_DEVICE_ADDRESS;

#ifdef  OVER_THE_AIR_ACTIVATION

#define IEEE_OUI							0xFF, 0xFF, 0xFF
#define LORAWAN_DEVICE_EUI					{ IEEE_OUI, 0x01, 0x02, 0x03, 0x04, 0x05 }
#define LORAWAN_APPLICATION_EUI				{ 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA }
#define LORAWAN_APPLICATION_KEY				{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }

static uint8_t DevEui[] = LORAWAN_DEVICE_EUI;
static uint8_t AppEui[] = LORAWAN_APPLICATION_EUI;
static uint8_t AppKey[] = LORAWAN_APPLICATION_KEY;
#else

#define LORAWAN_NETWORK_ID					0
#define LORAWAN_NWKSKEY						{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }
#define LORAWAN_APPSKEY						{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }

static const uint32_t	NwkID		= LORAWAN_NETWORK_ID;
static const uint8_t	NwkSKey[]	= LORAWAN_NWKSKEY;
static const uint8_t	AppSKey[]	= LORAWAN_APPSKEY;
#endif

static LoRaMacPrimitives_t LoRaMacPrimitives;
static LoRaMacCallback_t LoRaMacCallbacks;
static MibRequestConfirm_t mibReq;

static void McpsConfirm( McpsConfirm_t *mcpsConfirm );
static void McpsIndication( McpsIndication_t *mcpsIndication );
static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm );

#define APP_PORT						80
#define DATA_F_SIZE						1
#define ZCL_CLUSTER_ID_GEN_ON_OFF		0x0006
#define ZCL_DATATYPE_UINT8				0x20

/* message communicate endevice and gw */
typedef struct {
	uint16_t clusterId;
	uint8_t dataType;
	uint8_t dataLen;
	uint8_t data[DATA_F_SIZE];
} __attribute__((__packed__)) lorawan_msg_t;

static lorawan_msg_t notify_msg;

static void McpsConfirm( McpsConfirm_t *mcpsConfirm ) {
	if( mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
		switch( mcpsConfirm->McpsRequest ) {
		case MCPS_UNCONFIRMED: {
			APP_DBG("McpsConfirm-UNCONFIRMED\n");
			led_blink_reset(&led_life);
			button_enable(&button);
			break;
		}
		case MCPS_CONFIRMED: {
			APP_DBG("McpsConfirm-CONFIRMED\n");
			break;
		}
		case MCPS_PROPRIETARY: {
			APP_DBG("McpsConfirm-PROPRIETARY\n");
			break;
		}
		default:
			break;
		}

	}
}

static void McpsIndication( McpsIndication_t *mcpsIndication ) {
	if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK ) {
		return;
	}

	switch( mcpsIndication->McpsIndication ) {
	case MCPS_UNCONFIRMED: {
		APP_DBG("McpsIndication-UNCONFIRMED\n");

		lorawan_msg_t command_msg;
		memcpy(&command_msg, mcpsIndication->Buffer, sizeof(lorawan_msg_t));

		APP_DBG("command_msg.clusterId:x%04X\n" , command_msg.clusterId	);
		APP_DBG("command_msg.dataType:x%02X\n" , command_msg.dataType);
		APP_DBG("command_msg.dataLen:x%02X\n" , command_msg.dataLen	);
		APP_DBG("command_msg.data:x%02X\n" , command_msg.data[0]);

		if(command_msg.clusterId == ZCL_CLUSTER_ID_GEN_ON_OFF) {
			if(command_msg.data[0] == 1) {
				APP_DBG("\n\n\t[0]\n\n");
				led_green_on();
				led_red_off();
			}
			else {
				APP_DBG("\n\n\t[1]\n\n");
				led_red_on();
				led_green_off();
			}
		}


		button_enable(&button);

		break;
	}
	case MCPS_CONFIRMED: {
		APP_DBG("McpsIndication-MCPS_CONFIRMED\n");
		break;
	}
	case MCPS_PROPRIETARY: {
		APP_DBG("McpsIndication-MCPS_PROPRIETARY\n");
		break;
	}
	case MCPS_MULTICAST: {
		APP_DBG("McpsIndication:MCPS_MULTICAST\n");
		break;
	}
	default:
		break;
	}

	if( mcpsIndication->RxData == true ) {
		APP_DBG("McpsIndication-RxData:true\n");
	}
}

static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm ) {
	switch( mlmeConfirm->MlmeRequest ) {
	case MLME_JOIN: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			APP_DBG("MlmeConfirm-JOIN success\n");
		}
		else {
			APP_DBG("MlmeConfirm-JOIN fail\n");
			lorawan_join();
		}
		break;
	}
	case MLME_LINK_CHECK: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			APP_DBG("MlmeConfirm-LINK_CHECK\n");
		}
		break;
	}
	default:
		break;
	}

}


void lorawan_init() {
	APP_DBG("lorawan_init\n");

	LoRaMacPrimitives.MacMcpsConfirm = McpsConfirm;
	LoRaMacPrimitives.MacMcpsIndication = McpsIndication;
	LoRaMacPrimitives.MacMlmeConfirm = MlmeConfirm;
	LoRaMacCallbacks.GetBatteryLevel = 0;
	LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks );

	mibReq.Type = MIB_ADR;
	mibReq.Param.AdrEnable = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_REPEATER_SUPPORT;
	mibReq.Param.EnableRepeaterSupport = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_PUBLIC_NETWORK;
	mibReq.Param.EnablePublicNetwork = true;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_DEVICE_CLASS;
	mibReq.Param.Class = CLASS_C;
	LoRaMacMibSetRequestConfirm( &mibReq );

	/* init message to send */
	notify_msg.clusterId  = ZCL_CLUSTER_ID_GEN_ON_OFF;
	notify_msg.dataType = ZCL_DATATYPE_UINT8;
	notify_msg.dataLen = DATA_F_SIZE;
	notify_msg.data[0] = 1;

}

void lorawan_join() {
	APP_DBG("lorawan_join\n");

#ifdef OVER_THE_AIR_ACTIVATION
	MlmeReq_t mlmeReq;

	mlmeReq.Type = MLME_JOIN;
	mlmeReq.Req.Join.DevEui = DevEui;
	mlmeReq.Req.Join.AppEui = AppEui;
	mlmeReq.Req.Join.AppKey = AppKey;
	mlmeReq.Req.Join.NbTrials = 1;

	LoRaMacMlmeRequest( &mlmeReq );

#else

	mibReq.Type = MIB_NET_ID;
	mibReq.Param.NetID = NwkID;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_DEV_ADDR;
	mibReq.Param.DevAddr = DevAddr;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_NWK_SKEY;
	mibReq.Param.NwkSKey = (uint8_t*)NwkSKey;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_APP_SKEY;
	mibReq.Param.AppSKey = (uint8_t*)AppSKey;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_NETWORK_JOINED;
	mibReq.Param.IsNetworkJoined = true;
	LoRaMacMibSetRequestConfirm( &mibReq );

#endif
}

void lorawan_send() {
	APP_DBG("lorawan_send\n");

	MibRequestConfirm_t mibReq;
	LoRaMacStatus_t status;

	mibReq.Type = MIB_NETWORK_JOINED;
	status = LoRaMacMibGetRequestConfirm( &mibReq );

	if( status == LORAMAC_STATUS_OK ) {
		if( mibReq.Param.IsNetworkJoined == true ) {

			McpsReq_t mcpsReq;
			LoRaMacTxInfo_t txInfo;
			uint16_t data_size = sizeof(notify_msg);
			if( LoRaMacQueryTxPossible( data_size, &txInfo ) != LORAMAC_STATUS_OK )
			{
				// Send empty frame in order to flush MAC commands
				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = 0;
				mcpsReq.Req.Unconfirmed.fBuffer = 0;
				mcpsReq.Req.Unconfirmed.fBufferSize = 0;
				mcpsReq.Req.Unconfirmed.Datarate = LORAMAC_DEFAULT_DATARATE;

				LoRaMacMcpsRequest( &mcpsReq );

			}
			else {

				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = APP_PORT;
				mcpsReq.Req.Unconfirmed.fBuffer = &notify_msg;
				mcpsReq.Req.Unconfirmed.fBufferSize = data_size;
				mcpsReq.Req.Unconfirmed.Datarate = LORAMAC_DEFAULT_DATARATE;

				LoRaMacMcpsRequest( &mcpsReq );

				/* Led indication TX Running */
				led_blink_set(&led_life, 10, 5);
			}
		}
	}
	else {
		APP_DBG("joined not yet\n");
	}
}
