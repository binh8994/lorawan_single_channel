#ifndef __TASK_LORAMAC_H__
#define __TASK_LORAMAC_H__

#include "../ak/message.h"

extern q_msg_t mt_task_loramac_mailbox;
extern void* mt_task_loramac_entry(void*);

#endif // __TASK_LORAMAC_H__
