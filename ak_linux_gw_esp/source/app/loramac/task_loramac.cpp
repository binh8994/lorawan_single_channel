#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "app.h"
#include "app_if.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"
#include "task_loramac.h"
#include "task_lorawan.h"

#include "loramac/radio/sx1276/sx1276_cfg.h"
#include "loramac/radio/sx1276/sx1276.h"
#include "loramac/mac/LoRaMac-definitions.h"
#include "loramac/mac/LoRaMac.h"
#include "loramac/mac/LoRaMacCrypto.h"

q_msg_t mt_task_loramac_mailbox;
void* mt_task_loramac_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_loramac_entry\n");

	while (1) {
		while (msg_available(MT_TASK_LORAMAC_ID)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(MT_TASK_LORAMAC_ID);

			switch (msg->header->sig) {
			case LORAMAC_RADIO_TXTIMEOUT:
			case LORAMAC_RADIO_RXTIMEOUT: {

				SX1276OnTimeoutIrq();

			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(100);
	}

	return (void*)0;
}
