#ifndef __IF_APP_H__
#define __IF_APP_H__

#include "../ak/message.h"

extern q_msg_t mt_task_if_app_mailbox;
extern void* mt_task_if_app_entry(void*);

#endif //__IF_APP_H__
