/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: LoRa MAC layer global definitions

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
#ifndef __LORAMAC_BOARD_H__
#define __LORAMAC_BOARD_H__

/*!
 * Returns individual channel mask
 *
 * \param[IN] channelIndex Channel index 1 based
 * \retval channelMask
 */
#define USE_BAND_433

#define DelayMs		sys_ctrl_delay_ms
#define MIN( a, b ) ( ( ( a ) < ( b ) ) ? ( a ) : ( b ) )
#define MAX( a, b ) ( ( ( a ) > ( b ) ) ? ( a ) : ( b ) )
#define POW2( n ) ( 1 << n )

#define LC( channelIndex )            ( uint16_t )( 1 << ( channelIndex - 1 ) )

#if defined( USE_BAND_433 )

/*!
 * LoRaMac maximum number of channels
 */
#define LORA_MAX_NB_CHANNELS                        16

/*!
 * Minimal datarate that can be used by the node
 */
#define LORAMAC_TX_MIN_DATARATE                     DR_0

/*!
 * Maximal datarate that can be used by the node
 */
#define LORAMAC_TX_MAX_DATARATE                     DR_7

/*!
 * Minimal datarate that can be used by the node
 */
#define LORAMAC_RX_MIN_DATARATE                     DR_0

/*!
 * Maximal datarate that can be used by the node
 */
#define LORAMAC_RX_MAX_DATARATE                     DR_7

/*!
 * Default datarate used by the node
 */
#define LORAMAC_DEFAULT_DATARATE                    SINGLE_CHANNEL_GW_DR

/*!
 * Minimal Rx1 receive datarate offset
 */
#define LORAMAC_MIN_RX1_DR_OFFSET                   0

/*!
 * Maximal Rx1 receive datarate offset
 */
#define LORAMAC_MAX_RX1_DR_OFFSET                   5

/*!
 * Minimal Tx output power that can be used by the node
 */
#define LORAMAC_MIN_TX_POWER                        TX_POWER_M5_DBM

/*!
 * Maximal Tx output power that can be used by the node
 */
#define LORAMAC_MAX_TX_POWER                        TX_POWER_10_DBM

/*!
 * Default Tx output power used by the node
 */
#define LORAMAC_DEFAULT_TX_POWER                    TX_POWER_10_DBM

/*!
 * LoRaMac TxPower definition
 */
#define TX_POWER_10_DBM                             0
#define TX_POWER_07_DBM                             1
#define TX_POWER_04_DBM                             2
#define TX_POWER_01_DBM                             3
#define TX_POWER_M2_DBM                             4
#define TX_POWER_M5_DBM                             5

/*!
 * LoRaMac datarates definition
 */
#define DR_0                                        0  // SF12 - BW125
#define DR_1                                        1  // SF11 - BW125
#define DR_2                                        2  // SF10 - BW125
#define DR_3                                        3  // SF9  - BW125
#define DR_4                                        4  // SF8  - BW125
#define DR_5                                        5  // SF7  - BW125
#define DR_6                                        6  // SF7  - BW250
#define DR_7                                        7  // FSK

/*!
 * Verification of default datarate
 */
#if ( LORAMAC_DEFAULT_DATARATE > DR_5 )
#error "A default DR higher than DR_5 may lead to connectivity loss."
#endif

/*!
 * Second reception window channel definition.
 */
// Channel = { Frequency [Hz], Datarate }
#define RX_WND_2_CHANNEL                                  { SINGLE_CHANNEL_GW_FREQ, SINGLE_CHANNEL_GW_DR }

/*!
 * LoRaMac maximum number of bands
 */
#define LORA_MAX_NB_BANDS                           1

// Band = { DutyCycle, TxMaxPower, LastTxDoneTime, TimeOff }
#define BAND0              { 100, TX_POWER_10_DBM, 0,  0 } //  1.0 %

/*!
 * LoRaMac default channels
 */
// Channel = { Frequency [Hz], { ( ( DrMax << 4 ) | DrMin ) }, Band }
#define LC1                { SINGLE_CHANNEL_GW_FREQ, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC2                { SINGLE_CHANNEL_GW_FREQ, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC3                { SINGLE_CHANNEL_GW_FREQ, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }

/*!
 * LoRaMac channels which are allowed for the join procedure
 */
#define JOIN_CHANNELS      ( uint16_t )( LC( 1 ) | LC( 2 ) | LC( 3 ) )

/* binhnt61 */
/* BEGIN */
#define SINGLE_CHANNEL_GW_FREQ						433175000
#define SINGLE_CHANNEL_GW_BANDWIDTH					0
#define SINGLE_CHANNEL_GW_SF						12
#define SINGLE_CHANNEL_GW_DR						DR_0
#define SINGLE_CHANNEL_GW_CD						1
#define SINGLE_CHANNEL_GW_TX_POWER					14	// dBm
#define SINGLE_CHANNEL_GW_PREAMBLE_LENGTH			8
#define SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT			5
#define SINGLE_CHANNEL_GW_FIX_LENGTH_PAY			false
#define SINGLE_CHANNEL_GW_IQ_TX						false
#define SINGLE_CHANNEL_GW_IQ_RX						false
#define SINGLE_CHANNEL_GW_TX_TIMEOUT				10000
#define SINGLE_CHANNEL_GW_RX_TIMEOUT				0
/* BEGIN */
#else
    #error "Please define a frequency band in the compiler options."
#endif

#endif // __LORAMAC_BOARD_H__
