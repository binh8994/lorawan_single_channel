/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
	(C)2013 Semtech
 ___ _____ _   ___ _  _____ ___  ___  ___ ___
/ __|_   _/_\ / __| |/ / __/ _ \| _ \/ __| __|
\__ \ | |/ _ \ (__| ' <| _| (_) |   / (__| _|
|___/ |_/_/ \_\___|_|\_\_| \___/|_|_\\___|___|
embedded.connectivity.solutions===============

Description: LoRa MAC layer implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis ( Semtech ), Gregory Cristian ( Semtech ) and Daniel Jäckle ( STACKFORCE )
*/
#include <stdint.h>
#include <math.h>

#include "../ak/ak.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../app/app.h"
#include "../app/app_data.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../common/utils.h"

#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"

#include "../radio/radio.h"
#include "../radio/sx1276/sx1276.h"
#include "../radio/sx1276/sx1276_cfg.h"
#include "../crypto/aes.h"
#include "../crypto/cmac.h"
#include "LoRaMacCrypto.h"
#include "LoRaMac.h"
#include "LoRaMacTest.h"
#include "LoRaMac-definitions.h"
#include "xprintf.h"

#define USE_BAND_433
//#define ENABLE_GATEWAY_RESET_COUNTER

typedef struct
{
	RadioModems_t Modem;
	uint8_t       Addr;
	uint8_t       Value;
}RadioRegisters_t;

/*!
 * Maximum PHY layer payload size
 */
#define LORAMAC_PHY_MAXPAYLOAD                      255

/*!
 * Maximum MAC commands buffer size
 */
#define LORA_MAC_COMMAND_MAX_LENGTH                 15

/*!
 * FRMPayload overhead to be used when setting the Radio.SetMaxPayloadLength
 * in RxWindowSetup function.
 * Maximum PHYPayload = MaxPayloadOfDatarate/MaxPayloadOfDatarateRepeater + LORA_MAC_FRMPAYLOAD_OVERHEAD
 */
#define LORA_MAC_FRMPAYLOAD_OVERHEAD                13 // MHDR(1) + FHDR(7) + Port(1) + MIC(4)

/*!
 * LoRaMac duty cycle for the back-off procedure during the first hour.
 */
#define BACKOFF_DC_1_HOUR                           100

/*!
 * LoRaMac duty cycle for the back-off procedure during the next 10 hours.
 */
#define BACKOFF_DC_10_HOURS                         1000

/*!
 * LoRaMac duty cycle for the back-off procedure during the next 24 hours.
 */
#define BACKOFF_DC_24_HOURS                         10000

/*!
 * Device IEEE EUI
 */
static uint8_t *LoRaMacDevEui;

/*!
 * Application IEEE EUI
 */
static uint8_t *LoRaMacAppEui;

/*!
 * AES encryption/decryption cipher application key
 */
static uint8_t *LoRaMacAppKey;

/*!
 * AES encryption/decryption cipher network session key
 */
static uint8_t LoRaMacNwkSKey[] =
{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/*!
 * AES encryption/decryption cipher application session key
 */
static uint8_t LoRaMacAppSKey[] =
{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/*!
 * Device nonce is a random value extracted by issuing a sequence of RSSI
 * measurements
 */
static uint16_t LoRaMacDevNonce;

/*!
 * Network ID ( 3 bytes )
 */
static uint32_t LoRaMacNetID;

/*!
 * Mote Address
 */
static uint32_t LoRaMacDevAddr;

/*!
 * Multicast channels linked list
 */
static MulticastParams_t *MulticastChannels = NULL;

/*!
 * Actual device class
 */
static DeviceClass_t LoRaMacDeviceClass;

/*!
 * Indicates if the node is connected to a private or public network
 */
static bool PublicNetwork;

/*!
 * Indicates if the node supports repeaters
 */
static bool RepeaterSupport;

/*!
 * Buffer containing the data to be sent or received.
 */
static uint8_t LoRaMacBuffer[LORAMAC_PHY_MAXPAYLOAD];

/*!
 * Length of packet in LoRaMacBuffer
 */
static uint16_t LoRaMacBufferPktLen = 0;

/*!
 * Length of the payload in LoRaMacBuffer
 */
static uint8_t LoRaMacTxPayloadLen = 0;

/*!
 * Buffer containing the upper layer data.
 */
static uint8_t LoRaMacRxPayload[LORAMAC_PHY_MAXPAYLOAD];

/*!
 * LoRaMAC frame counter. Each time a packet is sent the counter is incremented.
 * Only the 16 LSB bits are sent
 */
static uint32_t UpLinkCounter = 0;

/*!
 * LoRaMAC frame counter. Each time a packet is received the counter is incremented.
 * Only the 16 LSB bits are received
 */
static uint32_t DownLinkCounter = 0;

/*!
 * IsPacketCounterFixed enables the MIC field tests by fixing the
 * UpLinkCounter value
 */
static bool IsUpLinkCounterFixed = false;

/*!
 * Used for test purposes. Disables the opening of the reception windows.
 */
static bool IsRxWindowsEnabled = true;

/*!
 * Indicates if the MAC layer has already joined a network.
 */
static bool IsLoRaMacNetworkJoined = false;

/*!
 * LoRaMac ADR control status
 */
static bool AdrCtrlOn = false;

/*!
 * Counts the number of missed ADR acknowledgements
 */
static uint32_t AdrAckCounter = 0;

/*!
 * If the node has sent a FRAME_TYPE_DATA_CONFIRMED_UP this variable indicates
 * if the nodes needs to manage the server acknowledgement.
 */
static bool NodeAckRequested = false;

/*!
 * If the server has sent a FRAME_TYPE_DATA_CONFIRMED_DOWN this variable indicates
 * if the ACK bit must be set for the next transmission
 */
static bool SrvAckRequested = false;

/*!
 * Indicates if the MAC layer wants to send MAC commands
 */
static bool MacCommandsInNextTx = false;

/*!
 * Contains the current MacCommandsBuffer index
 */
static uint8_t MacCommandsBufferIndex = 0;

/*!
 * Contains the current MacCommandsBuffer index for MAC commands to repeat
 */
static uint8_t MacCommandsBufferToRepeatIndex = 0;

/*!
 * Buffer containing the MAC layer commands
 */
static uint8_t MacCommandsBuffer[LORA_MAC_COMMAND_MAX_LENGTH];

/*!
 * Buffer containing the MAC layer commands which must be repeated
 */
static uint8_t MacCommandsBufferToRepeat[LORA_MAC_COMMAND_MAX_LENGTH];

#if defined( USE_BAND_433 )
/*!
 * Data rates table definition
 */
const uint8_t Datarates[]  = { 12, 11, 10,  9,  8,  7,  7, 50 };

/*!
 * Bandwidths table definition in Hz
 */
const uint32_t Bandwidths[] = { 125000, 125000, 125000, 125000, 125000, 125000, 250000, 0 };

/*!
 * Maximum payload with respect to the datarate index. Cannot operate with repeater.
 */
const uint8_t MaxPayloadOfDatarate[] = { 51, 51, 51, 115, 242, 242, 242, 242 };

/*!
 * Maximum payload with respect to the datarate index. Can operate with repeater.
 */
const uint8_t MaxPayloadOfDatarateRepeater[] = { 51, 51, 51, 115, 222, 222, 222, 222 };

/*!
 * Tx output powers table definition
 */
const int8_t TxPowers[]    = { 10, 7, 4, 1, -2, -5 };

/*!
 * LoRaMac bands
 */
static Band_t Bands[LORA_MAX_NB_BANDS] =
{
	BAND0,
};

/*!
 * LoRaMAC channels
 */
static ChannelParams_t Channels[LORA_MAX_NB_CHANNELS] =
{
	LC1,
	LC2,
	LC3,
};
#elif defined( USE_BAND_470 )

#endif

/*!
 * LoRaMac parameters
 */
LoRaMacParams_t LoRaMacParams;

/*!
 * LoRaMac default parameters
 */
LoRaMacParams_t LoRaMacParamsDefaults;

/*!
 * Uplink messages repetitions counter
 */
static uint8_t ChannelsNbRepCounter = 0;

/*!
 * Maximum duty cycle
 * \remark Possibility to shutdown the device.
 */
static uint8_t MaxDCycle = 0;

/*!
 * Aggregated duty cycle management
 */
static uint16_t AggregatedDCycle;
static uint32_t AggregatedLastTxDoneTime;
static uint32_t AggregatedTimeOff;

/*!
 * Enables/Disables duty cycle management (Test only)
 */
static bool DutyCycleOn;

/*!
 * Current channel index
 */
static uint8_t Channel;

/*!
 * Stores the time at LoRaMac initialization.
 *
 * \remark Used for the BACKOFF_DC computation.
 */
static uint32_t LoRaMacInitializationTime = 0;

/*!
 * LoRaMac internal states
 */
enum eLoRaMacState
{
	LORAMAC_IDLE          = 0x00000000,
	LORAMAC_TX_RUNNING    = 0x00000001,
	LORAMAC_RX            = 0x00000002,
	LORAMAC_ACK_REQ       = 0x00000004,
	LORAMAC_ACK_RETRY     = 0x00000008,
	LORAMAC_TX_DELAYED    = 0x00000010,
	LORAMAC_TX_CONFIG     = 0x00000020,
	LORAMAC_RX_ABORT      = 0x00000040,
};

/*!
 * LoRaMac internal state
 */
uint32_t LoRaMacState = LORAMAC_IDLE;

/*!
 * LoRaMac timer used to check the LoRaMacState (runs every second)
 */
//static uint32_t MacStateCheckTimer;

/*!
 * LoRaMac upper layer event functions
 */
static LoRaMacPrimitives_t *LoRaMacPrimitives;

/*!
 * LoRaMac upper layer callback functions
 */
static LoRaMacCallback_t *LoRaMacCallbacks;

/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

/*!
 * LoRaMac duty cycle delayed Tx timer
 */
//static uint32_t TxDelayedTimer;

/*!
 * LoRaMac reception windows timers
 */
//static uint32_t RxWindowTimer1;
//static uint32_t RxWindowTimer2;

/*!
 * LoRaMac reception windows delay
 * \remark normal frame: RxWindowXDelay = ReceiveDelayX - RADIO_WAKEUP_TIME
 *         join frame  : RxWindowXDelay = JoinAcceptDelayX - RADIO_WAKEUP_TIME
 */
static uint32_t RxWindow1Delay;
static uint32_t RxWindow2Delay;

/*!
 * Rx window parameters
 */
typedef struct
{
	int8_t Datarate;
	uint8_t Bandwidth;
	uint32_t RxWindowTimeout;
	int32_t RxOffset;
}RxConfigParams_t;

/*!
 * Rx windows params
 */
static RxConfigParams_t RxWindowsParams[2];

/*!
 * Acknowledge timeout timer. Used for packet retransmissions.
 */
//static uint32_t AckTimeoutTimer;

/*!
 * Number of trials to get a frame acknowledged
 */
static uint8_t AckTimeoutRetries = 1;

/*!
 * Number of trials to get a frame acknowledged
 */
static uint8_t AckTimeoutRetriesCounter = 1;

/*!
 * Indicates if the AckTimeout timer has expired or not
 */
static bool AckTimeoutRetry = false;

/*!
 * Last transmission time on air
 */
uint32_t TxTimeOnAir = 0;

/*!
 * Number of trials for the Join Request
 */
static uint8_t JoinRequestTrials;

/*!
 * Maximum number of trials for the Join Request
 */
static uint8_t MaxJoinRequestTrials;

/*!
 * Structure to hold an MCPS indication data.
 */
static McpsIndication_t McpsIndication;

/*!
 * Structure to hold MCPS confirm data.
 */
static McpsConfirm_t McpsConfirm;

/*!
 * Structure to hold MLME confirm data.
 */
static MlmeConfirm_t MlmeConfirm;

/*!
 * Holds the current rx window slot
 */
static uint8_t RxSlot = 0;

/*!
 * LoRaMac tx/rx operation state
 */
LoRaMacFlags_t LoRaMacFlags;

/*!
 * \brief Function to be executed on Radio Tx Done event
 */
static void OnRadioTxDone( void );

/*!
 * \brief This function prepares the MAC to abort the execution of function
 *        OnRadioRxDone in case of a reception error.
 */
static void PrepareRxDoneAbort( void );

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
static void OnRadioRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
static void OnRadioTxTimeout( void );

/*!
 * \brief Function executed on Radio Rx error event
 */
static void OnRadioRxError( void );

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
static void OnRadioRxTimeout( void );

/*!
 * \brief Function executed on Resend Frame timer event.
 */
//static void OnMacStateCheckTimerEvent( void );

/*!
 * \brief Function executed on duty cycle delayed Tx  timer event
 */
//static void OnTxDelayedTimerEvent( void );

/*!
 * \brief Function executed on first Rx window timer event
 */
//static void OnRxWindow1TimerEvent( void );

/*!
 * \brief Function executed on second Rx window timer event
 */
//static void OnRxWindow2TimerEvent( void );

/*!
 * \brief Function executed on AckTimeout timer event
 */
//static void OnAckTimeoutTimerEvent( void );

/*!
 * \brief Searches and set the next random available channel
 *
 * \param [OUT] Time to wait for the next transmission according to the duty
 *              cycle.
 *
 * \retval status  Function status [1: OK, 0: Unable to find a channel on the
 *                                  current datarate]
 */
static bool SetNextChannel( uint32_t* time );

/*!
 * \brief Initializes and opens the reception window
 *
 * \param [IN] freq window channel frequency
 * \param [IN] datarate window channel datarate
 * \param [IN] bandwidth window channel bandwidth
 * \param [IN] timeout window channel timeout
 *
 * \retval status Operation status [true: Success, false: Fail]
 */
static bool RxWindowSetup( uint32_t freq, int8_t datarate, uint32_t bandwidth, uint16_t timeout, bool rxContinuous );

/*!
 * \brief Verifies if the RX window 2 frequency is in range
 *
 * \param [IN] freq window channel frequency
 *
 * \retval status  Function status [1: OK, 0: Frequency not applicable]
 */
static bool Rx2FreqInRange( uint32_t freq );

/*!
 * \brief Adds a new MAC command to be sent.
 *
 * \Remark MAC layer internal function
 *
 * \param [in] cmd MAC command to be added
 *                 [MOTE_MAC_LINK_CHECK_REQ,
 *                  MOTE_MAC_LINK_ADR_ANS,
 *                  MOTE_MAC_DUTY_CYCLE_ANS,
 *                  MOTE_MAC_RX2_PARAM_SET_ANS,
 *                  MOTE_MAC_DEV_STATUS_ANS
 *                  MOTE_MAC_NEW_CHANNEL_ANS]
 * \param [in] p1  1st parameter ( optional depends on the command )
 * \param [in] p2  2nd parameter ( optional depends on the command )
 *
 * \retval status  Function status [0: OK, 1: Unknown command, 2: Buffer full]
 */
static LoRaMacStatus_t AddMacCommand( uint8_t cmd, uint8_t p1, uint8_t p2 );

/*!
 * \brief Parses the MAC commands which must be repeated.
 *
 * \Remark MAC layer internal function
 *
 * \param [IN] cmdBufIn  Buffer which stores the MAC commands to send
 * \param [IN] length  Length of the input buffer to parse
 * \param [OUT] cmdBufOut  Buffer which stores the MAC commands which must be
 *                         repeated.
 *
 * \retval Size of the MAC commands to repeat.
 */
static uint8_t ParseMacCommandsToRepeat( uint8_t* cmdBufIn, uint8_t length, uint8_t* cmdBufOut );

/*!
 * \brief Validates if the payload fits into the frame, taking the datarate
 *        into account.
 *
 * \details Refer to chapter 4.3.2 of the LoRaWAN specification, v1.0
 *
 * \param lenN Length of the application payload. The length depends on the
 *             datarate and is region specific
 *
 * \param datarate Current datarate
 *
 * \param fOptsLen Length of the fOpts field
 *
 * \retval [false: payload does not fit into the frame, true: payload fits into
 *          the frame]
 */
static bool ValidatePayloadLength( uint8_t lenN, int8_t datarate, uint8_t fOptsLen );

/*!
 * \brief Counts the number of bits in a mask.
 *
 * \param [IN] mask A mask from which the function counts the active bits.
 * \param [IN] nbBits The number of bits to check.
 *
 * \retval Number of enabled bits in the mask.
 */
static uint8_t CountBits( uint16_t mask, uint8_t nbBits );

#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#endif

/*!
 * \brief Validates the correctness of the datarate against the enable channels.
 *
 * \param [IN] datarate Datarate to be check
 * \param [IN] channelsMask Pointer to the first element of the channel mask
 *
 * \retval [true: datarate can be used, false: datarate can not be used]
 */
static bool ValidateDatarate( int8_t datarate, uint16_t* channelsMask );

/*!
 * \brief Limits the Tx power according to the number of enabled channels
 *
 * \param [IN] txPower txPower to limit
 * \param [IN] maxBandTxPower Maximum band allowed TxPower
 *
 * \retval Returns the maximum valid tx power
 */
static int8_t LimitTxPower( int8_t txPower, int8_t maxBandTxPower );

/*!
 * \brief Verifies, if a value is in a given range.
 *
 * \param value Value to verify, if it is in range
 *
 * \param min Minimum possible value
 *
 * \param max Maximum possible value
 *
 * \retval Returns the maximum valid tx power
 */
static bool ValueInRange( int8_t value, int8_t min, int8_t max );

/*!
 * \brief Calculates the next datarate to set, when ADR is on or off
 *
 * \param [IN] adrEnabled Specify whether ADR is on or off
 *
 * \param [IN] updateChannelMask Set to true, if the channel masks shall be updated
 *
 * \param [OUT] datarateOut Reports the datarate which will be used next
 *
 * \retval Returns the state of ADR ack request
 */
static bool AdrNextDr( bool adrEnabled, bool updateChannelMask, int8_t* datarateOut );

/*!
 * \brief Disables channel in a specified channel mask
 *
 * \param [IN] id - Id of the channel
 *
 * \param [IN] mask - Pointer to the channel mask to edit
 *
 * \retval [true, if disable was successful, false if not]
 */
static bool DisableChannelInMask( uint8_t id, uint16_t* mask );

/*!
 * \brief Decodes MAC commands in the fOpts field and in the payload
 */
static void ProcessMacCommands( uint8_t *payload, uint8_t macIndex, uint8_t commandsSize, uint8_t snr );

/*!
 * \brief LoRaMAC layer generic send frame
 *
 * \param [IN] macHdr      MAC header field
 * \param [IN] fPort       MAC payload port
 * \param [IN] fBuffer     MAC data buffer to be sent
 * \param [IN] fBufferSize MAC data buffer size
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t Send( LoRaMacHeader_t *macHdr, uint8_t fPort, void *fBuffer, uint16_t fBufferSize );

/*!
 * \brief LoRaMAC layer frame buffer initialization
 *
 * \param [IN] macHdr      MAC header field
 * \param [IN] fCtrl       MAC frame control field
 * \param [IN] fOpts       MAC commands buffer
 * \param [IN] fPort       MAC payload port
 * \param [IN] fBuffer     MAC data buffer to be sent
 * \param [IN] fBufferSize MAC data buffer size
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t PrepareFrame( LoRaMacHeader_t *macHdr, LoRaMacFrameCtrl_t *fCtrl, uint8_t fPort, void *fBuffer, uint16_t fBufferSize );

/*!
 * \brief Schedules the frame according to the duty cycle
 *
 * \retval Status of the operation
 */
static LoRaMacStatus_t ScheduleTx( void );

/*!
 * \brief Sets the duty cycle for the join procedure.
 *
 * \retval Duty cycle
 */
static uint16_t JoinDutyCycle( void );

/*!
 * \brief Calculates the back-off time for the band of a channel.
 *
 * \param [IN] channel     The last Tx channel index
 */
static void CalculateBackOff( uint8_t channel );

/*!
 * \brief Alternates the datarate of the channel for the join request.
 *
 * \param [IN] nbTrials    Number of performed join requests.
 * \retval Datarate to apply
 */
static int8_t AlternateDatarate( uint16_t nbTrials );

/*!
 * \brief LoRaMAC layer prepared frame buffer transmission with channel specification
 *
 * \remark PrepareFrame must be called at least once before calling this
 *         function.
 *
 * \param [IN] channel     Channel parameters
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t SendFrameOnChannel( ChannelParams_t channel );

/*!
 * \brief Sets the radio in continuous transmission mode
 *
 * \remark Uses the radio parameters set on the previous transmission.
 *
 * \param [IN] timeout     Time in seconds while the radio is kept in continuous wave mode
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t SetTxContinuousWave( uint16_t timeout );

/*!
 * \brief Sets the radio in continuous transmission mode
 *
 * \remark Uses the radio parameters set on the previous transmission.
 *
 * \param [IN] timeout     Time in seconds while the radio is kept in continuous wave mode
 * \param [IN] frequency   RF frequency to be set.
 * \param [IN] power       RF ouptput power to be set.
 * \retval status          Status of the operation.
 */
LoRaMacStatus_t SetTxContinuousWave1( uint16_t timeout, uint32_t frequency, uint8_t power );

/*!
 * \brief Resets MAC specific parameters to default
 */
static void ResetMacParameters( void );

/*
 * Rx window precise timing
 *
 * For more details please consult the following document, chapter 3.1.2.
 * http://www.semtech.com/images/datasheet/SX1272_settings_for_LoRaWAN_v2.0.pdf
 * or
 * http://www.semtech.com/images/datasheet/SX1276_settings_for_LoRaWAN_v2.0.pdf
 *
 *                 Downlink start: T = Tx + 1s (+/- 20 us)
 *                            |
 *             TRxEarly       |        TRxLate
 *                |           |           |
 *                |           |           +---+---+---+---+---+---+---+---+
 *                |           |           |       Latest Rx window        |
 *                |           |           +---+---+---+---+---+---+---+---+
 *                |           |           |
 *                +---+---+---+---+---+---+---+---+
 *                |       Earliest Rx window      |
 *                +---+---+---+---+---+---+---+---+
 *                            |
 *                            +---+---+---+---+---+---+---+---+
 *Downlink preamble 8 symbols |   |   |   |   |   |   |   |   |
 *                            +---+---+---+---+---+---+---+---+
 *
 *                     Worst case Rx window timings
 *
 * TRxLate  = DEFAULT_MIN_RX_SYMBOLS * tSymbol - RADIO_WAKEUP_TIME
 * TRxEarly = 8 - DEFAULT_MIN_RX_SYMBOLS * tSymbol - RxWindowTimeout - RADIO_WAKEUP_TIME
 *
 * TRxLate - TRxEarly = 2 * DEFAULT_SYSTEM_MAX_RX_ERROR
 *
 * RxOffset = ( TRxLate + TRxEarly ) / 2
 *
 * RxWindowTimeout = ( 2 * DEFAULT_MIN_RX_SYMBOLS - 8 ) * tSymbol + 2 * DEFAULT_SYSTEM_MAX_RX_ERROR
 * RxOffset = 4 * tSymbol - RxWindowTimeout / 2 - RADIO_WAKE_UP_TIME
 *
 * Minimal value of RxWindowTimeout must be 5 symbols which implies that the system always tolerates at least an error of 1.5 * tSymbol
 */
/*!
 * Computes the Rx window parameters.
 *
 * \param [IN] datarate     Rx window datarate to be used
 * \param [IN] rxError      Maximum timing error of the receiver. in milliseconds
 *                          The receiver will turn on in a [-rxError : +rxError] ms
 *                          interval around RxOffset
 *
 * \retval rxConfigParams   Returns a RxConfigParams_t structure.
 */
static RxConfigParams_t ComputeRxWindowParameters( int8_t datarate, uint32_t rxError );

/* binhnt61 */
/* BEGIN */
static uint32_t TimerGetElapsedTime( uint32_t eventInTime );
static uint32_t TimerGetCurrentTime( void );
static int32_t randr( int32_t min, int32_t max );
static uint32_t random_feed;
/* END */


static void OnRadioTxDone( void )
{
	SYS_DBG("OnRadioTxDone\n");

	uint32_t curTime = TimerGetCurrentTime( );

	if( LoRaMacDeviceClass != CLASS_C )
	{
		Radio.Sleep( );
	}
	else
	{
		OnRxWindow2TimerEvent( );
	}

	// Setup timers
	if( IsRxWindowsEnabled == true )
	{
		timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_RXWINDOW_1_TIMER, RxWindow1Delay, TIMER_ONE_SHOT);

		if( LoRaMacDeviceClass != CLASS_C )
		{
			timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_RXWINDOW_2_TIMER, RxWindow2Delay, TIMER_ONE_SHOT);
		}
		if( ( LoRaMacDeviceClass == CLASS_C ) || ( NodeAckRequested == true ) )
		{
			timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_ACK_TIMEOUT, RxWindow2Delay + ACK_TIMEOUT + randr( -ACK_TIMEOUT_RND, ACK_TIMEOUT_RND ), TIMER_ONE_SHOT);
		}
	}
	else
	{
		McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_OK;
		MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_RX2_TIMEOUT;

		if( LoRaMacFlags.Value == 0 )
		{
			LoRaMacFlags.Bits.McpsReq = 1;
		}
		LoRaMacFlags.Bits.MacDone = 1;
	}

	// Update last tx done time for the current channel
	Bands[Channels[Channel].Band].LastTxDoneTime = curTime;
	// Update Aggregated last tx done time
	AggregatedLastTxDoneTime = curTime;
	// Update Backoff
	CalculateBackOff( Channel );

	if( NodeAckRequested == false )
	{
		McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_OK;
		ChannelsNbRepCounter++;
	}
}

static void PrepareRxDoneAbort( void )
{
	LoRaMacState |= LORAMAC_RX_ABORT;

	if( NodeAckRequested )
	{
		OnAckTimeoutTimerEvent( );
	}

	LoRaMacFlags.Bits.McpsInd = 1;
	LoRaMacFlags.Bits.MacDone = 1;

	// Trig OnMacCheckTimerEvent call as soon as possible
	timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_STATE_CHECK_TIMER, 1, TIMER_ONE_SHOT);
}

static void OnRadioRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	SYS_DBG("OnRadioRxDone\n");
	SYS_DBG("\nsize:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);

	for(int i = 0; i < size; i++) SYS_DBG("x%02X ", payload[i]);
	SYS_DBG("\n");

	LoRaMacHeader_t macHdr;
	LoRaMacFrameCtrl_t fCtrl;
	bool skipIndication = false;

	uint8_t pktHeaderLen = 0;
	uint32_t address = 0;
	uint8_t appPayloadStartIndex = 0;
	uint8_t port = 0xFF;
	uint8_t frameLen = 0;
	uint32_t mic = 0;
	uint32_t micRx = 0;

	uint16_t sequenceCounter = 0;
	uint16_t sequenceCounterPrev = 0;
	uint16_t sequenceCounterDiff = 0;
	uint32_t downLinkCounter = 0;

	MulticastParams_t *curMulticastParams = NULL;
	uint8_t *nwkSKey = LoRaMacNwkSKey;
	uint8_t *appSKey = LoRaMacAppSKey;

	uint8_t multicast = 0;

	bool isMicOk = false;

	/*binhnt61*/
	uint8_t gateway_reset = false;

	McpsConfirm.AckReceived = false;
	McpsIndication.Rssi = rssi;
	McpsIndication.Snr = snr;
	McpsIndication.RxSlot = RxSlot;
	McpsIndication.Port = 0;
	McpsIndication.Multicast = 0;
	McpsIndication.FramePending = 0;
	McpsIndication.Buffer = NULL;
	McpsIndication.BufferSize = 0;
	McpsIndication.RxData = false;
	McpsIndication.AckReceived = false;
	McpsIndication.DownLinkCounter = 0;
	McpsIndication.McpsIndication = MCPS_UNCONFIRMED;

	Radio.Sleep( );

	timer_remove_attr(AK_TASK_LORAMAC_ID, LORAMAC_MAC_RXWINDOW_2_TIMER);

	macHdr.Value = payload[pktHeaderLen++];

	switch( macHdr.Bits.MType )
	{
	case FRAME_TYPE_JOIN_ACCEPT: {
		SYS_DBG("FRAME_TYPE_JOIN_ACCEPT\n");

		if( IsLoRaMacNetworkJoined == true )
		{
			McpsIndication.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;
			PrepareRxDoneAbort( );
			return;
		}

		//SYS_DBG("SizeForDecrypt:%d\n", size - 1);
		//SYS_DBG("Statr Decrypt from:x%02X to x%02X\n", *(payload + 1), *((payload + 1) + (size - 1) - 1 ));
		LoRaMacJoinDecrypt( payload + 1, size - 1, LoRaMacAppKey, LoRaMacRxPayload + 1 );
		//SYS_DBG("After Decrypt from:x%02X to x%02X\n", *(LoRaMacRxPayload+  1), *((LoRaMacRxPayload +1) + (size - 1) - 1 ));

		LoRaMacRxPayload[0] = macHdr.Value;

		SYS_DBG("bufferDecr\n");
		for (int i=0; i< size; i++) SYS_DBG("x%02X ", LoRaMacRxPayload[i]);
		SYS_DBG("\n");

		//SYS_DBG("size for mic_rx:%d\n", size - LORAMAC_MFR_LEN);
		LoRaMacJoinComputeMic( LoRaMacRxPayload, size - LORAMAC_MFR_LEN, LoRaMacAppKey, &mic );

		micRx |= ( uint32_t )LoRaMacRxPayload[size - LORAMAC_MFR_LEN];
		micRx |= ( ( uint32_t )LoRaMacRxPayload[size - LORAMAC_MFR_LEN + 1] << 8 );
		micRx |= ( ( uint32_t )LoRaMacRxPayload[size - LORAMAC_MFR_LEN + 2] << 16 );
		micRx |= ( ( uint32_t )LoRaMacRxPayload[size - LORAMAC_MFR_LEN + 3] << 24 );

		//SYS_DBG("micRx x%08X\n", micRx);
		//SYS_DBG("mic x%08X\n", mic);

		if( micRx == mic )
		{
			LoRaMacJoinComputeSKeys( LoRaMacAppKey, LoRaMacRxPayload + 1, LoRaMacDevNonce, LoRaMacNwkSKey, LoRaMacAppSKey );

			SYS_DBG("LoRaMacNwkSKey\n");
			for (int i=0; i< 16; i++) SYS_DBG("x%02X ", LoRaMacNwkSKey[i]);
			SYS_DBG("\n");

			SYS_DBG("LoRaMacAppSKey\n");
			for (int i=0; i< 16; i++) SYS_DBG("x%02X ", LoRaMacAppSKey[i]);
			SYS_DBG("\n");

			LoRaMacNetID = ( uint32_t )LoRaMacRxPayload[4];
			LoRaMacNetID |= ( ( uint32_t )LoRaMacRxPayload[5] << 8 );
			LoRaMacNetID |= ( ( uint32_t )LoRaMacRxPayload[6] << 16 );

			SYS_DBG("LoRaMacNetID x%08X\n", LoRaMacNetID);

			LoRaMacDevAddr = ( uint32_t )LoRaMacRxPayload[7];
			LoRaMacDevAddr |= ( ( uint32_t )LoRaMacRxPayload[8] << 8 );
			LoRaMacDevAddr |= ( ( uint32_t )LoRaMacRxPayload[9] << 16 );
			LoRaMacDevAddr |= ( ( uint32_t )LoRaMacRxPayload[10] << 24 );

			SYS_DBG("LoRaMacDevAddr x%08X\n", LoRaMacDevAddr);

			// DLSettings
			LoRaMacParams.Rx1DrOffset = ( LoRaMacRxPayload[11] >> 4 ) & 0x07;
			LoRaMacParams.Rx2Channel.Datarate = LoRaMacRxPayload[11] & 0x0F;

			//SYS_DBG("Rx1DrOffset x%02X\n", ( LoRaMacRxPayload[11] >> 4 ) & 0x07);
			//SYS_DBG("Rx2Channel.Datarate x%02X\n", LoRaMacRxPayload[11] & 0x0F);

			// RxDelay
			LoRaMacParams.ReceiveDelay1 = ( LoRaMacRxPayload[12] & 0x0F );
			//SYS_DBG("ReceiveDelay1 x%02X\n", LoRaMacRxPayload[12] & 0x0F);

			if( LoRaMacParams.ReceiveDelay1 == 0 )
			{
				LoRaMacParams.ReceiveDelay1 = 1;
			}
			LoRaMacParams.ReceiveDelay1 *= 1e3;
			LoRaMacParams.ReceiveDelay2 = LoRaMacParams.ReceiveDelay1 + 1e3;

#if !( defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID ) )
			//CFList
			if( ( size - 1 ) > 16 )
			{
				ChannelParams_t param;
				param.DrRange.Value = ( DR_5 << 4 ) | DR_0;

				LoRaMacState |= LORAMAC_TX_CONFIG;
				for( uint8_t i = 3, j = 0; i < ( 5 + 3 ); i++, j += 3 )
				{
					param.Frequency = ( ( uint32_t )LoRaMacRxPayload[13 + j] | ( ( uint32_t )LoRaMacRxPayload[14 + j] << 8 ) | ( ( uint32_t )LoRaMacRxPayload[15 + j] << 16 ) ) * 100;
					if( param.Frequency != 0 )
					{
						LoRaMacChannelAdd( i, param );
					}
					else
					{
						LoRaMacChannelRemove( i );
					}
				}
				LoRaMacState &= ~LORAMAC_TX_CONFIG;
			}
#endif
			MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_OK;
			IsLoRaMacNetworkJoined = true;
			LoRaMacParams.ChannelsDatarate = LoRaMacParamsDefaults.ChannelsDatarate;
		}
		else
		{
			MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_JOIN_FAIL;
		}
	}
		break;

	case FRAME_TYPE_DATA_CONFIRMED_DOWN:
	case FRAME_TYPE_DATA_UNCONFIRMED_DOWN:
	{
		SYS_DBG("FRAME_TYPE_DATA_CONFIRMED_DOWN / UNCONFIRMED_DOWN\n");

		address = payload[pktHeaderLen++];
		address |= ( (uint32_t)payload[pktHeaderLen++] << 8 );
		address |= ( (uint32_t)payload[pktHeaderLen++] << 16 );
		address |= ( (uint32_t)payload[pktHeaderLen++] << 24 );

		//SYS_DBG("address:x%08X\n", address);

		if( address != LoRaMacDevAddr )
		{
			curMulticastParams = MulticastChannels;
			while( curMulticastParams != NULL )
			{
				if( address == curMulticastParams->Address )
				{
					multicast = 1;
					nwkSKey = curMulticastParams->NwkSKey;
					appSKey = curMulticastParams->AppSKey;
					downLinkCounter = curMulticastParams->DownLinkCounter;
					break;
				}
				curMulticastParams = curMulticastParams->Next;
			}
			if( multicast == 0 )
			{
				// We are not the destination of this frame.
				McpsIndication.Status = LORAMAC_EVENT_INFO_STATUS_ADDRESS_FAIL;
				PrepareRxDoneAbort( );
				return;
			}
		}
		else
		{
			multicast = 0;
			nwkSKey = LoRaMacNwkSKey;
			appSKey = LoRaMacAppSKey;
			downLinkCounter = DownLinkCounter;
		}

		fCtrl.Value = payload[pktHeaderLen++];

		sequenceCounter = ( uint16_t )payload[pktHeaderLen++];
		sequenceCounter |= ( uint16_t )payload[pktHeaderLen++] << 8;
		//SYS_DBG("sequenceCounter:x%04X\n", sequenceCounter);

		appPayloadStartIndex = 8 + fCtrl.Bits.FOptsLen;

		micRx |= ( uint32_t )payload[size - LORAMAC_MFR_LEN];
		micRx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 1] << 8 );
		micRx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 2] << 16 );
		micRx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 3] << 24 );
		//SYS_DBG("micRx:x%08X\n", micRx);

		sequenceCounterPrev = ( uint16_t )downLinkCounter;
		sequenceCounterDiff = ( sequenceCounter - sequenceCounterPrev );

		//SYS_DBG("payload\n");
		//for(int i = 0; i < size - LORAMAC_MFR_LEN;i++) {
		//	SYS_DBG("x%02X ", *(payload + i));
		//}
		//SYS_DBG("\n");

		if( sequenceCounterDiff < ( 1 << 15 ) )
		{
			downLinkCounter += sequenceCounterDiff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, nwkSKey, address, DOWN_LINK, downLinkCounter, &mic );
			if( micRx == mic )
			{
				isMicOk = true;
			}
		}
		else
		{
			// check for sequence roll-over
			uint32_t  downLinkCounterTmp = downLinkCounter + 0x10000 + ( int16_t )sequenceCounterDiff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, nwkSKey, address, DOWN_LINK, downLinkCounterTmp, &mic );
			if( micRx == mic )
			{
				isMicOk = true;
				downLinkCounter = downLinkCounterTmp;

			}
#ifdef 	ENABLE_GATEWAY_RESET_COUNTER
			else /* binhnt61 */
			{
				/* if gw reset downlinkcounter => sequenceRx < DownLinkCounter */
				/* check with sequence not roll-over */
				LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, nwkSKey, address, DOWN_LINK, sequenceCounter, &mic );
				if(micRx == mic)
				{
					isMicOk = true;
					gateway_reset = true;
					downLinkCounter = sequenceCounter & 0x0000FFFF;
				}
			}
#endif
		}

		SYS_DBG("mic:x%08X\n", mic);

		// Check for a the maximum allowed counter difference
		if( sequenceCounterDiff >= MAX_FCNT_GAP )
		{
			if(gateway_reset == false) { /* binhnt61 */
				McpsIndication.Status = LORAMAC_EVENT_INFO_STATUS_DOWNLINK_TOO_MANY_FRAMES_LOSS;
				McpsIndication.DownLinkCounter = downLinkCounter;
				PrepareRxDoneAbort( );

				SYS_DBG("Too many messages lossed\n");
				return;
			}
		}

		if( isMicOk == true )
		{
			//SYS_DBG("isMicOK==true\n");

			McpsIndication.Status = LORAMAC_EVENT_INFO_STATUS_OK;
			McpsIndication.Multicast = multicast;
			McpsIndication.FramePending = fCtrl.Bits.FPending;
			McpsIndication.Buffer = NULL;
			McpsIndication.BufferSize = 0;
			McpsIndication.DownLinkCounter = downLinkCounter;

			McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_OK;

			AdrAckCounter = 0;
			MacCommandsBufferToRepeatIndex = 0;

			// Update 32 bits downlink counter
			if( multicast == 1 )
			{
				McpsIndication.McpsIndication = MCPS_MULTICAST;

				if( ( curMulticastParams->DownLinkCounter == downLinkCounter ) &&
						( curMulticastParams->DownLinkCounter != 0 ) )
				{
					McpsIndication.Status = LORAMAC_EVENT_INFO_STATUS_DOWNLINK_REPEATED;
					McpsIndication.DownLinkCounter = downLinkCounter;
					PrepareRxDoneAbort( );
					return;
				}
				curMulticastParams->DownLinkCounter = downLinkCounter;
			}
			else
			{
				if( macHdr.Bits.MType == FRAME_TYPE_DATA_CONFIRMED_DOWN )
				{
					SrvAckRequested = true;
					McpsIndication.McpsIndication = MCPS_CONFIRMED;

					if( ( DownLinkCounter == downLinkCounter ) &&
							( DownLinkCounter != 0 ) )
					{
						// Duplicated confirmed downlink. Skip indication.
						// In this case, the MAC layer shall accept the MAC commands
						// which are included in the downlink retransmission.
						// It should not provide the same frame to the application
						// layer again.
						skipIndication = true;
					}
				}
				else
				{
					SrvAckRequested = false;
					McpsIndication.McpsIndication = MCPS_UNCONFIRMED;

					if( ( DownLinkCounter == downLinkCounter ) &&
							( DownLinkCounter != 0 ) )
					{
						McpsIndication.Status = LORAMAC_EVENT_INFO_STATUS_DOWNLINK_REPEATED;
						McpsIndication.DownLinkCounter = downLinkCounter;
						PrepareRxDoneAbort( );
						return;
					}
				}
				DownLinkCounter = downLinkCounter;
			}

			// This must be done before parsing the payload and the MAC commands.
			// We need to reset the MacCommandsBufferIndex here, since we need
			// to take retransmissions and repititions into account. Error cases
			// will be handled in function OnMacStateCheckTimerEvent.
			if( McpsConfirm.McpsRequest == MCPS_CONFIRMED )
			{
				if( fCtrl.Bits.Ack == 1 )
				{// Reset MacCommandsBufferIndex when we have received an ACK.
					MacCommandsBufferIndex = 0;
				}
			}
			else
			{// Reset the variable if we have received any valid frame.
				MacCommandsBufferIndex = 0;
			}

			// Process payload and MAC commands
			if( ( ( size - 4 ) - appPayloadStartIndex ) > 0 )
			{
				port = payload[appPayloadStartIndex++];
				//SYS_DBG("port:x%02X\n", port);

				frameLen = ( size - 4 ) - appPayloadStartIndex;
				McpsIndication.Port = port;

				if( port == 0 )
				{
					// Only allow frames which do not have fOpts
					if( fCtrl.Bits.FOptsLen == 0 )
					{
						LoRaMacPayloadDecrypt( payload + appPayloadStartIndex,
											   frameLen,
											   nwkSKey,
											   address,
											   DOWN_LINK,
											   downLinkCounter,
											   LoRaMacRxPayload );

						// Decode frame payload MAC commands
						ProcessMacCommands( LoRaMacRxPayload, 0, frameLen, snr );
					}
					else
					{
						skipIndication = true;
					}
				}
				else
				{
					if( fCtrl.Bits.FOptsLen > 0 )
					{
						// Decode Options field MAC commands. Omit the fPort.
						ProcessMacCommands( payload, 8, appPayloadStartIndex - 1, snr );
					}

					LoRaMacPayloadDecrypt( payload + appPayloadStartIndex,
										   frameLen,
										   appSKey,
										   address,
										   DOWN_LINK,
										   downLinkCounter,
										   LoRaMacRxPayload );

					if( skipIndication == false )
					{
						McpsIndication.Buffer = LoRaMacRxPayload;
						McpsIndication.BufferSize = frameLen;
						McpsIndication.RxData = true;
					}
				}
			}
			else
			{
				if( fCtrl.Bits.FOptsLen > 0 )
				{
					// Decode Options field MAC commands
					ProcessMacCommands( payload, 8, appPayloadStartIndex, snr );
				}
			}

			if( skipIndication == false )
			{
				// Check if the frame is an acknowledgement
				if( fCtrl.Bits.Ack == 1 )
				{
					McpsConfirm.AckReceived = true;
					McpsIndication.AckReceived = true;

					// Stop the AckTimeout timer as no more retransmissions
					// are needed.
					timer_remove_attr(AK_TASK_LORAMAC_ID, LORAMAC_MAC_ACK_TIMEOUT);
				}
				else
				{
					McpsConfirm.AckReceived = false;

					if( AckTimeoutRetriesCounter > AckTimeoutRetries )
					{
						// Stop the AckTimeout timer as no more retransmissions
						// are needed.
						timer_remove_attr(AK_TASK_LORAMAC_ID, LORAMAC_MAC_ACK_TIMEOUT);
					}
				}
			}
			// Provide always an indication, skip the callback to the user application,
			// in case of a confirmed downlink retransmission.
			LoRaMacFlags.Bits.McpsInd = 1;
			LoRaMacFlags.Bits.McpsIndSkip = skipIndication;
		}
		else
		{
			McpsIndication.Status = LORAMAC_EVENT_INFO_STATUS_MIC_FAIL;
			PrepareRxDoneAbort( );

			SYS_DBG("micRx!=mic\n");
			return;
		}
	}
		break;

	case FRAME_TYPE_PROPRIETARY:
	{
		SYS_DBG("FRAME_TYPE_PROPRIETARY\n");

		memcpy( LoRaMacRxPayload, &payload[pktHeaderLen], size );

		McpsIndication.McpsIndication = MCPS_PROPRIETARY;
		McpsIndication.Status = LORAMAC_EVENT_INFO_STATUS_OK;
		McpsIndication.Buffer = LoRaMacRxPayload;
		McpsIndication.BufferSize = size - pktHeaderLen;

		LoRaMacFlags.Bits.McpsInd = 1;
		break;
	}

	default:
		McpsIndication.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;
		PrepareRxDoneAbort( );

		//SYS_DBG("FRAME_TYPE_UNKNOW\n");
		break;
	}
	LoRaMacFlags.Bits.MacDone = 1;

	// Trig OnMacCheckTimerEvent call as soon as possible
	timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_STATE_CHECK_TIMER, 1, TIMER_ONE_SHOT);

}

static void OnRadioTxTimeout( void )
{
	SYS_DBG("OnRadioTxTimeout\n");

	if( LoRaMacDeviceClass != CLASS_C )
	{
		Radio.Sleep( );
	}
	else
	{
		OnRxWindow2TimerEvent( );
	}

	McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_TX_TIMEOUT;
	MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_TX_TIMEOUT;
	LoRaMacFlags.Bits.MacDone = 1;
}

static void OnRadioRxError( void )
{
	SYS_DBG("OnRadioRxError\n");

	if( LoRaMacDeviceClass != CLASS_C )
	{
		Radio.Sleep( );
	}
	else
	{
		OnRxWindow2TimerEvent( );
	}

	if( RxSlot == 0 )
	{
		if( NodeAckRequested == true )
		{
			McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_RX1_ERROR;
		}
		MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_RX1_ERROR;

		if( TimerGetElapsedTime( AggregatedLastTxDoneTime ) >= RxWindow2Delay )
		{
			LoRaMacFlags.Bits.MacDone = 1;
		}
	}
	else
	{
		if( NodeAckRequested == true )
		{
			McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_RX2_ERROR;
		}
		MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_RX2_ERROR;
		LoRaMacFlags.Bits.MacDone = 1;
	}
}

static void OnRadioRxTimeout( void )
{
	SYS_DBG("OnRadioRxTimeout\n");

	if( LoRaMacDeviceClass != CLASS_C )
	{
		Radio.Sleep( );
	}
	else
	{
		OnRxWindow2TimerEvent( );
	}

	if( RxSlot == 1 )
	{
		if( NodeAckRequested == true )
		{
			McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_RX2_TIMEOUT;
		}
		MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_RX2_TIMEOUT;
		LoRaMacFlags.Bits.MacDone = 1;
	}
}

void OnMacStateCheckTimerEvent( void )
{

	//SYS_DBG("OnTimerEvent\n");

	timer_remove_attr(AK_TASK_LORAMAC_ID, LORAMAC_MAC_STATE_CHECK_TIMER);

	bool txTimeout = false;

	if( LoRaMacFlags.Bits.MacDone == 1 )
	{
		if( ( LoRaMacState & LORAMAC_RX_ABORT ) == LORAMAC_RX_ABORT )
		{
			LoRaMacState &= ~LORAMAC_RX_ABORT;
			LoRaMacState &= ~LORAMAC_TX_RUNNING;
		}

		if( ( LoRaMacFlags.Bits.MlmeReq == 1 ) || ( ( LoRaMacFlags.Bits.McpsReq == 1 ) ) )
		{
			if( ( McpsConfirm.Status == LORAMAC_EVENT_INFO_STATUS_TX_TIMEOUT ) ||
					( MlmeConfirm.Status == LORAMAC_EVENT_INFO_STATUS_TX_TIMEOUT ) )
			{
				// Stop transmit cycle due to tx timeout.
				LoRaMacState &= ~LORAMAC_TX_RUNNING;
				MacCommandsBufferIndex = 0;
				McpsConfirm.NbRetries = AckTimeoutRetriesCounter;
				McpsConfirm.AckReceived = false;
				McpsConfirm.TxTimeOnAir = 0;
				txTimeout = true;
			}
		}

		if( ( NodeAckRequested == false ) && ( txTimeout == false ) )
		{
			if( ( LoRaMacFlags.Bits.MlmeReq == 1 ) || ( ( LoRaMacFlags.Bits.McpsReq == 1 ) ) )
			{
				if( ( LoRaMacFlags.Bits.MlmeReq == 1 ) && ( MlmeConfirm.MlmeRequest == MLME_JOIN ) )
				{// Procedure for the join request
					MlmeConfirm.NbRetries = JoinRequestTrials;

					if( MlmeConfirm.Status == LORAMAC_EVENT_INFO_STATUS_OK )
					{// Node joined successfully
						UpLinkCounter = 0;
						ChannelsNbRepCounter = 0;
						LoRaMacState &= ~LORAMAC_TX_RUNNING;
					}
					else
					{
						if( JoinRequestTrials >= MaxJoinRequestTrials )
						{
							LoRaMacState &= ~LORAMAC_TX_RUNNING;
						}
						else
						{
							LoRaMacFlags.Bits.MacDone = 0;
							// Sends the same frame again
							OnTxDelayedTimerEvent( );
						}
					}
				}
				else
				{// Procedure for all other frames
					if( ( ChannelsNbRepCounter >= LoRaMacParams.ChannelsNbRep ) || ( LoRaMacFlags.Bits.McpsInd == 1 ) )
					{
						if( LoRaMacFlags.Bits.McpsInd == 0 )
						{   // Maximum repititions without downlink. Reset MacCommandsBufferIndex. Increase ADR Ack counter.
							// Only process the case when the MAC did not receive a downlink.
							MacCommandsBufferIndex = 0;
							AdrAckCounter++;
						}

						ChannelsNbRepCounter = 0;

						if( IsUpLinkCounterFixed == false )
						{
							UpLinkCounter++;
						}

						LoRaMacState &= ~LORAMAC_TX_RUNNING;
					}
					else
					{
						LoRaMacFlags.Bits.MacDone = 0;
						// Sends the same frame again
						OnTxDelayedTimerEvent( );
					}
				}
			}
		}

		if( LoRaMacFlags.Bits.McpsInd == 1 )
		{// Procedure if we received a frame
			if( ( McpsConfirm.AckReceived == true ) || ( AckTimeoutRetriesCounter > AckTimeoutRetries ) )
			{
				AckTimeoutRetry = false;
				NodeAckRequested = false;
				if( IsUpLinkCounterFixed == false )
				{
					UpLinkCounter++;
				}
				McpsConfirm.NbRetries = AckTimeoutRetriesCounter;

				LoRaMacState &= ~LORAMAC_TX_RUNNING;
			}
		}

		if( ( AckTimeoutRetry == true ) && ( ( LoRaMacState & LORAMAC_TX_DELAYED ) == 0 ) )
		{// Retransmissions procedure for confirmed uplinks
			AckTimeoutRetry = false;
			if( ( AckTimeoutRetriesCounter < AckTimeoutRetries ) && ( AckTimeoutRetriesCounter <= MAX_ACK_RETRIES ) )
			{
				AckTimeoutRetriesCounter++;

				if( ( AckTimeoutRetriesCounter % 2 ) == 1 )
				{
					LoRaMacParams.ChannelsDatarate = MAX( LoRaMacParams.ChannelsDatarate - 1, LORAMAC_TX_MIN_DATARATE );
				}
				// Try to send the frame again
				if( ScheduleTx( ) == LORAMAC_STATUS_OK )
				{
					LoRaMacFlags.Bits.MacDone = 0;
				}
				else
				{
					// The DR is not applicable for the payload size
					McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_TX_DR_PAYLOAD_SIZE_ERROR;

					MacCommandsBufferIndex = 0;
					LoRaMacState &= ~LORAMAC_TX_RUNNING;
					NodeAckRequested = false;
					McpsConfirm.AckReceived = false;
					McpsConfirm.NbRetries = AckTimeoutRetriesCounter;
					McpsConfirm.Datarate = LoRaMacParams.ChannelsDatarate;
					if( IsUpLinkCounterFixed == false )
					{
						UpLinkCounter++;
					}
				}
			}
			else
			{
#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
				// Re-enable default channels LC1, LC2, LC3
				LoRaMacParams.ChannelsMask[0] = LoRaMacParams.ChannelsMask[0] | ( LC( 1 ) + LC( 2 ) + LC( 3 ) );
#endif
				LoRaMacState &= ~LORAMAC_TX_RUNNING;

				MacCommandsBufferIndex = 0;
				NodeAckRequested = false;
				McpsConfirm.AckReceived = false;
				McpsConfirm.NbRetries = AckTimeoutRetriesCounter;
				if( IsUpLinkCounterFixed == false )
				{
					UpLinkCounter++;
				}
			}
		}
	}
	// Handle reception for Class B and Class C
	if( ( LoRaMacState & LORAMAC_RX ) == LORAMAC_RX )
	{
		LoRaMacState &= ~LORAMAC_RX;
	}
	if( LoRaMacState == LORAMAC_IDLE )
	{
		if( LoRaMacFlags.Bits.McpsReq == 1 )
		{
			LoRaMacPrimitives->MacMcpsConfirm( &McpsConfirm );
			LoRaMacFlags.Bits.McpsReq = 0;
		}

		if( LoRaMacFlags.Bits.MlmeReq == 1 )
		{
			LoRaMacPrimitives->MacMlmeConfirm( &MlmeConfirm );
			LoRaMacFlags.Bits.MlmeReq = 0;
		}

		// Procedure done. Reset variables.
		LoRaMacFlags.Bits.MacDone = 0;
	}
	else
	{
		// Operation not finished restart timer

		timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_STATE_CHECK_TIMER, MAC_STATE_CHECK_TIMEOUT, TIMER_ONE_SHOT);
	}

	if( LoRaMacFlags.Bits.McpsInd == 1 )
	{
		if( LoRaMacDeviceClass == CLASS_C )
		{// Activate RX2 window for Class C
			OnRxWindow2TimerEvent( );
		}
		if( LoRaMacFlags.Bits.McpsIndSkip == 0 )
		{
			LoRaMacPrimitives->MacMcpsIndication( &McpsIndication );
		}
		LoRaMacFlags.Bits.McpsIndSkip = 0;
		LoRaMacFlags.Bits.McpsInd = 0;
	}
}

void OnTxDelayedTimerEvent( void )
{
	SYS_DBG("OnTxDelayedEvent\n");

	LoRaMacHeader_t macHdr;
	LoRaMacFrameCtrl_t fCtrl;

	timer_remove_attr(AK_TASK_LORAMAC_ID, LORAMAC_MAC_TXDELAYED_TIMER);

	LoRaMacState &= ~LORAMAC_TX_DELAYED;

	if( ( LoRaMacFlags.Bits.MlmeReq == 1 ) && ( MlmeConfirm.MlmeRequest == MLME_JOIN ) )
	{
		ResetMacParameters( );
		// Add a +1, since we start to count from 0
		LoRaMacParams.ChannelsDatarate = AlternateDatarate( JoinRequestTrials + 1 );

		macHdr.Value = 0;
		macHdr.Bits.MType = FRAME_TYPE_JOIN_REQ;

		fCtrl.Value = 0;
		fCtrl.Bits.Adr = AdrCtrlOn;

		/* In case of join request retransmissions, the stack must prepare
		 * the frame again, because the network server keeps track of the random
		 * LoRaMacDevNonce values to prevent reply attacks. */
		PrepareFrame( &macHdr, &fCtrl, 0, NULL, 0 );
	}

	ScheduleTx( );
}

void OnRxWindow1TimerEvent( void )
{
	SYS_DBG("OnRxWindow1Event\n");

	timer_remove_attr(AK_TASK_LORAMAC_ID, LORAMAC_MAC_RXWINDOW_1_TIMER);

	RxSlot = 0;

	if( LoRaMacDeviceClass == CLASS_C )
	{
		Radio.Standby( );
	}

#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
	RxWindowSetup( Channels[Channel].Frequency, RxWindowsParams[0].Datarate, RxWindowsParams[0].Bandwidth, RxWindowsParams[0].RxWindowTimeout, false );
#endif
}

void OnRxWindow2TimerEvent( void )
{
	SYS_DBG("OnRxWindow2Event\n");

	bool rxContinuousMode = false;

	timer_remove_attr(AK_TASK_LORAMAC_ID, LORAMAC_MAC_RXWINDOW_2_TIMER);

	if( LoRaMacDeviceClass == CLASS_C )
	{
		rxContinuousMode = true;
	}
	if( RxWindowSetup( LoRaMacParams.Rx2Channel.Frequency, RxWindowsParams[1].Datarate, RxWindowsParams[1].Bandwidth, RxWindowsParams[1].RxWindowTimeout, rxContinuousMode ) == true )
	{
		RxSlot = 1;
	}
}

void OnAckTimeoutTimerEvent( void )
{
	SYS_DBG("OnAckTimeoutEvent\n");

	timer_remove_attr(AK_TASK_LORAMAC_ID, LORAMAC_MAC_ACK_TIMEOUT);

	if( NodeAckRequested == true )
	{
		AckTimeoutRetry = true;
		LoRaMacState &= ~LORAMAC_ACK_REQ;
	}
	if( LoRaMacDeviceClass == CLASS_C )
	{
		LoRaMacFlags.Bits.MacDone = 1;
	}
}

static bool SetNextChannel( uint32_t* time )
{
	uint8_t nbEnabledChannels = 0;
	uint8_t delayTx = 0;
	uint8_t enabledChannels[LORA_MAX_NB_CHANNELS];
	uint32_t nextTxDelay = ( uint32_t )( -1 );

	memset( enabledChannels, 0, LORA_MAX_NB_CHANNELS );

#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#else
	if( CountBits( LoRaMacParams.ChannelsMask[0], 16 ) == 0 )
	{
		// Re-enable default channels, if no channel is enabled
		LoRaMacParams.ChannelsMask[0] = LoRaMacParams.ChannelsMask[0] | ( LC( 1 ) + LC( 2 ) + LC( 3 ) );
	}
#endif

	// Update Aggregated duty cycle
	if( AggregatedTimeOff <= TimerGetElapsedTime( AggregatedLastTxDoneTime ) )
	{
		AggregatedTimeOff = 0;

		// Update bands Time OFF
		for( uint8_t i = 0; i < LORA_MAX_NB_BANDS; i++ )
		{
			if( ( IsLoRaMacNetworkJoined == false ) || ( DutyCycleOn == true ) )
			{
				if( Bands[i].TimeOff <= TimerGetElapsedTime( Bands[i].LastTxDoneTime ) )
				{
					Bands[i].TimeOff = 0;
				}
				if( Bands[i].TimeOff != 0 )
				{
					nextTxDelay = MIN( Bands[i].TimeOff - TimerGetElapsedTime( Bands[i].LastTxDoneTime ), nextTxDelay );
				}
			}
			else
			{
				if( DutyCycleOn == false )
				{
					Bands[i].TimeOff = 0;
				}
			}
		}

		// Search how many channels are enabled
		for( uint8_t i = 0, k = 0; i < LORA_MAX_NB_CHANNELS; i += 16, k++ )
		{
			for( uint8_t j = 0; j < 16; j++ )
			{
#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )
				if( ( ChannelsMaskRemaining[k] & ( 1 << j ) ) != 0 )
#else
				if( ( LoRaMacParams.ChannelsMask[k] & ( 1 << j ) ) != 0 )
#endif
				{
					if( Channels[i + j].Frequency == 0 )
					{ // Check if the channel is enabled
						continue;
					}
#if defined( USE_BAND_868 ) || defined( USE_BAND_433 ) || defined( USE_BAND_780 )
					if( IsLoRaMacNetworkJoined == false )
					{
						if( ( JOIN_CHANNELS & ( 1 << j ) ) == 0 )
						{
							continue;
						}
					}
#endif
					if( ( ( Channels[i + j].DrRange.Fields.Min <= LoRaMacParams.ChannelsDatarate ) &&
						  ( LoRaMacParams.ChannelsDatarate <= Channels[i + j].DrRange.Fields.Max ) ) == false )
					{ // Check if the current channel selection supports the given datarate
						continue;
					}
					if( Bands[Channels[i + j].Band].TimeOff > 0 )
					{ // Check if the band is available for transmission
						delayTx++;
						continue;
					}
					enabledChannels[nbEnabledChannels++] = i + j;
				}
			}
		}
	}
	else
	{
		delayTx++;
		nextTxDelay = AggregatedTimeOff - TimerGetElapsedTime( AggregatedLastTxDoneTime );
	}

	if( nbEnabledChannels > 0 )
	{
		Channel = enabledChannels[randr( 0, nbEnabledChannels - 1 )];
#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#endif
		*time = 0;
		return true;
	}
	else
	{
		if( delayTx > 0 )
		{
			// Delay transmission due to AggregatedTimeOff or to a band time off
			*time = nextTxDelay;
			return true;
		}
		// Datarate not supported by any channel
		*time = 0;
		return false;
	}
}

static bool RxWindowSetup( uint32_t freq, int8_t datarate, uint32_t bandwidth, uint16_t timeout, bool rxContinuous )
{
	uint8_t downlinkDatarate = Datarates[datarate];
	RadioModems_t modem;

	if( Radio.GetStatus( ) == RF_IDLE )
	{
		//SYS_DBG("RxWindowSetup-freq\n");
		Radio.SetChannel( freq );

		// Store downlink datarate
		McpsIndication.RxDatarate = ( uint8_t ) datarate;

#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
		if( datarate == DR_7 )
		{
			modem = MODEM_FSK;
			Radio.SetRxConfig( modem, 50e3, downlinkDatarate * 1e3, 0, 83.333e3, 5, timeout, false, 0, true, 0, 0, false, rxContinuous );
		}
		else
		{
			modem = MODEM_LORA;

			/*default:Radio.SetRxConfig( modem, bandwidth, downlinkDatarate, 1, 0, 8, timeout, false, 0, false, 0, 0, false, rxContinuous );*/

			Radio.SetRxConfig( modem, bandwidth, downlinkDatarate, 1, 0, 8, timeout, false, 0, false, 0, 0, SINGLE_CHANNEL_GW_IQ_RX, rxContinuous );
		}
#elif defined( USE_BAND_470 ) || defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#endif

		if( RepeaterSupport == true )
		{
			Radio.SetMaxPayloadLength( modem, MaxPayloadOfDatarateRepeater[datarate] + LORA_MAC_FRMPAYLOAD_OVERHEAD );
		}
		else
		{
			Radio.SetMaxPayloadLength( modem, MaxPayloadOfDatarate[datarate] + LORA_MAC_FRMPAYLOAD_OVERHEAD );
		}

		if( rxContinuous == false )
		{
			//xprintf("Rx(%d)\n", LoRaMacParams.MaxRxWindow);
			Radio.Rx( LoRaMacParams.MaxRxWindow );
		}
		else
		{
			//xprintf("Rx(%d)\n", 0);
			Radio.Rx( 0 ); // Continuous mode
		}
		return true;
	}
	return false;
}

static bool Rx2FreqInRange( uint32_t freq )
{
#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
	if( Radio.CheckRfFrequency( freq ) == true )
#endif
	{
		return true;
	}
	return false;
}

static bool ValidatePayloadLength( uint8_t lenN, int8_t datarate, uint8_t fOptsLen )
{
	uint16_t maxN = 0;
	uint16_t payloadSize = 0;

	// Get the maximum payload length
	if( RepeaterSupport == true )
	{
		maxN = MaxPayloadOfDatarateRepeater[datarate];
	}
	else
	{
		maxN = MaxPayloadOfDatarate[datarate];
	}

	// Calculate the resulting payload size
	payloadSize = ( lenN + fOptsLen );

	// Validation of the application payload size
	if( ( payloadSize <= maxN ) && ( payloadSize <= LORAMAC_PHY_MAXPAYLOAD ) )
	{
		return true;
	}
	return false;
}

static uint8_t CountBits( uint16_t mask, uint8_t nbBits )
{
	uint8_t nbActiveBits = 0;

	for( uint8_t j = 0; j < nbBits; j++ )
	{
		if( ( mask & ( 1 << j ) ) == ( 1 << j ) )
		{
			nbActiveBits++;
		}
	}
	return nbActiveBits;
}

#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#endif

static bool ValidateDatarate( int8_t datarate, uint16_t* channelsMask )
{
	if( ValueInRange( datarate, LORAMAC_TX_MIN_DATARATE, LORAMAC_TX_MAX_DATARATE ) == false )
	{
		return false;
	}
	for( uint8_t i = 0, k = 0; i < LORA_MAX_NB_CHANNELS; i += 16, k++ )
	{
		for( uint8_t j = 0; j < 16; j++ )
		{
			if( ( ( channelsMask[k] & ( 1 << j ) ) != 0 ) )
			{// Check datarate validity for enabled channels
				if( ValueInRange( datarate, Channels[i + j].DrRange.Fields.Min, Channels[i + j].DrRange.Fields.Max ) == true )
				{
					// At least 1 channel has been found we can return OK.
					return true;
				}
			}
		}
	}
	return false;
}

static int8_t LimitTxPower( int8_t txPower, int8_t maxBandTxPower )
{
	int8_t resultTxPower = txPower;

	// Limit tx power to the band max
	resultTxPower =  MAX( txPower, maxBandTxPower );

#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#endif
	return resultTxPower;
}

static bool ValueInRange( int8_t value, int8_t min, int8_t max )
{
	if( ( value >= min ) && ( value <= max ) )
	{
		return true;
	}
	return false;
}

static bool DisableChannelInMask( uint8_t id, uint16_t* mask )
{
	uint8_t index = 0;
	index = id / 16;

	if( ( index > 4 ) || ( id >= LORA_MAX_NB_CHANNELS ) )
	{
		return false;
	}

	// Deactivate channel
	mask[index] &= ~( 1 << ( id % 16 ) );

	return true;
}

static bool AdrNextDr( bool adrEnabled, bool updateChannelMask, int8_t* datarateOut )
{
	bool adrAckReq = false;
	int8_t datarate = LoRaMacParams.ChannelsDatarate;

	if( adrEnabled == true )
	{
		if( datarate == LORAMAC_TX_MIN_DATARATE )
		{
			AdrAckCounter = 0;
			adrAckReq = false;
		}
		else
		{
			if( AdrAckCounter >= ADR_ACK_LIMIT )
			{
				adrAckReq = true;
				LoRaMacParams.ChannelsTxPower = LORAMAC_MAX_TX_POWER;
			}
			else
			{
				adrAckReq = false;
			}
			if( AdrAckCounter >= ( ADR_ACK_LIMIT + ADR_ACK_DELAY ) )
			{
				if( ( AdrAckCounter % ADR_ACK_DELAY ) == 1 )
				{
#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
					if( datarate > LORAMAC_TX_MIN_DATARATE )
					{
						datarate--;
					}
					if( datarate == LORAMAC_TX_MIN_DATARATE )
					{
						if( updateChannelMask == true )
						{
							// Re-enable default channels LC1, LC2, LC3
							LoRaMacParams.ChannelsMask[0] = LoRaMacParams.ChannelsMask[0] | ( LC( 1 ) + LC( 2 ) + LC( 3 ) );
						}
					}
#elif defined( USE_BAND_470 )

#endif
				}
			}
		}
	}

	*datarateOut = datarate;

	return adrAckReq;
}

static LoRaMacStatus_t AddMacCommand( uint8_t cmd, uint8_t p1, uint8_t p2 )
{
	LoRaMacStatus_t status = LORAMAC_STATUS_BUSY;
	// The maximum buffer length must take MAC commands to re-send into account.
	uint8_t bufLen = LORA_MAC_COMMAND_MAX_LENGTH - MacCommandsBufferToRepeatIndex;

	switch( cmd )
	{
	case MOTE_MAC_LINK_CHECK_REQ:
		if( MacCommandsBufferIndex < bufLen )
		{
			MacCommandsBuffer[MacCommandsBufferIndex++] = cmd;
			// No payload for this command
			status = LORAMAC_STATUS_OK;
		}
		break;
	case MOTE_MAC_LINK_ADR_ANS:
		if( MacCommandsBufferIndex < ( bufLen - 1 ) )
		{
			MacCommandsBuffer[MacCommandsBufferIndex++] = cmd;
			// Margin
			MacCommandsBuffer[MacCommandsBufferIndex++] = p1;
			status = LORAMAC_STATUS_OK;
		}
		break;
	case MOTE_MAC_DUTY_CYCLE_ANS:
		if( MacCommandsBufferIndex < bufLen )
		{
			MacCommandsBuffer[MacCommandsBufferIndex++] = cmd;
			// No payload for this answer
			status = LORAMAC_STATUS_OK;
		}
		break;
	case MOTE_MAC_RX_PARAM_SETUP_ANS:
		if( MacCommandsBufferIndex < ( bufLen - 1 ) )
		{
			MacCommandsBuffer[MacCommandsBufferIndex++] = cmd;
			// Status: Datarate ACK, Channel ACK
			MacCommandsBuffer[MacCommandsBufferIndex++] = p1;
			status = LORAMAC_STATUS_OK;
		}
		break;
	case MOTE_MAC_DEV_STATUS_ANS:
		if( MacCommandsBufferIndex < ( bufLen - 2 ) )
		{
			MacCommandsBuffer[MacCommandsBufferIndex++] = cmd;
			// 1st byte Battery
			// 2nd byte Margin
			MacCommandsBuffer[MacCommandsBufferIndex++] = p1;
			MacCommandsBuffer[MacCommandsBufferIndex++] = p2;
			status = LORAMAC_STATUS_OK;
		}
		break;
	case MOTE_MAC_NEW_CHANNEL_ANS:
		if( MacCommandsBufferIndex < ( bufLen - 1 ) )
		{
			MacCommandsBuffer[MacCommandsBufferIndex++] = cmd;
			// Status: Datarate range OK, Channel frequency OK
			MacCommandsBuffer[MacCommandsBufferIndex++] = p1;
			status = LORAMAC_STATUS_OK;
		}
		break;
	case MOTE_MAC_RX_TIMING_SETUP_ANS:
		if( MacCommandsBufferIndex < bufLen )
		{
			MacCommandsBuffer[MacCommandsBufferIndex++] = cmd;
			// No payload for this answer
			status = LORAMAC_STATUS_OK;
		}
		break;
	default:
		return LORAMAC_STATUS_SERVICE_UNKNOWN;
	}
	if( status == LORAMAC_STATUS_OK )
	{
		MacCommandsInNextTx = true;
	}
	return status;
}

static uint8_t ParseMacCommandsToRepeat( uint8_t* cmdBufIn, uint8_t length, uint8_t* cmdBufOut )
{
	uint8_t i = 0;
	uint8_t cmdCount = 0;

	if( ( cmdBufIn == NULL ) || ( cmdBufOut == NULL ) )
	{
		return 0;
	}

	for( i = 0; i < length; i++ )
	{
		switch( cmdBufIn[i] )
		{
		// STICKY
		case MOTE_MAC_RX_PARAM_SETUP_ANS:
		{
			cmdBufOut[cmdCount++] = cmdBufIn[i++];
			cmdBufOut[cmdCount++] = cmdBufIn[i];
			break;
		}
		case MOTE_MAC_RX_TIMING_SETUP_ANS:
		{
			cmdBufOut[cmdCount++] = cmdBufIn[i];
			break;
		}
			// NON-STICKY
		case MOTE_MAC_DEV_STATUS_ANS:
		{ // 2 bytes payload
			i += 2;
			break;
		}
		case MOTE_MAC_LINK_ADR_ANS:
		case MOTE_MAC_NEW_CHANNEL_ANS:
		{ // 1 byte payload
			i++;
			break;
		}
		case MOTE_MAC_DUTY_CYCLE_ANS:
		case MOTE_MAC_LINK_CHECK_REQ:
		{ // 0 byte payload
			break;
		}
		default:
			break;
		}
	}

	return cmdCount;
}

static void ProcessMacCommands( uint8_t *payload, uint8_t macIndex, uint8_t commandsSize, uint8_t snr )
{
	while( macIndex < commandsSize )
	{
		// Decode Frame MAC commands
		switch( payload[macIndex++] )
		{
		case SRV_MAC_LINK_CHECK_ANS:
			MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_OK;
			MlmeConfirm.DemodMargin = payload[macIndex++];
			MlmeConfirm.NbGateways = payload[macIndex++];
			break;
		case SRV_MAC_LINK_ADR_REQ:
		{
			uint8_t i;
			uint8_t status = 0x07;
			uint16_t chMask;
			int8_t txPower = 0;
			int8_t datarate = 0;
			uint8_t nbRep = 0;
			uint8_t chMaskCntl = 0;
			uint16_t channelsMask[6] = { 0, 0, 0, 0, 0, 0 };

			// Initialize local copy of the channels mask array
			for( i = 0; i < 6; i++ )
			{
				channelsMask[i] = LoRaMacParams.ChannelsMask[i];
			}
			datarate = payload[macIndex++];
			txPower = datarate & 0x0F;
			datarate = ( datarate >> 4 ) & 0x0F;

			if( ( AdrCtrlOn == false ) &&
					( ( LoRaMacParams.ChannelsDatarate != datarate ) || ( LoRaMacParams.ChannelsTxPower != txPower ) ) )
			{ // ADR disabled don't handle ADR requests if server tries to change datarate or txpower
				// Answer the server with fail status
				// Power ACK     = 0
				// Data rate ACK = 0
				// Channel mask  = 0
				AddMacCommand( MOTE_MAC_LINK_ADR_ANS, 0, 0 );
				macIndex += 3;  // Skip over the remaining bytes of the request
				break;
			}
			chMask = ( uint16_t )payload[macIndex++];
			chMask |= ( uint16_t )payload[macIndex++] << 8;

			nbRep = payload[macIndex++];
			chMaskCntl = ( nbRep >> 4 ) & 0x07;
			nbRep &= 0x0F;
			if( nbRep == 0 )
			{
				nbRep = 1;
			}
#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
			if( ( chMaskCntl == 0 ) && ( chMask == 0 ) )
			{
				status &= 0xFE; // Channel mask KO
			}
			else if( ( ( chMaskCntl >= 1 ) && ( chMaskCntl <= 5 )) ||
					 ( chMaskCntl >= 7 ) )
			{
				// RFU
				status &= 0xFE; // Channel mask KO
			}
			else
			{
				for( i = 0; i < LORA_MAX_NB_CHANNELS; i++ )
				{
					if( chMaskCntl == 6 )
					{
						if( Channels[i].Frequency != 0 )
						{
							chMask |= 1 << i;
						}
					}
					else
					{
						if( ( ( chMask & ( 1 << i ) ) != 0 ) &&
								( Channels[i].Frequency == 0 ) )
						{// Trying to enable an undefined channel
							status &= 0xFE; // Channel mask KO
						}
					}
				}
				channelsMask[0] = chMask;
			}
#elif defined( USE_BAND_470 )

#endif
			if( ValidateDatarate( datarate, channelsMask ) == false )
			{
				status &= 0xFD; // Datarate KO
			}

			//
			// Remark MaxTxPower = 0 and MinTxPower = 5
			//
			if( ValueInRange( txPower, LORAMAC_MAX_TX_POWER, LORAMAC_MIN_TX_POWER ) == false )
			{
				status &= 0xFB; // TxPower KO
			}
			if( ( status & 0x07 ) == 0x07 )
			{
				LoRaMacParams.ChannelsDatarate = datarate;
				LoRaMacParams.ChannelsTxPower = txPower;

				memcpy( ( uint8_t* )LoRaMacParams.ChannelsMask, ( uint8_t* )channelsMask, sizeof( LoRaMacParams.ChannelsMask ) );

				LoRaMacParams.ChannelsNbRep = nbRep;
#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#endif
			}
			AddMacCommand( MOTE_MAC_LINK_ADR_ANS, status, 0 );
		}
			break;
		case SRV_MAC_DUTY_CYCLE_REQ:
			MaxDCycle = payload[macIndex++];
			AggregatedDCycle = 1 << MaxDCycle;
			AddMacCommand( MOTE_MAC_DUTY_CYCLE_ANS, 0, 0 );
			break;
		case SRV_MAC_RX_PARAM_SETUP_REQ:
		{
			uint8_t status = 0x07;
			int8_t datarate = 0;
			int8_t drOffset = 0;
			uint32_t freq = 0;

			drOffset = ( payload[macIndex] >> 4 ) & 0x07;
			datarate = payload[macIndex] & 0x0F;
			macIndex++;

			freq =  ( uint32_t )payload[macIndex++];
			freq |= ( uint32_t )payload[macIndex++] << 8;
			freq |= ( uint32_t )payload[macIndex++] << 16;
			freq *= 100;

			if( Rx2FreqInRange( freq ) == false )
			{
				status &= 0xFE; // Channel frequency KO
			}

			if( ValueInRange( datarate, LORAMAC_RX_MIN_DATARATE, LORAMAC_RX_MAX_DATARATE ) == false )
			{
				status &= 0xFD; // Datarate KO
			}
#if ( defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID ) )

#endif
			if( ValueInRange( drOffset, LORAMAC_MIN_RX1_DR_OFFSET, LORAMAC_MAX_RX1_DR_OFFSET ) == false )
			{
				status &= 0xFB; // Rx1DrOffset range KO
			}

			if( ( status & 0x07 ) == 0x07 )
			{
				LoRaMacParams.Rx2Channel.Datarate = datarate;
				LoRaMacParams.Rx2Channel.Frequency = freq;
				LoRaMacParams.Rx1DrOffset = drOffset;
			}
			AddMacCommand( MOTE_MAC_RX_PARAM_SETUP_ANS, status, 0 );
		}
			break;
		case SRV_MAC_DEV_STATUS_REQ:
		{
			uint8_t batteryLevel = BAT_LEVEL_NO_MEASURE;
			if( ( LoRaMacCallbacks != NULL ) && ( LoRaMacCallbacks->GetBatteryLevel != NULL ) )
			{
				batteryLevel = LoRaMacCallbacks->GetBatteryLevel( );
			}
			AddMacCommand( MOTE_MAC_DEV_STATUS_ANS, batteryLevel, snr );
			break;
		}
		case SRV_MAC_NEW_CHANNEL_REQ:
		{
			uint8_t status = 0x03;

#if defined( USE_BAND_470 ) || defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#else
			int8_t channelIndex = 0;
			ChannelParams_t chParam;

			channelIndex = payload[macIndex++];
			chParam.Frequency = ( uint32_t )payload[macIndex++];
			chParam.Frequency |= ( uint32_t )payload[macIndex++] << 8;
			chParam.Frequency |= ( uint32_t )payload[macIndex++] << 16;
			chParam.Frequency *= 100;
			chParam.DrRange.Value = payload[macIndex++];

			LoRaMacState |= LORAMAC_TX_CONFIG;
			if( chParam.Frequency == 0 )
			{
				if( channelIndex < 3 )
				{
					status &= 0xFC;
				}
				else
				{
					if( LoRaMacChannelRemove( channelIndex ) != LORAMAC_STATUS_OK )
					{
						status &= 0xFC;
					}
				}
			}
			else
			{
				switch( LoRaMacChannelAdd( channelIndex, chParam ) )
				{
				case LORAMAC_STATUS_OK:
				{
					break;
				}
				case LORAMAC_STATUS_FREQUENCY_INVALID:
				{
					status &= 0xFE;
					break;
				}
				case LORAMAC_STATUS_DATARATE_INVALID:
				{
					status &= 0xFD;
					break;
				}
				case LORAMAC_STATUS_FREQ_AND_DR_INVALID:
				{
					status &= 0xFC;
					break;
				}
				default:
				{
					status &= 0xFC;
					break;
				}
				}
			}
			LoRaMacState &= ~LORAMAC_TX_CONFIG;
#endif
			AddMacCommand( MOTE_MAC_NEW_CHANNEL_ANS, status, 0 );
		}
			break;
		case SRV_MAC_RX_TIMING_SETUP_REQ:
		{
			uint8_t delay = payload[macIndex++] & 0x0F;

			if( delay == 0 )
			{
				delay++;
			}
			LoRaMacParams.ReceiveDelay1 = delay * 1e3;
			LoRaMacParams.ReceiveDelay2 = LoRaMacParams.ReceiveDelay1 + 1e3;
			AddMacCommand( MOTE_MAC_RX_TIMING_SETUP_ANS, 0, 0 );
		}
			break;
		default:
			// Unknown command. ABORT MAC commands processing
			return;
		}
	}
}

LoRaMacStatus_t Send( LoRaMacHeader_t *macHdr, uint8_t fPort, void *fBuffer, uint16_t fBufferSize )
{
	LoRaMacFrameCtrl_t fCtrl;
	LoRaMacStatus_t status = LORAMAC_STATUS_PARAMETER_INVALID;

	fCtrl.Value = 0;
	fCtrl.Bits.FOptsLen      = 0;
	fCtrl.Bits.FPending      = 0;
	fCtrl.Bits.Ack           = false;
	fCtrl.Bits.AdrAckReq     = false;
	fCtrl.Bits.Adr           = AdrCtrlOn;

	// Prepare the frame
	status = PrepareFrame( macHdr, &fCtrl, fPort, fBuffer, fBufferSize );

	// Validate status
	if( status != LORAMAC_STATUS_OK )
	{
		//SYS_DBG("PrepareFrame return err\n");

		return status;
	}

	// Reset confirm parameters
	McpsConfirm.NbRetries = 0;
	McpsConfirm.AckReceived = false;
	McpsConfirm.UpLinkCounter = UpLinkCounter;

	status = ScheduleTx( );

	return status;
}

static LoRaMacStatus_t ScheduleTx( void )
{
	//SYS_DBG("ScheduleTx\n");

	uint32_t dutyCycleTimeOff = 0;

	// Check if the device is off
	if( MaxDCycle == 255 )
	{
		return LORAMAC_STATUS_DEVICE_OFF;
	}
	if( MaxDCycle == 0 )
	{
		AggregatedTimeOff = 0;
	}

	// Select channel
	while( SetNextChannel( &dutyCycleTimeOff ) == false )
	{
		// Set the default datarate
		LoRaMacParams.ChannelsDatarate = LoRaMacParamsDefaults.ChannelsDatarate;

#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
		// Re-enable default channels LC1, LC2, LC3
		LoRaMacParams.ChannelsMask[0] = LoRaMacParams.ChannelsMask[0] | ( LC( 1 ) + LC( 2 ) + LC( 3 ) );
#endif
	}

	// Compute Rx1 windows parameters
	RxWindowsParams[0] = ComputeRxWindowParameters( MAX( DR_0, LoRaMacParams.ChannelsDatarate - LoRaMacParams.Rx1DrOffset ), LoRaMacParams.SystemMaxRxError );

	// Compute Rx2 windows parameters
	RxWindowsParams[1] = ComputeRxWindowParameters( LoRaMacParams.Rx2Channel.Datarate, LoRaMacParams.SystemMaxRxError );

	if( IsLoRaMacNetworkJoined == false )
	{
		RxWindow1Delay = LoRaMacParams.JoinAcceptDelay1 + RxWindowsParams[0].RxOffset;
		RxWindow2Delay = LoRaMacParams.JoinAcceptDelay2 + RxWindowsParams[1].RxOffset;
	}
	else
	{
		if( ValidatePayloadLength( LoRaMacTxPayloadLen, LoRaMacParams.ChannelsDatarate, MacCommandsBufferIndex ) == false )
		{
			return LORAMAC_STATUS_LENGTH_ERROR;
		}
		RxWindow1Delay = LoRaMacParams.ReceiveDelay1 + RxWindowsParams[0].RxOffset;
		RxWindow2Delay = LoRaMacParams.ReceiveDelay2 + RxWindowsParams[1].RxOffset;
	}

	// Schedule transmission of frame
	if( dutyCycleTimeOff == 0 )
	{
		// Try to send now
		return SendFrameOnChannel( Channels[Channel] );
	}
	else
	{
		// Send later - prepare timer
		LoRaMacState |= LORAMAC_TX_DELAYED;

		timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_TXDELAYED_TIMER, dutyCycleTimeOff, TIMER_ONE_SHOT);

		return LORAMAC_STATUS_OK;
	}
}

static uint16_t JoinDutyCycle( void )
{
	uint16_t dutyCycle = 0;
	uint32_t timeElapsed = TimerGetElapsedTime( LoRaMacInitializationTime );

	if( timeElapsed < 3600e3 )
	{
		dutyCycle = BACKOFF_DC_1_HOUR;
	}
	else if( timeElapsed < ( 3600e3 + 36000e3 ) )
	{
		dutyCycle = BACKOFF_DC_10_HOURS;
	}
	else
	{
		dutyCycle = BACKOFF_DC_24_HOURS;
	}
	return dutyCycle;
}

static void CalculateBackOff( uint8_t channel )
{
	uint16_t dutyCycle = Bands[Channels[channel].Band].DCycle;
	uint16_t joinDutyCycle = 0;

	// Reset time-off to initial value.
	Bands[Channels[channel].Band].TimeOff = 0;

	if( IsLoRaMacNetworkJoined == false )
	{
		// The node has not joined yet. Apply join duty cycle to all regions.
		joinDutyCycle = JoinDutyCycle( );
		dutyCycle = MAX( dutyCycle, joinDutyCycle );

		// Update Band time-off.
		Bands[Channels[channel].Band].TimeOff = TxTimeOnAir * dutyCycle - TxTimeOnAir;
	}
	else
	{
		if( DutyCycleOn == true )
		{
			Bands[Channels[channel].Band].TimeOff = TxTimeOnAir * dutyCycle - TxTimeOnAir;
		}
	}

	// Update Aggregated Time OFF
	AggregatedTimeOff = AggregatedTimeOff + ( TxTimeOnAir * AggregatedDCycle - TxTimeOnAir );
}

static int8_t AlternateDatarate( uint16_t nbTrials )
{
	int8_t datarate = LORAMAC_TX_MIN_DATARATE;
#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#else

	/* binhnt61 */
	/* BEGIN */
	(void)nbTrials;
	datarate = LORAMAC_DEFAULT_DATARATE;
	/* END */
#if 0
	if( ( nbTrials % 48 ) == 0 )
	{
		datarate = DR_0;
	}
	else if( ( nbTrials % 32 ) == 0 )
	{
		datarate = DR_1;
	}
	else if( ( nbTrials % 24 ) == 0 )
	{
		datarate = DR_2;
	}
	else if( ( nbTrials % 16 ) == 0 )
	{
		datarate = DR_3;
	}
	else if( ( nbTrials % 8 ) == 0 )
	{
		datarate = DR_4;
	}
	else
	{
		datarate = DR_5;

	}
#endif

#endif
	return datarate;
}

static void ResetMacParameters( void )
{
	IsLoRaMacNetworkJoined = false;

	// Counters
	UpLinkCounter = 0;
	DownLinkCounter = 0;
	AdrAckCounter = 0;

	ChannelsNbRepCounter = 0;

	AckTimeoutRetries = 1;
	AckTimeoutRetriesCounter = 1;
	AckTimeoutRetry = false;

	MaxDCycle = 0;
	AggregatedDCycle = 1;

	MacCommandsBufferIndex = 0;
	MacCommandsBufferToRepeatIndex = 0;

	IsRxWindowsEnabled = true;

	LoRaMacParams.ChannelsTxPower = LoRaMacParamsDefaults.ChannelsTxPower;
	LoRaMacParams.ChannelsDatarate = LoRaMacParamsDefaults.ChannelsDatarate;

	LoRaMacParams.Rx1DrOffset = LoRaMacParamsDefaults.Rx1DrOffset;
	LoRaMacParams.Rx2Channel = LoRaMacParamsDefaults.Rx2Channel;

	memcpy( ( uint8_t* ) LoRaMacParams.ChannelsMask, ( uint8_t* ) LoRaMacParamsDefaults.ChannelsMask, sizeof( LoRaMacParams.ChannelsMask ) );

#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )
	memcpy( ( uint8_t* ) ChannelsMaskRemaining, ( uint8_t* ) LoRaMacParamsDefaults.ChannelsMask, sizeof( LoRaMacParams.ChannelsMask ) );
#endif


	NodeAckRequested = false;
	SrvAckRequested = false;
	MacCommandsInNextTx = false;

	// Reset Multicast downlink counters
	MulticastParams_t *cur = MulticastChannels;
	while( cur != NULL )
	{
		cur->DownLinkCounter = 0;
		cur = cur->Next;
	}

	// Initialize channel index.
	Channel = LORA_MAX_NB_CHANNELS;
}

LoRaMacStatus_t PrepareFrame( LoRaMacHeader_t *macHdr, LoRaMacFrameCtrl_t *fCtrl, uint8_t fPort, void *fBuffer, uint16_t fBufferSize )
{
	uint16_t i;
	uint8_t pktHeaderLen = 0;
	uint32_t mic = 0;
	const void* payload = fBuffer;
	uint8_t framePort = fPort;

	LoRaMacBufferPktLen = 0;

	NodeAckRequested = false;

	if( fBuffer == NULL )
	{
		fBufferSize = 0;
	}

	LoRaMacTxPayloadLen = fBufferSize;

	LoRaMacBuffer[pktHeaderLen++] = macHdr->Value;

	switch( macHdr->Bits.MType )
	{
	case FRAME_TYPE_JOIN_REQ:
		LoRaMacBufferPktLen = pktHeaderLen;

		memcpyr( LoRaMacBuffer + LoRaMacBufferPktLen, LoRaMacAppEui, 8 );
		LoRaMacBufferPktLen += 8;
		memcpyr( LoRaMacBuffer + LoRaMacBufferPktLen, LoRaMacDevEui, 8 );
		LoRaMacBufferPktLen += 8;

		LoRaMacDevNonce = Radio.Random( );

		//SYS_DBG("LoRaMacDevNonce:x%04X\n", LoRaMacDevNonce);

		LoRaMacBuffer[LoRaMacBufferPktLen++] = LoRaMacDevNonce & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen++] = ( LoRaMacDevNonce >> 8 ) & 0xFF;

		LoRaMacJoinComputeMic( LoRaMacBuffer, LoRaMacBufferPktLen & 0xFF, LoRaMacAppKey, &mic );

		//SYS_DBG("micTx:x%08X\n", mic);

		LoRaMacBuffer[LoRaMacBufferPktLen++] = mic & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen++] = ( mic >> 8 ) & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen++] = ( mic >> 16 ) & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen++] = ( mic >> 24 ) & 0xFF;

		break;
	case FRAME_TYPE_DATA_CONFIRMED_UP:
		NodeAckRequested = true;
		//Intentional fallthrough
	case FRAME_TYPE_DATA_UNCONFIRMED_UP:
		if( IsLoRaMacNetworkJoined == false )
		{
			return LORAMAC_STATUS_NO_NETWORK_JOINED; // No network has been joined yet
		}

		fCtrl->Bits.AdrAckReq = AdrNextDr( fCtrl->Bits.Adr, true, &LoRaMacParams.ChannelsDatarate );

		if( SrvAckRequested == true )
		{
			SrvAckRequested = false;
			fCtrl->Bits.Ack = 1;
		}

		LoRaMacBuffer[pktHeaderLen++] = ( LoRaMacDevAddr ) & 0xFF;
		LoRaMacBuffer[pktHeaderLen++] = ( LoRaMacDevAddr >> 8 ) & 0xFF;
		LoRaMacBuffer[pktHeaderLen++] = ( LoRaMacDevAddr >> 16 ) & 0xFF;
		LoRaMacBuffer[pktHeaderLen++] = ( LoRaMacDevAddr >> 24 ) & 0xFF;

		LoRaMacBuffer[pktHeaderLen++] = fCtrl->Value;

		LoRaMacBuffer[pktHeaderLen++] = UpLinkCounter & 0xFF;
		LoRaMacBuffer[pktHeaderLen++] = ( UpLinkCounter >> 8 ) & 0xFF;

		// Copy the MAC commands which must be re-send into the MAC command buffer
		memcpy( &MacCommandsBuffer[MacCommandsBufferIndex], MacCommandsBufferToRepeat, MacCommandsBufferToRepeatIndex );
		MacCommandsBufferIndex += MacCommandsBufferToRepeatIndex;

		if( ( payload != NULL ) && ( LoRaMacTxPayloadLen > 0 ) )
		{
			if( ( MacCommandsBufferIndex <= LORA_MAC_COMMAND_MAX_LENGTH ) && ( MacCommandsInNextTx == true ) )
			{
				fCtrl->Bits.FOptsLen += MacCommandsBufferIndex;

				// Update FCtrl field with new value of OptionsLength
				LoRaMacBuffer[0x05] = fCtrl->Value;
				for( i = 0; i < MacCommandsBufferIndex; i++ )
				{
					LoRaMacBuffer[pktHeaderLen++] = MacCommandsBuffer[i];
				}
			}
		}
		else
		{
			if( ( MacCommandsBufferIndex > 0 ) && ( MacCommandsInNextTx ) )
			{
				LoRaMacTxPayloadLen = MacCommandsBufferIndex;
				payload = MacCommandsBuffer;
				framePort = 0;
			}
		}
		MacCommandsInNextTx = false;
		// Store MAC commands which must be re-send in case the device does not receive a downlink anymore
		MacCommandsBufferToRepeatIndex = ParseMacCommandsToRepeat( MacCommandsBuffer, MacCommandsBufferIndex, MacCommandsBufferToRepeat );
		if( MacCommandsBufferToRepeatIndex > 0 )
		{
			MacCommandsInNextTx = true;
		}

		if( ( payload != NULL ) && ( LoRaMacTxPayloadLen > 0 ) )
		{
			LoRaMacBuffer[pktHeaderLen++] = framePort;

			if( framePort == 0 )
			{
				LoRaMacPayloadEncrypt( (uint8_t* ) payload, LoRaMacTxPayloadLen, LoRaMacNwkSKey, LoRaMacDevAddr, UP_LINK, UpLinkCounter, &LoRaMacBuffer[pktHeaderLen] );
			}
			else
			{
				LoRaMacPayloadEncrypt( (uint8_t* ) payload, LoRaMacTxPayloadLen, LoRaMacAppSKey, LoRaMacDevAddr, UP_LINK, UpLinkCounter, &LoRaMacBuffer[pktHeaderLen] );
			}
		}
		LoRaMacBufferPktLen = pktHeaderLen + LoRaMacTxPayloadLen;

		LoRaMacComputeMic( LoRaMacBuffer, LoRaMacBufferPktLen, LoRaMacNwkSKey, LoRaMacDevAddr, UP_LINK, UpLinkCounter, &mic );

		LoRaMacBuffer[LoRaMacBufferPktLen + 0] = mic & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen + 1] = ( mic >> 8 ) & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen + 2] = ( mic >> 16 ) & 0xFF;
		LoRaMacBuffer[LoRaMacBufferPktLen + 3] = ( mic >> 24 ) & 0xFF;

		LoRaMacBufferPktLen += LORAMAC_MFR_LEN;

		break;
	case FRAME_TYPE_PROPRIETARY:
		if( ( fBuffer != NULL ) && ( LoRaMacTxPayloadLen > 0 ) )
		{
			memcpy( LoRaMacBuffer + pktHeaderLen, ( uint8_t* ) fBuffer, LoRaMacTxPayloadLen );
			LoRaMacBufferPktLen = pktHeaderLen + LoRaMacTxPayloadLen;
		}
		break;
	default:
		return LORAMAC_STATUS_SERVICE_UNKNOWN;
	}

	return LORAMAC_STATUS_OK;
}

LoRaMacStatus_t SendFrameOnChannel( ChannelParams_t channel )
{
	//SYS_DBG("SendFrameOnChannel\n");

	int8_t datarate = Datarates[LoRaMacParams.ChannelsDatarate];
	int8_t txPowerIndex = 0;
	int8_t txPower = 0;

	txPowerIndex = LimitTxPower( LoRaMacParams.ChannelsTxPower, Bands[channel.Band].TxMaxPower );
	txPower = TxPowers[txPowerIndex];

	MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;
	McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;
	McpsConfirm.Datarate = LoRaMacParams.ChannelsDatarate;
	McpsConfirm.TxPower = txPowerIndex;
	McpsConfirm.UpLinkFrequency = channel.Frequency;

	//SYS_DBG("SendFrameOnChannel-freq\n");
	Radio.SetChannel( channel.Frequency );

#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
	if( LoRaMacParams.ChannelsDatarate == DR_7 )
	{ // High Speed FSK channel
		Radio.SetMaxPayloadLength( MODEM_FSK, LoRaMacBufferPktLen );
		Radio.SetTxConfig( MODEM_FSK, txPower, 25e3, 0, datarate * 1e3, 0, 5, false, true, 0, 0, false, 3e3 );
		TxTimeOnAir = Radio.TimeOnAir( MODEM_FSK, LoRaMacBufferPktLen );

	}
	else if( LoRaMacParams.ChannelsDatarate == DR_6 )
	{ // High speed LoRa channel
		Radio.SetMaxPayloadLength( MODEM_LORA, LoRaMacBufferPktLen );
		Radio.SetTxConfig( MODEM_LORA, txPower, 0, 1, datarate, 1, 8, false, true, 0, 0, SINGLE_CHANNEL_GW_IQ_TX, 3e3 );
		TxTimeOnAir = Radio.TimeOnAir( MODEM_LORA, LoRaMacBufferPktLen );
	}
	else
	{ // Normal LoRa channel
		Radio.SetMaxPayloadLength( MODEM_LORA, LoRaMacBufferPktLen );
		Radio.SetTxConfig( MODEM_LORA, txPower, 0, 0, datarate, 1, 8, false, true, 0, 0, SINGLE_CHANNEL_GW_IQ_TX, 3e3 );
		TxTimeOnAir = Radio.TimeOnAir( MODEM_LORA, LoRaMacBufferPktLen );
	}
#elif defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#endif

	// Store the time on air
	McpsConfirm.TxTimeOnAir = TxTimeOnAir;
	MlmeConfirm.TxTimeOnAir = TxTimeOnAir;

	// Starts the MAC layer status check timer
	timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_STATE_CHECK_TIMER, MAC_STATE_CHECK_TIMEOUT, TIMER_ONE_SHOT);

	if( IsLoRaMacNetworkJoined == false )
	{
		JoinRequestTrials++;
	}

	// Send now
	Radio.Send( LoRaMacBuffer, LoRaMacBufferPktLen );

	LoRaMacState |= LORAMAC_TX_RUNNING;

	return LORAMAC_STATUS_OK;
}

LoRaMacStatus_t SetTxContinuousWave( uint16_t timeout )
{
	int8_t txPowerIndex = 0;
	int8_t txPower = 0;

	txPowerIndex = LimitTxPower( LoRaMacParams.ChannelsTxPower, Bands[Channels[Channel].Band].TxMaxPower );
	txPower = TxPowers[txPowerIndex];

	// Starts the MAC layer status check timer
	timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_STATE_CHECK_TIMER, MAC_STATE_CHECK_TIMEOUT, TIMER_ONE_SHOT);

	Radio.SetTxContinuousWave( Channels[Channel].Frequency, txPower, timeout );

	LoRaMacState |= LORAMAC_TX_RUNNING;

	return LORAMAC_STATUS_OK;
}

LoRaMacStatus_t SetTxContinuousWave1( uint16_t timeout, uint32_t frequency, uint8_t power )
{
	Radio.SetTxContinuousWave( frequency, power, timeout );

	// Starts the MAC layer status check timer
	timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_STATE_CHECK_TIMER, MAC_STATE_CHECK_TIMEOUT, TIMER_ONE_SHOT);

	LoRaMacState |= LORAMAC_TX_RUNNING;

	return LORAMAC_STATUS_OK;
}

LoRaMacStatus_t LoRaMacInitialization( LoRaMacPrimitives_t *primitives, LoRaMacCallback_t *callbacks )
{
	SYS_DBG("LoRaMacInitialization\n");

	if( primitives == NULL )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}

	if( ( primitives->MacMcpsConfirm == NULL ) ||
			( primitives->MacMcpsIndication == NULL ) ||
			( primitives->MacMlmeConfirm == NULL ) )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}

	LoRaMacPrimitives = primitives;
	LoRaMacCallbacks = callbacks;

	LoRaMacFlags.Value = 0;

	LoRaMacDeviceClass = CLASS_A;
	LoRaMacState = LORAMAC_IDLE;

	JoinRequestTrials = 0;
	MaxJoinRequestTrials = 1;
	RepeaterSupport = false;

	// Reset duty cycle times
	AggregatedLastTxDoneTime = 0;
	AggregatedTimeOff = 0;

	// Duty cycle
#if defined( USE_BAND_433 )
	//BINHNT61
	//DutyCycleOn = true;
	DutyCycleOn = false;
#endif

	// Reset to defaults
	LoRaMacParamsDefaults.ChannelsTxPower = LORAMAC_DEFAULT_TX_POWER;
	LoRaMacParamsDefaults.ChannelsDatarate = LORAMAC_DEFAULT_DATARATE;

	LoRaMacParamsDefaults.SystemMaxRxError = 10;
	LoRaMacParamsDefaults.MinRxSymbols = 6;
	LoRaMacParamsDefaults.MaxRxWindow = MAX_RX_WINDOW;
	LoRaMacParamsDefaults.ReceiveDelay1 = RECEIVE_DELAY1;
	LoRaMacParamsDefaults.ReceiveDelay2 = RECEIVE_DELAY2;
	LoRaMacParamsDefaults.JoinAcceptDelay1 = JOIN_ACCEPT_DELAY1;
	LoRaMacParamsDefaults.JoinAcceptDelay2 = JOIN_ACCEPT_DELAY2;

	LoRaMacParamsDefaults.ChannelsNbRep = 1;
	LoRaMacParamsDefaults.Rx1DrOffset = 0;

	LoRaMacParamsDefaults.Rx2Channel = ( Rx2ChannelParams_t )RX_WND_2_CHANNEL;



#if defined( USE_BAND_433 )
	LoRaMacParamsDefaults.ChannelsMask[0] = LC( 1 ) + LC( 2 ) + LC( 3 );
#endif

	// Init parameters which are not set in function ResetMacParameters
	LoRaMacParams.SystemMaxRxError = LoRaMacParamsDefaults.SystemMaxRxError;
	LoRaMacParams.MinRxSymbols = LoRaMacParamsDefaults.MinRxSymbols;
	LoRaMacParams.MaxRxWindow = LoRaMacParamsDefaults.MaxRxWindow;
	LoRaMacParams.ReceiveDelay1 = LoRaMacParamsDefaults.ReceiveDelay1;
	LoRaMacParams.ReceiveDelay2 = LoRaMacParamsDefaults.ReceiveDelay2;
	LoRaMacParams.JoinAcceptDelay1 = LoRaMacParamsDefaults.JoinAcceptDelay1;
	LoRaMacParams.JoinAcceptDelay2 = LoRaMacParamsDefaults.JoinAcceptDelay2;
	LoRaMacParams.ChannelsNbRep = LoRaMacParamsDefaults.ChannelsNbRep;

	ResetMacParameters( );

	// Initialize timers
	//TimerInit( &MacStateCheckTimer, OnMacStateCheckTimerEvent );
	//TimerSetValue( &MacStateCheckTimer, MAC_STATE_CHECK_TIMEOUT );
	//TimerInit( &TxDelayedTimer, OnTxDelayedTimerEvent );
	//TimerInit( &RxWindowTimer1, OnRxWindow1TimerEvent );
	//TimerInit( &RxWindowTimer2, OnRxWindow2TimerEvent );
	//TimerInit( &AckTimeoutTimer, OnAckTimeoutTimerEvent );

	// Store the current initialization time
	LoRaMacInitializationTime = TimerGetCurrentTime( );

	// Initialize Radio driver
	RadioEvents.TxDone = OnRadioTxDone;
	RadioEvents.RxDone = OnRadioRxDone;
	RadioEvents.RxError = OnRadioRxError;
	RadioEvents.TxTimeout = OnRadioTxTimeout;
	RadioEvents.RxTimeout = OnRadioRxTimeout;
	Radio.Init( &RadioEvents );

	// Random seed initialization
	//srand1( Radio.Random( ) );
	random_feed = Radio.Random();

	PublicNetwork = true;
	Radio.SetPublicNetwork( PublicNetwork );
	Radio.Sleep( );

	return LORAMAC_STATUS_OK;
}

LoRaMacStatus_t LoRaMacQueryTxPossible( uint8_t size, LoRaMacTxInfo_t* txInfo )
{
	int8_t datarate = LoRaMacParamsDefaults.ChannelsDatarate;
	uint8_t fOptLen = MacCommandsBufferIndex + MacCommandsBufferToRepeatIndex;

	if( txInfo == NULL )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}

	AdrNextDr( AdrCtrlOn, false, &datarate );

	if( RepeaterSupport == true )
	{
		txInfo->CurrentPayloadSize = MaxPayloadOfDatarateRepeater[datarate];
	}
	else
	{
		txInfo->CurrentPayloadSize = MaxPayloadOfDatarate[datarate];
	}

	if( txInfo->CurrentPayloadSize >= fOptLen )
	{
		txInfo->MaxPossiblePayload = txInfo->CurrentPayloadSize - fOptLen;
	}
	else
	{
		return LORAMAC_STATUS_MAC_CMD_LENGTH_ERROR;
	}

	if( ValidatePayloadLength( size, datarate, 0 ) == false )
	{
		return LORAMAC_STATUS_LENGTH_ERROR;
	}

	if( ValidatePayloadLength( size, datarate, fOptLen ) == false )
	{
		return LORAMAC_STATUS_MAC_CMD_LENGTH_ERROR;
	}

	return LORAMAC_STATUS_OK;
}

LoRaMacStatus_t LoRaMacMibGetRequestConfirm( MibRequestConfirm_t *mibGet )
{
	LoRaMacStatus_t status = LORAMAC_STATUS_OK;

	if( mibGet == NULL )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}

	switch( mibGet->Type )
	{
	case MIB_DEVICE_CLASS:
	{
		mibGet->Param.Class = LoRaMacDeviceClass;
		break;
	}
	case MIB_NETWORK_JOINED:
	{
		mibGet->Param.IsNetworkJoined = IsLoRaMacNetworkJoined;
		break;
	}
	case MIB_ADR:
	{
		mibGet->Param.AdrEnable = AdrCtrlOn;
		break;
	}
	case MIB_NET_ID:
	{
		mibGet->Param.NetID = LoRaMacNetID;
		break;
	}
	case MIB_DEV_ADDR:
	{
		mibGet->Param.DevAddr = LoRaMacDevAddr;
		break;
	}
	case MIB_NWK_SKEY:
	{
		mibGet->Param.NwkSKey = LoRaMacNwkSKey;
		break;
	}
	case MIB_APP_SKEY:
	{
		mibGet->Param.AppSKey = LoRaMacAppSKey;
		break;
	}
	case MIB_PUBLIC_NETWORK:
	{
		mibGet->Param.EnablePublicNetwork = PublicNetwork;
		break;
	}
	case MIB_REPEATER_SUPPORT:
	{
		mibGet->Param.EnableRepeaterSupport = RepeaterSupport;
		break;
	}
	case MIB_CHANNELS:
	{
		mibGet->Param.ChannelList = Channels;
		break;
	}
	case MIB_RX2_CHANNEL:
	{
		mibGet->Param.Rx2Channel = LoRaMacParams.Rx2Channel;
		break;
	}
	case MIB_RX2_DEFAULT_CHANNEL:
	{
		mibGet->Param.Rx2Channel = LoRaMacParamsDefaults.Rx2Channel;
		break;
	}
	case MIB_CHANNELS_DEFAULT_MASK:
	{
		mibGet->Param.ChannelsDefaultMask = LoRaMacParamsDefaults.ChannelsMask;
		break;
	}
	case MIB_CHANNELS_MASK:
	{
		mibGet->Param.ChannelsMask = LoRaMacParams.ChannelsMask;
		break;
	}
	case MIB_CHANNELS_NB_REP:
	{
		mibGet->Param.ChannelNbRep = LoRaMacParams.ChannelsNbRep;
		break;
	}
	case MIB_MAX_RX_WINDOW_DURATION:
	{
		mibGet->Param.MaxRxWindow = LoRaMacParams.MaxRxWindow;
		break;
	}
	case MIB_RECEIVE_DELAY_1:
	{
		mibGet->Param.ReceiveDelay1 = LoRaMacParams.ReceiveDelay1;
		break;
	}
	case MIB_RECEIVE_DELAY_2:
	{
		mibGet->Param.ReceiveDelay2 = LoRaMacParams.ReceiveDelay2;
		break;
	}
	case MIB_JOIN_ACCEPT_DELAY_1:
	{
		mibGet->Param.JoinAcceptDelay1 = LoRaMacParams.JoinAcceptDelay1;
		break;
	}
	case MIB_JOIN_ACCEPT_DELAY_2:
	{
		mibGet->Param.JoinAcceptDelay2 = LoRaMacParams.JoinAcceptDelay2;
		break;
	}
	case MIB_CHANNELS_DEFAULT_DATARATE:
	{
		mibGet->Param.ChannelsDefaultDatarate = LoRaMacParamsDefaults.ChannelsDatarate;
		break;
	}
	case MIB_CHANNELS_DATARATE:
	{
		mibGet->Param.ChannelsDatarate = LoRaMacParams.ChannelsDatarate;
		break;
	}
	case MIB_CHANNELS_DEFAULT_TX_POWER:
	{
		mibGet->Param.ChannelsDefaultTxPower = LoRaMacParamsDefaults.ChannelsTxPower;
		break;
	}
	case MIB_CHANNELS_TX_POWER:
	{
		mibGet->Param.ChannelsTxPower = LoRaMacParams.ChannelsTxPower;
		break;
	}
	case MIB_UPLINK_COUNTER:
	{
		mibGet->Param.UpLinkCounter = UpLinkCounter;
		break;
	}
	case MIB_DOWNLINK_COUNTER:
	{
		mibGet->Param.DownLinkCounter = DownLinkCounter;
		break;
	}
	case MIB_MULTICAST_CHANNEL:
	{
		mibGet->Param.MulticastList = MulticastChannels;
		break;
	}
	case MIB_SYSTEM_MAX_RX_ERROR:
	{
		mibGet->Param.SystemMaxRxError = LoRaMacParams.SystemMaxRxError;
		break;
	}
	case MIB_MIN_RX_SYMBOLS:
	{
		mibGet->Param.MinRxSymbols = LoRaMacParams.MinRxSymbols;
		break;
	}
	default:
		status = LORAMAC_STATUS_SERVICE_UNKNOWN;
		break;
	}

	return status;
}

LoRaMacStatus_t LoRaMacMibSetRequestConfirm( MibRequestConfirm_t *mibSet )
{
	LoRaMacStatus_t status = LORAMAC_STATUS_OK;

	if( mibSet == NULL )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}
	if( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING )
	{
		return LORAMAC_STATUS_BUSY;
	}

	switch( mibSet->Type )
	{
	case MIB_DEVICE_CLASS:
	{
		LoRaMacDeviceClass = mibSet->Param.Class;
		switch( LoRaMacDeviceClass )
		{
		case CLASS_A:
		{
			// Set the radio into sleep to setup a defined state
			Radio.Sleep( );
			break;
		}
		case CLASS_B:
		{
			break;
		}
		case CLASS_C:
		{
			// Set the NodeAckRequested indicator to default
			NodeAckRequested = false;
			OnRxWindow2TimerEvent( );
			break;
		}
		}
		break;
	}
	case MIB_NETWORK_JOINED:
	{
		IsLoRaMacNetworkJoined = mibSet->Param.IsNetworkJoined;
		break;
	}
	case MIB_ADR:
	{
		AdrCtrlOn = mibSet->Param.AdrEnable;
		break;
	}
	case MIB_NET_ID:
	{
		LoRaMacNetID = mibSet->Param.NetID;
		break;
	}
	case MIB_DEV_ADDR:
	{
		LoRaMacDevAddr = mibSet->Param.DevAddr;
		break;
	}
	case MIB_NWK_SKEY:
	{
		if( mibSet->Param.NwkSKey != NULL )
		{
			memcpy( LoRaMacNwkSKey, mibSet->Param.NwkSKey,
					sizeof( LoRaMacNwkSKey ) );
		}
		else
		{
			status = LORAMAC_STATUS_PARAMETER_INVALID;
		}
		break;
	}
	case MIB_APP_SKEY:
	{
		if( mibSet->Param.AppSKey != NULL )
		{
			memcpy( LoRaMacAppSKey, mibSet->Param.AppSKey,
					sizeof( LoRaMacAppSKey ) );
		}
		else
		{
			status = LORAMAC_STATUS_PARAMETER_INVALID;
		}
		break;
	}
	case MIB_PUBLIC_NETWORK:
	{
		PublicNetwork = mibSet->Param.EnablePublicNetwork;
		Radio.SetPublicNetwork( PublicNetwork );
		break;
	}
	case MIB_REPEATER_SUPPORT:
	{
		RepeaterSupport = mibSet->Param.EnableRepeaterSupport;
		break;
	}
	case MIB_RX2_CHANNEL:
	{
		LoRaMacParams.Rx2Channel = mibSet->Param.Rx2Channel;
		break;
	}
	case MIB_RX2_DEFAULT_CHANNEL:
	{
		LoRaMacParamsDefaults.Rx2Channel = mibSet->Param.Rx2DefaultChannel;
		break;
	}
	case MIB_CHANNELS_DEFAULT_MASK:
	{
		if( mibSet->Param.ChannelsDefaultMask )
		{
#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )

#else
			memcpy( ( uint8_t* ) LoRaMacParamsDefaults.ChannelsMask,
					( uint8_t* ) mibSet->Param.ChannelsDefaultMask, 2 );
#endif
		}
		else
		{
			status = LORAMAC_STATUS_PARAMETER_INVALID;
		}
		break;
	}
	case MIB_CHANNELS_MASK:
	{
		if( mibSet->Param.ChannelsMask )
		{
#if defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )
#else
			memcpy( ( uint8_t* ) LoRaMacParams.ChannelsMask,
					( uint8_t* ) mibSet->Param.ChannelsMask, 2 );
#endif
		}
		else
		{
			status = LORAMAC_STATUS_PARAMETER_INVALID;
		}
		break;
	}
	case MIB_CHANNELS_NB_REP:
	{
		if( ( mibSet->Param.ChannelNbRep >= 1 ) &&
				( mibSet->Param.ChannelNbRep <= 15 ) )
		{
			LoRaMacParams.ChannelsNbRep = mibSet->Param.ChannelNbRep;
		}
		else
		{
			status = LORAMAC_STATUS_PARAMETER_INVALID;
		}
		break;
	}
	case MIB_MAX_RX_WINDOW_DURATION:
	{
		LoRaMacParams.MaxRxWindow = mibSet->Param.MaxRxWindow;
		break;
	}
	case MIB_RECEIVE_DELAY_1:
	{
		LoRaMacParams.ReceiveDelay1 = mibSet->Param.ReceiveDelay1;
		break;
	}
	case MIB_RECEIVE_DELAY_2:
	{
		LoRaMacParams.ReceiveDelay2 = mibSet->Param.ReceiveDelay2;
		break;
	}
	case MIB_JOIN_ACCEPT_DELAY_1:
	{
		LoRaMacParams.JoinAcceptDelay1 = mibSet->Param.JoinAcceptDelay1;
		break;
	}
	case MIB_JOIN_ACCEPT_DELAY_2:
	{
		LoRaMacParams.JoinAcceptDelay2 = mibSet->Param.JoinAcceptDelay2;
		break;
	}
	case MIB_CHANNELS_DEFAULT_DATARATE:
	{
#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
		if( ValueInRange( mibSet->Param.ChannelsDefaultDatarate,
						  DR_0, DR_5 ) )
		{
			LoRaMacParamsDefaults.ChannelsDatarate = mibSet->Param.ChannelsDefaultDatarate;
		}
#endif
		else
		{
			status = LORAMAC_STATUS_PARAMETER_INVALID;
		}
		break;
	}
	case MIB_CHANNELS_DATARATE:
	{
		if( ValueInRange( mibSet->Param.ChannelsDatarate,
						  LORAMAC_TX_MIN_DATARATE, LORAMAC_TX_MAX_DATARATE ) )
		{
			LoRaMacParams.ChannelsDatarate = mibSet->Param.ChannelsDatarate;
		}
		else
		{
			status = LORAMAC_STATUS_PARAMETER_INVALID;
		}
		break;
	}
	case MIB_CHANNELS_DEFAULT_TX_POWER:
	{
		if( ValueInRange( mibSet->Param.ChannelsDefaultTxPower,
						  LORAMAC_MAX_TX_POWER, LORAMAC_MIN_TX_POWER ) )
		{
			LoRaMacParamsDefaults.ChannelsTxPower = mibSet->Param.ChannelsDefaultTxPower;
		}
		else
		{
			status = LORAMAC_STATUS_PARAMETER_INVALID;
		}
		break;
	}
	case MIB_CHANNELS_TX_POWER:
	{
		if( ValueInRange( mibSet->Param.ChannelsTxPower,
						  LORAMAC_MAX_TX_POWER, LORAMAC_MIN_TX_POWER ) )
		{
			LoRaMacParams.ChannelsTxPower = mibSet->Param.ChannelsTxPower;
		}
		else
		{
			status = LORAMAC_STATUS_PARAMETER_INVALID;
		}
		break;
	}
	case MIB_UPLINK_COUNTER:
	{
		UpLinkCounter = mibSet->Param.UpLinkCounter;
		break;
	}
	case MIB_DOWNLINK_COUNTER:
	{
		DownLinkCounter = mibSet->Param.DownLinkCounter;
		break;
	}
	case MIB_SYSTEM_MAX_RX_ERROR:
	{
		LoRaMacParams.SystemMaxRxError = LoRaMacParamsDefaults.SystemMaxRxError = mibSet->Param.SystemMaxRxError;
		break;
	}
	case MIB_MIN_RX_SYMBOLS:
	{
		LoRaMacParams.MinRxSymbols = LoRaMacParamsDefaults.MinRxSymbols = mibSet->Param.MinRxSymbols;
		break;
	}
	default:
		status = LORAMAC_STATUS_SERVICE_UNKNOWN;
		break;
	}

	return status;
}

LoRaMacStatus_t LoRaMacChannelAdd( uint8_t id, ChannelParams_t params )
{
#if defined( USE_BAND_470 ) || defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID )
	return LORAMAC_STATUS_PARAMETER_INVALID;
#else
	bool datarateInvalid = false;
	bool frequencyInvalid = false;
	uint8_t band = 0;

	// The id must not exceed LORA_MAX_NB_CHANNELS
	if( id >= LORA_MAX_NB_CHANNELS )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}
	// Validate if the MAC is in a correct state
	if( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING )
	{
		if( ( LoRaMacState & LORAMAC_TX_CONFIG ) != LORAMAC_TX_CONFIG )
		{
			return LORAMAC_STATUS_BUSY;
		}
	}
	// Validate the datarate
	if( ( params.DrRange.Fields.Min > params.DrRange.Fields.Max ) ||
			( ValueInRange( params.DrRange.Fields.Min, LORAMAC_TX_MIN_DATARATE,
							LORAMAC_TX_MAX_DATARATE ) == false ) ||
			( ValueInRange( params.DrRange.Fields.Max, LORAMAC_TX_MIN_DATARATE,
							LORAMAC_TX_MAX_DATARATE ) == false ) )
	{
		datarateInvalid = true;
	}

#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
	if( id < 3 )
	{
		if( params.Frequency != Channels[id].Frequency )
		{
			frequencyInvalid = true;
		}

		if( params.DrRange.Fields.Min > DR_0 )
		{
			datarateInvalid = true;
		}
		if( ValueInRange( params.DrRange.Fields.Max, DR_5, LORAMAC_TX_MAX_DATARATE ) == false )
		{
			datarateInvalid = true;
		}
	}
#endif

	// Validate the frequency
	if( ( Radio.CheckRfFrequency( params.Frequency ) == true ) && ( params.Frequency > 0 ) && ( frequencyInvalid == false ) )
	{
#if defined( USE_BAND_868 )

#endif
	}
	else
	{
		frequencyInvalid = true;
	}

	if( ( datarateInvalid == true ) && ( frequencyInvalid == true ) )
	{
		return LORAMAC_STATUS_FREQ_AND_DR_INVALID;
	}
	if( datarateInvalid == true )
	{
		return LORAMAC_STATUS_DATARATE_INVALID;
	}
	if( frequencyInvalid == true )
	{
		return LORAMAC_STATUS_FREQUENCY_INVALID;
	}

	// Every parameter is valid, activate the channel
	Channels[id] = params;
	Channels[id].Band = band;
	LoRaMacParams.ChannelsMask[0] |= ( 1 << id );

	return LORAMAC_STATUS_OK;
#endif
}

LoRaMacStatus_t LoRaMacChannelRemove( uint8_t id )
{
#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
	if( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING )
	{
		if( ( LoRaMacState & LORAMAC_TX_CONFIG ) != LORAMAC_TX_CONFIG )
		{
			return LORAMAC_STATUS_BUSY;
		}
	}

	if( ( id < 3 ) || ( id >= LORA_MAX_NB_CHANNELS ) )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}
	else
	{
		// Remove the channel from the list of channels
		Channels[id] = ( ChannelParams_t ){ 0, { 0 }, 0 };

		// Disable the channel as it doesn't exist anymore
		if( DisableChannelInMask( id, LoRaMacParams.ChannelsMask ) == false )
		{
			return LORAMAC_STATUS_PARAMETER_INVALID;
		}
	}
	return LORAMAC_STATUS_OK;
#elif ( defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID ) || defined( USE_BAND_470 ) )

#endif
}

LoRaMacStatus_t LoRaMacMulticastChannelLink( MulticastParams_t *channelParam )
{
	if( channelParam == NULL )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}
	if( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING )
	{
		return LORAMAC_STATUS_BUSY;
	}

	// Reset downlink counter
	channelParam->DownLinkCounter = 0;

	if( MulticastChannels == NULL )
	{
		// New node is the fist element
		MulticastChannels = channelParam;
	}
	else
	{
		MulticastParams_t *cur = MulticastChannels;

		// Search the last node in the list
		while( cur->Next != NULL )
		{
			cur = cur->Next;
		}
		// This function always finds the last node
		cur->Next = channelParam;
	}

	return LORAMAC_STATUS_OK;
}

LoRaMacStatus_t LoRaMacMulticastChannelUnlink( MulticastParams_t *channelParam )
{
	if( channelParam == NULL )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}
	if( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING )
	{
		return LORAMAC_STATUS_BUSY;
	}

	if( MulticastChannels != NULL )
	{
		if( MulticastChannels == channelParam )
		{
			// First element
			MulticastChannels = channelParam->Next;
		}
		else
		{
			MulticastParams_t *cur = MulticastChannels;

			// Search the node in the list
			while( cur->Next && cur->Next != channelParam )
			{
				cur = cur->Next;
			}
			// If we found the node, remove it
			if( cur->Next )
			{
				cur->Next = channelParam->Next;
			}
		}
		channelParam->Next = NULL;
	}

	return LORAMAC_STATUS_OK;
}

LoRaMacStatus_t LoRaMacMlmeRequest( MlmeReq_t *mlmeRequest )
{
	LoRaMacStatus_t status = LORAMAC_STATUS_SERVICE_UNKNOWN;
	LoRaMacHeader_t macHdr;

	if( mlmeRequest == NULL )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}
	if( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING )
	{
		return LORAMAC_STATUS_BUSY;
	}

	memset( ( uint8_t* ) &MlmeConfirm, 0, sizeof( MlmeReq_t ) );

	MlmeConfirm.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;

	switch( mlmeRequest->Type )
	{
	case MLME_JOIN:
	{
		if( ( LoRaMacState & LORAMAC_TX_DELAYED ) == LORAMAC_TX_DELAYED )
		{
			return LORAMAC_STATUS_BUSY;
		}

		if( ( mlmeRequest->Req.Join.DevEui == NULL ) ||
				( mlmeRequest->Req.Join.AppEui == NULL ) ||
				( mlmeRequest->Req.Join.AppKey == NULL ) ||
				( mlmeRequest->Req.Join.NbTrials == 0 ) )
		{
			return LORAMAC_STATUS_PARAMETER_INVALID;
		}

#if ( defined( USE_BAND_915 ) || defined( USE_BAND_915_HYBRID ) )

#else
		// Enables at least the usage of all datarates.
		if( mlmeRequest->Req.Join.NbTrials < 48 )
		{
			mlmeRequest->Req.Join.NbTrials = 48;
		}
#endif

		LoRaMacFlags.Bits.MlmeReq = 1;
		MlmeConfirm.MlmeRequest = mlmeRequest->Type;

		LoRaMacDevEui = mlmeRequest->Req.Join.DevEui;
		LoRaMacAppEui = mlmeRequest->Req.Join.AppEui;
		LoRaMacAppKey = mlmeRequest->Req.Join.AppKey;
		MaxJoinRequestTrials = mlmeRequest->Req.Join.NbTrials;

		// Reset variable JoinRequestTrials
		JoinRequestTrials = 0;

		// Setup header information
		macHdr.Value = 0;
		macHdr.Bits.MType  = FRAME_TYPE_JOIN_REQ;

		ResetMacParameters( );

		// Add a +1, since we start to count from 0
		LoRaMacParams.ChannelsDatarate = AlternateDatarate( JoinRequestTrials + 1 );

		status = Send( &macHdr, 0, NULL, 0 );
		break;
	}
	case MLME_LINK_CHECK:
	{
		LoRaMacFlags.Bits.MlmeReq = 1;
		// LoRaMac will send this command piggy-pack
		MlmeConfirm.MlmeRequest = mlmeRequest->Type;

		status = AddMacCommand( MOTE_MAC_LINK_CHECK_REQ, 0, 0 );
		break;
	}
	case MLME_TXCW:
	{
		MlmeConfirm.MlmeRequest = mlmeRequest->Type;
		LoRaMacFlags.Bits.MlmeReq = 1;
		status = SetTxContinuousWave( mlmeRequest->Req.TxCw.Timeout );
		break;
	}
	case MLME_TXCW_1:
	{
		MlmeConfirm.MlmeRequest = mlmeRequest->Type;
		LoRaMacFlags.Bits.MlmeReq = 1;
		status = SetTxContinuousWave1( mlmeRequest->Req.TxCw.Timeout, mlmeRequest->Req.TxCw.Frequency, mlmeRequest->Req.TxCw.Power );
		break;
	}
	default:
		break;
	}

	if( status != LORAMAC_STATUS_OK )
	{
		NodeAckRequested = false;
		LoRaMacFlags.Bits.MlmeReq = 0;
	}

	return status;
}

LoRaMacStatus_t LoRaMacMcpsRequest( McpsReq_t *mcpsRequest )
{
	LoRaMacStatus_t status = LORAMAC_STATUS_SERVICE_UNKNOWN;
	LoRaMacHeader_t macHdr;
	uint8_t fPort = 0;
	void *fBuffer;
	uint16_t fBufferSize;
	int8_t datarate;
	bool readyToSend = false;

	if( mcpsRequest == NULL )
	{
		return LORAMAC_STATUS_PARAMETER_INVALID;
	}
	if( ( ( LoRaMacState & LORAMAC_TX_RUNNING ) == LORAMAC_TX_RUNNING ) ||
			( ( LoRaMacState & LORAMAC_TX_DELAYED ) == LORAMAC_TX_DELAYED ) )
	{
		SYS_DBG("LORAMAC_STATUS_BUSY\n");

		return LORAMAC_STATUS_BUSY;
	}

	macHdr.Value = 0;
	memset ( ( uint8_t* ) &McpsConfirm, 0, sizeof( McpsConfirm ) );
	McpsConfirm.Status = LORAMAC_EVENT_INFO_STATUS_ERROR;

	switch( mcpsRequest->Type )
	{
	case MCPS_UNCONFIRMED:
	{
		//SYS_DBG("MCPS_UNCONFIRMED\n");

		readyToSend = true;
		AckTimeoutRetries = 1;

		macHdr.Bits.MType = FRAME_TYPE_DATA_UNCONFIRMED_UP;
		fPort = mcpsRequest->Req.Unconfirmed.fPort;
		fBuffer = mcpsRequest->Req.Unconfirmed.fBuffer;
		fBufferSize = mcpsRequest->Req.Unconfirmed.fBufferSize;
		datarate = mcpsRequest->Req.Unconfirmed.Datarate;
		break;
	}
	case MCPS_CONFIRMED:
	{
		//SYS_DBG("MCPS_CONFIRMED\n");

		readyToSend = true;
		AckTimeoutRetriesCounter = 1;
		AckTimeoutRetries = mcpsRequest->Req.Confirmed.NbTrials;

		macHdr.Bits.MType = FRAME_TYPE_DATA_CONFIRMED_UP;
		fPort = mcpsRequest->Req.Confirmed.fPort;
		fBuffer = mcpsRequest->Req.Confirmed.fBuffer;
		fBufferSize = mcpsRequest->Req.Confirmed.fBufferSize;
		datarate = mcpsRequest->Req.Confirmed.Datarate;
		break;
	}
	case MCPS_PROPRIETARY:
	{
		//SYS_DBG("MCPS_PROPRIETARY\n");

		readyToSend = true;
		AckTimeoutRetries = 1;

		macHdr.Bits.MType = FRAME_TYPE_PROPRIETARY;
		fBuffer = mcpsRequest->Req.Proprietary.fBuffer;
		fBufferSize = mcpsRequest->Req.Proprietary.fBufferSize;
		datarate = mcpsRequest->Req.Proprietary.Datarate;
		break;
	}
	default:
		break;
	}

	if( readyToSend == true )
	{
		if( AdrCtrlOn == false )
		{
			if( ValueInRange( datarate, LORAMAC_TX_MIN_DATARATE, LORAMAC_TX_MAX_DATARATE ) == true )
			{
				LoRaMacParams.ChannelsDatarate = datarate;
			}
			else
			{
				//SYS_DBG("Datarate invalid\n");
				return LORAMAC_STATUS_PARAMETER_INVALID;
			}
		}

		status = Send( &macHdr, fPort, fBuffer, fBufferSize );
		if( status == LORAMAC_STATUS_OK )
		{
			McpsConfirm.McpsRequest = mcpsRequest->Type;
			LoRaMacFlags.Bits.McpsReq = 1;
		}
		else
		{
			//SYS_DBG("Send return err\n");
			NodeAckRequested = false;
		}
	}

	return status;
}

void LoRaMacTestRxWindowsOn( bool enable )
{
	IsRxWindowsEnabled = enable;
}

void LoRaMacTestSetMic( uint16_t txPacketCounter )
{
	UpLinkCounter = txPacketCounter;
	IsUpLinkCounterFixed = true;
}

void LoRaMacTestSetDutyCycleOn( bool enable )
{
#if ( defined( USE_BAND_868 ) || defined( USE_BAND_433 ) || defined( USE_BAND_780 ) )
	DutyCycleOn = enable;
#endif
}

void LoRaMacTestSetChannel( uint8_t channel )
{
	Channel = channel;
}

static RxConfigParams_t ComputeRxWindowParameters( int8_t datarate, uint32_t rxError )
{
	RxConfigParams_t rxConfigParams = { 0, 0, 0, 0 };
	double tSymbol = 0.0;

	rxConfigParams.Datarate = datarate;
	switch( Bandwidths[datarate] )
	{
	default:
	case 125000:
		rxConfigParams.Bandwidth = 0;
		break;
	case 250000:
		rxConfigParams.Bandwidth = 1;
		break;
	case 500000:
		rxConfigParams.Bandwidth = 2;
		break;
	}

#if defined( USE_BAND_433 ) || defined( USE_BAND_780 ) || defined( USE_BAND_868 )
	if( datarate == DR_7 )
	{ // FSK
		tSymbol = ( 1.0 / ( double )Datarates[datarate] ) * 8.0; // 1 symbol equals 1 byte
	}
	else
#endif
	{ // LoRa
		tSymbol = ( ( double )( 1 << Datarates[datarate] ) / ( double )Bandwidths[datarate] ) * 1e3;
	}

	rxConfigParams.RxWindowTimeout = MAX( ( uint32_t )ceil( ( ( 2 * LoRaMacParams.MinRxSymbols - 8 ) * tSymbol + 2 * rxError ) / tSymbol ), LoRaMacParams.MinRxSymbols ); // Computed number of symbols

	rxConfigParams.RxOffset = ( int32_t )ceil( ( 4.0 * tSymbol ) - ( ( rxConfigParams.RxWindowTimeout * tSymbol ) / 2.0 ) - RADIO_WAKEUP_TIME );

	return rxConfigParams;
}

/* binhnt61 */
/* BEGIN */
uint32_t TimerGetElapsedTime( uint32_t eventInTime )
{
	uint32_t elapsedTime = 0;

	// Needed at boot, cannot compute with 0 or elapsed time will be equal to current time
	if( eventInTime == 0 )
	{
		return 0;
	}

	elapsedTime = sys_ctrl_millis();

	if( elapsedTime < eventInTime )
	{ // roll over of the counter
		return( elapsedTime + ( 0xFFFFFFFF - eventInTime ) );
	}
	else
	{
		return( elapsedTime - eventInTime );
	}
}

uint32_t TimerGetCurrentTime(void) {
	return sys_ctrl_millis();
}

int32_t randr( int32_t min, int32_t max )
{
	return ( int32_t )(sys_ctrl_millis() - random_feed) % ( max - min + 1 ) + min;
}

void memcpyr( uint8_t *dst, const uint8_t *src, uint16_t size )
{
	dst = dst + ( size - 1 );
	while( size-- )
	{
		*dst-- = *src++;
	}
}
/* END */
