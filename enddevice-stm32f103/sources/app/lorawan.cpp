#include <string.h>

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../platform/stm32f10x/io_cfg.h"
#include "../common/xprintf.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"
#include "app_bsp.h"

#include "loramac/radio/sx1276/sx1276_cfg.h"
#include "loramac/radio/sx1276/sx1276.h"

#include "loramac/mac/LoRaMac-definitions.h"
#include "loramac/mac/LoRaMac.h"
#include "loramac/mac/LoRaMacCrypto.h"

#include "../crypto/aes.h"
#include "../crypto/cmac.h"

#include "lorawan.h"

//#define PING_PONG_MODE
//#define OVER_THE_AIR_ACTIVATION

#ifdef  OVER_THE_AIR_ACTIVATION
#define LORAWAN_APPLICATION_EUI				{ 0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x00, 0x77, 0x57 }
#define LORAWAN_APPLICATION_KEY				{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }
static uint8_t AppEui[] = LORAWAN_APPLICATION_EUI;
static uint8_t AppKey[] = LORAWAN_APPLICATION_KEY;
#else

#define LORAWAN_NETWORK_ID					0
#define LORAWAN_DEVICE_ADDR					{ 0x26, 0x04, 0x19, 0x5E }
#define LORAWAN_NWKSKEY						{ 0xCB, 0xF4, 0x2B, 0x80, 0xA3, 0x10, 0x3C, 0xC4, 0xDF, 0x84, 0x71, 0xC2, 0x3F, 0x90, 0xB0, 0xB9 }
#define LORAWAN_APPSKEY						{ 0x73, 0xAB, 0x8E, 0xAB, 0x4D, 0x5B, 0xD7, 0xEB, 0xFB, 0x34, 0xF8, 0x82, 0x0C, 0x75, 0x01, 0x40 }

static const uint8_t	DevAddr[]	= LORAWAN_DEVICE_ADDR;
static const uint32_t	NwkID		= LORAWAN_NETWORK_ID;
static const uint8_t	NwkSKey[]	= LORAWAN_NWKSKEY;
static const uint8_t	AppSKey[]	= LORAWAN_APPSKEY;
#endif

static LoRaMacPrimitives_t LoRaMacPrimitives;
static LoRaMacCallback_t LoRaMacCallbacks;
static MibRequestConfirm_t mibReq;

static void McpsConfirm( McpsConfirm_t *mcpsConfirm );
static void McpsIndication( McpsIndication_t *mcpsIndication );
static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm );



static lorawan_msg_t notify_msg;
static lorawan_leave_msg_t leave_msg;

#ifdef PING_PONG_MODE
RadioEvents_t pingpong_events;
void on_ping_pong_tx_done( void );
void on_ping_pong_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
void on_ping_pong_tx_timeout( void );
void on_ping_pong_rx_timeout( void );
void on_ping_pong_rx_error( void );
#endif

static void McpsConfirm( McpsConfirm_t *mcpsConfirm ) {
	if( mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
		switch( mcpsConfirm->McpsRequest ) {
		case MCPS_UNCONFIRMED: {
			// Check Datarate
			// Check TxPower
			APP_DBG("McpsConfirm-MCPS_UNCONFIRMED\n");

			break;
		}
		case MCPS_CONFIRMED: {
			// Check Datarate
			// Check TxPower
			// Check AckReceived
			// Check NbTrials
			APP_DBG("McpsConfirm-MCPS_CONFIRMED\n");
			break;
		}
		case MCPS_PROPRIETARY: {
			APP_DBG("McpsConfirm-MCPS_PROPRIETARY\n");

//			ak_msg_t* smsg = get_pure_msg();
//			set_msg_sig(smsg, AK_SENT_LEAVE_NETWORK);
//			task_post(AK_TASK_UI_ID, smsg);
			break;
		}
		default:
			break;
		}

	}
}

static void McpsIndication( McpsIndication_t *mcpsIndication ) {
	if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK ) {
		return;
	}

	switch( mcpsIndication->McpsIndication ) {
	case MCPS_UNCONFIRMED: {
		APP_DBG("McpsIndication-MCPS_UNCONFIRMED\n");
		break;
	}
	case MCPS_CONFIRMED: {
		APP_DBG("McpsIndication-MCPS_CONFIRMED\n");
		break;
	}
	case MCPS_PROPRIETARY: {
		APP_DBG("McpsIndication-MCPS_PROPRIETARY\n");
		break;
	}
	case MCPS_MULTICAST: {
		APP_DBG("McpsIndication:MCPS_MULTICAST\n");
		break;
	}
	default:
		break;
	}


	if( mcpsIndication->RxData == true ) {
		APP_DBG("McpsIndication-RxData:true\n");
	}
}

static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm ) {
	switch( mlmeConfirm->MlmeRequest ) {
	case MLME_JOIN: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			// Status is OK, node has joined the network
			APP_DBG("MlmeConfirm-MLME_JOIN SUCCESS\n");
		}
		else {
			// Join was not successful. Try to join again
			APP_DBG("MlmeConfirm-MLME_JOIN FAIL\n");
			lorawan_join();
		}
		break;
	}
	case MLME_LINK_CHECK: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			// Check DemodMargin
			// Check NbGateways
			APP_DBG("MlmeConfirm-MLME_LINK_CHECK\n");
		}
		break;
	}
	default:
		break;
	}

}


void lorawan_init() {
	APP_DBG("lorawan_init()\n");

#if 0

	uint8_t key[24] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
	uint8_t buffer_encrypt[24] = {0x55 ,0xF1 ,0x11 ,0x60 ,0xA2 ,0x39 ,0x3D ,0xEA ,0xF3 ,0xA2 ,0x92 ,0x1B ,0x99 ,0xAD ,0x4E ,0x08 ,0x10};
	uint8_t buffer_decrypt[24];

	LoRaMacJoinDecrypt(buffer_encrypt, 17, key, buffer_decrypt);
	APP_DBG("buffer_decrypt\n");
	for (int i=0; i< 17; i++) APP_DBG("x%02X ", buffer_decrypt[i]);
	APP_DBG("\n");

#endif

	LoRaMacPrimitives.MacMcpsConfirm = McpsConfirm;
	LoRaMacPrimitives.MacMcpsIndication = McpsIndication;
	LoRaMacPrimitives.MacMlmeConfirm = MlmeConfirm;
	LoRaMacCallbacks.GetBatteryLevel = 0;
	LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks );

	mibReq.Type = MIB_ADR;
	mibReq.Param.AdrEnable = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_REPEATER_SUPPORT;
	mibReq.Param.EnableRepeaterSupport = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_PUBLIC_NETWORK;
	mibReq.Param.EnablePublicNetwork = true;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_DEVICE_CLASS;
	mibReq.Param.Class = CLASS_C;
	LoRaMacMibSetRequestConfirm( &mibReq );

	/* init mes to send */
	notify_msg.device_addr = *((uint32_t*)DevAddr);
	notify_msg.cluster_id  = ZCL_CLUSTER_ID_GEN_ON_OFF;
	notify_msg.sequence = 0;
	notify_msg.cmd_id = ZCL_CMD_ID_REPORT;
	notify_msg.data_type = ZCL_DATATYPE_UINT8;
	notify_msg.data_len = DATA_F_SIZE;
	notify_msg.data[0] = 2;

	/*init to leave*/
	uint8_t DevEui[8];
	get_device_Eui(DevEui);
	memcpyr(leave_msg.device_Eui, DevEui, 8);

	APP_DBG("DevEui:\n");
	for(int i = 0; i< 8; i++) APP_DBG("0x%02X,", DevEui[i]);
	APP_DBG("\n");
}

void lorawan_join() {
	APP_DBG("lorawan_join()\n");

#ifdef OVER_THE_AIR_ACTIVATION
	MlmeReq_t mlmeReq;
	uint8_t DevEui[8];
	get_device_Eui(DevEui);

	mlmeReq.Type = MLME_JOIN;
	mlmeReq.Req.Join.DevEui = DevEui;
	mlmeReq.Req.Join.AppEui = AppEui;
	mlmeReq.Req.Join.AppKey = AppKey;
	mlmeReq.Req.Join.NbTrials = 3;

	LoRaMacMlmeRequest( &mlmeReq );

#else

	mibReq.Type = MIB_NET_ID;
	mibReq.Param.NetID = NwkID;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_DEV_ADDR;
	mibReq.Param.DevAddr = *((uint32_t*)DevAddr);
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_NWK_SKEY;
	mibReq.Param.NwkSKey = (uint8_t*)NwkSKey;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_APP_SKEY;
	mibReq.Param.AppSKey = (uint8_t*)AppSKey;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_NETWORK_JOINED;
	mibReq.Param.IsNetworkJoined = true;
	LoRaMacMibSetRequestConfirm( &mibReq );

#endif

	timer_set(AK_TASK_UI_ID, AK_LORAWAN_SENT_TEST, 4000, TIMER_PERIODIC);

}

void lorawan_send() {
	APP_DBG("lorawan_send()\n");

	MibRequestConfirm_t mibReq;
	LoRaMacStatus_t status;

	mibReq.Type = MIB_NETWORK_JOINED;
	status = LoRaMacMibGetRequestConfirm( &mibReq );

	if( status == LORAMAC_STATUS_OK ) {
		if( mibReq.Param.IsNetworkJoined == true ) {

			McpsReq_t mcpsReq;
			LoRaMacTxInfo_t txInfo;

			uint8_t data_size = sizeof(lorawan_msg_t);

			if( LoRaMacQueryTxPossible( data_size, &txInfo ) != LORAMAC_STATUS_OK )
			{
				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = 0;
				mcpsReq.Req.Unconfirmed.fBuffer = 0;
				mcpsReq.Req.Unconfirmed.fBufferSize = 0;
				mcpsReq.Req.Unconfirmed.Datarate = LORAMAC_DEFAULT_DATARATE;

			}
			else {
				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = APP_PORT;
				mcpsReq.Req.Unconfirmed.fBuffer = &notify_msg;
				mcpsReq.Req.Unconfirmed.fBufferSize = data_size;
				mcpsReq.Req.Unconfirmed.Datarate = LORAMAC_DEFAULT_DATARATE;
			}

			if(LoRaMacMcpsRequest( &mcpsReq ) == LORAMAC_STATUS_OK) {

			}

		}
		else {
			APP_DBG("Joined not yet\n");
		}
	}
}

void lorawan_leave() {
	APP_DBG("lorawan_leave()\n");

	MibRequestConfirm_t mibReq;
	LoRaMacStatus_t status;

	mibReq.Type = MIB_NETWORK_JOINED;
	status = LoRaMacMibGetRequestConfirm( &mibReq );

	if( status == LORAMAC_STATUS_OK ) {
		if( mibReq.Param.IsNetworkJoined == true ) {

			mibReq.Type = MIB_DEV_ADDR;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				leave_msg.device_addr = mibReq.Param.DevAddr;

				McpsReq_t mcpsReq;
				mcpsReq.Type = MCPS_PROPRIETARY;
				mcpsReq.Req.Proprietary.fBuffer = &leave_msg;
				mcpsReq.Req.Proprietary.fBufferSize = sizeof(lorawan_leave_msg_t);
				mcpsReq.Req.Proprietary.Datarate = LORAMAC_DEFAULT_DATARATE;

				if(LoRaMacMcpsRequest( &mcpsReq ) == LORAMAC_STATUS_OK) {

				}
			}
		}
		else {
			APP_DBG("Joined not yet\n");

//			ak_msg_t* smsg = get_pure_msg();
//			set_msg_sig(smsg, AK_SENT_LEAVE_NETWORK);
//			task_post(AK_TASK_UI_ID, smsg);
		}
	}
}

#ifdef PING_PONG_MODE
void lora_pingpong_init() {
	APP_DBG("lora_pingpong_init\n");

	pingpong_events.TxDone =	on_ping_pong_tx_done;
	pingpong_events.RxDone =	on_ping_pong_rx_done;
	pingpong_events.TxTimeout = on_ping_pong_tx_timeout;
	pingpong_events.RxTimeout = on_ping_pong_rx_timeout;
	pingpong_events.RxError =	on_ping_pong_rx_error;

	Radio.Init( &pingpong_events );

	Radio.SetChannel( SINGLE_CHANNEL_GW_FREQ );

	Radio.SetPublicNetwork(false);

	Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, SINGLE_CHANNEL_GW_BANDWIDTH,
					   SINGLE_CHANNEL_GW_SF, SINGLE_CHANNEL_GW_CD,
					   SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
					   true, 0, 0, SINGLE_CHANNEL_GW_IQ_TX, SINGLE_CHANNEL_GW_TX_TIMEOUT );

	Radio.SetRxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF,
					   SINGLE_CHANNEL_GW_CD, 0, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH,
					   SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
					   0, true, 0, 0, SINGLE_CHANNEL_GW_IQ_RX, true );

	timer_set(AK_TASK_LORAMAC_ID, LORAMAC_MAC_SEND_TEST, 1, TIMER_ONE_SHOT);

	//Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}

void lora_pingpong_send() {
	APP_DBG("lora_pingpong_send\n");

	if(Radio.GetStatus() != RF_TX_RUNNING) {
		const uint8_t b_size = 100;
		uint8_t buf[b_size] = {0x11, 0x22, 0x33, 0x44};
		Radio.Send(buf, b_size);
	}
}

void on_ping_pong_tx_done( void )
{
	APP_DBG("on_tx_done\n");
	//Radio.Sleep( );
	//Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
	lora_pingpong_send();
}

void on_ping_pong_tx_timeout( void )
{
	APP_DBG("on_tx_timeout\n");
	//Radio.Sleep( );
	//Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
	lora_pingpong_send();
}

void on_ping_pong_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	APP_DBG("on_rx_done-size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);
	//Radio.Sleep( );

	for(int i = 0; i< size; i++)
		xprintf("x%02X ", *(payload+i));
	xprintf("\n");

	//Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );

}

void on_ping_pong_rx_timeout( void )
{
	APP_DBG("on_rx_timeout\n");
	//Radio.Sleep( );
	//Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}

void on_ping_pong_rx_error( void )
{
	APP_DBG("on_rx_error\n");
	//Radio.Sleep( );
	//Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}
#endif
