#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../sys/sys_dbg.h"
#include "../sys/sys_irq.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"
#include "task_loramac.h"
#include "loramac/radio/sx1276/sx1276.h"
#include "loramac/mac/LoRaMac.h"
#include "lorawan.h"

void task_loramac(ak_msg_t* msg) {
	switch (msg->sig) {
	case LORAMAC_RADIO_TXTIMEOUT:
	case LORAMAC_RADIO_RXTIMEOUT:
	case LORAMAC_RADIO_RXTIMEOUT_SYNCWORD:

		SX1276OnTimeoutIrq();

		break;

	case LORAMAC_MAC_STATE_CHECK_TIMER:

		OnMacStateCheckTimerEvent();

		break;

	case LORAMAC_MAC_TXDELAYED_TIMER:

		OnTxDelayedTimerEvent();

		break;

	case LORAMAC_MAC_RXWINDOW_1_TIMER:

		OnRxWindow1TimerEvent();

		break;

	case LORAMAC_MAC_RXWINDOW_2_TIMER:

		OnRxWindow2TimerEvent();

		break;

	case LORAMAC_MAC_ACK_TIMEOUT:

		OnAckTimeoutTimerEvent();

		break;

	case LORAMAC_MAC_SEND_TEST:

		lora_pingpong_send();

		break;

	default:
		break;
	}
}


void sys_irq_sx1276_dio_0() {
	SX1276OnDio0Irq();
}

void sys_irq_sx1276_dio_1() {
	SX1276OnDio1Irq();
}

void sys_irq_sx1276_dio_2() {
	SX1276OnDio2Irq();
}

void sys_irq_sx1276_dio_3() {
	SX1276OnDio3Irq();
}

void sys_irq_sx1276_dio_4() {
	SX1276OnDio4Irq();
}

void sys_irq_sx1276_dio_5() {
	SX1276OnDio5Irq();
}


