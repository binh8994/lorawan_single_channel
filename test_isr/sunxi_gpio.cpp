#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include <poll.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ioctl.h>

#include "sunxi_gpio.h"

#ifndef	TRUE
#define	TRUE	(1==1)
#define	FALSE	(1==2)
#endif

// Environment Variables

#define	ENV_DEBUG	"WIRINGPI_DEBUG"
#define	ENV_CODES	"WIRINGPI_CODES"


// Mask for the bottom 64 pins which belong to the Banana Pro
//	The others are available for the other devices

#define	PI_GPIO_MASK	(0xFFFFFFC0)

struct wiringPiNodeStruct *wiringPiNodes = NULL ;

// BCM Magic

#define	BCM_PASSWORD		0x5A000000


// The BCM2835 has 54 GPIO pins.
//	BCM2835 data sheet, Page 90 onwards.
//	There are 6 control registers, each control the functions of a block
//	of 10 pins.
//	Each control register has 10 sets of 3 bits per GPIO pin - the ALT values
//
//	000 = GPIO Pin X is an input
//	001 = GPIO Pin X is an output
//	100 = GPIO Pin X takes alternate function 0
//	101 = GPIO Pin X takes alternate function 1
//	110 = GPIO Pin X takes alternate function 2
//	111 = GPIO Pin X takes alternate function 3
//	011 = GPIO Pin X takes alternate function 4
//	010 = GPIO Pin X takes alternate function 5
//
// So the 3 bits for port X are:
//	X / 10 + ((X % 10) * 3)

// Port function select bits

#define	FSEL_INPT		0b000
#define	FSEL_OUTP		0b001
#define	FSEL_ALT0		0b100
#define	FSEL_ALT1		0b101
#define	FSEL_ALT2		0b110
#define	FSEL_ALT3		0b111
#define	FSEL_ALT4		0b011
#define	FSEL_ALT5		0b010

// Access from ARM Running Linux
//	Taken from Gert/Doms code. Some of this is not in the manual
//	that I can find )-:

#define BCM2708_PERI_BASE	                     0x20000000
#define GPIO_PADS		(BCM2708_PERI_BASE + 0x00100000)
#define CLOCK_BASE		(BCM2708_PERI_BASE + 0x00101000)
#define GPIO_BASE		(BCM2708_PERI_BASE + 0x00200000)
#define GPIO_TIMER		(BCM2708_PERI_BASE + 0x0000B000)
#define GPIO_PWM		(BCM2708_PERI_BASE + 0x0020C000)

#define	PAGE_SIZE		(4*1024)
#define	BLOCK_SIZE		(4*1024)


// Timer
//	Word offsets

#define	TIMER_LOAD	(0x400 >> 2)
#define	TIMER_VALUE	(0x404 >> 2)
#define	TIMER_CONTROL	(0x408 >> 2)
#define	TIMER_IRQ_CLR	(0x40C >> 2)
#define	TIMER_IRQ_RAW	(0x410 >> 2)
#define	TIMER_IRQ_MASK	(0x414 >> 2)
#define	TIMER_RELOAD	(0x418 >> 2)
#define	TIMER_PRE_DIV	(0x41C >> 2)
#define	TIMER_COUNTER	(0x420 >> 2)

// Locals to hold pointers to the hardware

static volatile uint32_t *gpio ;

#ifdef	USE_TIMER
static volatile uint32_t *timer ;
static volatile uint32_t *timerIrqRaw ;
#endif

/*add for BananaPro by LeMaker team*/
// for mmap BananaPro
#define	MAX_PIN_NUM		(0x40)  //64
#define SUNXI_GPIO_BASE (0x01C20800)
#define MAP_SIZE	(4096*2)
#define MAP_MASK	(MAP_SIZE - 1)


#define GPIO_PADS_BP		(0x00100000)
#define CLOCK_BASE_BP		(0x00101000)
//	addr should 4K*n
//	#define GPIO_BASE_BP		(SUNXI_GPIO_BASE)
#define GPIO_BASE_BP		(0x01C20000)
#define GPIO_TIMER_BP		(0x0000B000)
#define GPIO_PWM_BP		(0x01c20000)  //need 4k*n

static int wiringPinMode = WPI_MODE_UNINITIALISED ;
int wiringPiCodes = FALSE ;
/*end 2014.09.18*/

// Data for use with the boardId functions.
//	The order of entries here to correspond with the PI_MODEL_X
//	and PI_VERSION_X defines in wiringPi.h
//	Only intended for the gpio command - use at your own risk!

const char *piModelNames [6] =
{
	"Unknown",
	"Model A",
	"Model B",
	"Model B+",
	"Compute Module",
	"Banana Pro",    //add for BananaPro by LeMaker team
} ;

const char *piRevisionNames [5] =
{
	"Unknown",
	"1",
	"1.1",
	"1.2",
	"2",
} ;

const char *piMakerNames [5] =
{
	"Unknown",
	"Egoman",
	"Sony",
	"Qusda",
	"LeMaker", //add for BananaPro by LeMaker team
} ;


// Time for easy calculations

static uint64_t epochMilli, epochMicro ;

// Misc

static int wiringPiMode = WPI_MODE_UNINITIALISED ;
static volatile int    pinPass = -1 ;
static pthread_mutex_t pinMutex ;

// Debugging & Return codes

int wiringPiDebug       = TRUE;  // guenter FALSE ;
int wiringPiReturnCodes = FALSE ;

// sysFds:
//	Map a file descriptor from the /sys/class/gpio/gpioX/value

static int sysFds [300] =
{
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
} ;

// ISR Data

static void (*isrFunctions [64])(void) ;


// Doing it the Arduino way with lookup tables...
//	Yes, it's probably more innefficient than all the bit-twidling, but it
//	does tend to make it all a bit clearer. At least to me!

// pinToGpio:
//	Take a Wiring pin (0 through X) and re-map it to the BCM_GPIO pin
//	Cope for 3 different board revisions here.

static int *pinToGpio ;

// Revision 1, 1.1:

static int pinToGpioR1 [64] =
{
	17, 18, 21, 22, 23, 24, 25, 4,	// From the Original Wiki - GPIO 0 through 7:	wpi  0 -  7
	0,  1,				// I2C  - SDA1, SCL1				wpi  8 -  9
	8,  7,				// SPI  - CE1, CE0				wpi 10 - 11
	10,  9, 11, 				// SPI  - MOSI, MISO, SCLK			wpi 12 - 14
	14, 15,				// UART - Tx, Rx				wpi 15 - 16

	// Padding:

	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 31
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 47
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 63
} ;

// Revision 2:

static int pinToGpioR2 [64] =
{
	17, 18, 27, 22, 23, 24, 25, 4,	// From the Original Wiki - GPIO 0 through 7:	wpi  0 -  7
	2,  3,				// I2C  - SDA0, SCL0				wpi  8 -  9
	8,  7,				// SPI  - CE1, CE0				wpi 10 - 11
	10,  9, 11, 				// SPI  - MOSI, MISO, SCLK			wpi 12 - 14
	14, 15,				// UART - Tx, Rx				wpi 15 - 16
	28, 29, 30, 31,			// Rev 2: New GPIOs 8 though 11			wpi 17 - 20
	5,  6, 13, 19, 26,			// B+						wpi 21, 22, 23, 24, 25
	12, 16, 20, 21,			// B+						wpi 26, 27, 28, 29
	0,  1,				// B+						wpi 30, 31

	// Padding:

	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 47
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 63
} ;


// physToGpio:
//	Take a physical pin (1 through 26) and re-map it to the BCM_GPIO pin
//	Cope for 2 different board revisions here.
//	Also add in the P5 connector, so the P5 pins are 3,4,5,6, so 53,54,55,56

static int *physToGpio ;

static int physToGpioR1 [64] =
{
	-1,		// 0
	-1, -1,	// 1, 2
	0, -1,
	1, -1,
	4, 14,
	-1, 15,
	17, 18,
	21, -1,
	22, 23,
	-1, 24,
	10, -1,
	9, 25,
	11,  8,
	-1,  7,	// 25, 26

	-1, -1, -1, -1, -1,	// ... 31
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 47
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 63
} ;

static int physToGpioR2 [64] =
{
	-1,		// 0
	-1, -1,	// 1, 2
	2, -1,
	3, -1,
	4, 14,
	-1, 15,
	17, 18,
	27, -1,
	22, 23,
	-1, 24,
	10, -1,
	9, 25,
	11,  8,
	-1,  7,	// 25, 26

	// B+

	0,  1,
	5, -1,
	6, 12,
	13, -1,
	19, 16,
	26, 20,
	-1, 21,

	// the P5 connector on the Rev 2 boards:

	-1, -1,
	-1, -1,
	-1, -1,
	-1, -1,
	-1, -1,
	28, 29,
	30, 31,
	-1, -1,
	-1, -1,
	-1, -1,
	-1, -1,
} ;

// gpioToGPFSEL:
//	Map a BCM_GPIO pin to it's Function Selection
//	control port. (GPFSEL 0-5)
//	Groups of 10 - 3 bits per Function - 30 bits per port

static uint8_t gpioToGPFSEL [] =
{
	0,0,0,0,0,0,0,0,0,0,
	1,1,1,1,1,1,1,1,1,1,
	2,2,2,2,2,2,2,2,2,2,
	3,3,3,3,3,3,3,3,3,3,
	4,4,4,4,4,4,4,4,4,4,
	5,5,5,5,5,5,5,5,5,5,
} ;


// gpioToShift
//	Define the shift up for the 3 bits per pin in each GPFSEL port

static uint8_t gpioToShift [] =
{
	0,3,6,9,12,15,18,21,24,27,
	0,3,6,9,12,15,18,21,24,27,
	0,3,6,9,12,15,18,21,24,27,
	0,3,6,9,12,15,18,21,24,27,
	0,3,6,9,12,15,18,21,24,27,
} ;


// gpioToGPSET:
//	(Word) offset to the GPIO Set registers for each GPIO pin

static uint8_t gpioToGPSET [] =
{
	7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
} ;

// gpioToGPCLR:
//	(Word) offset to the GPIO Clear registers for each GPIO pin

static uint8_t gpioToGPCLR [] =
{
	10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
} ;


// gpioToGPLEV:
//	(Word) offset to the GPIO Input level registers for each GPIO pin

static uint8_t gpioToGPLEV [] =
{
	13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
	14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
} ;


// GPPUD:
//	GPIO Pin pull up/down register

#define	GPPUD	37

// gpioToPUDCLK
//	(Word) offset to the Pull Up Down Clock regsiter

static uint8_t gpioToPUDCLK [] =
{
	38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,
	39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,
} ;


static int *physToPin ;

static int upDnConvert[3] = {0, 2, 1};

static int pinToGpio_BP [64] =
{
	-1,110,
	-1,-1,
	68,71,
	6,7,
	8,200,
	9,-1,
	10,201,
	20,198,
	199, -1,
	-1, -1,
	-1, -1,
	-1, -1,
	-1, -1,
	-1, -1,
	-1,-1,
	-1,-1,      // ...31

	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // ... 47
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,// ... 63
} ;
// guenter neu ende


// guenter ... dieses braucht nicht umgewandelt werden, da kein /sys/class/gpio auf orange pi
static int pinTobcm_BP [64] =
{
	257,256,   //map to BCM GPIO0,1
	53,52,       //map to BCM GPIO2,3
	226,35,     //map to BCM GPIO4,5
	277,270,   //map to BCM GPIO6,7
	266,269,   //map to BCM GPIO8,9
	268,267,   //map to BCM GPIO10,11
	276,45,      //map to BCM GPIO12,13
	228,229,   //map to BCM GPIO14,15
	38,275,    //map to BCM GPIO16,17
	259,39,    //map to BCM GPIO18,19
	44, 40,     //map to BCM GPIO20,21
	273,244,    //map to BCM GPIO22,23
	245,272,    //map to BCM GPIO24,25
	37, 274,     //map to BCM GPIO26,27

	-1,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  // 29... 44
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //45... 60
	-1, -1, -1, -1                                                                   // ...63
} ;

static int physToGpio_BP [64] =
{
	-1,          // 0
	-1,     -1,     //1, 2
	-1,    -1,     //3, 4
	-1,    -1,     //5, 6
	6,    -1,   //7, 8
	-1,     -1,   //9, 10
	-1,   110,   //11, 12
	-1,   -1,     //13, 14
	-1,   68,   //15, 16
	-1,     71,   //17, 18
	-1,   -1,     //19, 20
	-1,  -1,   //21, 22
	-1,   -1,   //23, 24
	-1,    -1,   //25, 26
	-1,   -1,  //27, 28
	7,    -1,    //29, 30
	8,  200,   //31, 32
	9,   -1,      //33, 34
	10,   201,    //35, 36
	20,   198,     //37, 38
	-1,   199,    //39, 40
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //41-> 55
	-1, -1, -1, -1, -1, -1, -1, -1 // 56-> 63
} ;
// guenter ende



static int syspin [64] =
{
	-1, -1, 2, 3, 4, 5, 6, 7,   //GPIO0,1 used to I2C
	8, 9, 10, 11, 12,13, 14, 15,
	16, 17, 18, 19, 20, 21, 22, 23,
	24, 25, 26, 27, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
} ;

static int edge [64] =
{
	-1, -1, -1, -1, 4, -1, -1, 7,   //support the INT
	8, 9, 10, 11, -1,-1, 14, 15,
	-1, 17, -1, -1, -1, -1, 22, 23,
	24, 25, -1, 27, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
} ;

static int pinToGpioR3 [64] =
{
	17, 18, 27, 22, 23, 24, 25, 4,	// From the Original Wiki - GPIO 0 through 7:	wpi  0 -  7
	2,  3,				// I2C  - SDA0, SCL0				wpi  8 -  9
	8,  7,				// SPI  - CE1, CE0				wpi 10 - 11
	10,  9, 11, 				// SPI  - MOSI, MISO, SCLK			wpi 12 - 14
	14, 15,				// UART - Tx, Rx				wpi 15 - 16
	-1, -1, -1, -1,			// Rev 2: New GPIOs 8 though 11			wpi 17 - 20
	5,  6, 13, 19, 26,			// B+						wpi 21, 22, 23, 24, 25
	12, 16, 20, 21,			// B+						wpi 26, 27, 28, 29
	0,  1,				// B+						wpi 30, 31

	// Padding:

	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 47
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 63
} ;

static int physToGpioR3 [64] =//head num map to BCMpin
{
		-1,		// 0
		-1, -1,	// 1, 2
		2, -1,
		3, -1,
		4, 14,
		-1, 15,
		17, 18,
		27, -1,
		22, 23,
		-1, 24,
		10, -1,
		9, 25,
		11,  8,
		-1,  7,	// 25, 26

		0,   1,   //27, 28
		5,  -1,  //29, 30
		6,  12,  //31, 32
		13, -1, //33, 34
		19, 16, //35, 36
		26, 20, //37, 38
		-1, 21, //39, 40
		// Padding:

		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 56
		-1, -1, -1, -1, -1, -1, -1, 	// ... 63
		} ;

static int physToPinR3 [64] = //return wiringPI pin
{
		-1,		// 0
		-1, -1,	// 1, 2
		8, -1,  //3, 4
		9, -1,  //5, 6
		7, 15,  //7, 8
		-1, 16, //9,10
		0, 1, //11,12
		2, -1, //13,14
		3, 4, //15,16
		-1, 5, //17,18
		12, -1, //19,20
		13, 6, //21,22
		14, 10, //23, 24
		-1,  11,	// 25, 26

		30,   31,   //27, 28
		21,  -1,  //29, 30
		22,  26,  //31, 32
		23, -1, //33, 34
		24, 27, //35, 36
		25, 28, //37, 38
		-1, 29, //39, 40
		// Padding:

		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	// ... 56
		-1, -1, -1, -1, -1, -1, -1, 	// ... 63
		} ;

// guenter anfang ... welche pins werden freigegeben .. -1 = gesperrt
static int BP_PIN_MASK[9][32] =  //[BANK]  [INDEX]
{
{-1,-1,-1,-1,-1,-1, 6, 7, 8, 9,10,-1,-1,-1,-1,-1,-1,-1,-1,-1,20,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,},//PA
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,},//PB
{-1,-1,-1,-1, 4,-1,-1, 7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,},//PC
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,14,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,},//PD
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,},//PE
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,},//PF
{-1,-1,-1,-1,-1,-1, 6, 7, 8, 9,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,},//PG
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,},//PH
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,},//PI
		};
// guenter ende


static int version=0;

/*end 20140918*/

/*
 * Functions
 *********************************************************************************
 */

/*add for BananaPro by LeMaker team*/
uint32_t readl(uint32_t addr)
{
	uint32_t val = 0;
	uint32_t mmap_base = (addr & ~MAP_MASK);
	uint32_t mmap_seek = ((addr - mmap_base) >> 2);
	val = *(gpio + mmap_seek);
	return val;

}
void writel(uint32_t val, uint32_t addr)
{
	uint32_t mmap_base = (addr & ~MAP_MASK);
	uint32_t mmap_seek = ((addr - mmap_base) >> 2);
	*(gpio + mmap_seek) = val;
}

int sunxi_get_gpio_mode(int pin)
{
	uint32_t regval = 0;
	int bank = pin >> 5;
	int index = pin - (bank << 5);
	int offset = ((index - ((index >> 3) << 3)) << 2);
	uint32_t reval=0;
	uint32_t phyaddr = SUNXI_GPIO_BASE + (bank * 36) + ((index >> 3) << 2);
	if (wiringPiDebug)
		printf("func:%s pin:%d,  bank:%d index:%d phyaddr:0x%x\n",__func__, pin , bank,index,phyaddr);
	if(BP_PIN_MASK[bank][index] != -1)
	{
		regval = readl(phyaddr);
		if (wiringPiDebug)
			printf("read reg val: 0x%x offset:%d  return: %d\n",regval,offset,reval);
		// reval=regval &(reval+(7 << offset));
		reval=(regval>>offset)&7;
		if (wiringPiDebug)
			printf("read reg val: 0x%x offset:%d  return: %d\n",regval,offset,reval);
		return reval;
	}
	else
	{
		printf("line:%dpin number error\n",__LINE__);
		return reval;
	}
}

void sunxi_set_gpio_mode(int pin,int mode)
{
	uint32_t regval = 0;
	int bank = pin >> 5;
	int index = pin - (bank << 5);
	int offset = ((index - ((index >> 3) << 3)) << 2);
	uint32_t phyaddr = SUNXI_GPIO_BASE + (bank * 36) + ((index >> 3) << 2);
	if (wiringPiDebug)
		printf("func:%s pin:%d, MODE:%d bank:%d index:%d phyaddr:0x%x\n",__func__, pin , mode,bank,index,phyaddr);
	if(BP_PIN_MASK[bank][index] != -1)
	{
		regval = readl(phyaddr);
		if (wiringPiDebug)
			printf("read reg val: 0x%x offset:%d\n",regval,offset);
		if(INPUT == mode)
		{
			regval &= ~(7 << offset);
			writel(regval, phyaddr);
			regval = readl(phyaddr);
			if (wiringPiDebug)
				printf("Input mode set over reg val: 0x%x\n",regval);
		}
		else if(OUTPUT == mode)
		{
			regval &= ~(7 << offset);
			regval |=  (1 << offset);
			if (wiringPiDebug)
				printf("Out mode ready set val: 0x%x\n",regval);
			writel(regval, phyaddr);
			regval = readl(phyaddr);
			if (wiringPiDebug)
				printf("Out mode set over reg val: 0x%x\n",regval);
		}
	}
	else
	{
		printf("line:%dpin number error\n",__LINE__);
	}

	return ;
}

void sunxi_digitalWrite(int pin, int value)
{
	uint32_t regval = 0;
	int bank = pin >> 5;
	int index = pin - (bank << 5);
	uint32_t phyaddr = SUNXI_GPIO_BASE + (bank * 36) + 0x10; // +0x10 -> data reg
	if (wiringPiDebug)
		printf("func:%s pin:%d, value:%d bank:%d index:%d phyaddr:0x%x\n",__func__, pin , value,bank,index,phyaddr);
	if(BP_PIN_MASK[bank][index] != -1)
	{
		regval = readl(phyaddr);
		if (wiringPiDebug)
			printf("befor write reg val: 0x%x,index:%d\n",regval,index);
		if(0 == value)
		{
			regval &= ~(1 << index);
			writel(regval, phyaddr);
			regval = readl(phyaddr);
			if (wiringPiDebug)
				printf("LOW val set over reg val: 0x%x\n",regval);
		}
		else
		{
			regval |= (1 << index);
			writel(regval, phyaddr);
			regval = readl(phyaddr);
			if (wiringPiDebug)
				printf("HIGH val set over reg val: 0x%x\n",regval);
		}
	}
	else
	{
		printf("pin number error\n");
	}

	return ;
}

int sunxi_digitalRead(int pin)
{
	uint32_t regval = 0;
	int bank = pin >> 5;
	int index = pin - (bank << 5);
	uint32_t phyaddr = SUNXI_GPIO_BASE + (bank * 36) + 0x10; // +0x10 -> data reg
	if (wiringPiDebug)
		printf("func:%s pin:%d,bank:%d index:%d phyaddr:0x%x\n",__func__, pin,bank,index,phyaddr);
	if(BP_PIN_MASK[bank][index] != -1)
	{
		regval = readl(phyaddr);
		regval = regval >> index;
		regval &= 1;
		if (wiringPiDebug)
			printf("***** read reg val: 0x%x,bank:%d,index:%d,line:%d\n",regval,bank,index,__LINE__);
		return regval;
	}
	else
	{
		printf("Sunxi_digitalRead() pin - number error\n");
		return regval;
	}
}

void sunxi_pullUpDnControl (int pin, int pud)
{
	uint32_t regval = 0;
	int bank = pin >> 5;
	int index = pin - (bank << 5);
	int sub = index >> 4;
	int sub_index = index - 16*sub;
	uint32_t phyaddr = SUNXI_GPIO_BASE + (bank * 36) + 0x1c + sub*4; // +0x10 -> pullUpDn reg
	if (wiringPiDebug)
		printf("func:%s pin:%d,bank:%d index:%d sub:%d phyaddr:0x%x\n",__func__, pin,bank,index,sub,phyaddr);
	if(BP_PIN_MASK[bank][index] != -1)
	{  //PI13~PI21 need check again
		regval = readl(phyaddr);
		if (wiringPiDebug)
			printf("pullUpDn reg:0x%x, pud:0x%x sub_index:%d\n", regval, pud, sub_index);
		regval &= ~(3 << (sub_index << 1));
		regval |= (pud << (sub_index << 1));
		if (wiringPiDebug)
			printf("pullUpDn val ready to set:0x%x\n", regval);
		writel(regval, phyaddr);
		regval = readl(phyaddr);
		if (wiringPiDebug)
			printf("pullUpDn reg after set:0x%x  addr:0x%x\n", regval, phyaddr);
	}
	else
	{
		printf("pin number error\n");
	}
	delay (1) ;
	return ;
}


int wiringPiFailure (int fatal, const char *message, ...)
{
	va_list argp ;
	char buffer [1024] ;

	if (!fatal && wiringPiReturnCodes)
		return -1 ;

	va_start (argp, message) ;
	vsnprintf (buffer, 1023, message, argp) ;
	va_end (argp) ;

	fprintf (stderr, "%s", buffer) ;
	exit (EXIT_FAILURE) ;

	return 0 ;
}


/*
 * piBoardRev:
 *	Return a number representing the hardware revision of the board.
 *
 *	Revision 1 really means the early Model B's.
 *	Revision 2 is everything else - it covers the B, B+ and CM.
 *
 *	Seems there are some boards with 0000 in them (mistake in manufacture)
 *	So the distinction between boards that I can see is:
 *	0000 - Error
 *	0001 - Not used
 *	0002 - Model B,  Rev 1,   256MB, Egoman
 *	0003 - Model B,  Rev 1.1, 256MB, Egoman, Fuses/D14 removed.
 *	0004 - Model B,  Rev 2,   256MB, Sony
 *	0005 - Model B,  Rev 2,   256MB, Qisda
 *	0006 - Model B,  Rev 2,   256MB, Egoman
 *	0007 - Model A,  Rev 2,   256MB, Egoman
 *	0008 - Model A,  Rev 2,   256MB, Sony
 *	0009 - Model A,  Rev 2,   256MB, Qisda
 *	000d - Model B,  Rev 2,   512MB, Egoman
 *	000e - Model B,  Rev 2,   512MB, Sony
 *	000f - Model B,  Rev 2,   512MB, Qisda
 *	0010 - Model B+, Rev 1.2, 512MB, Sony
 *	0011 - Pi CM,    Rev 1.2, 512MB, Sony
 *
 *	A small thorn is the olde style overvolting - that will add in
 *		1000000
 *
 *	The Pi compute module has an revision of 0011 - since we only check the
 *	last digit, then it's 1, therefore it'll default to not 2 or 3 for a
 *	Rev 1, so will appear as a Rev 2. This is fine for the most part, but
 *	we'll properly detect the Compute Module later and adjust accordingly.
 *
 *********************************************************************************
 */

static void piBoardRevOops (const char *why)
{
	fprintf (stderr, "piBoardRev: Unable to determine board revision from /proc/cpuinfo\n") ;
	fprintf (stderr, " -> %s\n", why) ;
	fprintf (stderr, " ->  You may want to check:\n") ;
	fprintf (stderr, " ->  http://www.lemaker.org/\n") ;  /*modify for BananaPro by LeMmaker team*/
	exit (EXIT_FAILURE) ;
}

/*add for BananaPro by LeMaker team*/
int isA20(void)
{
	FILE *cpuFd ;
	char line [120] ;
	char *d;
	if ((cpuFd = fopen ("/proc/cpuinfo", "r")) == NULL)
		piBoardRevOops ("Unable to open /proc/cpuinfo") ;
	while (fgets (line, 120, cpuFd) != NULL)
	{
		if (strncmp (line, "Hardware", 8) == 0)
			break ;
	}

	fclose (cpuFd) ;
	if (strncmp (line, "Hardware", 8) != 0)
		piBoardRevOops ("No \"Hardware\" line") ;

	for (d = &line [strlen (line) - 1] ; (*d == '\n') || (*d == '\r') ; --d)
		*d = 0 ;
	if (wiringPiDebug)
		printf ("piboardRev: Hardware string: %s\n", line) ;

	if (strstr(line,"sun7i") != NULL)
	{
		if (wiringPiDebug)
			printf ("Hardware:%s\n",line) ;
		return 1 ;
	}
	else
	{
		if (wiringPiDebug)
			printf ("Hardware:%s\n",line) ;
		return 0 ;
	}
}
/*end 2014.09.18*/



/*add for H3 guenter*/
int isH3(void)
{
	FILE *cpuFd ;
	char line [120] ;
	char *d;
	if ((cpuFd = fopen ("/proc/cpuinfo", "r")) == NULL)
		piBoardRevOops ("Unable to open /proc/cpuinfo") ;
	while (fgets (line, 120, cpuFd) != NULL)
	{
		if (strncmp (line, "Hardware", 8) == 0)
			break ;
	}

	fclose (cpuFd) ;
	if (strncmp (line, "Hardware", 8) != 0)
		piBoardRevOops ("No \"Hardware\" line") ;

	for (d = &line [strlen (line) - 1] ; (*d == '\n') || (*d == '\r') ; --d)
		*d = 0 ;
	if (wiringPiDebug)
		printf ("piboardRev: Hardware string: %s\n", line) ;

	if (strstr(line,"sun8i") != NULL)			//guenter von sun7i auf sun8i
	{
		if (wiringPiDebug)
			printf ("Hardware:%s\n",line) ;
		return 1 ;
	}
	else
	{
		if (wiringPiDebug)
			printf ("Hardware:%s\n",line) ;
		return 0 ;
	}
}
/* guenter ende */

int piBoardRev (void)
{
	FILE *cpuFd ;
	char line [120] ;
	char *c ;
	static int  boardRev = -1 ;

	/*add for orange pi guenter */
	if(isH3())			//guenter if(isA20())
	{
		version = BPRVER;
		if (wiringPiDebug)
			printf ("piboardRev:  %d\n", version) ;
		return BPRVER ;
	}
	/*end 2014.09.18*/

	if (boardRev != -1)	// No point checking twice
		return boardRev ;

	if ((cpuFd = fopen ("/proc/cpuinfo", "r")) == NULL)
		piBoardRevOops ("Unable to open /proc/cpuinfo") ;

	while (fgets (line, 120, cpuFd) != NULL)
		if (strncmp (line, "Revision", 8) == 0)
			break ;

	fclose (cpuFd) ;

	if (strncmp (line, "Revision", 8) != 0)
		piBoardRevOops ("No \"Revision\" line") ;

	// Chomp trailing CR/NL

	for (c = &line [strlen (line) - 1] ; (*c == '\n') || (*c == '\r') ; --c)
		*c = 0 ;

	if (wiringPiDebug)
		printf ("piboardRev: Revision string: %s\n", line) ;

	// Scan to first digit

	for (c = line ; *c ; ++c)
		if (isdigit (*c))
			break ;

	if (!isdigit (*c))
		piBoardRevOops ("No numeric revision string") ;

	// Make sure its long enough

	if (strlen (c) < 4)
		piBoardRevOops ("Bogus \"Revision\" line (too small)") ;

	// If you have overvolted the Pi, then it appears that the revision
	//	has 100000 added to it!

	if (wiringPiDebug)
		if (strlen (c) != 4)
			printf ("piboardRev: This Pi has/is overvolted!\n") ;

	// Isolate  last 4 characters:

	c = c + strlen (c) - 4 ;

	if (wiringPiDebug)
		printf ("piboardRev: last4Chars are: \"%s\"\n", c) ;

	if ( (strcmp (c, "0002") == 0) || (strcmp (c, "0003") == 0))
		boardRev = 1 ;
	else
		boardRev = 2 ;

	if (wiringPiDebug)
		printf ("piBoardRev: Returning revision: %d\n", boardRev) ;

	return boardRev ;
}


/*
 * piBoardId:
 *	Do more digging into the board revision string as above, but return
 *	as much details as we can.
 *	This is undocumented and really only intended for the GPIO command.
 *	Use at your own risk!
 *********************************************************************************
 */

void piBoardId (int *model, int *rev, int *mem, int *maker, int *overVolted)
{
	FILE *cpuFd ;
	char line [120] ;
	char *c ;

	(void)piBoardRev () ;	// Call this first to make sure all's OK. Don't care about the result.

	if ((cpuFd = fopen ("/proc/cpuinfo", "r")) == NULL)
		piBoardRevOops ("Unable to open /proc/cpuinfo") ;

	while (fgets (line, 120, cpuFd) != NULL)
		if (strncmp (line, "Revision", 8) == 0)
			break ;

	fclose (cpuFd) ;

	if (strncmp (line, "Revision", 8) != 0)
		piBoardRevOops ("No \"Revision\" line") ;

	// Chomp trailing CR/NL

	for (c = &line [strlen (line) - 1] ; (*c == '\n') || (*c == '\r') ; --c)
		*c = 0 ;

	if (wiringPiDebug)
		printf ("piboardId: Revision string: %s\n", line) ;

	// Scan to first digit

	for (c = line ; *c ; ++c)
		if (isdigit (*c))
			break ;

	// Make sure its long enough

	if (strlen (c) < 4)
		piBoardRevOops ("Bogus \"Revision\" line") ;

	// If longer than 4, we'll assume it's been overvolted

	*overVolted = strlen (c) > 4 ;

	// Extract last 4 characters:

	c = c + strlen (c) - 4 ;

	// Fill out the replys as appropriate

	/**/ if (strcmp (c, "0002") == 0) { *model = PI_MODEL_B  ; *rev = PI_VERSION_1   ; *mem = 256 ; *maker = PI_MAKER_EGOMAN ; }
	else if (strcmp (c, "0003") == 0) { *model = PI_MODEL_B  ; *rev = PI_VERSION_1_1 ; *mem = 256 ; *maker = PI_MAKER_EGOMAN ; }
	else if (strcmp (c, "0004") == 0) { *model = PI_MODEL_B  ; *rev = PI_VERSION_2   ; *mem = 256 ; *maker = PI_MAKER_SONY   ; }
	else if (strcmp (c, "0005") == 0) { *model = PI_MODEL_B  ; *rev = PI_VERSION_2   ; *mem = 256 ; *maker = PI_MAKER_QISDA  ; }
	else if (strcmp (c, "0006") == 0) { *model = PI_MODEL_B  ; *rev = PI_VERSION_2   ; *mem = 256 ; *maker = PI_MAKER_EGOMAN ; }
	else if (strcmp (c, "0007") == 0) { *model = PI_MODEL_A  ; *rev = PI_VERSION_2   ; *mem = 256 ; *maker = PI_MAKER_EGOMAN ; }
	else if (strcmp (c, "0008") == 0) { *model = PI_MODEL_A  ; *rev = PI_VERSION_2   ; *mem = 256 ; *maker = PI_MAKER_SONY ; ; }
	else if (strcmp (c, "0009") == 0) { *model = PI_MODEL_B  ; *rev = PI_VERSION_2   ; *mem = 256 ; *maker = PI_MAKER_QISDA  ; }
	else if (strcmp (c, "000d") == 0) { *model = PI_MODEL_B  ; *rev = PI_VERSION_2   ; *mem = 512 ; *maker = PI_MAKER_EGOMAN ; }
	else if (strcmp (c, "000e") == 0) { *model = PI_MODEL_B  ; *rev = PI_VERSION_2   ; *mem = 512 ; *maker = PI_MAKER_SONY   ; }
	else if (strcmp (c, "000f") == 0) { *model = PI_MODEL_B  ; *rev = PI_VERSION_2   ; *mem = 512 ; *maker = PI_MAKER_EGOMAN ; }
	else if (strcmp (c, "0010") == 0) { *model = PI_MODEL_BP ; *rev = PI_VERSION_1_2 ; *mem = 512 ; *maker = PI_MAKER_SONY   ; }
	else if (strcmp (c, "0011") == 0) { *model = PI_MODEL_CM ; *rev = PI_VERSION_1_2 ; *mem = 512 ; *maker = PI_MAKER_SONY   ; }
	//add for BananaPro by LeMaker team
	else if (strcmp (c, "0000") == 0) { *model = PI_MODEL_BPR;  *rev = PI_VERSION_1_2;  *mem = 1024;  *maker = PI_MAKER_LEMAKER;}
	//end 2014.09.30
	else                              { *model = 0           ; *rev = 0              ; *mem =   0 ; *maker = 0 ;               }
}



/*
 * wpiPinToGpio:
 *	Translate a wiringPi Pin number to native GPIO pin number.
 *	Provided for external support.
 *********************************************************************************
 */

int wpiPinToGpio (int wpiPin)
{
	return pinToGpio_BP [wpiPin & 63] ;
}


/*
 * physPinToGpio:
 *	Translate a physical Pin number to native GPIO pin number.
 *	Provided for external support.
 *********************************************************************************
 */

int physPinToGpio (int physPin)
{
	return physToGpio_BP [physPin & 63] ;
}

/*
 * physPinToGpio:
 *	Translate a physical Pin number to wiringPi  pin number. add by lemaker team for BananaPi
 *	Provided for external support.
 *********************************************************************************
 */
int physPinToPin(int physPin)
{
	return physToPin [physPin & 63] ;
}


/*
 * getAlt:
 *	Returns the ALT bits for a given port. Only really of-use
 *	for the gpio readall command (I think)
 *********************************************************************************
 */

int getAlt (int pin)
{
	int fSel, shift, alt ;

	pin &= 63 ;
	/*add for BananaPro by LeMaker team*/
	if(BPRVER == version)
	{

		//printf("[%s:L%d] the pin:%d  mode: %d is invaild,please check it over!\n", __func__,  __LINE__, pin, wiringPiMode);
		if (wiringPiMode == WPI_MODE_PINS)
			pin = pinToGpio_BP [pin] ;
		else if (wiringPiMode == WPI_MODE_PHYS)
			pin = physToGpio_BP[pin] ;
		else if (wiringPiMode == WPI_MODE_GPIO)
			pin=pinTobcm_BP[pin];//need map A20 to bcm
		else return 0 ;

		if(-1 == pin)
		{
			printf("[%s:L%d] the pin:%d  mode: %d is invaild,please check it over!\n", __func__,  __LINE__, pin, wiringPiMode);
			return -1;
		}
		alt=sunxi_get_gpio_mode(pin);
		return alt ;
	}
	/*end 2014.08.19*/

	/**/ if (wiringPiMode == WPI_MODE_PINS)
		pin = pinToGpio [pin] ;
	else if (wiringPiMode == WPI_MODE_PHYS)
		pin = physToGpio [pin] ;
	else if (wiringPiMode != WPI_MODE_GPIO)
		return 0 ;

	fSel    = gpioToGPFSEL [pin] ;
	shift   = gpioToShift  [pin] ;

	alt = (*(gpio + fSel) >> shift) & 7 ;

	return alt ;
}


/*
 * wiringPiFindNode:
 *      Locate our device node
 *********************************************************************************
 */

struct wiringPiNodeStruct *wiringPiFindNode (int pin)
{
	struct wiringPiNodeStruct *node = wiringPiNodes ;

	while (node != NULL)
		if ((pin >= node->pinBase) && (pin <= node->pinMax))
			return node ;
		else
			node = node->next ;

	return NULL ;
}


/*
 * wiringPiNewNode:
 *	Create a new GPIO node into the wiringPi handling system
 *********************************************************************************
 */

static void pinModeDummy             (struct wiringPiNodeStruct *node, int pin, int mode)  { return ; }
static void pullUpDnControlDummy     (struct wiringPiNodeStruct *node, int pin, int pud)   { return ; }
static int  digitalReadDummy         (struct wiringPiNodeStruct *node, int pin)            { return LOW ; }
static void digitalWriteDummy        (struct wiringPiNodeStruct *node, int pin, int value) { return ; }
static void pwmWriteDummy            (struct wiringPiNodeStruct *node, int pin, int value) { return ; }
static int  analogReadDummy          (struct wiringPiNodeStruct *node, int pin)            { return 0 ; }
static void analogWriteDummy         (struct wiringPiNodeStruct *node, int pin, int value) { return ; }

struct wiringPiNodeStruct *wiringPiNewNode (int pinBase, int numPins)
{
	int    pin ;
	struct wiringPiNodeStruct *node ;

	// Minimum pin base is 64

	if (pinBase < 64)
		(void)wiringPiFailure (WPI_FATAL, "wiringPiNewNode: pinBase of %d is < 64\n", pinBase) ;

	// Check all pins in-case there is overlap:

	for (pin = pinBase ; pin < (pinBase + numPins) ; ++pin)
		if (wiringPiFindNode (pin) != NULL)
			(void)wiringPiFailure (WPI_FATAL, "wiringPiNewNode: Pin %d overlaps with existing definition\n", pin) ;

	node = (struct wiringPiNodeStruct *)calloc (sizeof (struct wiringPiNodeStruct), 1) ;	// calloc zeros
	if (node == NULL)
		(void)wiringPiFailure (WPI_FATAL, "wiringPiNewNode: Unable to allocate memory: %s\n", strerror (errno)) ;

	node->pinBase         = pinBase ;
	node->pinMax          = pinBase + numPins - 1 ;
	node->pinMode         = pinModeDummy ;
	node->pullUpDnControl = pullUpDnControlDummy ;
	node->digitalRead     = digitalReadDummy ;
	node->digitalWrite    = digitalWriteDummy ;
	node->pwmWrite        = pwmWriteDummy ;
	node->analogRead      = analogReadDummy ;
	node->analogWrite     = analogWriteDummy ;
	node->next            = wiringPiNodes ;
	wiringPiNodes         = node ;

	return node ;
}


/*
 *********************************************************************************
 * Core Functions
 *********************************************************************************
 */

/*
 * pinModeAlt:
 *	This is an un-documented special to let you set any pin to any mode
 *********************************************************************************
 */

void pinModeAlt (int pin, int mode)
{
	int fSel, shift ;
	/*add for BananaPro by LeMaker team*/
	if (BPRVER == version)
	{
		return;
	}
	/*end 2014.08.19*/

	if ((pin & PI_GPIO_MASK) == 0)		// On-board pin
	{
		/**/ if (wiringPiMode == WPI_MODE_PINS)
			pin = pinToGpio [pin] ;
		else if (wiringPiMode == WPI_MODE_PHYS)
			pin = physToGpio [pin] ;
		else if (wiringPiMode != WPI_MODE_GPIO)
			return ;

		fSel  = gpioToGPFSEL [pin] ;
		shift = gpioToShift  [pin] ;

		*(gpio + fSel) = (*(gpio + fSel) & ~(7 << shift)) | ((mode & 0x7) << shift) ;
	}
}


/*
 * pinMode:
 *	Sets the mode of a pin to be input, output or PWM output
 *********************************************************************************
 */

void pinMode (int pin, int mode)
{
	int    fSel, shift;
	struct wiringPiNodeStruct *node = wiringPiNodes ;

	/*add for BananaPro by LeMaker team*/
	if(BPRVER == version )
	{
		if (wiringPiDebug)
			printf ("%s,%d,pin:%d,mode:%d\n", __func__, __LINE__,pin,mode) ;
		if ((pin & PI_GPIO_MASK) == 0)		// On-board pin
		{
			if (wiringPiMode == WPI_MODE_PINS)
					pin = pinToGpio_BP [pin] ;
			else if (wiringPiMode == WPI_MODE_PHYS)
				pin = physToGpio_BP[pin] ;
			else if (wiringPiMode == WPI_MODE_GPIO)
				pin=pinTobcm_BP[pin];//need map A20 to bcm
			else return ;

			if (-1 == pin)  /*VCC or GND return directly*/
			{
				//printf("[%s:L%d] the pin:%d is invaild,please check it over!\n", __func__,  __LINE__, pin);
				return;
			}

			if (mode == INPUT)
			{
				sunxi_set_gpio_mode(pin,INPUT);
				wiringPinMode = INPUT;
				return ;
			}
			else if (mode == OUTPUT)
			{
				sunxi_set_gpio_mode(pin, OUTPUT); //gootoomoon_set_mode
				wiringPinMode = OUTPUT;
				return ;
			}
			else
				return ;
		}
		else
		{
			if ((node = wiringPiFindNode (pin)) != NULL)
				node->pinMode (node, pin, mode) ;
			return ;
		}
	}
	/*end 2014.08.19*/

	if ((pin & PI_GPIO_MASK) == 0)		// On-board pin
	{
		/**/ if (wiringPiMode == WPI_MODE_PINS)
			pin = pinToGpio [pin] ;
		else if (wiringPiMode == WPI_MODE_PHYS)
			pin = physToGpio [pin] ;
		else if (wiringPiMode != WPI_MODE_GPIO)
			return ;

		//softPwmStop  (origPin) ;
		//softToneStop (origPin) ;

		fSel    = gpioToGPFSEL [pin] ;
		shift   = gpioToShift  [pin] ;

		/**/ if (mode == INPUT)
			*(gpio + fSel) = (*(gpio + fSel) & ~(7 << shift)) ; // Sets bits to zero = input
		else if (mode == OUTPUT)
			*(gpio + fSel) = (*(gpio + fSel) & ~(7 << shift)) | (1 << shift) ;
	}
	else
	{
		if ((node = wiringPiFindNode (pin)) != NULL)
			node->pinMode (node, pin, mode) ;
		return ;
	}
}


/*
 * pullUpDownCtrl:
 *	Control the internal pull-up/down resistors on a GPIO pin
 *	The Arduino only has pull-ups and these are enabled by writing 1
 *	to a port when in input mode - this paradigm doesn't quite apply
 *	here though.
 *********************************************************************************
 */

void pullUpDnControl (int pin, int pud)
{
	struct wiringPiNodeStruct *node = wiringPiNodes ;

	/*add for BananaPro by LeMaker team*/
	if(BPRVER == version)
	{
		if ((pin & PI_GPIO_MASK) == 0)		// On-Board Pin
		{
			if (wiringPiMode == WPI_MODE_PINS)
				pin = pinToGpio_BP [pin] ;
			else if (wiringPiMode == WPI_MODE_PHYS)
				pin = physToGpio_BP[pin] ;
			else if (wiringPiMode == WPI_MODE_GPIO)
				pin=pinTobcm_BP[pin];//need map A20 to bcm
			else return ;
			if (wiringPiDebug)
				printf ("%s,%d,pin:%d\n", __func__, __LINE__,pin) ;

			if (-1 == pin)
			{
				printf("[%s:L%d] the pin:%d is invaild,please check it over!\n", __func__,  __LINE__, pin);
				return;
			}

			pud = upDnConvert[pud]; // convert wiringpi pud to sunxi pud value
			sunxi_pullUpDnControl(pin, pud);
			return;
		}
		else	// Extension module
		{
			if ((node = wiringPiFindNode (pin)) != NULL)
				node->pullUpDnControl (node, pin, pud) ;
			return ;
		}
	}
	/*end 2014.08.19*/

	if ((pin & PI_GPIO_MASK) == 0)		// On-Board Pin
	{
		/**/ if (wiringPiMode == WPI_MODE_PINS)
			pin = pinToGpio [pin] ;
		else if (wiringPiMode == WPI_MODE_PHYS)
			pin = physToGpio [pin] ;
		else if (wiringPiMode != WPI_MODE_GPIO)
			return ;

		*(gpio + GPPUD)              = pud & 3 ;		delayMicroseconds (5) ;
		*(gpio + gpioToPUDCLK [pin]) = 1 << (pin & 31) ;	delayMicroseconds (5) ;

		*(gpio + GPPUD)              = 0 ;			delayMicroseconds (5) ;
		*(gpio + gpioToPUDCLK [pin]) = 0 ;			delayMicroseconds (5) ;
	}
	else						// Extension module
	{
		if ((node = wiringPiFindNode (pin)) != NULL)
			node->pullUpDnControl (node, pin, pud) ;
		return ;
	}
}


/*
 * digitalRead:
 *	Read the value of a given Pin, returning HIGH or LOW
 *********************************************************************************
 */

int digitalRead (int pin)
{
	char c ;
	struct wiringPiNodeStruct *node = wiringPiNodes ;

	/*add for BananaPro by LeMaker team*/
	if(BPRVER == version)
	{
		if ((pin & PI_GPIO_MASK) == 0)		// On-Board Pin
		{
			int _pinMode = getAlt(pin);
			if (wiringPiMode == WPI_MODE_GPIO_SYS)	// Sys mode
			{
				if(pin==0)
				{
					//printf("%d %s,%d invalid pin,please check it over.\n",pin,__func__, __LINE__);
					return 0;
				}
				if(syspin[pin]==-1)
				{
					//printf("%d %s,%d invalid pin,please check it over.\n",pin,__func__, __LINE__);
					return 0;
				}
				if (sysFds [pin] == -1)
				{
					if (wiringPiDebug)
						printf ("pin %d sysFds -1.%s,%d\n", pin ,__func__, __LINE__) ;
					return LOW;
				}
				if (wiringPiDebug)
					printf ("pin %d :%d.%s,%d\n", pin ,sysFds [pin],__func__, __LINE__) ;
				lseek  (sysFds [pin], 0L, SEEK_SET) ;
				read   (sysFds [pin], &c, 1) ;
				return (c == '0') ? LOW : HIGH ;
			}
			else if (wiringPiMode == WPI_MODE_PINS)
				pin = pinToGpio_BP [pin] ;
			else if (wiringPiMode == WPI_MODE_PHYS)
				pin = physToGpio_BP[pin] ;
			else if (wiringPiMode == WPI_MODE_GPIO)
				pin=pinTobcm_BP[pin];//need map A20 to bcm
			else
				return LOW ;
			if(-1 == pin){
				printf("[%s:L%d] the pin:%d is invalid, please check it over!\n", __func__,  __LINE__, pin);
				return LOW;
			}
			if(_pinMode == 6){
				// -- Allwinner H3 --
				//ALT2, ie. EINT is enabled!
				//Gotta export the pin if it's not exported already, then read its value that way
				char fName[64];
				struct stat s;
				sprintf(fName, "/sys/class/gpio/gpio%d", pin);
				if(stat(fName, &s) == -1) {
					int tempFd = open("/sys/class/gpio/export", O_WRONLY);
					if(tempFd < 0) return LOW;
					sprintf(fName, "%d\n", pin);
					write(tempFd, fName, strlen(fName));
					close(tempFd);
				}
				sprintf(fName, "/sys/class/gpio/gpio%d/value", pin);
				if(sysFds[pin] == -1){
					if ((sysFds[pin] = open (fName, O_RDWR)) < 0){
						printf("digitalRead(): Failed to open %s, pin not exported?\n", fName);
						return LOW;
					}
				}
				lseek(sysFds[pin], 0L, SEEK_SET);
				read(sysFds[pin], &c, 1);
				return (c == '0') ? LOW : HIGH;
			}
			return sunxi_digitalRead(pin);
		}
		else
		{
			if ((node = wiringPiFindNode (pin)) == NULL)
				return LOW ;
			return node->digitalRead (node, pin) ;
		}
	}
	/*end 2014.08.19*/

	if ((pin & PI_GPIO_MASK) == 0)		// On-Board Pin
	{
		/**/ if (wiringPiMode == WPI_MODE_GPIO_SYS)	// Sys mode
		{
			if (sysFds [pin] == -1)
				return LOW ;

			lseek  (sysFds [pin], 0L, SEEK_SET) ;
			read   (sysFds [pin], &c, 1) ;
			return (c == '0') ? LOW : HIGH ;
		}
		else if (wiringPiMode == WPI_MODE_PINS)
			pin = pinToGpio [pin] ;
		else if (wiringPiMode == WPI_MODE_PHYS)
			pin = physToGpio [pin] ;
		else if (wiringPiMode != WPI_MODE_GPIO)
			return LOW ;

		if ((*(gpio + gpioToGPLEV [pin]) & (1 << (pin & 31))) != 0)
			return HIGH ;
		else
			return LOW ;
	}
	else
	{
		if ((node = wiringPiFindNode (pin)) == NULL)
			return LOW ;
		return node->digitalRead (node, pin) ;
	}
}


/*
 * digitalWrite:
 *	Set an output bit
 *********************************************************************************
 */

void digitalWrite (int pin, int value)
{
	struct wiringPiNodeStruct *node = wiringPiNodes ;
	/*add for BananaPro by LeMaker team*/
	if(BPRVER == version)
	{
		if (wiringPiDebug)
			printf ("%s,%d\n", __func__, __LINE__) ;
		if ((pin & PI_GPIO_MASK) == 0)		// On-Board Pin
		{
			/**/ if (wiringPiMode == WPI_MODE_GPIO_SYS)	// Sys mode
			{
				if (wiringPiDebug)
				{
					printf("%d %s,%d invalid pin,please check it over.\n",pin,__func__, __LINE__);
				}
				if(pin==0)
				{
					//printf("%d %s,%d invalid pin,please check it over.\n",pin,__func__, __LINE__);
					return;
				}
				if(syspin[pin]==-1)
				{
					//printf("%d %s,%d invalid pin,please check it over.\n",pin,__func__, __LINE__);
					return;
				}
				if (sysFds [pin] == -1)
				{
					if (wiringPiDebug)
						printf ("pin %d sysFds -1.%s,%d\n", pin ,__func__, __LINE__) ;
				}
				if (sysFds [pin] != -1)
				{
					if (wiringPiDebug)
						printf ("pin %d :%d.%s,%d\n", pin ,sysFds [pin],__func__, __LINE__) ;
					if (value == LOW)
						write (sysFds [pin], "0\n", 2) ;
					else
						write (sysFds [pin], "1\n", 2) ;
				}
				return ;
			}
			else if (wiringPiMode == WPI_MODE_PINS)
				pin = pinToGpio_BP [pin] ;
			else if (wiringPiMode == WPI_MODE_PHYS)
				pin = physToGpio_BP[pin] ;
			else if (wiringPiMode == WPI_MODE_GPIO)
				pin=pinTobcm_BP[pin];//need map A20 to bcm
			else  return ;
			if(-1 == pin){
				//printf("[%s:L%d] the pin:%d is invaild,please check it over!\n", __func__,  __LINE__, pin);
				return ;
			}
			sunxi_digitalWrite(pin, value);
		}
		else
		{
			if ((node = wiringPiFindNode (pin)) != NULL)
				node->digitalWrite (node, pin, value) ;
		}
		return;
	}
	/*end 2014.08.19*/

	if ((pin & PI_GPIO_MASK) == 0)		// On-Board Pin
	{
		/**/ if (wiringPiMode == WPI_MODE_GPIO_SYS)	// Sys mode
		{
			if (sysFds [pin] != -1)
			{
				if (value == LOW)
					write (sysFds [pin], "0\n", 2) ;
				else
					write (sysFds [pin], "1\n", 2) ;
			}
			return ;
		}
		else if (wiringPiMode == WPI_MODE_PINS)
			pin = pinToGpio [pin] ;
		else if (wiringPiMode == WPI_MODE_PHYS)
			pin = physToGpio [pin] ;
		else if (wiringPiMode != WPI_MODE_GPIO)
			return ;

		if (value == LOW)
			*(gpio + gpioToGPCLR [pin]) = 1 << (pin & 31) ;
		else
			*(gpio + gpioToGPSET [pin]) = 1 << (pin & 31) ;
	}
	else
	{
		if ((node = wiringPiFindNode (pin)) != NULL)
			node->digitalWrite (node, pin, value) ;
	}
}


/*
 * digitalWriteByte:
 *	Pi Specific
 *	Write an 8-bit byte to the first 8 GPIO pins - try to do it as
 *	fast as possible.
 *	However it still needs 2 operations to set the bits, so any external
 *	hardware must not rely on seeing a change as there will be a change
 *	to set the outputs bits to zero, then another change to set the 1's
 *********************************************************************************
 */
static int head2win[8]={11,12,13,15,16,18,22,7}; /*add for BananaPro by lemaker team*/
void digitalWriteByte (int value)
{
	uint32_t pinSet = 0 ;
	uint32_t pinClr = 0 ;
	int mask = 1 ;
	int pin ;

	/*add for BananaPro by LeMaker team*/
	if(BPRVER == version)
	{
		if (wiringPiMode == WPI_MODE_GPIO_SYS||wiringPiMode == WPI_MODE_GPIO)
		{

			for (pin = 0 ; pin < 8 ; ++pin)
			{

				pinMode(pin,OUTPUT);
				delay(1);
				digitalWrite (pinToGpio [pin], value & mask) ;
				mask <<= 1 ;
			}

		}
		else if(wiringPiMode == WPI_MODE_PINS)
		{

			for (pin = 0 ; pin < 8 ; ++pin)
			{

				pinMode(pin,OUTPUT);
				delay(1);
				digitalWrite (pin, value & mask) ;
				mask <<= 1 ;
			}
		}
		else
		{
			for (pin = 0 ; pin < 8 ; ++pin)
			{
				pinMode(head2win[pin],OUTPUT);
				delay(1);
				digitalWrite (head2win[pin], value & mask) ;
				mask <<= 1 ;
			}
		}
		return ;
	}
	/*end 2014.08.19*/

	/**/ if (wiringPiMode == WPI_MODE_GPIO_SYS)
	{
		for (pin = 0 ; pin < 8 ; ++pin)
		{
			digitalWrite (pin, value & mask) ;
			mask <<= 1 ;
		}
		return ;
	}
	else
	{
		for (pin = 0 ; pin < 8 ; ++pin)
		{
			if ((value & mask) == 0)
				pinClr |= (1 << pinToGpio [pin]) ;
			else
				pinSet |= (1 << pinToGpio [pin]) ;

			mask <<= 1 ;
		}

		*(gpio + gpioToGPCLR [0]) = pinClr ;
		*(gpio + gpioToGPSET [0]) = pinSet ;
	}
}


/*
 * waitForInterrupt:
 *	Pi Specific.
 *	Wait for Interrupt on a GPIO pin.
 *	This is actually done via the /sys/class/gpio interface regardless of
 *	the wiringPi access mode in-use. Maybe sometime it might get a better
 *	way for a bit more efficiency.
 *********************************************************************************
 */

int waitForInterrupt (int pin, int mS)
{
	int fd, x ;
	uint8_t c ;
	struct pollfd polls ;

	/**/ if (wiringPiMode == WPI_MODE_PINS)
		pin = pinToGpio_BP [pin] ;
	else if (wiringPiMode == WPI_MODE_PHYS)
		pin = physToGpio_BP [pin] ;

	if ((fd = sysFds [pin]) == -1)
		return -2 ;

	// Setup poll structure

	polls.fd     = fd ;
	polls.events = POLLPRI ;	// Urgent data!

	// Wait for it ...

	x = poll (&polls, 1, mS) ;

	// Do a dummy read to clear the interrupt
	//	A one character read appars to be enough.

	(void)read (fd, &c, 1) ;

	return x ;
}


/*
 * interruptHandler:
 *	This is a thread and gets started to wait for the interrupt we're
 *	hoping to catch. It will call the user-function when the interrupt
 *	fires.
 *********************************************************************************
 */

static void *interruptHandler (void *arg)
{
	int myPin ;

	//(void)piHiPri (55) ;	// Only effective if we run as root

	myPin   = pinPass ;
	pinPass = -1 ;

	for (;;)
		if (waitForInterrupt (myPin, -1) > 0)
			isrFunctions [myPin] () ;

	return NULL ;
}


/*
 * wiringPiISR:
 *	Pi Specific.
 *	Take the details and create an interrupt handler that will do a call-
 *	back to the user supplied function.
 *********************************************************************************
 */

int wiringPiISR (int pin, int mode, void (*function)(void))
{
	pthread_t threadId ;
	const char *modeS ;
	char fName   [64] ;
	char  pinS [8] ;
	pid_t pid ;
	int   count, i ;
	char  c ;
	int   bcmGpioPin ;

	printf("pin:%d\n", pin);

	if ((pin < 0) || (pin > 63))
		return wiringPiFailure (WPI_FATAL, "wiringPiISR: pin must be 0-63 (%d)\n", pin) ;

	/**/ if (wiringPiMode == WPI_MODE_UNINITIALISED)
		return wiringPiFailure (WPI_FATAL, "wiringPiISR: wiringPi has not been initialised. Unable to continue.\n") ;
	else if (wiringPiMode == WPI_MODE_PINS)
		bcmGpioPin = pinToGpio_BP [pin] ;
	else if (wiringPiMode == WPI_MODE_PHYS)
		bcmGpioPin = physToGpio_BP [pin] ;
	else
		bcmGpioPin = pin ;

	printf("bcmGpioPin:%d\n", bcmGpioPin);

	/*add for BananaPro by LeMaker team*/

	if(BPRVER == version)
	{
		if(-1 == bcmGpioPin)  /**/
		{
			printf("[%s:L%d] the pin:%d is invalid, please check it over!\n", __func__,  __LINE__, pin);
			return -1;
		}
		if(isH3()){
			// All PA- and PG-pins support interrupts, the others don't
			if(bcmGpioPin > 31 && bcmGpioPin < 192)
				return wiringPiFailure (WPI_FATAL, "wiringPiISR: the specified pin does not support interrupts on this board (Allwinner H3) (%d,%d)\n", pin,bcmGpioPin) ;
		}
		else if(edge[bcmGpioPin]==-1)
			return wiringPiFailure (WPI_FATAL, "wiringPiISR: pin not supported on Banana Pi (%d,%d)\n", pin,bcmGpioPin) ;
	}
	/*end 2014.08.19*/

	// Now export the pin and set the right edge
	//	We're going to use the gpio program to do this, so it assumes
	//	a full installation of wiringPi. It's a bit 'clunky', but it
	//	is a way that will work when we're running in "Sys" mode, as
	//	a non-root user. (without sudo)

	if (mode != INT_EDGE_SETUP)
	{
		/**/ if (mode == INT_EDGE_FALLING)
			modeS = "falling" ;
		else if (mode == INT_EDGE_RISING)
			modeS = "rising" ;
		else
			modeS = "both" ;

		sprintf (pinS, "%d", bcmGpioPin) ;

		if ((pid = fork ()) < 0)	// Fail
			return wiringPiFailure (WPI_FATAL, "wiringPiISR: fork failed: %s\n", strerror (errno)) ;

		if (pid == 0)	// Child, exec
		{
			/**/ if (access ("/usr/local/bin/gpio", X_OK) == 0)
			{
				execl ("/usr/local/bin/gpio", "gpio", "edge", pinS, modeS, (char *)NULL) ;
				return wiringPiFailure (WPI_FATAL, "wiringPiISR: execl failed: %s\n", strerror (errno)) ;
			}
			else if (access ("/usr/bin/gpio", X_OK) == 0)
			{
				execl ("/usr/bin/gpio", "gpio", "edge", pinS, modeS, (char *)NULL) ;
				return wiringPiFailure (WPI_FATAL, "wiringPiISR: execl failed: %s\n", strerror (errno)) ;
			}
			else
				return wiringPiFailure (WPI_FATAL, "wiringPiISR: Can't find gpio program\n") ;
		}
		else		// Parent, wait
			wait (NULL) ;
	}

	// Now pre-open the /sys/class node - but it may already be open if
	//	we are in Sys mode...

	if (sysFds [bcmGpioPin] == -1)
	{
		sprintf (fName, "/sys/class/gpio/gpio%d/value", bcmGpioPin) ;
		if ((sysFds [bcmGpioPin] = open (fName, O_RDWR)) < 0)
			return wiringPiFailure (WPI_FATAL, "wiringPiISR: unable to open %s: %s\n", fName, strerror (errno)) ;
	}

	// Clear any initial pending interrupt

	ioctl (sysFds [bcmGpioPin], FIONREAD, &count) ;
	for (i = 0 ; i < count ; ++i)
		read (sysFds [bcmGpioPin], &c, 1) ;

	isrFunctions [pin] = function ;

	pthread_mutex_lock (&pinMutex) ;
	pinPass = pin ;
	pthread_create (&threadId, NULL, interruptHandler, NULL) ;
	while (pinPass != -1)
		delay (1) ;
	pthread_mutex_unlock (&pinMutex) ;

	return 0 ;
}


/*
 * initialiseEpoch:
 *	Initialise our start-of-time variable to be the current unix
 *	time in milliseconds and microseconds.
 *********************************************************************************
 */

static void initialiseEpoch (void)
{
	struct timeval tv ;

	gettimeofday (&tv, NULL) ;
	epochMilli = (uint64_t)tv.tv_sec * (uint64_t)1000    + (uint64_t)(tv.tv_usec / 1000) ;
	epochMicro = (uint64_t)tv.tv_sec * (uint64_t)1000000 + (uint64_t)(tv.tv_usec) ;
}


/*
 * delay:
 *	Wait for some number of milliseconds
 *********************************************************************************
 */

void delay (unsigned int howLong)
{
	struct timespec sleeper, dummy ;

	sleeper.tv_sec  = (time_t)(howLong / 1000) ;
	sleeper.tv_nsec = (long)(howLong % 1000) * 1000000 ;

	nanosleep (&sleeper, &dummy) ;
}


/*
 * delayMicroseconds:
 *	This is somewhat intersting. It seems that on the Pi, a single call
 *	to nanosleep takes some 80 to 130 microseconds anyway, so while
 *	obeying the standards (may take longer), it's not always what we
 *	want!
 *
 *	So what I'll do now is if the delay is less than 100uS we'll do it
 *	in a hard loop, watching a built-in counter on the ARM chip. This is
 *	somewhat sub-optimal in that it uses 100% CPU, something not an issue
 *	in a microcontroller, but under a multi-tasking, multi-user OS, it's
 *	wastefull, however we've no real choice )-:
 *
 *      Plan B: It seems all might not be well with that plan, so changing it
 *      to use gettimeofday () and poll on that instead...
 *********************************************************************************
 */

void delayMicrosecondsHard (unsigned int howLong)
{
	struct timeval tNow, tLong, tEnd ;

	gettimeofday (&tNow, NULL) ;
	tLong.tv_sec  = howLong / 1000000 ;
	tLong.tv_usec = howLong % 1000000 ;
	timeradd (&tNow, &tLong, &tEnd) ;

	while (timercmp (&tNow, &tEnd, <))
		gettimeofday (&tNow, NULL) ;
}

void delayMicroseconds (unsigned int howLong)
{
	struct timespec sleeper ;
	unsigned int uSecs = howLong % 1000000 ;
	unsigned int wSecs = howLong / 1000000 ;

	/**/ if (howLong ==   0)
		return ;
	else if (howLong  < 100)
		delayMicrosecondsHard (howLong) ;
	else
	{
		sleeper.tv_sec  = wSecs ;
		sleeper.tv_nsec = (long)(uSecs * 1000L) ;
		nanosleep (&sleeper, NULL) ;
	}
}


/*
 * millis:
 *	Return a number of milliseconds as an unsigned int.
 *********************************************************************************
 */

unsigned int millis (void)
{
	struct timeval tv ;
	uint64_t now ;

	gettimeofday (&tv, NULL) ;
	now  = (uint64_t)tv.tv_sec * (uint64_t)1000 + (uint64_t)(tv.tv_usec / 1000) ;

	return (uint32_t)(now - epochMilli) ;
}


/*
 * micros:
 *	Return a number of microseconds as an unsigned int.
 *********************************************************************************
 */

unsigned int micros (void)
{
	struct timeval tv ;
	uint64_t now ;

	gettimeofday (&tv, NULL) ;
	now  = (uint64_t)tv.tv_sec * (uint64_t)1000000 + (uint64_t)tv.tv_usec ;

	return (uint32_t)(now - epochMicro) ;
}


/*
 * wiringPiSetup:
 *	Must be called once at the start of your program execution.
 *
 * Default setup: Initialises the system into wiringPi Pin mode and uses the
 *	memory mapped hardware directly.
 *
 * Changed now to revert to "gpio" mode if we're running on a Compute Module.
 *********************************************************************************
 */

int wiringPiSetup (void)
{
	int   fd ;
	int   boardRev ;
	int   model, rev, mem, maker, overVolted ;
	memset(&sysFds, -1, sizeof(int)*300); // Initialize the filedescriptor - table all to -1

	if (getenv (ENV_DEBUG) != NULL)
		wiringPiDebug = TRUE ;

	if (getenv (ENV_CODES) != NULL)
		wiringPiReturnCodes = TRUE ;

	if (geteuid () != 0)
		(void)wiringPiFailure (WPI_FATAL, "wiringPiSetup: Must be root. (Did you forget sudo?)\n") ;

	if (wiringPiDebug)
		printf ("wiringPi: wiringPiSetup called\n") ;

	boardRev = piBoardRev () ;

	if (BPRVER == boardRev)  /*modify for BananaPro by LeMaker team zhaolei*/
	{
		pinToGpio =  pinToGpioR3 ;
		physToGpio = physToGpioR3 ;
		physToPin = physToPinR3;
	}
	else
	{
		/**/ if (boardRev == 1)	// A, B, Rev 1, 1.1
		{
			pinToGpio =  pinToGpioR1 ;
			physToGpio = physToGpioR1 ;
		}
		else 				// A, B, Rev 2, B+, CM
		{
			pinToGpio =  pinToGpioR2 ;
			physToGpio = physToGpioR2 ;
		}
	}

	// Open the master /dev/memory device

	if ((fd = open ("/dev/mem", O_RDWR | O_SYNC | O_CLOEXEC) ) < 0)
		return wiringPiFailure (WPI_ALMOST, "wiringPiSetup: Unable to open /dev/mem: %s\n", strerror (errno)) ;

	if (BPRVER == boardRev)  /*modify for BananaPro by LeMaker team*/
	{
		// GPIO:
		gpio = (uint32_t *)mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_BASE_BP);


		if (gpio == MAP_FAILED)
			return wiringPiFailure (WPI_ALMOST,"wiringPiSetup: mmap (GPIO) failed: %s\n", strerror (errno)) ;

#ifdef	USE_TIMER
		// The system timer

		timer = (uint32_t *)mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_TIMER_BP) ;
		if ((int32_t)timer == -1)
			return wiringPiFailure (WPI_ALMOST,"wiringPiSetup: mmap (TIMER) failed: %s\n", strerror (errno)) ;

		// Set the timer to free-running, 1MHz.
		//	0xF9 is 249, the timer divide is base clock / (divide+1)
		//	so base clock is 250MHz / 250 = 1MHz.

		*(timer + TIMER_CONTROL) = 0x0000280 ;
		*(timer + TIMER_PRE_DIV) = 0x00000F9 ;
		timerIrqRaw = timer + TIMER_IRQ_RAW ;
#endif
	}
	else
	{
		// GPIO:
		gpio = (uint32_t *)mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_BASE) ;
		if (gpio == MAP_FAILED)
			return wiringPiFailure (WPI_ALMOST, "wiringPiSetup: mmap (GPIO) failed: %s\n", strerror (errno)) ;

#ifdef	USE_TIMER
		// The system timer

		timer = (uint32_t *)mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_TIMER) ;
		if ((int32_t)timer == -1)
			return wiringPiFailure (WPI_ALMOST, "wiringPiSetup: mmap (TIMER) failed: %s\n", strerror (errno)) ;

		// Set the timer to free-running, 1MHz.
		//	0xF9 is 249, the timer divide is base clock / (divide+1)
		//	so base clock is 250MHz / 250 = 1MHz.

		*(timer + TIMER_CONTROL) = 0x0000280 ;
		*(timer + TIMER_PRE_DIV) = 0x00000F9 ;
		timerIrqRaw = timer + TIMER_IRQ_RAW ;
#endif
	}
	initialiseEpoch () ;

	// If we're running on a compute module, then wiringPi pin numbers don't really many anything...

	piBoardId (&model, &rev, &mem, &maker, &overVolted) ;
	if (model == PI_MODEL_CM) {
		wiringPiMode = WPI_MODE_GPIO ;
		//printf("wiringPiMode = WPI_MODE_GPIO\n");
	}
	else {
		wiringPiMode = WPI_MODE_PINS ;
		//printf("wiringPiMode = WPI_MODE_PINS\n");
	}

	return 0 ;
}


/*
 * wiringPiSetupGpio:
 *	Must be called once at the start of your program execution.
 *
 * GPIO setup: Initialises the system into GPIO Pin mode and uses the
 *	memory mapped hardware directly.
 *********************************************************************************
 */

int wiringPiSetupGpio (void)
{
	(void)wiringPiSetup () ;

	if (wiringPiDebug)
		printf ("wiringPi: wiringPiSetupGpio called\n") ;

	wiringPiMode = WPI_MODE_GPIO ;

	return 0 ;
}


/*
 * wiringPiSetupPhys:
 *	Must be called once at the start of your program execution.
 *
 * Phys setup: Initialises the system into Physical Pin mode and uses the
 *	memory mapped hardware directly.
 *********************************************************************************
 */

int wiringPiSetupPhys (void)
{
	(void)wiringPiSetup () ;

	if (wiringPiDebug)
		printf ("wiringPi: wiringPiSetupPhys called\n") ;

	wiringPiMode = WPI_MODE_PHYS ;

	return 0 ;
}


/*
 * wiringPiSetupSys:
 *	Must be called once at the start of your program execution.
 *
 * Initialisation (again), however this time we are using the /sys/class/gpio
 *	interface to the GPIO systems - slightly slower, but always usable as
 *	a non-root user, assuming the devices are already exported and setup correctly.
 */

int wiringPiSetupSys (void)
{
	int boardRev ;
	int pin ;
	char fName [128] ;

	if (getenv (ENV_DEBUG) != NULL)
		wiringPiDebug = TRUE ;

	if (getenv (ENV_CODES) != NULL)
		wiringPiReturnCodes = TRUE ;

	if (wiringPiDebug)
		printf ("wiringPi: wiringPiSetupSys called\n") ;

	boardRev = piBoardRev () ;
	if (BPRVER == boardRev)   /*modify for BananaPro by LeMaker team*/
	{
		pinToGpio =  pinToGpioR3 ;
		physToGpio = physToGpioR3 ;
		physToPin = physToPinR3;
	}
	else
	{
		if (boardRev == 1)
		{
			pinToGpio =  pinToGpioR1 ;
			physToGpio = physToGpioR1 ;
		}
		else
		{
			pinToGpio =  pinToGpioR2 ;
			physToGpio = physToGpioR2 ;
		}
	}
	// Open and scan the directory, looking for exported GPIOs, and pre-open
	//	the 'value' interface to speed things up for later
	if(BPRVER == boardRev)    /*modify for BananaPro by LeMaker team*/
	{
		for (pin = 1 ; pin < 32 ; ++pin)
		{
			sprintf (fName, "/sys/class/gpio/gpio%d/value", pin) ;
			sysFds [pin] = open (fName, O_RDWR) ;
		}
	}
	else
	{
		for (pin = 0 ; pin < 64 ; ++pin)
		{
			sprintf (fName, "/sys/class/gpio/gpio%d/value", pin) ;
			sysFds [pin] = open (fName, O_RDWR) ;
		}
	}
	initialiseEpoch () ;

	wiringPiMode = WPI_MODE_GPIO_SYS ;

	return 0 ;
}
