#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include <poll.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <poll.h>
#include "sunxi_gpio.h"



void myInterrupt0 (void) {

	static int counter=0;
	printf("Counter:%d\n", ++counter);

}

int main (void)
{
	int fd, x ;
	uint8_t c ;
	struct pollfd polls ;

//	wiringPiSetup () ;
//	wiringPiSetupSys();

//	wiringPiISR (20, INT_EDGE_FALLING, &myInterrupt0) ;

//	pinMode(199, OUTPUT);

//	printf ("Waiting ... \n") ;

	fd = open("/sys/class/gpio/gpio20/edge", O_RDWR);

	if(fd < 0) return 0;

	printf ("Waiting ... \n") ;

	for (;;)
	{
//		digitalWrite(199, HIGH);
//		delay(500);
//		digitalWrite(199, LOW);
//		delay(500);

		polls.fd     = fd ;
		polls.events = POLLPRI ;	// Urgent data!

		// Wait for it ...

		x = poll (&polls, 1, -1) ;

		// Do a dummy read to clear the interrupt
		//	A one character read appars to be enough.

		(void)read (fd, &c, 1) ;

		printf("int\n");
	}

	return 0 ;
}
