CFLAGS		+= -I./sources/app/loramac/crypto

CPPFLAGS        += -I./sources/app/loramac
CPPFLAGS        += -I./sources/app/loramac/mac
CPPFLAGS        += -I./sources/app/loramac/radio

VPATH += sources/app/loramac
VPATH += sources/app/loramac/crypto
VPATH += sources/app/loramac/mac
VPATH += sources/app/loramac/radio
VPATH += sources/app/loramac/radio/sx1276

SOURCES += sources/app/loramac/crypto/aes.c
SOURCES += sources/app/loramac/crypto/cmac.c

SOURCES_CPP += sources/app/loramac/mac/LoRaMac.cpp
SOURCES_CPP += sources/app/loramac/mac/LoRaMacCrypto.cpp
SOURCES_CPP += sources/app/loramac/radio/sx1276/sx1276.cpp
SOURCES_CPP += sources/app/loramac/radio/sx1276/sx1276_cfg.cpp

