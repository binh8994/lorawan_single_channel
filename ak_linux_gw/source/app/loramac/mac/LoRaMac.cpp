/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
	(C)2013 Semtech
 ___ _____ _   ___ _  _____ ___  ___  ___ ___
/ __|_   _/_\ / __| |/ / __/ _ \| _ \/ __| __|
\__ \ | |/ _ \ (__| ' <| _| (_) |   / (__| _|
|___/ |_/_/ \_\___|_|\_\_| \___/|_|_\\___|___|
embedded.connectivity.solutions===============

Description: LoRa MAC layer implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis ( Semtech ), Gregory Cristian ( Semtech ) and Daniel Jäckle ( STACKFORCE )
*/
#include <stdint.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

#include "../ak/ak.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../app/app.h"
#include "../app/app_data.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../sys/sys_dbg.h"

#include "../radio/radio.h"
#include "../radio/sx1276/sx1276.h"
#include "../radio/sx1276/sx1276_cfg.h"
#include "../device_mng/device_mng.h"
#include "../mac/LoRaMacCrypto.h"
#include "../mac/LoRaMac.h"
#include "../mac/LoRaMacTest.h"
#include "../mac/LoRaMac-definitions.h"

#include "../app/lorawan.h"

#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

#define USE_BAND_433

#define LORAMAC_PHY_MAXPAYLOAD                      255
#define LORA_MAC_COMMAND_MAX_LENGTH                 15
#define LORA_MAC_FRMPAYLOAD_OVERHEAD                13 // MHDR(1) + FHDR(7) + Port(1) + MIC(4)

pthread_mutex_t status_lock;

static uint8_t loramac_app_key[] =
{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
static uint8_t loramac_nwkskey_static[] =
{
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
	0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16
};
static uint8_t loramac_appskey_static[] =
{
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
	0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16
};


static uint32_t loramac_netid;
static device_mng device_list;

static RadioEvents_t radio_events;

static void on_radio_tx_done( void );
static void on_radio_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_radio_tx_timeout( void );
static void on_radio_rx_error( void );
static void on_radio_rx_timeout( void );
static void	change_to_rx(void);

static void on_radio_tx_done( void ) {
	SYS_DBG("on_radio_tx_done()\n");
	change_to_rx();
	pthread_mutex_unlock(&status_lock);
}

static void on_radio_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr ) {

	pthread_mutex_lock(&status_lock);

	SYS_DBG("on_radio_rx_done() %d\n", size);
	SYS_DBG("radio_payload:\n");
	for(int i=0; i<size; i++) SYS_DBG("x%02X ", payload[i]);
	SYS_DBG("\n");

	LoRaMacHeader_t macHdr;
	uint8_t pkt_header_len = 0;
	bool isMicOk = false;

	Radio.Sleep( );

	macHdr.Value = payload[pkt_header_len++];

	switch( macHdr.Bits.MType ) {

	case FRAME_TYPE_JOIN_REQ: {
		SYS_DBG("FRAME_TYPE_JOIN_REQ\n");

		lorawan_data_t device_join_info;
		uint32_t mic = 0;
		uint32_t mic_rx = 0;
		uint16_t device_nonce;
		uint8_t *appKey = loramac_app_key;

		device_join_info.type_msg = JOIN_TYPE;
		device_join_info.join.is_rejoin = false;

		memcpy(device_join_info.join.app_Eui, payload + pkt_header_len, 8);
		SYS_DBG("app_Eui:\n");
		for(int i=0; i<8; i++) SYS_DBG("x%02X ", device_join_info.join.app_Eui[i]);
		SYS_DBG("\n");

		pkt_header_len += 8;
		memcpy(device_join_info.join.device_Eui, payload + pkt_header_len, 8);
		SYS_DBG("dev_Eui:\n");
		for(int i=0; i<8; i++) SYS_DBG("x%02X ", device_join_info.join.device_Eui[i]);
		SYS_DBG("\n");

		pkt_header_len += 8;
		device_nonce = payload[pkt_header_len++];
		device_nonce |= ( (uint16_t)payload[pkt_header_len++] << 8 );
		//SYS_DBG("device_nonce:x%04X\n", device_nonce);

		device_join_info.join.device_nonce = device_nonce;

		lorawan_device_mng_t* current_device;
		current_device = device_list.get_device_by((const uint8_t*)device_join_info.join.device_Eui);
		if( current_device != NULL ) {
			if(current_device->device_nonce == device_join_info.join.device_nonce) {
				//SYS_DBG("device_nonce exist and device_Eui exist\n");
				SYS_DBG("join request repeated\n");
				break;
			}
			else {
				//SYS_DBG("device_Eui exist and device_nonce change\n");
				SYS_DBG("rejoin request\n");
				device_join_info.join.is_rejoin = true;

				//send for device old device_addr, but nwkskey and appskey change
				device_join_info.join.device_addr = current_device->device_addr;
				SYS_DBG("before device_addr:x%08X\n", device_join_info.join.device_addr);
			}
		}
		else {
			/*create random new addr (>0xFFFF) for accept msg after*/
			uint32_t device_addr;
			do {
				device_addr = (uint32_t)rand() & 0xFFFFFFFF;
			}
			while( (device_addr < (1 << 17)) || (device_list.device_exist(device_addr) == 0 ) );

			device_join_info.join.device_addr = device_addr;
			SYS_DBG("new device_addr:x%08X\n", device_join_info.join.device_addr);

		}
		mic_rx |= (   uint32_t )payload[size - LORAMAC_MFR_LEN];
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 1] << 8 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 2] << 16 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 3] << 24 );
		//SYS_DBG("mic_rx:x%08X\n", mic_rx);

		LoRaMacJoinComputeMic( payload, size - LORAMAC_MFR_LEN, appKey, &mic );
		//SYS_DBG("compute_mic:x%08X\n", mic);

		if(mic == mic_rx) {
			//SYS_DBG("compute_mic=mic_rx\n");

			ak_msg_t* smsg = get_dymanic_msg();
			set_data_dynamic_msg(smsg, (uint8_t*)&device_join_info, sizeof(lorawan_data_t));
			set_msg_sig(smsg, LORAWAN_DEVICE_RECV);
			task_post(MT_TASK_LORAWAN_ID, smsg);
		}
		else {
			SYS_DBG("compute_mic!=mic_rx\n");
		}

	}
		break;

	case FRAME_TYPE_DATA_CONFIRMED_UP: {
		SYS_DBG("FRAME_TYPE_DATA_CONFIRMED_UP\n");
		goto HANDLE;
	}
		break;

	case FRAME_TYPE_DATA_UNCONFIRMED_UP: {
		SYS_DBG("FRAME_TYPE_DATA_UNCONFIRMED_UP\n");
HANDLE:
		LoRaMacFrameCtrl_t fCtrl;
		uint32_t mic_rx = 0;
		uint32_t compute_mic = 0;
		uint8_t frame_len = 0;
		uint8_t *nwkSKey = NULL;
		uint8_t *appSKey = NULL;

		uint32_t address = 0;
		uint8_t* loramac_rx_payload = NULL;
		uint8_t app_payload_start_index = 0;
		uint8_t port = 0xFF;

		uint16_t sequence_counter = 0;
		uint16_t sequence_counter_prev = 0;
		uint16_t sequence_counter_diff = 0;
		uint32_t up_link_counter = 0;

		address = payload[pkt_header_len++];
		address |= ( (uint32_t)payload[pkt_header_len++] << 8 );
		address |= ( (uint32_t)payload[pkt_header_len++] << 16 );
		address |= ( (uint32_t)payload[pkt_header_len++] << 24 );
		SYS_DBG("device_address:x%08X\n", address);

		lorawan_device_mng_t* current_device;
		if(device_list.device_exist(address) != 0) {
			SYS_DBG("device not found\n");
			break;
		}
		else {
			current_device = device_list.get_device_by(address);
			//SYS_DBG("device found\n");
		}

		nwkSKey = (uint8_t*)current_device->nwkskey;
		appSKey = (uint8_t*)current_device->appskey;

		up_link_counter = current_device->up_link_cnt;

		fCtrl.Value = payload[pkt_header_len++];
		//SYS_DBG("FOpts_len:%d\n", fCtrl.Bits.FOptsLen);

		sequence_counter = ( uint16_t )payload[pkt_header_len++];
		sequence_counter |= ( uint16_t )payload[pkt_header_len++] << 8;
		SYS_DBG("sequence_counter:%d\n", sequence_counter);

		app_payload_start_index = 8 + fCtrl.Bits.FOptsLen;

		mic_rx |= (   uint32_t )payload[size - LORAMAC_MFR_LEN];
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 1] << 8 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 2] << 16 );
		mic_rx |= ( ( uint32_t )payload[size - LORAMAC_MFR_LEN + 3] << 24 );
		//SYS_DBG("mic_rx:x%08X\n", mic_rx);

		sequence_counter_prev = ( uint16_t )up_link_counter;
		sequence_counter_diff = ( sequence_counter - sequence_counter_prev );
		//SYS_DBG("sequence_counter_diff:%d\n", sequence_counter_diff);

		if( sequence_counter_diff < ( 1 << 15 ) ) {
			up_link_counter += sequence_counter_diff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, nwkSKey, address, UP_LINK, up_link_counter, &compute_mic );
			//SYS_DBG("compute_mic:x%08X\n", compute_mic);
			if( mic_rx == compute_mic ) {
				isMicOk = true;
				SYS_DBG("mic_rx=compute_mic\n");
			}
		}
		else {
			// check for sequence roll-over
			uint32_t  up_link_counter_tmp = up_link_counter + 0x10000 + ( int16_t )sequence_counter_diff;
			LoRaMacComputeMic( payload, size - LORAMAC_MFR_LEN, \
							   appSKey, address, UP_LINK, \
							   up_link_counter_tmp, &compute_mic );
			//SYS_DBG("roll-over, compute_mic:x%08X\n", compute_mic);
			if( mic_rx == compute_mic ) {
				isMicOk = true;
				//SYS_DBG("mic_rx=compute_mic\n");
				up_link_counter = up_link_counter_tmp;
			}
		}

		if( isMicOk == true ) {
			if( ( current_device->up_link_cnt == up_link_counter ) &&
					( current_device->up_link_cnt != 0 ) ) {
				SYS_DBG("message repeated\n");
				break;
			}

			current_device->up_link_cnt = up_link_counter;

			// Process payload and MAC commands
			if( ( ( size - 4 ) - app_payload_start_index ) > 0 ) {
				port = payload[app_payload_start_index++];
				frame_len = ( size - 4 ) - app_payload_start_index;

				SYS_DBG("port:%d\n", port);
				SYS_DBG("frame_len:%d\n", frame_len);

				loramac_rx_payload = (uint8_t*)malloc(frame_len);
				if(loramac_rx_payload == NULL) break;
				memset(loramac_rx_payload, 0, frame_len);

				if( port == 0 ) {
					// Only allow frames which do not have fOpts
					if( fCtrl.Bits.FOptsLen == 0 )
					{
						LoRaMacPayloadDecrypt( payload + app_payload_start_index,
											   frame_len,
											   nwkSKey,
											   address,
											   UP_LINK,
											   up_link_counter,
											   loramac_rx_payload );

						// Decode frame payload MAC commands
						//process_mac_cmd( loramac_rx_pay_len, 0, frameLen, snr );
					}
				}
				else
				{
					if( fCtrl.Bits.FOptsLen > 0 ) {
						// Decode Options field MAC commands. Omit the fPort.
						//process_mac_cmd( payload, 8, appPayloadStartIndex - 1, snr );
					}

					LoRaMacPayloadDecrypt( payload + app_payload_start_index,
										   frame_len,
										   appSKey,
										   address,
										   UP_LINK,
										   up_link_counter,
										   loramac_rx_payload );

					SYS_DBG("loramac_rx_payload:");
					for(int i = 0; i < frame_len; i++) SYS_DBG("x%02X ", loramac_rx_payload[i]);
					SYS_DBG("\n");

					lorawan_data_t lorawan_data;
					memset(&lorawan_data, 0, sizeof(lorawan_data_t));

					if( macHdr.Bits.MType == FRAME_TYPE_DATA_CONFIRMED_UP ) {
						current_device->device_ack_req = true;

						lorawan_data.type_msg = CONFIRMED_TYPE;
						lorawan_data.confirmed.port = port;
						lorawan_data.confirmed.device_addr = address;
						lorawan_data.confirmed.data_len = frame_len;
						lorawan_data.confirmed.data = (uint8_t*)loramac_rx_payload;
						lorawan_data.confirmed.receive_ack = fCtrl.Bits.Ack;

					}
					else {
						current_device->device_ack_req = false;

						lorawan_data.type_msg = UNCONFIRMED_TYPE;
						lorawan_data.unconfirmed.port = port;
						lorawan_data.unconfirmed.device_addr = address;
						lorawan_data.unconfirmed.data_len = frame_len;
						lorawan_data.unconfirmed.data = (uint8_t*)loramac_rx_payload;
						lorawan_data.unconfirmed.receive_ack = fCtrl.Bits.Ack;

					}

					device_list.update_file();

					ak_msg_t* smsg = get_dymanic_msg();
					set_data_dynamic_msg(smsg, (uint8_t*)&lorawan_data, sizeof(lorawan_data_t));
					set_msg_sig(smsg, LORAWAN_DEVICE_RECV);
					task_post(MT_TASK_LORAWAN_ID, smsg);


				}
			}
			else {
				if( fCtrl.Bits.FOptsLen > 0 ) {
					// Decode Options field MAC commands
					//process_mac_cmd( payload, 8, appPayloadStartIndex, snr );
				}
			}
		}
		else {
			SYS_DBG("mic_rx!=compute_mic\n");
		}
	}
		break;

	case FRAME_TYPE_PROPRIETARY: {
		SYS_DBG("FRAME_TYPE_PROPRIETARY\n");
#if 0
		uint32_t address = 0;
		uint32_t up_link_counter;
		uint8_t* appskey;
		lorawan_data_t lorawan_data;
		uint8_t frame_len = size - pkt_header_len;

		memset(&lorawan_data, 0, sizeof(lorawan_data_t));

		lorawan_data.type_msg = PROPRIETARY;
		lorawan_data.proprietary.data_len = frame_len;
		SYS_DBG("frame_len:%d\n",frame_len);

		/*Decrypt*/
		appskey = loramac_appskey_static;
		lorawan_msg_t* zcl_data = (lorawan_msg_t*)malloc(frame_len);
		if(zcl_data == NULL) break;

		LoRaPayloadDecryptProprietary((uint8_t*)&payload[pkt_header_len], \
									  frame_len, appskey, \
									  (uint8_t*)zcl_data);

		//SYS_DBG("appskey:\n");
		//for(int i = 0; i < 16; i++) SYS_DBG("x%02X ", *(appskey + i));
		//SYS_DBG("\n");

		SYS_DBG("decrypt buffer:\n");
		for(int i = 0; i < frame_len; i++) SYS_DBG("x%02X ", *((uint8_t*)zcl_data + i));
		SYS_DBG("\n");
#if 0
		uint8_t buf[20];
		LoRaPayloadEncryptProprietary((uint8_t*)zcl_data, frame_len, appskey, (uint8_t*)buf);
		for(int i = 0; i < frame_len; i++) SYS_DBG("x%02X ", buf[i]);
		SYS_DBG("\n");
#endif
		address = zcl_data->deviceAddr;
		SYS_DBG("device_address:x%08X\n",address);

		lorawan_device_mng_t* current_device;
		if(device_list.device_exist(address) != 0) {
			SYS_DBG("device not found\n");
			break;
		}
		else {
			current_device = device_list.get_device_by(address);
			//SYS_DBG("device found\n");
		}

		/*reset seuqence when endevice reset*/
		if(zcl_data->data[0] == 0x03 ) {
			//reset sequence
			current_device->up_link_cnt = 0;
			current_device->down_link_cnt = 0;
			device_list.update_file();
			SYS_DBG("reset sequence request\n");
			break;
		}

		up_link_counter = current_device->up_link_cnt;
		//SYS_DBG("up_link_counter:x%08X\n",up_link_counter);

		if(zcl_data->sequence < up_link_counter) {
			SYS_DBG("zcl_data->sequence < up_link_counter\n");
			break;
		}

		up_link_counter  = zcl_data->sequence;
		current_device->up_link_cnt = up_link_counter;
		appskey = current_device->appskey;
		device_list.update_file();

		lorawan_data.proprietary.data = (uint8_t*)zcl_data;

		ak_msg_t* smsg = get_dymanic_msg();
		set_data_dynamic_msg(smsg, (uint8_t*)&lorawan_data, sizeof(lorawan_data_t));
		set_msg_sig(smsg, LORAWAN_DEVICE_RECV);
		task_post(MT_TASK_LORAWAN_ID, smsg);
#endif

		if((size - 1) != 12)
			return;

		uint8_t dev_Eui[8] = {0};
		memcpy(dev_Eui, payload + pkt_header_len, 8);
		SYS_DBG("dev_Eui:\n");
		for(int i=0; i<8; i++) SYS_DBG("x%02X ", dev_Eui[i]);
		SYS_DBG("\n");

		pkt_header_len += 8;

		uint32_t address = 0;
		address	 = payload[pkt_header_len++];
		address |= ( (uint32_t)payload[pkt_header_len++] << 8 );
		address |= ( (uint32_t)payload[pkt_header_len++] << 16 );
		address |= ( (uint32_t)payload[pkt_header_len++] << 24 );

		SYS_DBG("device_address:x%08X\n", address);

		if(device_list.device_exist(address) == 0) {
			device_list.remove_device_by((const uint8_t*) dev_Eui);
		}
	}
		break;

	default:
		//SYS_DBG("FRAME_TYPE_UNKNOW\n");
		break;
	}

	change_to_rx();
	pthread_mutex_unlock(&status_lock);
}

static void on_radio_tx_timeout( void ) {
	SYS_DBG("on_radio_tx_timeout()\n");
	change_to_rx();
}

static void on_radio_rx_error( void ) {
	SYS_DBG("on_radio_rx_error()\n");
	//pthread_mutex_lock(&status_lock);
	//pthread_mutex_unlock(&status_lock);
	change_to_rx();
}

static void on_radio_rx_timeout( void ) {
	SYS_DBG("on_radio_rx_timeout()\n");
	FATAL("LORA", 0xDD);
	//change_to_rx();
}

void loramac_initial( const uint32_t nwkid, const uint8_t* appk) {
	SYS_DBG("loramac_initial\n");

	device_list.initializer("/device_list.txt");

#if 0
	uint8_t raw[11] = {0x06 ,0x50 ,0x01 ,0xBF ,0x47 ,0x96 ,0xA4 ,0x35 ,0x10 ,0x43 ,0x66};
	uint8_t enc[11];
	uint8_t dec[11];

	LoRaPayloadEncryptProprietary(raw, 11, loramac_appskey_static, enc);

	for(int i=0; i<11; i++) SYS_DBG("x%02X ", enc[i]);
	SYS_DBG("\n");

	LoRaPayloadDecryptProprietary(enc, 11, loramac_appskey_static, dec);

	for(int i=0; i<11; i++) SYS_DBG("x%02X ", dec[i]);
	SYS_DBG("\n");

#endif

	/*add device static, without join process*/
	dlsettings_t dlsetting;
	dlsetting.value = 0;
	dlsetting.bits.RFU = 0;
	dlsetting.bits.RX1DRoffset = 0;
	dlsetting.bits.RX2DR = SINGLE_CHANNEL_GW_DR;

	delay_t delay;
	delay.value = 0;
	delay.bits.RFU = 0;
	delay.bits.Del = 0; //0=>1s, Delay 1s btw Tx & Rx1, Rx2=Rx1+1s

	lorawan_device_mng_t device;
	memset(&device, 0,sizeof(device));
	if(device_list.device_exist(STATIC_ADDR) != 0) {
		memcpy(device.appskey, loramac_appskey_static, 16);
		device.device_addr = STATIC_ADDR;
		device.dlsettting = dlsetting;
		device.delay = delay;
		device_list.add_new_device(&device);
	}

	srand(Radio.Random());

	loramac_netid = nwkid;
	memcpy(loramac_app_key, appk, 16);

	// Initialize Radio driver
	radio_events.TxDone = on_radio_tx_done;
	radio_events.RxDone = on_radio_rx_done;
	radio_events.RxError = on_radio_rx_error;
	radio_events.TxTimeout = on_radio_tx_timeout;
	radio_events.RxTimeout = on_radio_rx_timeout;
	Radio.Init( &radio_events );

	Radio.SetChannel( SINGLE_CHANNEL_GW_FREQ );

	Radio.SetPublicNetwork(true);

	Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, \
					   SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF, \
					   SINGLE_CHANNEL_GW_CD, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, \
					   SINGLE_CHANNEL_GW_FIX_LENGTH_PAY, true, 0, 0, \
					   SINGLE_CHANNEL_GW_IQ_TX, 3000 );

	Radio.SetRxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF,
					   SINGLE_CHANNEL_GW_CD, 0, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH,
					   SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
					   0, true, 0, 0, SINGLE_CHANNEL_GW_IQ_RX, true );

	Radio.Rx(SINGLE_CHANNEL_GW_RX_TIMEOUT);
}

void change_to_rx(void) {
	Radio.SetMaxPayloadLength( MODEM_LORA, LORAMAC_PHY_MAXPAYLOAD );
	Radio.SetRxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF,
					   SINGLE_CHANNEL_GW_CD, 0, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH,
					   SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
					   0, true, 0, 0, SINGLE_CHANNEL_GW_IQ_RX, true );
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}

void loramac_send_msg(const lorawan_data_t* data ) {

	pthread_mutex_lock(&status_lock);

	switch (data->type_msg) {
	case JOIN_TYPE: {
		SYS_DBG("SEND_JOIN_TYPE\n");

		LoRaMacHeader_t macHdr;
		uint8_t pkt_header_len;
		uint16_t loramac_buf_pkt_len;
		uint8_t* buffer;
		uint8_t* loramac_buffer;
		uint32_t app_nonce;
		uint16_t device_nonce;
		uint8_t* device_Eui;
		uint8_t* app_Eui;
		uint32_t net_id;
		uint32_t device_addr;
		uint32_t mic;
		uint8_t* appkey;

		lorawan_device_mng_t current_device;

		appkey = loramac_app_key;

		//device_addr is before if device_Eui exits
		device_addr = data->join.device_addr;
		device_nonce = data->join.device_nonce;
		app_Eui = (uint8_t*)data->join.app_Eui;
		device_Eui = (uint8_t*)data->join.device_Eui;

		buffer = (uint8_t*) malloc(LORAMAC_PHY_MAXPAYLOAD);
		if(buffer == NULL) return;

		loramac_buf_pkt_len = 0;
		pkt_header_len = 0;
		app_nonce = (uint32_t)rand() | 0x00FFFFFF;//3bytes
		net_id = loramac_netid;

		dlsettings_t dlsetting;
		dlsetting.value = 0;
		dlsetting.bits.RFU = 0;
		dlsetting.bits.RX1DRoffset = 0;
		dlsetting.bits.RX2DR = SINGLE_CHANNEL_GW_DR;

		delay_t delay;
		delay.value = 0;
		delay.bits.RFU = 0;
		delay.bits.Del = 0; //0=>1s, Delay 1s btw Tx & Rx1, Rx2=Rx1+1s

		//SYS_DBG("app_nonce:x%08X\n", app_nonce);
		SYS_DBG("device_address:x%08X\n", device_addr);

		macHdr.Value = 0;
		macHdr.Bits.MType = FRAME_TYPE_JOIN_ACCEPT;

		buffer[pkt_header_len++] = macHdr.Value;

		buffer[pkt_header_len++] = ( app_nonce ) & 0xFF;
		buffer[pkt_header_len++] = ( app_nonce >> 8 ) & 0xFF;
		buffer[pkt_header_len++] = ( app_nonce >> 16 ) & 0xFF;

		buffer[pkt_header_len++] = ( net_id ) & 0xFF;
		buffer[pkt_header_len++] = ( net_id >> 8 ) & 0xFF;
		buffer[pkt_header_len++] = ( net_id >> 16 ) & 0xFF;

		buffer[pkt_header_len++] = ( device_addr ) & 0xFF;
		buffer[pkt_header_len++] = ( device_addr >> 8 ) & 0xFF;
		buffer[pkt_header_len++] = ( device_addr >> 16 ) & 0xFF;
		buffer[pkt_header_len++] = ( device_addr >> 24 ) & 0xFF;

		buffer[pkt_header_len++] = dlsetting.value;
		buffer[pkt_header_len++] = delay.value;

		loramac_buf_pkt_len = pkt_header_len;

		LoRaMacJoinComputeMic( buffer, loramac_buf_pkt_len, appkey, &mic );

		//SYS_DBG("mic_tx:x%08X\n", mic);

		buffer[loramac_buf_pkt_len + 0] = mic & 0xFF;
		buffer[loramac_buf_pkt_len + 1] = ( mic >> 8 ) & 0xFF;
		buffer[loramac_buf_pkt_len + 2] = ( mic >> 16 ) & 0xFF;
		buffer[loramac_buf_pkt_len + 3] = ( mic >> 24 ) & 0xFF;

		loramac_buf_pkt_len += LORAMAC_MFR_LEN;

		loramac_buffer = (uint8_t*) malloc(LORAMAC_PHY_MAXPAYLOAD);
		if(loramac_buffer == NULL) return;

		//SYS_DBG("raw_buffer:\n");
		//for (int i=0; i< loramac_buf_pkt_len; i++) SYS_DBG("x%02X ", buffer[i]);
		//SYS_DBG("\n");

		LoRaMacJoinEncrypt(buffer + 1, loramac_buf_pkt_len - 1, appkey, loramac_buffer + 1);

#if 0
		//uint8_t key[24] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
		//uint8_t buffer_encrypt[24] = {0x55 ,0xF1 ,0x11 ,0x60 ,0xA2 ,0x39 ,0x3D ,0xEA ,0xF3 ,0xA2 ,0x92 ,0x1B ,0x99 ,0xAD ,0x4E ,0x08 ,0x10};
		uint8_t buffer_decrypt[20];

		LoRaMacJoinDecrypt(loramac_buffer + 1, loramac_buf_pkt_len - 1, appkey, buffer_decrypt);
		SYS_DBG("buffer_decrypt:");
		for (int i=0; i< loramac_buf_pkt_len - 1; i++) SYS_DBG("x%02X ", buffer_decrypt[i]);
		SYS_DBG("\n");

#endif

		memset(&current_device, 0, sizeof(current_device));
		current_device.device_addr = device_addr;
		current_device.device_nonce = device_nonce;
		memcpy(current_device.device_Eui, device_Eui, 8);
		memcpy(current_device.app_Eui, app_Eui, 8);
		current_device.dlsettting = dlsetting;
		current_device.delay = delay;
		LoRaMacJoinComputeSKeys( appkey, buffer + 1, device_nonce, current_device.nwkskey, current_device.appskey );

		free(buffer);

		SYS_DBG("appskey:\n");
		for (int i=0; i< 16; i++) SYS_DBG("x%02X ", current_device.appskey[i]);
		SYS_DBG("\nnwkskey:\n");
		for (int i=0; i< 16; i++) SYS_DBG("x%02X ", current_device.nwkskey[i]);
		SYS_DBG("\n");

		loramac_buffer[0] = macHdr.Value;

		//Radio.SetMaxPayloadLength( MODEM_LORA, loramac_buf_pkt_len );
		Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, \
						   SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF, \
						   SINGLE_CHANNEL_GW_CD, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, \
						   SINGLE_CHANNEL_GW_FIX_LENGTH_PAY, true, 0, 0, \
						   SINGLE_CHANNEL_GW_IQ_TX, 3000 );
		Radio.Send( loramac_buffer, loramac_buf_pkt_len );

		SYS_DBG("loramac_buffer:\n");
		for (int i=0; i< loramac_buf_pkt_len; i++) SYS_DBG("x%02X ", loramac_buffer[i]);
		SYS_DBG("\n");

		free(loramac_buffer);

		if(device_list.device_exist(device_addr) == 0) {
			device_list.update_device(&current_device);
		}
		else {
			device_list.add_new_device(&current_device);
		}
	}
		break;

	case CONFIRMED_TYPE: {
		SYS_DBG("SEND_CONFIRMED_TYPE\n");

		LoRaMacHeader_t macHdr;
		LoRaMacFrameCtrl_t fCtrl;
		uint8_t* buffer;
		uint8_t lora_mac_tx_pay_len;
		uint32_t device_addr;
		uint32_t down_link_counter;
		uint8_t port;
		uint32_t mic = 0;
		uint8_t* loramac_buffer;
		uint8_t pktHeaderLen = 0;
		uint16_t loramac_buf_pkt_len = 0;
		bool device_ack_req;

		uint8_t* nwkskey;
		uint8_t* appskey;

		device_addr = data->confirmed.device_addr;
		SYS_DBG("device_address:x%08X\n", device_addr);

		lorawan_device_mng_t* current_device;
		if((current_device = device_list.get_device_by(device_addr)) == NULL ) {
			SYS_DBG("device not found\n");
			break;
		}

		nwkskey = current_device->nwkskey;
		appskey = current_device->appskey;
		down_link_counter = current_device->down_link_cnt;
		device_ack_req = current_device->device_ack_req;

		lora_mac_tx_pay_len = data->confirmed.data_len;
		buffer = (uint8_t*)data->confirmed.data;
		if(buffer == NULL) break;

		port = data->confirmed.port;

		loramac_buffer = (uint8_t*) malloc(LORAMAC_PHY_MAXPAYLOAD);
		if(loramac_buffer == NULL) break;

		macHdr.Value = 0;
		macHdr.Bits.MType = FRAME_TYPE_DATA_CONFIRMED_DOWN;
		fCtrl.Value = 0;
		fCtrl.Bits.FOptsLen      = 0;
		fCtrl.Bits.FPending      = 1;
		fCtrl.Bits.Ack           = device_ack_req;
		fCtrl.Bits.AdrAckReq     = 0;
		fCtrl.Bits.Adr           = 0;

		loramac_buffer[pktHeaderLen++] = macHdr.Value;

		loramac_buffer[pktHeaderLen++] = ( device_addr ) & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( device_addr >> 8 ) & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( device_addr >> 16 ) & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( device_addr >> 24 ) & 0xFF;

		loramac_buffer[pktHeaderLen++] = fCtrl.Value;

		loramac_buffer[pktHeaderLen++] = down_link_counter & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( down_link_counter >> 8 ) & 0xFF;

		if( lora_mac_tx_pay_len > 0 ) {
			loramac_buffer[pktHeaderLen++] = port;
			if( port == 0 ) {
				LoRaMacPayloadEncrypt( (uint8_t* ) buffer, lora_mac_tx_pay_len,\
									   nwkskey, device_addr, DOWN_LINK, \
									   down_link_counter, &loramac_buffer[pktHeaderLen] );
			}
			else {
				LoRaMacPayloadEncrypt( (uint8_t* ) buffer, lora_mac_tx_pay_len,\
									   appskey, device_addr, DOWN_LINK,\
									   down_link_counter, &loramac_buffer[pktHeaderLen] );
			}
		}

		loramac_buf_pkt_len = pktHeaderLen + lora_mac_tx_pay_len;
		LoRaMacComputeMic( loramac_buffer, loramac_buf_pkt_len, nwkskey,\
						   device_addr, DOWN_LINK, down_link_counter, &mic );

		loramac_buffer[loramac_buf_pkt_len + 0] = mic & 0xFF;
		loramac_buffer[loramac_buf_pkt_len + 1] = ( mic >> 8 ) & 0xFF;
		loramac_buffer[loramac_buf_pkt_len + 2] = ( mic >> 16 ) & 0xFF;
		loramac_buffer[loramac_buf_pkt_len + 3] = ( mic >> 24 ) & 0xFF;

		loramac_buf_pkt_len += LORAMAC_MFR_LEN;

		//Radio.SetMaxPayloadLength( MODEM_LORA, loramac_buf_pkt_len );
		Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, \
						   SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF, \
						   SINGLE_CHANNEL_GW_CD, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, \
						   SINGLE_CHANNEL_GW_FIX_LENGTH_PAY, true, 0, 0, \
						   SINGLE_CHANNEL_GW_IQ_TX, 3000 );
		Radio.Send( loramac_buffer, loramac_buf_pkt_len );

		SYS_DBG("loramac_buffer:\n");
		for (int i=0; i< loramac_buf_pkt_len; i++) SYS_DBG("x%02X ", loramac_buffer[i]);
		SYS_DBG("\n");

		free(loramac_buffer);

		/*update this device into file*/
		device_ack_req = false;
		down_link_counter++;
		current_device->device_ack_req = device_ack_req;
		current_device->down_link_cnt = down_link_counter;
		device_list.update_file();

	}
		break;

	case UNCONFIRMED_TYPE: {
		SYS_DBG("SEND_UNCONFIRMED_TYPE\n");

		LoRaMacHeader_t macHdr;
		LoRaMacFrameCtrl_t fCtrl;
		uint8_t* buffer;
		uint8_t lora_mac_tx_pay_len;
		uint32_t device_addr;
		uint32_t down_link_counter;
		uint8_t port;
		uint32_t mic = 0;
		uint8_t* loramac_buffer;
		uint8_t pktHeaderLen = 0;
		uint16_t loramac_buf_pkt_len = 0;
		bool device_ack_req;

		uint8_t* nwkskey;
		uint8_t* appskey;

		device_addr = data->unconfirmed.device_addr;
		SYS_DBG("device_address:x%08X\n", device_addr);

		lorawan_device_mng_t* current_device;
		if((current_device = device_list.get_device_by(device_addr)) == NULL ) {
			SYS_DBG("device not found\n");
			break;
		}

		nwkskey = current_device->nwkskey;
		appskey = current_device->appskey;
		down_link_counter = current_device->down_link_cnt;
		device_ack_req = current_device->device_ack_req;

		lora_mac_tx_pay_len = data->unconfirmed.data_len;
		buffer = (uint8_t*)data->unconfirmed.data;
		if(buffer == NULL) break;

		port = data->unconfirmed.port;

		loramac_buffer = (uint8_t*) malloc(LORAMAC_PHY_MAXPAYLOAD);
		if(loramac_buffer == NULL) break;

		macHdr.Value = 0;
		macHdr.Bits.MType = FRAME_TYPE_DATA_UNCONFIRMED_DOWN;
		fCtrl.Value = 0;
		fCtrl.Bits.FOptsLen      = 0;
		fCtrl.Bits.FPending      = 1;
		fCtrl.Bits.Ack           = device_ack_req;
		fCtrl.Bits.AdrAckReq     = 0;
		fCtrl.Bits.Adr           = 0;

		loramac_buffer[pktHeaderLen++] = macHdr.Value;

		loramac_buffer[pktHeaderLen++] = ( device_addr ) & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( device_addr >> 8 ) & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( device_addr >> 16 ) & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( device_addr >> 24 ) & 0xFF;

		loramac_buffer[pktHeaderLen++] = fCtrl.Value;

		loramac_buffer[pktHeaderLen++] = down_link_counter & 0xFF;
		loramac_buffer[pktHeaderLen++] = ( down_link_counter >> 8 ) & 0xFF;
		//SYS_DBG("down_link_counter:x%08X\n", down_link_counter);

		if(lora_mac_tx_pay_len > 0 ) {
			loramac_buffer[pktHeaderLen++] = port;
			if( port == 0 ) {
				LoRaMacPayloadEncrypt( (uint8_t* ) buffer, lora_mac_tx_pay_len, \
									   nwkskey, device_addr, DOWN_LINK, \
									   down_link_counter, &loramac_buffer[pktHeaderLen] );
			}
			else {
				LoRaMacPayloadEncrypt( (uint8_t* ) buffer, lora_mac_tx_pay_len, \
									   appskey, device_addr, DOWN_LINK, \
									   down_link_counter, &loramac_buffer[pktHeaderLen] );
			}
		}

		loramac_buf_pkt_len = pktHeaderLen + lora_mac_tx_pay_len;

		LoRaMacComputeMic( loramac_buffer, loramac_buf_pkt_len, nwkskey, device_addr, \
						   DOWN_LINK, down_link_counter, &mic );
		//SYS_DBG("mic_tx:x%08X\n", mic);

		loramac_buffer[loramac_buf_pkt_len + 0] =   mic & 0xFF;
		loramac_buffer[loramac_buf_pkt_len + 1] = ( mic >> 8 ) & 0xFF;
		loramac_buffer[loramac_buf_pkt_len + 2] = ( mic >> 16 ) & 0xFF;
		loramac_buffer[loramac_buf_pkt_len + 3] = ( mic >> 24 ) & 0xFF;

		loramac_buf_pkt_len += LORAMAC_MFR_LEN;

		//Radio.SetMaxPayloadLength( MODEM_LORA, loramac_buf_pkt_len );
		Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, \
						   SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF, \
						   SINGLE_CHANNEL_GW_CD, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, \
						   SINGLE_CHANNEL_GW_FIX_LENGTH_PAY, true, 0, 0, \
						   SINGLE_CHANNEL_GW_IQ_TX, 3000 );
		Radio.Send( loramac_buffer, loramac_buf_pkt_len );

		SYS_DBG("loramac_buffer:\n");
		for (int i=0; i< loramac_buf_pkt_len; i++) SYS_DBG("x%02X ", loramac_buffer[i]);
		SYS_DBG("\n");

		free(loramac_buffer);

		/*update this device into file*/
		device_ack_req = false;
		down_link_counter++;
		current_device->device_ack_req = device_ack_req;
		current_device->down_link_cnt = down_link_counter;
		device_list.update_file();
	}
		break;

	case PROPRIETARY_TYPE: {
		SYS_DBG("SEND_PROPRIETARY_TYPE\n");

		LoRaMacHeader_t macHdr;
		uint32_t device_addr;
		lorawan_msg_t* messages_out;
		uint32_t down_link_counter;
		uint8_t lora_mac_tx_pay_len;
		uint8_t pktHeaderLen;
		uint16_t loramac_buf_pkt_len;
		uint8_t* loramac_buffer;
		uint8_t* appskey;

		pktHeaderLen = 0;
		loramac_buf_pkt_len = 0;

		lora_mac_tx_pay_len = data->proprietary.data_len;
		messages_out = (lorawan_msg_t*)data->proprietary.data;
		if(messages_out == NULL) break;

		device_addr = messages_out->device_addr;
		SYS_DBG("device_address:x%08X\n", device_addr);

		lorawan_device_mng_t* current_device;
		if((current_device = device_list.get_device_by(device_addr)) == NULL ) {
			SYS_DBG("device not found\n");
			break;
		}

		down_link_counter = current_device->down_link_cnt;
		//SYS_DBG("down_link_counter:x%08X\n", down_link_counter);

		appskey = current_device->appskey;

		messages_out->sequence = (uint16_t)down_link_counter & 0x0000FFFF;

		loramac_buffer = (uint8_t*) malloc(LORAMAC_PHY_MAXPAYLOAD);
		if(loramac_buffer == NULL) break;

		macHdr.Value = 0;
		macHdr.Bits.MType = FRAME_TYPE_PROPRIETARY;

		loramac_buffer[pktHeaderLen++] = macHdr.Value;

#if 1
		LoRaPayloadEncryptProprietary((const uint8_t*)messages_out, lora_mac_tx_pay_len, appskey, loramac_buffer + pktHeaderLen);
#else
		memcpy( loramac_buffer + pktHeaderLen, ( uint8_t* ) messages_out, lora_mac_tx_pay_len );
#endif

		loramac_buf_pkt_len = pktHeaderLen + lora_mac_tx_pay_len;

		//Radio.SetMaxPayloadLength( MODEM_LORA, loramac_buf_pkt_len );
		Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, \
						   SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF, \
						   SINGLE_CHANNEL_GW_CD, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, \
						   SINGLE_CHANNEL_GW_FIX_LENGTH_PAY, true, 0, 0, \
						   SINGLE_CHANNEL_GW_IQ_TX, 3000 );
		// Send now
		Radio.Send( loramac_buffer, loramac_buf_pkt_len );
#if 0
		//Check decrypt begin
		uint8_t* dbuffer = (uint8_t*) malloc(lora_mac_tx_pay_len);
		if(dbuffer == NULL) {
			break;
		}
		LoRaPayloadDecryptProprietary(( uint8_t* ) (loramac_buffer + 1), lora_mac_tx_pay_len, loramac_appskey, dbuffer);
		for(int i=0; i<lora_mac_tx_pay_len; i++) SYS_DBG("x%02X ", *(dbuffer+i));
		free(dbuffer);
		//Check decrypt end
#endif
		free(loramac_buffer);

		down_link_counter++;
		current_device->down_link_cnt = down_link_counter;
		device_list.update_file();
	}
		break;

	default:
		//SYS_DBG("UNKNOW_TYPE\n");
		break;
	}

	pthread_mutex_unlock(&status_lock);
}

void remove_device(const uint8_t* mac) {
	device_list.remove_device_by((const uint8_t*)mac);
}
