CXXFLAGS        += -I./source/app/loramac
CXXFLAGS        += -I./source/app/loramac/mac
CXXFLAGS        += -I./source/app/loramac/radio
CXXFLAGS        += -I./source/app/loramac/linux_driver
CXXFLAGS        += -I./source/app/loramac/device_mng

VPATH += source/app/loramac
VPATH += source/app/loramac/mac
VPATH += source/app/loramac/radio
VPATH += source/app/loramac/radio/sx1276
VPATH += source/app/loramac/crypto
VPATH += source/app/loramac/linux_driver
VPATH += source/app/loramac/device_mng

OBJ += $(OBJ_DIR)/task_loramac.o
OBJ += $(OBJ_DIR)/LoRaMac.o
OBJ += $(OBJ_DIR)/LoRaMacCrypto.o
OBJ += $(OBJ_DIR)/sx1276.o
OBJ += $(OBJ_DIR)/sx1276_cfg.o
OBJ += $(OBJ_DIR)/aes.o
OBJ += $(OBJ_DIR)/cmac.o
OBJ += $(OBJ_DIR)/gpio.o
OBJ += $(OBJ_DIR)/spi.o
OBJ += $(OBJ_DIR)/device_mng.o
