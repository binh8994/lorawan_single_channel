#ifndef __APP_H__
#define __APP_H__

#include <string>
#include "app_config.h"

using namespace std;

/*****************************************************************************/
/* task MT_SYS define.
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define MT_SYS_WATCH_DOG_REPORT_REQ					(1)

/*****************************************************************************/
/*  task MT_RF24 define.
 */
/*****************************************************************************/
/* define timer */
#define MT_RF24_IF_TIMER_PACKET_DELAY_INTERVAL		(100)

/* define signal */
#define MT_RF24_IF_PURE_MSG_OUT						(1)
#define MT_RF24_IF_COMMON_MSG_OUT					(2)
#define MT_RF24_IF_TIMER_PACKET_DELAY				(3)

/*****************************************************************************/
/*  task MT_CONSOLE define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
#define MT_CONSOLE_INTERNAL_LOGIN_CMD				(1)

/*****************************************************************************/
/* task MT_IF define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define MT_IF_PURE_MSG_IN							(1)
#define MT_IF_PURE_MSG_OUT							(2)
#define MT_IF_COMMON_MSG_IN							(3)
#define MT_IF_COMMON_MSG_OUT						(4)

/*****************************************************************************/
/*  loramac task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */

#define LORAMAC_RADIO_TXTIMEOUT						(1)
#define LORAMAC_RADIO_RXTIMEOUT						(2)

/*****************************************************************************/
/*  lorawan task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define LORAWAN_DEVICE_SEND							(1)
#define LORAWAN_DEVICE_RECV							(2)
#define LORAWAN_DEVICE_SEND_PROPRIETARY				(3)
#define LORAWAN_DEVICE_JOIN_TIMER					(4)
#define LORAWAN_DEVICE_JOIN_ENABLE					(5)
#define LORAWAN_DEVICE_REMOVE						(6)

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK										(0x00)
#define APP_NG										(0x01)

#define APP_FLAG_OFF								(0x00)
#define APP_FLAG_ON									(0x01)

#define APP_ROOT_PATH_DISK							"/root/fpt_gateway"
#define APP_ROOT_PATH_RAM							"/run/fpt_gateway"

extern app_config gateway_configure;
extern app_config_parameter_t gateway_configure_parameter;

#endif // __APP_H__
