#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../common/utils.h"

#include "../app/app.h"
#include "../app/app_dbg.h"
#include "../app/task_list.h"

#include "../sys/sys_ctrl.h"
#include "../platform/stm32l/io_cfg.h"

#include "../driver/ds1302/DS1302.h"

#include "../platform/stm32l/sx1276_cfg.h"
#include "../lora/radio.h"
#include "../lora/task_sx1276.h"

#include "task_switch.h"

#define RF_FREQUENCY                                434000000 // Hz

#define TX_OUTPUT_POWER                             14        // dBm

// [0: 125 kHz,
//  1: 250 kHz,
//  2: 500 kHz,
//  3: Reserved]
#define LORA_BANDWIDTH                              0

// [SF7..SF12]
#define LORA_SPREADING_FACTOR                       7

// [1: 4/5,
//  2: 4/6,
//  3: 4/7,
//  4: 4/8]
#define LORA_CODINGRATE								1

#define LORA_PREAMBLE_LENGTH						8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT							5         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON					false

#define LORA_IQ_INVERSION_ON_TX						false
#define LORA_IQ_INVERSION_ON_RX						false

#define MY_RX_TIMEOUT_VALUE                            0

/*!
 * Radio events function pointer
 */

static void on_tx_done( void );
static void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_tx_timeout( void );
static void on_rx_timeout( void );
static void on_rx_error( void );

static RadioEvents_t radioevents;


void task_switch(ak_msg_t* msg){

	switch (msg->sig) {

	case LORA_SWITCH_INIT:{
		APP_PRINT("LORA_SWITCH_INIT\n\n");

		radioevents.TxDone = on_tx_done;
		radioevents.RxDone = on_rx_done;
		radioevents.TxTimeout = on_tx_timeout;
		radioevents.RxTimeout = on_rx_timeout;
		radioevents.RxError = on_rx_error;

		Radio.Init( &radioevents );

		Radio.SetChannel( RF_FREQUENCY );

		//Radio.SetPublicNetwork(true);

		Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
						   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
						   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
						   true, 0, 0, false, 2000 );

		Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
						   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
						   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
						   0, true, 0, 0, false, true );

		//Radio.Rx( MY_RX_TIMEOUT_VALUE );

		timer_set(LORA_TASK_SWITCH_ID, LORA_SWITCH_TEST, 4000, TIMER_PERIODIC);

	}
		break;

	case LORA_SWITCH_TEST: {
		APP_PRINT("LORA_SWITCH_TEST\n\n");

		static uint32_t counter = 0;

		const char* buf = "From stm32l151";
		Radio.Send((uint8_t*)buf, str_len((const int8_t*)buf));

		APP_DBG("counter:%d\n", counter++);

	}
		break;


	default:
		break;
	}

}


void on_tx_done( void )
{
	APP_DBG("on_tx_done\n\n");
	Radio.Sleep( );
	Radio.Rx( MY_RX_TIMEOUT_VALUE );

}

void on_tx_timeout( void )
{
	APP_DBG("on_tx_timeout\n\n");
	Radio.Sleep( );
	Radio.Rx( MY_RX_TIMEOUT_VALUE );
}

void on_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	APP_DBG("on_rx_done\n\n");
	APP_DBG("size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);
	Radio.Sleep( );
	Radio.Rx( MY_RX_TIMEOUT_VALUE );
}

void on_rx_timeout( void )
{
	APP_DBG("on_rx_timeout\n\n");
	Radio.Sleep( );
	Radio.Rx( MY_RX_TIMEOUT_VALUE );
}

void on_rx_error( void )
{
	APP_DBG("on_rx_error\n\n");
	Radio.Sleep( );
	Radio.Rx( MY_RX_TIMEOUT_VALUE );
}
