#ifndef SEND_SMS_H
#define SEND_SMS_H
#include <stdint.h>


// conditon of scene
#define EQUAL_CONDITION     0
#define MAX_CONDITION       1
#define MIN_CONTIDION       2

//--------------------

typedef struct {
	char m_send_sms_path[256];
} m_send_sms_path_t;

extern m_send_sms_path_t send_sms_path;

void send_sms_initializer(char * file_name);
void set_send_sms_path_file(char*);
void get_send_sms_path_file(char*);
uint8_t parser_send_sms_file(uint8_t* data, uint32_t len);
uint8_t add_new_send_sms_file(uint8_t* message, uint32_t len);
uint8_t delete_send_sms_file();
void bytetostring(uint8_t data, uint8_t* str, uint8_t* len);
#endif // SEND_SMS_H
