#ifndef TASK_SCENE_H
#define TASK_SCENE_H


#include "../ak/message.h"

extern q_msg_t gw_task_scene_mailbox;
extern void* gw_task_scene_entry(void*);

#endif // TASK_SCENE_H
