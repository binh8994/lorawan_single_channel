/*
 * if_socket.cpp
 *
 *  Created on: Jun 2, 2017
 *      Author: tudt13
 */

#include<arpa/inet.h> //inet_addr
#include<pthread.h> //for threading , link with lpthread
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <time.h>

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>//FD_SET, FD_ISSET, FD_ZERO macros
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <semaphore.h>

#include "../ak/ak.h"

#include "../sys/sys_dbg.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

using namespace std;
#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "if_socket_online.h"
#include "../znp/zcl.h"
#include "../znp/znp_serial.h"
#include "global_parameters.h"
//#define TRUE										1
//#define FALSE										0
#define ONLINE_PORT									8080
#define ONLINE_SERVER_IP							"118.69.193.218"

/*if-socket mailbox ak message mechanism*/
q_msg_t gw_task_if_socket_online_mailbox;
void* gw_task_if_socket_online_entry(void*);

int8_t sendMessageToServer(uint8_t *client_message, int32_t len);

void* gw_task_if_socket_online_entry(void*) {
	uint8_t client_message[MAX_MESSAGE_LEN];

	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_if_socket_entry\n");

	while (1) {
		while (msg_available(ID_TASK_IF_SOCKET_ONLINE)) {
			/* get message */
			ak_msg_t* msg = rev_msg(ID_TASK_IF_SOCKET_ONLINE);

			switch (msg->header->sig) {
			case IF_SOCKET_ONLINE_SENSOR_DATA: {
				uint8_t i;
				zclOutgoingMsg_t *outgoingMsg; //data send to mqtt
				uint8_t* data = (uint8_t *) msg->header->payload;
				outgoingMsg = (zclOutgoingMsg_t *) data;
				data += sizeof(zclOutgoingMsg_t);

				outgoingMsg->data = data;
				//add prefix "@logsensor"
				memcpy(client_message, "GET /?@logsensor", strlen("GET /?logsensor="));

				i = strlen((const char*) client_message);

				//add short address
				bytestoHexChars((uint8_t*) &outgoingMsg->short_addr, 2, (uint8_t*) &client_message[i]);
				i += 4;

				//add cluster_id
				bytestoHexChars((uint8_t*) &outgoingMsg->cluster_id, 2, (uint8_t*) &client_message[i]);
				i += 4;

				//add command
				bytestoHexChars((uint8_t*) &outgoingMsg->cmd, 1, (uint8_t*) &client_message[i]);
				i += 2;

				//add atribbute ID
				bytestoHexChars((uint8_t*) &outgoingMsg->attrID, 2, (uint8_t*) &client_message[i]);
				i += 4;

				//add data type
				bytestoHexChars((uint8_t*) &outgoingMsg->dataType, 1, (uint8_t*) &client_message[i]);
				i += 2;
				//add data len
				bytestoHexChars((uint8_t*) &outgoingMsg->dataLen, 1, (uint8_t*) &client_message[i]);
				i += 2;
				//add data
				bytestoHexChars(data, outgoingMsg->dataLen, (uint8_t*) &client_message[i]);
				i += outgoingMsg->dataLen * 2;


				if (sendMessageToServer(client_message, i) == FALSE) {
					APP_PRINT("Send message error!\n");
				}

			}
				break;
			case IF_SOCKET_ONLINE_ADD_NEW_ZED_RESPONSE: {
				int32_t i = 0;
				//add token communicate
				client_message[i] = SEPARATE_CHAR;
				i++;
				memcpy((uint8_t*) &client_message[i], g_strTokenCommunicate.c_str(), g_strTokenCommunicate.size());
				i += g_strTokenCommunicate.size();

				//add time
				client_message[i] = SEPARATE_CHAR;
				i++;
				time_t rawtime = time(NULL);
				bytestoHexChars((uint8_t*) &rawtime, sizeof(time_t), (uint8_t*) &client_message[i]);
				i += sizeof(time_t);

				//add command adddevice
				client_message[i] = SEPARATE_CHAR;
				i++;
				memcpy((uint8_t*) &client_message[i], g_strCommand[adddevice].c_str(), g_strCommand[adddevice].size());
				i += g_strCommand[adddevice].size();
				//add data
				bytestoHexChars((uint8_t*) msg->header->payload, msg->header->len, (uint8_t*) &client_message[i]);
				i += msg->header->len;
				client_message[i] = '\0';
				i++;
				if (sendMessageToServer(client_message, i) == FALSE) {
					APP_PRINT("Send message error!\n");
				}
			}
				break;
			default:
				break;
			}
			/* free message */
			free_msg(msg);
		}
		usleep(1000);
	}
	return (void*) 0;
}

int8_t sendMessageToServer(uint8_t *client_message, int32_t len) {
	//Send HTTP GET to online socket(temporary)
	int sock;
	struct sockaddr_in server;

	//Create socket
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		APP_PRINT("Could not create socket");
		return FALSE;
	}
	puts("Socket created");

	server.sin_addr.s_addr = inet_addr(ONLINE_SERVER_IP);
	server.sin_family = AF_INET;
	server.sin_port = htons(ONLINE_PORT);

	//Connect to remote server
	if (connect(sock, (struct sockaddr *) &server, sizeof(server)) < 0) {
		perror("Connect to online server failed. Error");
		return FALSE;
	}

	puts("Online server Connected\n");

	if (send(sock, client_message, len, 0) < 0) {
		puts("Send failed");
		return FALSE;
	}
	close(sock);

	return TRUE;
}

int8_t isServerStatus(void) {
	//Send HTTP GET to online socket(temporary)
	int sock;
	struct sockaddr_in server;

	//Create socket
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		APP_PRINT("Could not create socket");
		return OFFLINE;
	}
	puts("Socket created");

	server.sin_addr.s_addr = inet_addr(ONLINE_SERVER_IP);
	server.sin_family = AF_INET;
	server.sin_port = htons(ONLINE_PORT);

	//Connect to remote server
	if (connect(sock, (struct sockaddr *) &server, sizeof(server)) < 0) {
		perror("Connect to online server failed. Error");
		return OFFLINE;
	}

	puts("Online server Connected\n");

	close(sock);

	return ONLINE;
}
