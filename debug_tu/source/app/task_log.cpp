#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <dirent.h>

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_log.h"
#include "znp/zcl.h"
using namespace std;

q_msg_t gw_task_log_mailbox;

string log_folder_path;
string log_sensors_report_file_path;

static FILE* log_file_obj;

void update_report_sensor_file(zclOutgoingMsg_t* sensor_data);
void init_report_sensor_file(void);
void create_log_data_file_path();
void write_data_into_log_data_file(uint8_t *data, int32_t len);
void clean_log_files();
void resetLog();

void* gw_task_log_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_log_entry\n");
	create_log_data_file_path();
	while (1) {

		while (msg_available(ID_TASK_LOG)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(ID_TASK_LOG);

			/* handler message */

			switch (msg->header->sig) {
			case LOG_SENSOR_DATA: {
				APP_DBG("write log \n");

				write_data_into_log_data_file((uint8_t *) msg->header->payload, msg->header->len);
			}
				break;
			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*) 0;
}

void init_report_sensor_file(void) {
	log_file_obj = fopen(log_sensors_report_file_path.data(), "a+b");
	if (log_file_obj == NULL) {
		APP_DBG("can not open %s file\n", log_sensors_report_file_path.data());
	} else {

		fprintf(log_file_obj, "Time				short_address	group_id	cluster_id	command		attrID		dataType	dataLen		*data\n");
		fclose(log_file_obj);
	}
}
void update_report_sensor_file(zclOutgoingMsg_t* sensor_data) {
	time_t rawtime;
	struct tm *time_info;
	time(&rawtime);
	time_info = localtime(&rawtime);
	char data_temp[10];
	sprintf(data_temp, "%04d%02d%02d", (time_info->tm_year + 1900), (time_info->tm_mon + 1), time_info->tm_mday);
	//check date to create a new log file
	for (int i = 0; i < 6; i++) {
		if (data_temp[i] != log_sensors_report_file_path.c_str()[log_sensors_report_file_path.size() - 10 + i]) {
			APP_PRINT("create new log!\n");
			create_log_data_file_path();
			break;
		}
	}
	log_file_obj = fopen(log_sensors_report_file_path.data(), "a+b");
	if (log_file_obj == NULL) {
		APP_DBG("can not open %s file\n", log_sensors_report_file_path.data());
	} else {

		fprintf(log_file_obj, "%04d/%02d/%02d %02d:%02d:%02d		", (time_info->tm_year + 1900), (time_info->tm_mon + 1), time_info->tm_mday, time_info->tm_hour, time_info->tm_min, time_info->tm_sec);

		fprintf(log_file_obj, "%04X		%04X		%04X		%02X		%04X		%02X		%02X		", sensor_data->short_addr, sensor_data->group_id, sensor_data->cluster_id, sensor_data->cmd, sensor_data->attrID, sensor_data->dataType, sensor_data->dataLen);
		uint8_t *data = (uint8_t *) sensor_data;
		data += sizeof(zclOutgoingMsg_t);
		for (int i = 0; i < sensor_data->dataLen; i++) {
			fprintf(log_file_obj, "%02X ", sensor_data->data[i]);
		}
		fprintf(log_file_obj, "\n");
		fclose(log_file_obj);
	}
}

void write_data_into_log_data_file(uint8_t *data, int32_t len) {
	time_t rawtime;
	struct tm *time_info;
	time(&rawtime);
	time_info = localtime(&rawtime);
	char data_temp[10];
	sprintf(data_temp, "%04d%02d%02d", (time_info->tm_year + 1900), (time_info->tm_mon + 1), time_info->tm_mday);
	//check date to create a new log file
//	APP_PRINT("data_temp = %s, file_path = %s", data_temp, &log_sensors_report_file_path.c_str()[log_sensors_report_file_path.size() - 12]);
	for (int i = 0; i < 6; i++) {
		if (data_temp[i] != log_sensors_report_file_path.c_str()[log_sensors_report_file_path.size() - 12 + i]) {
			APP_PRINT("create new log!\n");
			create_log_data_file_path();
			break;
		}
	}
	log_file_obj = fopen(log_sensors_report_file_path.data(), "a+b");
	if (log_file_obj == NULL) {
		APP_DBG("can not open %s file\n", log_sensors_report_file_path.data());
	} else {
		fprintf(log_file_obj, "%s", data);
		fprintf(log_file_obj, "\n");
		fclose(log_file_obj);
	}
}

void create_log_data_file_path() {
	time_t rawtime;
	struct tm *time_info;
	time(&rawtime);
	time_info = localtime(&rawtime);
	char data[10];
//	int n = 0;
	sprintf(data, "%04d%02d%02d", (time_info->tm_year + 1900), (time_info->tm_mon + 1), time_info->tm_mday);
	APP_PRINT("[time data = %s]\n", data);
	struct stat st = { 0 };
    // creat log folder;
	log_folder_path = static_cast<string>(APP_ROOT_PATH) + static_cast<string>("/log/");
	if (stat(log_folder_path.data(), &st) == -1) {
		mkdir(log_folder_path.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}
	log_sensors_report_file_path = log_folder_path + static_cast<string>(LOG_FILE_NAME) + static_cast<string>(data) + static_cast<string>(".txt");
	if (stat(log_sensors_report_file_path.data(), &st) == -1) {
		init_report_sensor_file();
	}
	APP_PRINT("[file = %s]\n", log_sensors_report_file_path.c_str());
	clean_log_files();
}

void clean_log_files() {
	try {
		string minfile;
		int minfilename = 999999999;
		int filecount = 0;
		DIR * dirp;
		struct dirent * entry;

		dirp = opendir(log_folder_path.c_str());
		/* There should be error handling after this */
		while ((entry = readdir(dirp)) != NULL) {
			if (entry->d_type == DT_REG) { /* If the entry is a regular file */
				string filename = entry->d_name;
				filename.erase(filename.begin(), filename.begin() + 9);
				if (atoi(filename.c_str()) < minfilename) {
					minfilename = atoi(filename.c_str());
					minfile = entry->d_name;
				}
				filecount++;
			}
		}
		string filetodelete = log_folder_path + minfile;
		if (filecount >= 7) {
			if (remove(filetodelete.c_str()) != 0)
				perror("Error deleting file");
			else {
				puts("File successfully deleted");
			}
		}
		closedir(dirp);
	} catch (exception e) {
		printf("Delete log file: %s\n", e.what());
	}
}
