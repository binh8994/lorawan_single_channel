#include "scene.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hal_defs.h"

#include <iostream>
#include <fstream>

#include "../znp/znp_serial.h"
#include "../znp/znp_serial.h"
#include "task_list.h"


#include "global_parameters.h"

string scene_folder;
string scene_file_path;
m_scene_path_t scene_path;

void scene_initializer(char * file_name) {
    struct stat st = { 0 };
    /* create scene path */
    string scene_folder = static_cast<string>(APP_ROOT_PATH) + static_cast<string>("/scene/");
    if (stat(scene_folder.data(), &st) == -1) {
        mkdir(scene_folder.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }

    string scene_file_path = scene_folder + static_cast<string>((const char*) file_name);

    APP_PRINT("config_file_path: %s\n", scene_file_path.c_str());
    strcpy(scene_path.m_scene_path, scene_file_path.c_str());
}

void set_scene_path_file(char* path) {
    strcpy(scene_path.m_scene_path, (const char*) path);
}

void get_scene_path_file(char* path) {
    strcpy(path, (const char*) scene_path.m_scene_path);
}

uint8_t parser_scene_file(uint8_t* message, uint32_t len_message) {

    fstream f;
    FILE* configure_file_obj = fopen(scene_path.m_scene_path, "r");
    if (configure_file_obj == NULL) {
        return -1;
    }
    fclose(configure_file_obj);

    f.open(scene_path.m_scene_path, ios::app);
    f.close();
    f.open(scene_path.m_scene_path, ios::in);

    string data;
    string line;
    uint8_t* pline ;
    uint8_t check_scene;
    uint32_t len_senser_in;
    uint32_t len_scene_in;

//    APP_PRINT("\n");
//    APP_PRINT("%s",message);
//    APP_PRINT("\n");

    parse_communicate_string_t parse_senser = parseClientString(message, len_message );
    if (parse_senser.totalString < 6) {
        return FALSE;
    }
    len_senser_in = parse_senser.position[5] - parse_senser.position[3];

    uint16_t k = 0;
    uint8_t condition;
    uint16_t data_senser;
    uint16_t data_in_line;
    while (!f.eof()) {
        check_scene = 0;
        getline(f, line);
        data += line;
        pline = (uint8_t*)line.c_str();
        k++;

        parse_communicate_string_t parse_line = parseClientString(pline, line.length());

        if (parse_line.totalString < 7) {
            continue;
        }
        int8_t i = 0;

        len_scene_in = parse_line.position[2] - parse_line.position[0]; // short address + type data;
        //APP_PRINT("len_scene_in %d\n",len_scene_in);
        if (len_senser_in == len_scene_in) {
            if (compera_string(&message[parse_senser.position[3]] ,&pline[parse_line.position[0]],len_scene_in) == TRUE) {
               // APP_PRINT("in put true\n");
            } else {
               continue;
            }
        }else {
           continue;
        }

        // read condition
        hexCharsToBytes((uint8_t*) &pline[parse_line.position[2]], parse_line.len[2], (uint8_t*) &condition);


         data_senser = 0;
         data_in_line = 0;
         hexCharsToBytes((uint8_t*) &pline[parse_line.position[3]], parse_line.len[3], (uint8_t*) &data_in_line);
         hexCharsToBytes((uint8_t*) &message[parse_senser.position[5]], parse_senser.len[5], (uint8_t*) &data_senser);


        //APP_PRINT("device in found scene action\n");
        switch (condition) {
        case EQUAL_SCENE: {
            if(data_senser == data_in_line){
                APP_PRINT("scene equal\n");
                check_scene = 1;
            }
        }
            break;
        case MAX_SCENE: {
            if(data_senser > data_in_line){
                APP_PRINT("scene max\n");
                check_scene = 1;
            }
        }
            break;
        case MIN_SCENE: {
            if(data_senser < data_in_line){
                APP_PRINT("scene min\n");
                check_scene = 1;
            }
        }
            break;
        default: {
            continue;
        }
            break;
        }



//            // send data to AF_REQUEST
            if(check_scene) {

                uint32_t timer_scene;
                uint8_t j;
                uint8_t* data1;
                uint8_t len;
                af_data_request_t afDataRequest;

                i = 4; //note: remove: token_communication, time, device id, sensor type, start data sensor.
                if (pline[parse_line.position[i]] == '{'){
                    i++; // next to list out

                    uint8_t number_list = ((parse_line.totalString - i) /3) + 1;
                    for (uint8_t l = 0; l <number_list; l++) {
                        if (pline[parse_line.position[i]] == '}') {
                            return TRUE;
                        }
                        j = 0;
                        len = (parse_line.len[i+1] + parse_line.len[i])/2 + sizeof(afDataRequest.data);
                        data1 = (uint8_t*)malloc(len);

                        // short address
                        hexCharsToBytes((uint8_t*) &pline[parse_line.position[i]], 2 * 2, (uint8_t*) &data1[j]);
                        j +=2;
                        i ++; // move to data out
                        //data out
                        hexCharsToBytes((uint8_t*) &pline[parse_line.position[i]], (parse_line.position[i+1] - parse_line.position[i] - 1), (uint8_t*) &data1[j]);
                        i++;
                        //timer
                        hexCharsToBytes((uint8_t*) &pline[parse_line.position[i]], parse_line.len[i], (uint8_t*) &timer_scene);
                        i++;
                        //APP_PRINT("timer : %d\n",timer_scene);

                        afDataRequest = *(af_data_request_t*) data1;

                        if (afDataRequest.len > (len - sizeof(afDataRequest))) {
                            return FALSE;
                        }

                        uint8_t *data2 = (uint8_t*) malloc(afDataRequest.len);
                        memcpy(data2, (uint8_t*) &data1[sizeof(afDataRequest) - sizeof(afDataRequest.data)], afDataRequest.len);
                        memcpy((uint8_t*) &data1[sizeof(afDataRequest)], data2, afDataRequest.len);

                        {
                        ak_msg_t* s_msg = get_dymanic_msg();
                        set_msg_sig(s_msg, AF_DATA_REQUEST);
                        set_data_dynamic_msg(s_msg, data1, len);
                        set_msg_src_task_id(s_msg, ID_TASK_SCENE);
                        task_post(ID_TASK_IF_ZNP, s_msg);
                        }

                        usleep(15000);

                        free(data2);
                        free(data1);
                    }

                }else {
                   return FALSE;
                }
            }
    }
    f.close();
    return TRUE;
}

//int scene::add_new_scene_file(scene_parameter_t* cfg) {
uint8_t add_new_scene_file(uint8_t* message, uint32_t len) {

    FILE* configure_file_obj = fopen(scene_path.m_scene_path, "w+b");
    if (configure_file_obj == NULL) {
//        APP_PRINT("not found file");
        return -1;
    }
   fwrite(message, len, 1, configure_file_obj);
   //APP_PRINT("[%s]", message_str);
   fclose(configure_file_obj);
   printf("add new sucessull\n");

    return TRUE;
}
uint8_t delete_scene_file() {

    FILE* configure_file_obj = fopen(scene_path.m_scene_path, "w");
    if (configure_file_obj == NULL) {
//        APP_PRINT("not found file to delete\n");
        return -1;
    }
    uint8_t data_str[] = " ";
    fwrite(data_str, strlen((const char*) data_str), 1, configure_file_obj);
    fclose(configure_file_obj);
    APP_PRINT("delete scene file\n");

    return TRUE;
}

uint8_t compera_string(uint8_t* data1, uint8_t* data2, uint32_t len) {

    for (uint32_t i = 0; i<len; i++) {
        if ((data1[i]) != (data2[i])) {
            return FALSE;
        }
    }
    return TRUE;

}

