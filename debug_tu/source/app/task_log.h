#ifndef __TASK_LOG_H__
#define __TASK_LOG_H__

#include "../ak/message.h"

extern q_msg_t gw_task_log_mailbox;
extern void* gw_task_log_entry(void*);

#endif //__TASK_LOG_H__
