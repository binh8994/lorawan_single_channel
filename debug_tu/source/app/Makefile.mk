-include source/app/mqtt/Makefile.mk
-include source/app/interfaces/Makefile.mk
-include source/app/door_manager/Makefile.mk
-include source/app/znp/Makefile.mk

CXXFLAGS	+= -I./source/app

VPATH += source/app

OBJ += $(OBJ_DIR)/app.o
OBJ += $(OBJ_DIR)/app_config.o
OBJ += $(OBJ_DIR)/task_list.o
OBJ += $(OBJ_DIR)/task_log.o
OBJ += $(OBJ_DIR)/task_mqtt.o
OBJ += $(OBJ_DIR)/task_handle_msg.o
OBJ += $(OBJ_DIR)/task_scene.o
OBJ += $(OBJ_DIR)/scene.o
OBJ += $(OBJ_DIR)/task_gsm.o
OBJ += $(OBJ_DIR)/send_sms.o
OBJ += $(OBJ_DIR)/task_send_sms.o
