#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../sys/sys_ctrl.h"
#include "../driver/button/button.h"
#include "../platform/stm32f10x/io_cfg.h"
#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_ui.h"
#include "lorawan.h"
#include "app_bsp.h"


void task_ui(ak_msg_t* msg) {
	switch (msg->sig) {
	case AK_BUTTON_ONE_CLICK:

		button_disable(&button);
		lorawan_send();
		button_enable(&button);

		break;

	case AK_BUTTON_LONG_PRESS:

		button_disable(&button);
		sys_ctrl_reset();

		break;

	case AK_LORAWAN_RESPONSE_ACK_TIMEOUT:
		APP_DBG("AK_LORAWAN_RESPONSE_ACK_TIMEOUT\n");

		led_ack_off();

		break;

	default:
		break;
	}
}
