#include <string.h>

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../platform/stm32f10x/io_cfg.h"
#include "../common/xprintf.h"
#include "../driver/led/led.h"
#include "../driver/button/button.h"
#include "app_bsp.h"

#include "task_life.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"

#include "loramac/radio/sx1276/sx1276_cfg.h"
#include "loramac/radio/sx1276/sx1276.h"

#include "loramac/mac/LoRaMac-definitions.h"
#include "loramac/mac/LoRaMac.h"
#include "loramac/mac/LoRaMacCrypto.h"

#include "../crypto/aes.h"
#include "../crypto/cmac.h"

#include "lorawan.h"

//#define OVER_THE_AIR_ACTIVATION

#ifdef  OVER_THE_AIR_ACTIVATION
#define LORAWAN_APPLICATION_EUI				{ 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA }
#define LORAWAN_APPLICATION_KEY				{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }

static uint8_t AppEui[] = LORAWAN_APPLICATION_EUI;
static uint8_t AppKey[] = LORAWAN_APPLICATION_KEY;
#else

#define LORAWAN_NETWORK_ID					0
#define LORAWAN_NWKSKEY						{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }
#define LORAWAN_APPSKEY						{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }

static const uint32_t	NwkID		= LORAWAN_NETWORK_ID;
static const uint8_t	NwkSKey[]	= LORAWAN_NWKSKEY;
static const uint8_t	AppSKey[]	= LORAWAN_APPSKEY;
#endif

static LoRaMacPrimitives_t LoRaMacPrimitives;
static LoRaMacCallback_t LoRaMacCallbacks;
static MibRequestConfirm_t mibReq;

static void McpsConfirm( McpsConfirm_t *mcpsConfirm );
static void McpsIndication( McpsIndication_t *mcpsIndication );
static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm );

#define APP_PORT						80
#define DATA_F_SIZE						2
#define ZCL_CLUSTER_ID_GEN_ON_OFF		0x0006
#define ZCL_DATATYPE_UINT8				0x20
#define ZCL_CMD_ID_DEFAULT_RESPONSE		0x0B
#define ZCL_CMD_ID_CONTROL				0x01
#define ZCL_CMD_ID_REPORT				0x0A

typedef struct {
	uint16_t clusterId;
	uint32_t deviceAddr;
	uint16_t sequence;
	uint8_t cmd_id;
	uint8_t dataType;
	uint8_t dataLen;
	uint8_t data[DATA_F_SIZE];
} __attribute__((__packed__)) lorawan_msg_t;

static lorawan_msg_t warning_msg;
static uint16_t sequence_counter = 0;

static void McpsConfirm( McpsConfirm_t *mcpsConfirm ) {
	if( mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
		switch( mcpsConfirm->McpsRequest ) {
		case MCPS_UNCONFIRMED: {
			APP_DBG("McpsConfirm-UNCONFIRMED\n");
			break;
		}
		case MCPS_CONFIRMED: {
			APP_DBG("McpsConfirm-CONFIRMED\n");
			break;
		}
		case MCPS_PROPRIETARY: {
			APP_DBG("McpsConfirm-PROPRIETARY\n");
			break;
		}
		default:
			break;
		}

	}
}

static void McpsIndication( McpsIndication_t *mcpsIndication ) {
	if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK ) {
		return;
	}

	switch( mcpsIndication->McpsIndication ) {
	case MCPS_UNCONFIRMED: {
		APP_DBG("McpsIndication-UNCONFIRMED\n");

		lorawan_msg_t command_msg;
		memcpy(&command_msg, mcpsIndication->Buffer, sizeof(lorawan_msg_t));

		APP_DBG("command_msg.deviceAddr:x%08X\n",  command_msg.deviceAddr);
		APP_DBG("command_msg.clusterId:x%04X\n" , command_msg.clusterId	);
		APP_DBG("command_msg.sequence:x%04X\n" , command_msg.sequence);
		APP_DBG("command_msg.cmd_id:x%02X\n" , command_msg.cmd_id);
		APP_DBG("command_msg.dataType:x%02X\n" , command_msg.dataType);
		APP_DBG("command_msg.dataLen:x%02X\n" , command_msg.dataLen	);
		APP_DBG("command_msg.data[0]:x%02X\n" , command_msg.data[0]);

		if(command_msg.clusterId == ZCL_CLUSTER_ID_GEN_ON_OFF) {
			if(command_msg.cmd_id == ZCL_CMD_ID_CONTROL) {
				if(command_msg.data[0] == 1) {
					led_green_off();
					led_red_on();
				}
				else {
					led_red_off();
					led_green_on();
				}

				timer_remove_attr(AK_TASK_UI_ID, AK_LORAWAN_RESPONSE_ACK_TIMEOUT);
				led_ack_off();

				/*send ack*/
				lorawan_msg_t ack_msg = command_msg;
				ack_msg.cmd_id = ZCL_CMD_ID_DEFAULT_RESPONSE;
				ack_msg.sequence = sequence_counter;
				ack_msg.dataLen = 2;
				ack_msg.data[0] = 1;
				ack_msg.data[1] = command_msg.data[0];


				APP_DBG("\ncommand_msg.deviceAddr:x%08X\n",  ack_msg.deviceAddr);
				APP_DBG("command_msg.clusterId:x%04X\n" , ack_msg.clusterId	);
				APP_DBG("command_msg.sequence:x%04X\n" , ack_msg.sequence);
				APP_DBG("command_msg.cmd_id:x%02X\n" , ack_msg.cmd_id);
				APP_DBG("command_msg.dataType:x%02X\n" , ack_msg.dataType);
				APP_DBG("command_msg.dataLen:x%02X\n" , ack_msg.dataLen	);
				APP_DBG("command_msg.data[0]:x%02X\n" , ack_msg.data[0]);
				APP_DBG("command_msg.data[1]:x%02X\n" , ack_msg.data[1]);


				McpsReq_t mcpsReq;
				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = APP_PORT;
				mcpsReq.Req.Unconfirmed.fBuffer = &ack_msg;
				mcpsReq.Req.Unconfirmed.fBufferSize = sizeof(lorawan_msg_t);
				mcpsReq.Req.Unconfirmed.Datarate = LORAMAC_DEFAULT_DATARATE;

				LoRaMacMcpsRequest( &mcpsReq );
				APP_DBG("Schedule send ack\n");

				sequence_counter++;

			}
			else if(command_msg.cmd_id == ZCL_CMD_ID_DEFAULT_RESPONSE) {
				timer_remove_attr(AK_TASK_UI_ID, AK_LORAWAN_RESPONSE_ACK_TIMEOUT);
				led_ack_off();
			}
		}

		break;
	}
	case MCPS_CONFIRMED: {
		APP_DBG("McpsIndication-MCPS_CONFIRMED\n");
		break;
	}
	case MCPS_PROPRIETARY: {
		APP_DBG("McpsIndication-MCPS_PROPRIETARY\n");
		break;
	}
	case MCPS_MULTICAST: {
		APP_DBG("McpsIndication:MCPS_MULTICAST\n");
		break;
	}
	default:
		break;
	}

	if( mcpsIndication->RxData == true ) {
		APP_DBG("McpsIndication-RxData:true\n");
	}
}

static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm ) {
	switch( mlmeConfirm->MlmeRequest ) {
	case MLME_JOIN: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			APP_DBG("MlmeConfirm-JOIN SUCCESS\n");
		}
		else {
			APP_DBG("MlmeConfirm-JOIN FAIL\n");
			lorawan_join();
		}
		break;
	}
	case MLME_LINK_CHECK: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			//APP_DBG("MlmeConfirm-LINK_CHECK\n");
		}
		break;
	}
	default:
		break;
	}

}


void lorawan_init() {
	APP_DBG("lorawan_init()\n");

	LoRaMacPrimitives.MacMcpsConfirm = McpsConfirm;
	LoRaMacPrimitives.MacMcpsIndication = McpsIndication;
	LoRaMacPrimitives.MacMlmeConfirm = MlmeConfirm;
	LoRaMacCallbacks.GetBatteryLevel = 0;
	LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks );

	mibReq.Type = MIB_ADR;
	mibReq.Param.AdrEnable = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_REPEATER_SUPPORT;
	mibReq.Param.EnableRepeaterSupport = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_PUBLIC_NETWORK;
	mibReq.Param.EnablePublicNetwork = true;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_DEVICE_CLASS;
	mibReq.Param.Class = CLASS_C;
	LoRaMacMibSetRequestConfirm( &mibReq );

	/* init mes to send */
	warning_msg.deviceAddr = get_device_id();
	warning_msg.clusterId  = ZCL_CLUSTER_ID_GEN_ON_OFF;
	warning_msg.sequence = sequence_counter;
	warning_msg.cmd_id = ZCL_CMD_ID_REPORT;
	warning_msg.dataType = ZCL_DATATYPE_UINT8;
	warning_msg.dataLen = 1;
	warning_msg.data[0] = 2;


	uint8_t DevEui[8];
	get_device_Eui(DevEui);
	APP_DBG("DevEui:");
	for(int i = 0; i < 8; i++)	APP_DBG("0x%02X ", DevEui[i]);
	APP_DBG("\n");
}

void lorawan_join() {
	APP_DBG("lorawan_join()\n");

#ifdef OVER_THE_AIR_ACTIVATION
	MlmeReq_t mlmeReq;

	mlmeReq.Type = MLME_JOIN;
	mlmeReq.Req.Join.DevEui = DevEui;
	mlmeReq.Req.Join.AppEui = AppEui;
	mlmeReq.Req.Join.AppKey = AppKey;
	mlmeReq.Req.Join.NbTrials = 1;

	LoRaMacMlmeRequest( &mlmeReq );

#else

	mibReq.Type = MIB_NET_ID;
	mibReq.Param.NetID = NwkID;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_DEV_ADDR;
	mibReq.Param.DevAddr = get_device_id();
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_NWK_SKEY;
	mibReq.Param.NwkSKey = (uint8_t*)NwkSKey;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_APP_SKEY;
	mibReq.Param.AppSKey = (uint8_t*)AppSKey;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_NETWORK_JOINED;
	mibReq.Param.IsNetworkJoined = true;
	LoRaMacMibSetRequestConfirm( &mibReq );

	uint32_t DevAddress = get_device_id();
	APP_DBG("[APB] DevAddr:0x%08X\n", DevAddress);

#endif
}

void lorawan_send() {
	APP_DBG("lorawan_send()\n");

	MibRequestConfirm_t mibReq;
	LoRaMacStatus_t status;

	mibReq.Type = MIB_NETWORK_JOINED;
	status = LoRaMacMibGetRequestConfirm( &mibReq );

	if( status == LORAMAC_STATUS_OK ) {
		if( mibReq.Param.IsNetworkJoined == true ) {

			McpsReq_t mcpsReq;
			LoRaMacTxInfo_t txInfo;
			uint16_t data_size = sizeof(warning_msg);
			if( LoRaMacQueryTxPossible( data_size, &txInfo ) != LORAMAC_STATUS_OK ) {
				// Send empty frame in order to flush MAC commands
				mcpsReq.Type = MCPS_CONFIRMED;
				mcpsReq.Req.Confirmed.fPort = 0;
				mcpsReq.Req.Confirmed.fBuffer = 0;
				mcpsReq.Req.Confirmed.fBufferSize = 0;
				mcpsReq.Req.Confirmed.Datarate = LORAMAC_DEFAULT_DATARATE;
				mcpsReq.Req.Confirmed.NbTrials = 0;
				LoRaMacMcpsRequest( &mcpsReq );

			}
			else {

				warning_msg.sequence = sequence_counter;

				APP_DBG("warning_msg.deviceAddr:x%08X\n",  warning_msg.deviceAddr);
				APP_DBG("warning_msg.clusterId:x%04X\n" , warning_msg.clusterId	);
				APP_DBG("warning_msg.sequence:x%04X\n" , warning_msg.sequence);
				APP_DBG("warning_msg.cmd_id:x%02X\n" , warning_msg.cmd_id);
				APP_DBG("warning_msg.dataType:x%02X\n" , warning_msg.dataType);
				APP_DBG("warning_msg.dataLen:x%02X\n" , warning_msg.dataLen	);
				APP_DBG("warning_msg.data[0]:x%02X\n" , warning_msg.data[0]);

				mcpsReq.Type = MCPS_CONFIRMED;
				mcpsReq.Req.Confirmed.fPort = APP_PORT;
				mcpsReq.Req.Confirmed.fBuffer = &warning_msg;
				mcpsReq.Req.Confirmed.fBufferSize = data_size;
				mcpsReq.Req.Confirmed.Datarate = LORAMAC_DEFAULT_DATARATE;
				mcpsReq.Req.Confirmed.NbTrials = 2;

				if(LoRaMacMcpsRequest( &mcpsReq ) == LORAMAC_STATUS_OK) {
					sequence_counter++;

					led_ack_on();
					led_red_off();
					led_green_off();
					timer_set(AK_TASK_UI_ID, AK_LORAWAN_RESPONSE_ACK_TIMEOUT, 10000, TIMER_ONE_SHOT);
				}
			}
		}
		else {
			APP_DBG("Joined not yet\n");
		}
	}

}
