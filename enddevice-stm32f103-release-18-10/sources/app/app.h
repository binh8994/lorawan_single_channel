/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef APP_H
#define APP_H

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************/
/*  life task define
 */
/*****************************************************************************/
/* define timer */
#define AK_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
#define AK_LIFE_SYSTEM_CHECK						(1)

/*****************************************************************************/
/*  shell task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define AK_SHELL_LOGIN_CMD							(1)
#define AK_SHELL_REMOTE_CMD							(2)

/*****************************************************************************/
/*  loramac task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */

#define LORAMAC_RADIO_TXTIMEOUT						(1)
#define LORAMAC_RADIO_RXTIMEOUT						(2)
#define LORAMAC_RADIO_RXTIMEOUT_SYNCWORD			(3)
#define LORAMAC_MAC_STATE_CHECK_TIMER				(4)
#define LORAMAC_MAC_TXDELAYED_TIMER					(5)
#define LORAMAC_MAC_RXWINDOW_1_TIMER				(6)
#define LORAMAC_MAC_RXWINDOW_2_TIMER				(7)
#define LORAMAC_MAC_ACK_TIMEOUT						(8)

/*****************************************************************************/
/*  button task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define AK_BUTTON_ONE_CLICK							(1)
#define AK_BUTTON_LONG_PRESS						(2)
#define AK_LORAWAN_RESPONSE_ACK_TIMEOUT				(3)

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK                                      (0x00)
#define APP_NG                                      (0x01)

#define APP_FLAG_OFF                                (0x00)
#define APP_FLAG_ON                                 (0x01)

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
#define AC_NUMBER_SAMPLE_CT_SENSOR				3000

#define MY_DEVICE_ADDR							0x00001122

extern int  main_app();

#ifdef __cplusplus
}
#endif

#endif //APP_H
