#ifndef __SYS_IRQ_H__
#define __SYS_IRQ_H__

#ifdef __cplusplus
extern "C"
{
#endif

extern void sys_irq_nrf24l01();
extern void sys_irq_shell();
extern void sys_irq_timer_10ms();
extern void sys_irq_sx1276_dio_0();//__attribute__((__weak__));
extern void sys_irq_sx1276_dio_1();//__attribute__((__weak__));
extern void sys_irq_sx1276_dio_2();//__attribute__((__weak__));
extern void sys_irq_sx1276_dio_3();//__attribute__((__weak__));
extern void sys_irq_sx1276_dio_4();//__attribute__((__weak__));
extern void sys_irq_sx1276_dio_5();//__attribute__((__weak__));

#ifdef __cplusplus
}
#endif

#endif // __SYS_IRQ_H__
