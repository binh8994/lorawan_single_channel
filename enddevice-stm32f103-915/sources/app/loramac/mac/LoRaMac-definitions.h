/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: LoRa MAC layer global definitions

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
#ifndef __LORAMAC_BOARD_H__
#define __LORAMAC_BOARD_H__

/*!
 * Returns individual channel mask
 *
 * \param[IN] channelIndex Channel index 1 based
 * \retval channelMask
 */
#define USE_BAND_915
#define DelayMs		sys_ctrl_delay_ms
#define MIN( a, b ) ( ( ( a ) < ( b ) ) ? ( a ) : ( b ) )
#define MAX( a, b ) ( ( ( a ) > ( b ) ) ? ( a ) : ( b ) )
#define POW2( n ) ( 1 << n )

#define LC( channelIndex )            ( uint16_t )( 1 << ( channelIndex - 1 ) )

#ifdef  USE_BAND_915

/*!
 * LoRaMac maximum number of channels
 */
#define LORA_MAX_NB_CHANNELS                        72

/*!
 * Minimal datarate that can be used by the node
 */
#define LORAMAC_TX_MIN_DATARATE                     DR_0

/*!
 * Maximal datarate that can be used by the node
 */
#define LORAMAC_TX_MAX_DATARATE                     DR_4

/*!
 * Minimal datarate that can be used by the node
 */
#define LORAMAC_RX_MIN_DATARATE                     DR_8

/*!
 * Maximal datarate that can be used by the node
 */
#define LORAMAC_RX_MAX_DATARATE                     DR_13

/*!
 * Default datarate used by the node
 */
#define LORAMAC_DEFAULT_DATARATE                    SINGLE_CHANNEL_GW_DR

/*!
 * Minimal Rx1 receive datarate offset
 */
#define LORAMAC_MIN_RX1_DR_OFFSET                   0

/*!
 * Maximal Rx1 receive datarate offset
 */
#define LORAMAC_MAX_RX1_DR_OFFSET                   3

/*!
 * Minimal Tx output power that can be used by the node
 */
#define LORAMAC_MIN_TX_POWER                        TX_POWER_10_DBM

/*!
 * Maximal Tx output power that can be used by the node
 */
#define LORAMAC_MAX_TX_POWER                        TX_POWER_30_DBM

/*!
 * Default Tx output power used by the node
 */
#define LORAMAC_DEFAULT_TX_POWER                    TX_POWER_30_DBM

/*!
 * LoRaMac TxPower definition
 */
#define TX_POWER_30_DBM                             0
#define TX_POWER_28_DBM                             1
#define TX_POWER_26_DBM                             2
#define TX_POWER_24_DBM                             3
#define TX_POWER_22_DBM                             4
#define TX_POWER_20_DBM                             5
#define TX_POWER_18_DBM                             6
#define TX_POWER_16_DBM                             7
#define TX_POWER_14_DBM                             8
#define TX_POWER_12_DBM                             9
#define TX_POWER_10_DBM                             10

/*!
 * LoRaMac datarates definition
 */
#define DR_0                                        0  // SF10 - BW125 |
#define DR_1                                        1  // SF9  - BW125 |
#define DR_2                                        2  // SF8  - BW125 +-> Up link
#define DR_3                                        3  // SF7  - BW125 |
#define DR_4                                        4  // SF8  - BW500 |
#define DR_5                                        5  // RFU
#define DR_6                                        6  // RFU
#define DR_7                                        7  // RFU
#define DR_8                                        8  // SF12 - BW500 |
#define DR_9                                        9  // SF11 - BW500 |
#define DR_10                                       10 // SF10 - BW500 |
#define DR_11                                       11 // SF9  - BW500 |
#define DR_12                                       12 // SF8  - BW500 +-> Down link
#define DR_13                                       13 // SF7  - BW500 |
#define DR_14                                       14 // RFU          |
#define DR_15                                       15 // RFU          |

/*!
 * Second reception window channel definition.
 */
// Channel = { Frequency [Hz], Datarate }
#define RX_WND_2_CHANNEL                                  { SINGLE_CHANNEL_GW_FREQ, SINGLE_CHANNEL_GW_DR }

/*!
 * LoRaMac maximum number of bands
 */
#define LORA_MAX_NB_BANDS                           1

// Band = { DutyCycle, TxMaxPower, LastTxDoneTime, TimeOff }
#define BAND0              { 1, TX_POWER_20_DBM, 0,  0 } //  100.0 %

/*!
 * LoRaMac default channels
 */
// Channel = { Frequency [Hz], { ( ( DrMax << 4 ) | DrMin ) }, Band }
/*
 * US band channels are initialized using a loop in LoRaMacInit function
 * \code
 * // 125 kHz channels
 * for( uint8_t i = 0; i < LORA_MAX_NB_CHANNELS - 8; i++ )
 * {
 *     Channels[i].Frequency = 902.3e6 + i * 200e3;
 *     Channels[i].DrRange.Value = ( DR_3 << 4 ) | DR_0;
 *     Channels[i].Band = 0;
 * }
 * // 500 kHz channels
 * for( uint8_t i = LORA_MAX_NB_CHANNELS - 8; i < LORA_MAX_NB_CHANNELS; i++ )
 * {
 *     Channels[i].Frequency = 903.0e6 + ( i - ( LORA_MAX_NB_CHANNELS - 8 ) ) * 1.6e6;
 *     Channels[i].DrRange.Value = ( DR_4 << 4 ) | DR_4;
 *     Channels[i].Band = 0;
 * }
 * \endcode
 */
#else
	#error "Please define a frequency band in the compiler options."
#endif

/* binhnt61 */
/* BEGIN */
#define SINGLE_CHANNEL_GW_FREQ						915000000
#define SINGLE_CHANNEL_GW_BANDWIDTH					0
#define SINGLE_CHANNEL_GW_SF						10
#define SINGLE_CHANNEL_GW_DR						DR_0
#define SINGLE_CHANNEL_GW_CD						1
#define SINGLE_CHANNEL_GW_TX_POWER					30	// dBm
#define SINGLE_CHANNEL_GW_PREAMBLE_LENGTH			8
#define SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT			5
#define SINGLE_CHANNEL_GW_FIX_LENGTH_PAY			false
#define SINGLE_CHANNEL_GW_IQ_TX						false
#define SINGLE_CHANNEL_GW_IQ_RX						false
#define SINGLE_CHANNEL_GW_TX_TIMEOUT				15000
#define SINGLE_CHANNEL_GW_RX_TIMEOUT				5000
/* BEGIN */

#endif // __LORAMAC_BOARD_H__
